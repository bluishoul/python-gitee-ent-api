# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class WeekReportRelation(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'issues': 'IssueMain',
        'pull_requests': 'PullRequest'
    }

    attribute_map = {
        'issues': 'issues',
        'pull_requests': 'pull_requests'
    }

    def __init__(self, issues=None, pull_requests=None):  # noqa: E501
        """WeekReportRelation - a model defined in Swagger"""  # noqa: E501
        self._issues = None
        self._pull_requests = None
        self.discriminator = None
        if issues is not None:
            self.issues = issues
        if pull_requests is not None:
            self.pull_requests = pull_requests

    @property
    def issues(self):
        """Gets the issues of this WeekReportRelation.  # noqa: E501


        :return: The issues of this WeekReportRelation.  # noqa: E501
        :rtype: IssueMain
        """
        return self._issues

    @issues.setter
    def issues(self, issues):
        """Sets the issues of this WeekReportRelation.


        :param issues: The issues of this WeekReportRelation.  # noqa: E501
        :type: IssueMain
        """

        self._issues = issues

    @property
    def pull_requests(self):
        """Gets the pull_requests of this WeekReportRelation.  # noqa: E501


        :return: The pull_requests of this WeekReportRelation.  # noqa: E501
        :rtype: PullRequest
        """
        return self._pull_requests

    @pull_requests.setter
    def pull_requests(self, pull_requests):
        """Sets the pull_requests of this WeekReportRelation.


        :param pull_requests: The pull_requests of this WeekReportRelation.  # noqa: E501
        :type: PullRequest
        """

        self._pull_requests = pull_requests

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(WeekReportRelation, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, WeekReportRelation):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

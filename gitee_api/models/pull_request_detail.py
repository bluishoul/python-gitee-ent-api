# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class PullRequestDetail(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'iid': 'int',
        'title': 'str',
        'project_id': 'int',
        'state': 'str',
        'check_state': 'int',
        'test_state': 'int',
        'priority': 'str',
        'lightweight': 'bool',
        'labels': 'Label',
        'conflict': 'bool',
        'project': 'Project',
        'source_branch': 'object',
        'target_branch': 'object',
        'can_merge': 'str',
        'assignees': 'PrAssign',
        'pr_assign_num': 'int',
        'testers': 'PrAssign',
        'close_related_issue': 'int',
        'prune_branch': 'int',
        'pr_test_num': 'int',
        'latest_scan_task': 'object',
        'gitee_go_enabled': 'bool',
        'milestone': 'Milestone',
        'body': 'str',
        'body_html': 'str',
        'reactions': 'Reaction',
        'clone_url': 'object',
        'can_be_resolve': 'bool',
        'user_can_resolve': 'bool',
        'ide_conflict_path': 'str',
        'can_change_pr_assigner': 'str',
        'can_change_pr_tester': 'str',
        'commits_count': 'int',
        'comments_count': 'int',
        'files_count': 'int',
        'sonar_url': 'str',
        'code_owners': 'str'
    }

    attribute_map = {
        'id': 'id',
        'iid': 'iid',
        'title': 'title',
        'project_id': 'project_id',
        'state': 'state',
        'check_state': 'check_state',
        'test_state': 'test_state',
        'priority': 'priority',
        'lightweight': 'lightweight',
        'labels': 'labels',
        'conflict': 'conflict',
        'project': 'project',
        'source_branch': 'source_branch',
        'target_branch': 'target_branch',
        'can_merge': 'can_merge',
        'assignees': 'assignees',
        'pr_assign_num': 'pr_assign_num',
        'testers': 'testers',
        'close_related_issue': 'close_related_issue',
        'prune_branch': 'prune_branch',
        'pr_test_num': 'pr_test_num',
        'latest_scan_task': 'latest_scan_task',
        'gitee_go_enabled': 'gitee_go_enabled',
        'milestone': 'milestone',
        'body': 'body',
        'body_html': 'body_html',
        'reactions': 'reactions',
        'clone_url': 'clone_url',
        'can_be_resolve': 'can_be_resolve',
        'user_can_resolve': 'user_can_resolve',
        'ide_conflict_path': 'ide_conflict_path',
        'can_change_pr_assigner': 'can_change_pr_assigner',
        'can_change_pr_tester': 'can_change_pr_tester',
        'commits_count': 'commits_count',
        'comments_count': 'comments_count',
        'files_count': 'files_count',
        'sonar_url': 'sonar_url',
        'code_owners': 'code_owners'
    }

    def __init__(self, id=None, iid=None, title=None, project_id=None, state=None, check_state=None, test_state=None, priority=None, lightweight=None, labels=None, conflict=None, project=None, source_branch=None, target_branch=None, can_merge=None, assignees=None, pr_assign_num=None, testers=None, close_related_issue=None, prune_branch=None, pr_test_num=None, latest_scan_task=None, gitee_go_enabled=None, milestone=None, body=None, body_html=None, reactions=None, clone_url=None, can_be_resolve=None, user_can_resolve=None, ide_conflict_path=None, can_change_pr_assigner=None, can_change_pr_tester=None, commits_count=None, comments_count=None, files_count=None, sonar_url=None, code_owners=None):  # noqa: E501
        """PullRequestDetail - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._iid = None
        self._title = None
        self._project_id = None
        self._state = None
        self._check_state = None
        self._test_state = None
        self._priority = None
        self._lightweight = None
        self._labels = None
        self._conflict = None
        self._project = None
        self._source_branch = None
        self._target_branch = None
        self._can_merge = None
        self._assignees = None
        self._pr_assign_num = None
        self._testers = None
        self._close_related_issue = None
        self._prune_branch = None
        self._pr_test_num = None
        self._latest_scan_task = None
        self._gitee_go_enabled = None
        self._milestone = None
        self._body = None
        self._body_html = None
        self._reactions = None
        self._clone_url = None
        self._can_be_resolve = None
        self._user_can_resolve = None
        self._ide_conflict_path = None
        self._can_change_pr_assigner = None
        self._can_change_pr_tester = None
        self._commits_count = None
        self._comments_count = None
        self._files_count = None
        self._sonar_url = None
        self._code_owners = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if iid is not None:
            self.iid = iid
        if title is not None:
            self.title = title
        if project_id is not None:
            self.project_id = project_id
        if state is not None:
            self.state = state
        if check_state is not None:
            self.check_state = check_state
        if test_state is not None:
            self.test_state = test_state
        if priority is not None:
            self.priority = priority
        if lightweight is not None:
            self.lightweight = lightweight
        if labels is not None:
            self.labels = labels
        if conflict is not None:
            self.conflict = conflict
        if project is not None:
            self.project = project
        if source_branch is not None:
            self.source_branch = source_branch
        if target_branch is not None:
            self.target_branch = target_branch
        if can_merge is not None:
            self.can_merge = can_merge
        if assignees is not None:
            self.assignees = assignees
        if pr_assign_num is not None:
            self.pr_assign_num = pr_assign_num
        if testers is not None:
            self.testers = testers
        if close_related_issue is not None:
            self.close_related_issue = close_related_issue
        if prune_branch is not None:
            self.prune_branch = prune_branch
        if pr_test_num is not None:
            self.pr_test_num = pr_test_num
        if latest_scan_task is not None:
            self.latest_scan_task = latest_scan_task
        if gitee_go_enabled is not None:
            self.gitee_go_enabled = gitee_go_enabled
        if milestone is not None:
            self.milestone = milestone
        if body is not None:
            self.body = body
        if body_html is not None:
            self.body_html = body_html
        if reactions is not None:
            self.reactions = reactions
        if clone_url is not None:
            self.clone_url = clone_url
        if can_be_resolve is not None:
            self.can_be_resolve = can_be_resolve
        if user_can_resolve is not None:
            self.user_can_resolve = user_can_resolve
        if ide_conflict_path is not None:
            self.ide_conflict_path = ide_conflict_path
        if can_change_pr_assigner is not None:
            self.can_change_pr_assigner = can_change_pr_assigner
        if can_change_pr_tester is not None:
            self.can_change_pr_tester = can_change_pr_tester
        if commits_count is not None:
            self.commits_count = commits_count
        if comments_count is not None:
            self.comments_count = comments_count
        if files_count is not None:
            self.files_count = files_count
        if sonar_url is not None:
            self.sonar_url = sonar_url
        if code_owners is not None:
            self.code_owners = code_owners

    @property
    def id(self):
        """Gets the id of this PullRequestDetail.  # noqa: E501

        PR 的 id  # noqa: E501

        :return: The id of this PullRequestDetail.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this PullRequestDetail.

        PR 的 id  # noqa: E501

        :param id: The id of this PullRequestDetail.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def iid(self):
        """Gets the iid of this PullRequestDetail.  # noqa: E501

        仓库内唯一的 PR id 标识符  # noqa: E501

        :return: The iid of this PullRequestDetail.  # noqa: E501
        :rtype: int
        """
        return self._iid

    @iid.setter
    def iid(self, iid):
        """Sets the iid of this PullRequestDetail.

        仓库内唯一的 PR id 标识符  # noqa: E501

        :param iid: The iid of this PullRequestDetail.  # noqa: E501
        :type: int
        """

        self._iid = iid

    @property
    def title(self):
        """Gets the title of this PullRequestDetail.  # noqa: E501

        PR 的标题  # noqa: E501

        :return: The title of this PullRequestDetail.  # noqa: E501
        :rtype: str
        """
        return self._title

    @title.setter
    def title(self, title):
        """Sets the title of this PullRequestDetail.

        PR 的标题  # noqa: E501

        :param title: The title of this PullRequestDetail.  # noqa: E501
        :type: str
        """

        self._title = title

    @property
    def project_id(self):
        """Gets the project_id of this PullRequestDetail.  # noqa: E501

        仓库 id  # noqa: E501

        :return: The project_id of this PullRequestDetail.  # noqa: E501
        :rtype: int
        """
        return self._project_id

    @project_id.setter
    def project_id(self, project_id):
        """Sets the project_id of this PullRequestDetail.

        仓库 id  # noqa: E501

        :param project_id: The project_id of this PullRequestDetail.  # noqa: E501
        :type: int
        """

        self._project_id = project_id

    @property
    def state(self):
        """Gets the state of this PullRequestDetail.  # noqa: E501

        PR 的状态(opened: 开启; reopened: 关闭后重开; closed: 关闭; merged: 已合并;)  # noqa: E501

        :return: The state of this PullRequestDetail.  # noqa: E501
        :rtype: str
        """
        return self._state

    @state.setter
    def state(self, state):
        """Sets the state of this PullRequestDetail.

        PR 的状态(opened: 开启; reopened: 关闭后重开; closed: 关闭; merged: 已合并;)  # noqa: E501

        :param state: The state of this PullRequestDetail.  # noqa: E501
        :type: str
        """

        self._state = state

    @property
    def check_state(self):
        """Gets the check_state of this PullRequestDetail.  # noqa: E501

        PR的审查状态(0: 不需要审查; 1: 待审查; 2: 审查已全部通过;)  # noqa: E501

        :return: The check_state of this PullRequestDetail.  # noqa: E501
        :rtype: int
        """
        return self._check_state

    @check_state.setter
    def check_state(self, check_state):
        """Sets the check_state of this PullRequestDetail.

        PR的审查状态(0: 不需要审查; 1: 待审查; 2: 审查已全部通过;)  # noqa: E501

        :param check_state: The check_state of this PullRequestDetail.  # noqa: E501
        :type: int
        """

        self._check_state = check_state

    @property
    def test_state(self):
        """Gets the test_state of this PullRequestDetail.  # noqa: E501

        PR的测试状态(0: 不需要测试; 1: 待测试; 2: 测试已全部通过;)  # noqa: E501

        :return: The test_state of this PullRequestDetail.  # noqa: E501
        :rtype: int
        """
        return self._test_state

    @test_state.setter
    def test_state(self, test_state):
        """Sets the test_state of this PullRequestDetail.

        PR的测试状态(0: 不需要测试; 1: 待测试; 2: 测试已全部通过;)  # noqa: E501

        :param test_state: The test_state of this PullRequestDetail.  # noqa: E501
        :type: int
        """

        self._test_state = test_state

    @property
    def priority(self):
        """Gets the priority of this PullRequestDetail.  # noqa: E501

        PR 的优先级。(0: 不指定; 1: 不重要; 2: 次要; 3: 主要; 4: 严重;)  # noqa: E501

        :return: The priority of this PullRequestDetail.  # noqa: E501
        :rtype: str
        """
        return self._priority

    @priority.setter
    def priority(self, priority):
        """Sets the priority of this PullRequestDetail.

        PR 的优先级。(0: 不指定; 1: 不重要; 2: 次要; 3: 主要; 4: 严重;)  # noqa: E501

        :param priority: The priority of this PullRequestDetail.  # noqa: E501
        :type: str
        """

        self._priority = priority

    @property
    def lightweight(self):
        """Gets the lightweight of this PullRequestDetail.  # noqa: E501

        是否轻量级 PR  # noqa: E501

        :return: The lightweight of this PullRequestDetail.  # noqa: E501
        :rtype: bool
        """
        return self._lightweight

    @lightweight.setter
    def lightweight(self, lightweight):
        """Sets the lightweight of this PullRequestDetail.

        是否轻量级 PR  # noqa: E501

        :param lightweight: The lightweight of this PullRequestDetail.  # noqa: E501
        :type: bool
        """

        self._lightweight = lightweight

    @property
    def labels(self):
        """Gets the labels of this PullRequestDetail.  # noqa: E501


        :return: The labels of this PullRequestDetail.  # noqa: E501
        :rtype: Label
        """
        return self._labels

    @labels.setter
    def labels(self, labels):
        """Sets the labels of this PullRequestDetail.


        :param labels: The labels of this PullRequestDetail.  # noqa: E501
        :type: Label
        """

        self._labels = labels

    @property
    def conflict(self):
        """Gets the conflict of this PullRequestDetail.  # noqa: E501

        是否存在冲突  # noqa: E501

        :return: The conflict of this PullRequestDetail.  # noqa: E501
        :rtype: bool
        """
        return self._conflict

    @conflict.setter
    def conflict(self, conflict):
        """Sets the conflict of this PullRequestDetail.

        是否存在冲突  # noqa: E501

        :param conflict: The conflict of this PullRequestDetail.  # noqa: E501
        :type: bool
        """

        self._conflict = conflict

    @property
    def project(self):
        """Gets the project of this PullRequestDetail.  # noqa: E501


        :return: The project of this PullRequestDetail.  # noqa: E501
        :rtype: Project
        """
        return self._project

    @project.setter
    def project(self, project):
        """Sets the project of this PullRequestDetail.


        :param project: The project of this PullRequestDetail.  # noqa: E501
        :type: Project
        """

        self._project = project

    @property
    def source_branch(self):
        """Gets the source_branch of this PullRequestDetail.  # noqa: E501

        源分支  # noqa: E501

        :return: The source_branch of this PullRequestDetail.  # noqa: E501
        :rtype: object
        """
        return self._source_branch

    @source_branch.setter
    def source_branch(self, source_branch):
        """Sets the source_branch of this PullRequestDetail.

        源分支  # noqa: E501

        :param source_branch: The source_branch of this PullRequestDetail.  # noqa: E501
        :type: object
        """

        self._source_branch = source_branch

    @property
    def target_branch(self):
        """Gets the target_branch of this PullRequestDetail.  # noqa: E501

        目标分支  # noqa: E501

        :return: The target_branch of this PullRequestDetail.  # noqa: E501
        :rtype: object
        """
        return self._target_branch

    @target_branch.setter
    def target_branch(self, target_branch):
        """Sets the target_branch of this PullRequestDetail.

        目标分支  # noqa: E501

        :param target_branch: The target_branch of this PullRequestDetail.  # noqa: E501
        :type: object
        """

        self._target_branch = target_branch

    @property
    def can_merge(self):
        """Gets the can_merge of this PullRequestDetail.  # noqa: E501

        是否可合并  # noqa: E501

        :return: The can_merge of this PullRequestDetail.  # noqa: E501
        :rtype: str
        """
        return self._can_merge

    @can_merge.setter
    def can_merge(self, can_merge):
        """Sets the can_merge of this PullRequestDetail.

        是否可合并  # noqa: E501

        :param can_merge: The can_merge of this PullRequestDetail.  # noqa: E501
        :type: str
        """

        self._can_merge = can_merge

    @property
    def assignees(self):
        """Gets the assignees of this PullRequestDetail.  # noqa: E501


        :return: The assignees of this PullRequestDetail.  # noqa: E501
        :rtype: PrAssign
        """
        return self._assignees

    @assignees.setter
    def assignees(self, assignees):
        """Sets the assignees of this PullRequestDetail.


        :param assignees: The assignees of this PullRequestDetail.  # noqa: E501
        :type: PrAssign
        """

        self._assignees = assignees

    @property
    def pr_assign_num(self):
        """Gets the pr_assign_num of this PullRequestDetail.  # noqa: E501

        最少审查人数  # noqa: E501

        :return: The pr_assign_num of this PullRequestDetail.  # noqa: E501
        :rtype: int
        """
        return self._pr_assign_num

    @pr_assign_num.setter
    def pr_assign_num(self, pr_assign_num):
        """Sets the pr_assign_num of this PullRequestDetail.

        最少审查人数  # noqa: E501

        :param pr_assign_num: The pr_assign_num of this PullRequestDetail.  # noqa: E501
        :type: int
        """

        self._pr_assign_num = pr_assign_num

    @property
    def testers(self):
        """Gets the testers of this PullRequestDetail.  # noqa: E501


        :return: The testers of this PullRequestDetail.  # noqa: E501
        :rtype: PrAssign
        """
        return self._testers

    @testers.setter
    def testers(self, testers):
        """Sets the testers of this PullRequestDetail.


        :param testers: The testers of this PullRequestDetail.  # noqa: E501
        :type: PrAssign
        """

        self._testers = testers

    @property
    def close_related_issue(self):
        """Gets the close_related_issue of this PullRequestDetail.  # noqa: E501

        合并 PR 后关闭关联的任务  # noqa: E501

        :return: The close_related_issue of this PullRequestDetail.  # noqa: E501
        :rtype: int
        """
        return self._close_related_issue

    @close_related_issue.setter
    def close_related_issue(self, close_related_issue):
        """Sets the close_related_issue of this PullRequestDetail.

        合并 PR 后关闭关联的任务  # noqa: E501

        :param close_related_issue: The close_related_issue of this PullRequestDetail.  # noqa: E501
        :type: int
        """

        self._close_related_issue = close_related_issue

    @property
    def prune_branch(self):
        """Gets the prune_branch of this PullRequestDetail.  # noqa: E501

        合并 PR 后删除关联分支  # noqa: E501

        :return: The prune_branch of this PullRequestDetail.  # noqa: E501
        :rtype: int
        """
        return self._prune_branch

    @prune_branch.setter
    def prune_branch(self, prune_branch):
        """Sets the prune_branch of this PullRequestDetail.

        合并 PR 后删除关联分支  # noqa: E501

        :param prune_branch: The prune_branch of this PullRequestDetail.  # noqa: E501
        :type: int
        """

        self._prune_branch = prune_branch

    @property
    def pr_test_num(self):
        """Gets the pr_test_num of this PullRequestDetail.  # noqa: E501

        最少测试人数  # noqa: E501

        :return: The pr_test_num of this PullRequestDetail.  # noqa: E501
        :rtype: int
        """
        return self._pr_test_num

    @pr_test_num.setter
    def pr_test_num(self, pr_test_num):
        """Sets the pr_test_num of this PullRequestDetail.

        最少测试人数  # noqa: E501

        :param pr_test_num: The pr_test_num of this PullRequestDetail.  # noqa: E501
        :type: int
        """

        self._pr_test_num = pr_test_num

    @property
    def latest_scan_task(self):
        """Gets the latest_scan_task of this PullRequestDetail.  # noqa: E501

        最后一次gitee scan扫描结果  # noqa: E501

        :return: The latest_scan_task of this PullRequestDetail.  # noqa: E501
        :rtype: object
        """
        return self._latest_scan_task

    @latest_scan_task.setter
    def latest_scan_task(self, latest_scan_task):
        """Sets the latest_scan_task of this PullRequestDetail.

        最后一次gitee scan扫描结果  # noqa: E501

        :param latest_scan_task: The latest_scan_task of this PullRequestDetail.  # noqa: E501
        :type: object
        """

        self._latest_scan_task = latest_scan_task

    @property
    def gitee_go_enabled(self):
        """Gets the gitee_go_enabled of this PullRequestDetail.  # noqa: E501

        所属仓库GiteeGo服务是否可用  # noqa: E501

        :return: The gitee_go_enabled of this PullRequestDetail.  # noqa: E501
        :rtype: bool
        """
        return self._gitee_go_enabled

    @gitee_go_enabled.setter
    def gitee_go_enabled(self, gitee_go_enabled):
        """Sets the gitee_go_enabled of this PullRequestDetail.

        所属仓库GiteeGo服务是否可用  # noqa: E501

        :param gitee_go_enabled: The gitee_go_enabled of this PullRequestDetail.  # noqa: E501
        :type: bool
        """

        self._gitee_go_enabled = gitee_go_enabled

    @property
    def milestone(self):
        """Gets the milestone of this PullRequestDetail.  # noqa: E501


        :return: The milestone of this PullRequestDetail.  # noqa: E501
        :rtype: Milestone
        """
        return self._milestone

    @milestone.setter
    def milestone(self, milestone):
        """Sets the milestone of this PullRequestDetail.


        :param milestone: The milestone of this PullRequestDetail.  # noqa: E501
        :type: Milestone
        """

        self._milestone = milestone

    @property
    def body(self):
        """Gets the body of this PullRequestDetail.  # noqa: E501

        PR 的描述内容（markdown 原文）  # noqa: E501

        :return: The body of this PullRequestDetail.  # noqa: E501
        :rtype: str
        """
        return self._body

    @body.setter
    def body(self, body):
        """Sets the body of this PullRequestDetail.

        PR 的描述内容（markdown 原文）  # noqa: E501

        :param body: The body of this PullRequestDetail.  # noqa: E501
        :type: str
        """

        self._body = body

    @property
    def body_html(self):
        """Gets the body_html of this PullRequestDetail.  # noqa: E501

        PR 的描述内容（html 格式）  # noqa: E501

        :return: The body_html of this PullRequestDetail.  # noqa: E501
        :rtype: str
        """
        return self._body_html

    @body_html.setter
    def body_html(self, body_html):
        """Sets the body_html of this PullRequestDetail.

        PR 的描述内容（html 格式）  # noqa: E501

        :param body_html: The body_html of this PullRequestDetail.  # noqa: E501
        :type: str
        """

        self._body_html = body_html

    @property
    def reactions(self):
        """Gets the reactions of this PullRequestDetail.  # noqa: E501


        :return: The reactions of this PullRequestDetail.  # noqa: E501
        :rtype: Reaction
        """
        return self._reactions

    @reactions.setter
    def reactions(self, reactions):
        """Sets the reactions of this PullRequestDetail.


        :param reactions: The reactions of this PullRequestDetail.  # noqa: E501
        :type: Reaction
        """

        self._reactions = reactions

    @property
    def clone_url(self):
        """Gets the clone_url of this PullRequestDetail.  # noqa: E501

        PR 克隆地址  # noqa: E501

        :return: The clone_url of this PullRequestDetail.  # noqa: E501
        :rtype: object
        """
        return self._clone_url

    @clone_url.setter
    def clone_url(self, clone_url):
        """Sets the clone_url of this PullRequestDetail.

        PR 克隆地址  # noqa: E501

        :param clone_url: The clone_url of this PullRequestDetail.  # noqa: E501
        :type: object
        """

        self._clone_url = clone_url

    @property
    def can_be_resolve(self):
        """Gets the can_be_resolve of this PullRequestDetail.  # noqa: E501

        是否可以通过 webide 处理的  # noqa: E501

        :return: The can_be_resolve of this PullRequestDetail.  # noqa: E501
        :rtype: bool
        """
        return self._can_be_resolve

    @can_be_resolve.setter
    def can_be_resolve(self, can_be_resolve):
        """Sets the can_be_resolve of this PullRequestDetail.

        是否可以通过 webide 处理的  # noqa: E501

        :param can_be_resolve: The can_be_resolve of this PullRequestDetail.  # noqa: E501
        :type: bool
        """

        self._can_be_resolve = can_be_resolve

    @property
    def user_can_resolve(self):
        """Gets the user_can_resolve of this PullRequestDetail.  # noqa: E501

        当前用户是否有权限处理冲突  # noqa: E501

        :return: The user_can_resolve of this PullRequestDetail.  # noqa: E501
        :rtype: bool
        """
        return self._user_can_resolve

    @user_can_resolve.setter
    def user_can_resolve(self, user_can_resolve):
        """Sets the user_can_resolve of this PullRequestDetail.

        当前用户是否有权限处理冲突  # noqa: E501

        :param user_can_resolve: The user_can_resolve of this PullRequestDetail.  # noqa: E501
        :type: bool
        """

        self._user_can_resolve = user_can_resolve

    @property
    def ide_conflict_path(self):
        """Gets the ide_conflict_path of this PullRequestDetail.  # noqa: E501


        :return: The ide_conflict_path of this PullRequestDetail.  # noqa: E501
        :rtype: str
        """
        return self._ide_conflict_path

    @ide_conflict_path.setter
    def ide_conflict_path(self, ide_conflict_path):
        """Sets the ide_conflict_path of this PullRequestDetail.


        :param ide_conflict_path: The ide_conflict_path of this PullRequestDetail.  # noqa: E501
        :type: str
        """

        self._ide_conflict_path = ide_conflict_path

    @property
    def can_change_pr_assigner(self):
        """Gets the can_change_pr_assigner of this PullRequestDetail.  # noqa: E501


        :return: The can_change_pr_assigner of this PullRequestDetail.  # noqa: E501
        :rtype: str
        """
        return self._can_change_pr_assigner

    @can_change_pr_assigner.setter
    def can_change_pr_assigner(self, can_change_pr_assigner):
        """Sets the can_change_pr_assigner of this PullRequestDetail.


        :param can_change_pr_assigner: The can_change_pr_assigner of this PullRequestDetail.  # noqa: E501
        :type: str
        """

        self._can_change_pr_assigner = can_change_pr_assigner

    @property
    def can_change_pr_tester(self):
        """Gets the can_change_pr_tester of this PullRequestDetail.  # noqa: E501


        :return: The can_change_pr_tester of this PullRequestDetail.  # noqa: E501
        :rtype: str
        """
        return self._can_change_pr_tester

    @can_change_pr_tester.setter
    def can_change_pr_tester(self, can_change_pr_tester):
        """Sets the can_change_pr_tester of this PullRequestDetail.


        :param can_change_pr_tester: The can_change_pr_tester of this PullRequestDetail.  # noqa: E501
        :type: str
        """

        self._can_change_pr_tester = can_change_pr_tester

    @property
    def commits_count(self):
        """Gets the commits_count of this PullRequestDetail.  # noqa: E501

        提交记录数  # noqa: E501

        :return: The commits_count of this PullRequestDetail.  # noqa: E501
        :rtype: int
        """
        return self._commits_count

    @commits_count.setter
    def commits_count(self, commits_count):
        """Sets the commits_count of this PullRequestDetail.

        提交记录数  # noqa: E501

        :param commits_count: The commits_count of this PullRequestDetail.  # noqa: E501
        :type: int
        """

        self._commits_count = commits_count

    @property
    def comments_count(self):
        """Gets the comments_count of this PullRequestDetail.  # noqa: E501

        评论数  # noqa: E501

        :return: The comments_count of this PullRequestDetail.  # noqa: E501
        :rtype: int
        """
        return self._comments_count

    @comments_count.setter
    def comments_count(self, comments_count):
        """Sets the comments_count of this PullRequestDetail.

        评论数  # noqa: E501

        :param comments_count: The comments_count of this PullRequestDetail.  # noqa: E501
        :type: int
        """

        self._comments_count = comments_count

    @property
    def files_count(self):
        """Gets the files_count of this PullRequestDetail.  # noqa: E501

        diff文件数  # noqa: E501

        :return: The files_count of this PullRequestDetail.  # noqa: E501
        :rtype: int
        """
        return self._files_count

    @files_count.setter
    def files_count(self, files_count):
        """Sets the files_count of this PullRequestDetail.

        diff文件数  # noqa: E501

        :param files_count: The files_count of this PullRequestDetail.  # noqa: E501
        :type: int
        """

        self._files_count = files_count

    @property
    def sonar_url(self):
        """Gets the sonar_url of this PullRequestDetail.  # noqa: E501

        是否有 sonar 扫描报告  # noqa: E501

        :return: The sonar_url of this PullRequestDetail.  # noqa: E501
        :rtype: str
        """
        return self._sonar_url

    @sonar_url.setter
    def sonar_url(self, sonar_url):
        """Sets the sonar_url of this PullRequestDetail.

        是否有 sonar 扫描报告  # noqa: E501

        :param sonar_url: The sonar_url of this PullRequestDetail.  # noqa: E501
        :type: str
        """

        self._sonar_url = sonar_url

    @property
    def code_owners(self):
        """Gets the code_owners of this PullRequestDetail.  # noqa: E501


        :return: The code_owners of this PullRequestDetail.  # noqa: E501
        :rtype: str
        """
        return self._code_owners

    @code_owners.setter
    def code_owners(self, code_owners):
        """Sets the code_owners of this PullRequestDetail.


        :param code_owners: The code_owners of this PullRequestDetail.  # noqa: E501
        :type: str
        """

        self._code_owners = code_owners

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(PullRequestDetail, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PullRequestDetail):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class ProjectsList(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'name': 'str',
        'path': 'str',
        'public': 'int',
        'enterprise_id': 'int',
        'security_hole_enabled': 'bool',
        'namespace': 'Namespace',
        'creator': 'UserWithRemark',
        'is_fork': 'bool',
        'parent_project': 'Project',
        'status': 'int',
        'status_name': 'int',
        'outsourced': 'bool',
        'repo_size': 'int',
        'can_admin_project': 'bool',
        'members_count': 'int',
        'last_push_at': 'bool',
        'watches_count': 'int',
        'stars_count': 'int',
        'forked_count': 'int',
        'enable_backup': 'bool',
        'has_backups': 'bool',
        'vip': 'bool',
        'recomm': 'bool',
        'template': 'Project',
        'template_enabled': 'bool',
        'description': 'str',
        'get_default_branch': 'str',
        'releases_count': 'int',
        'total_pr_count': 'int',
        'is_star': 'bool',
        'used_template_count': 'int',
        'fork_enabled': 'bool',
        'pull_requests_enabled': 'bool'
    }

    attribute_map = {
        'id': 'id',
        'name': 'name',
        'path': 'path',
        'public': 'public',
        'enterprise_id': 'enterprise_id',
        'security_hole_enabled': 'security_hole_enabled',
        'namespace': 'namespace',
        'creator': 'creator',
        'is_fork': 'is_fork',
        'parent_project': 'parent_project',
        'status': 'status',
        'status_name': 'status_name',
        'outsourced': 'outsourced',
        'repo_size': 'repo_size',
        'can_admin_project': 'can_admin_project',
        'members_count': 'members_count',
        'last_push_at': 'last_push_at',
        'watches_count': 'watches_count',
        'stars_count': 'stars_count',
        'forked_count': 'forked_count',
        'enable_backup': 'enable_backup',
        'has_backups': 'has_backups',
        'vip': 'vip',
        'recomm': 'recomm',
        'template': 'template',
        'template_enabled': 'template_enabled',
        'description': 'description',
        'get_default_branch': 'get_default_branch',
        'releases_count': 'releases_count',
        'total_pr_count': 'total_pr_count',
        'is_star': 'is_star',
        'used_template_count': 'used_template_count',
        'fork_enabled': 'fork_enabled',
        'pull_requests_enabled': 'pull_requests_enabled'
    }

    def __init__(self, id=None, name=None, path=None, public=None, enterprise_id=None, security_hole_enabled=None, namespace=None, creator=None, is_fork=None, parent_project=None, status=None, status_name=None, outsourced=None, repo_size=None, can_admin_project=None, members_count=None, last_push_at=None, watches_count=None, stars_count=None, forked_count=None, enable_backup=None, has_backups=None, vip=None, recomm=None, template=None, template_enabled=None, description=None, get_default_branch=None, releases_count=None, total_pr_count=None, is_star=None, used_template_count=None, fork_enabled=None, pull_requests_enabled=None):  # noqa: E501
        """ProjectsList - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._name = None
        self._path = None
        self._public = None
        self._enterprise_id = None
        self._security_hole_enabled = None
        self._namespace = None
        self._creator = None
        self._is_fork = None
        self._parent_project = None
        self._status = None
        self._status_name = None
        self._outsourced = None
        self._repo_size = None
        self._can_admin_project = None
        self._members_count = None
        self._last_push_at = None
        self._watches_count = None
        self._stars_count = None
        self._forked_count = None
        self._enable_backup = None
        self._has_backups = None
        self._vip = None
        self._recomm = None
        self._template = None
        self._template_enabled = None
        self._description = None
        self._get_default_branch = None
        self._releases_count = None
        self._total_pr_count = None
        self._is_star = None
        self._used_template_count = None
        self._fork_enabled = None
        self._pull_requests_enabled = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if name is not None:
            self.name = name
        if path is not None:
            self.path = path
        if public is not None:
            self.public = public
        if enterprise_id is not None:
            self.enterprise_id = enterprise_id
        if security_hole_enabled is not None:
            self.security_hole_enabled = security_hole_enabled
        if namespace is not None:
            self.namespace = namespace
        if creator is not None:
            self.creator = creator
        if is_fork is not None:
            self.is_fork = is_fork
        if parent_project is not None:
            self.parent_project = parent_project
        if status is not None:
            self.status = status
        if status_name is not None:
            self.status_name = status_name
        if outsourced is not None:
            self.outsourced = outsourced
        if repo_size is not None:
            self.repo_size = repo_size
        if can_admin_project is not None:
            self.can_admin_project = can_admin_project
        if members_count is not None:
            self.members_count = members_count
        if last_push_at is not None:
            self.last_push_at = last_push_at
        if watches_count is not None:
            self.watches_count = watches_count
        if stars_count is not None:
            self.stars_count = stars_count
        if forked_count is not None:
            self.forked_count = forked_count
        if enable_backup is not None:
            self.enable_backup = enable_backup
        if has_backups is not None:
            self.has_backups = has_backups
        if vip is not None:
            self.vip = vip
        if recomm is not None:
            self.recomm = recomm
        if template is not None:
            self.template = template
        if template_enabled is not None:
            self.template_enabled = template_enabled
        if description is not None:
            self.description = description
        if get_default_branch is not None:
            self.get_default_branch = get_default_branch
        if releases_count is not None:
            self.releases_count = releases_count
        if total_pr_count is not None:
            self.total_pr_count = total_pr_count
        if is_star is not None:
            self.is_star = is_star
        if used_template_count is not None:
            self.used_template_count = used_template_count
        if fork_enabled is not None:
            self.fork_enabled = fork_enabled
        if pull_requests_enabled is not None:
            self.pull_requests_enabled = pull_requests_enabled

    @property
    def id(self):
        """Gets the id of this ProjectsList.  # noqa: E501

        仓库ID  # noqa: E501

        :return: The id of this ProjectsList.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ProjectsList.

        仓库ID  # noqa: E501

        :param id: The id of this ProjectsList.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def name(self):
        """Gets the name of this ProjectsList.  # noqa: E501

        仓库名称  # noqa: E501

        :return: The name of this ProjectsList.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this ProjectsList.

        仓库名称  # noqa: E501

        :param name: The name of this ProjectsList.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def path(self):
        """Gets the path of this ProjectsList.  # noqa: E501

        仓库路径  # noqa: E501

        :return: The path of this ProjectsList.  # noqa: E501
        :rtype: str
        """
        return self._path

    @path.setter
    def path(self, path):
        """Sets the path of this ProjectsList.

        仓库路径  # noqa: E501

        :param path: The path of this ProjectsList.  # noqa: E501
        :type: str
        """

        self._path = path

    @property
    def public(self):
        """Gets the public of this ProjectsList.  # noqa: E501

        仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开;  # noqa: E501

        :return: The public of this ProjectsList.  # noqa: E501
        :rtype: int
        """
        return self._public

    @public.setter
    def public(self, public):
        """Sets the public of this ProjectsList.

        仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开;  # noqa: E501

        :param public: The public of this ProjectsList.  # noqa: E501
        :type: int
        """

        self._public = public

    @property
    def enterprise_id(self):
        """Gets the enterprise_id of this ProjectsList.  # noqa: E501

        企业 id  # noqa: E501

        :return: The enterprise_id of this ProjectsList.  # noqa: E501
        :rtype: int
        """
        return self._enterprise_id

    @enterprise_id.setter
    def enterprise_id(self, enterprise_id):
        """Sets the enterprise_id of this ProjectsList.

        企业 id  # noqa: E501

        :param enterprise_id: The enterprise_id of this ProjectsList.  # noqa: E501
        :type: int
        """

        self._enterprise_id = enterprise_id

    @property
    def security_hole_enabled(self):
        """Gets the security_hole_enabled of this ProjectsList.  # noqa: E501

        是否允许用户创建涉及敏感信息的任务  # noqa: E501

        :return: The security_hole_enabled of this ProjectsList.  # noqa: E501
        :rtype: bool
        """
        return self._security_hole_enabled

    @security_hole_enabled.setter
    def security_hole_enabled(self, security_hole_enabled):
        """Sets the security_hole_enabled of this ProjectsList.

        是否允许用户创建涉及敏感信息的任务  # noqa: E501

        :param security_hole_enabled: The security_hole_enabled of this ProjectsList.  # noqa: E501
        :type: bool
        """

        self._security_hole_enabled = security_hole_enabled

    @property
    def namespace(self):
        """Gets the namespace of this ProjectsList.  # noqa: E501


        :return: The namespace of this ProjectsList.  # noqa: E501
        :rtype: Namespace
        """
        return self._namespace

    @namespace.setter
    def namespace(self, namespace):
        """Sets the namespace of this ProjectsList.


        :param namespace: The namespace of this ProjectsList.  # noqa: E501
        :type: Namespace
        """

        self._namespace = namespace

    @property
    def creator(self):
        """Gets the creator of this ProjectsList.  # noqa: E501


        :return: The creator of this ProjectsList.  # noqa: E501
        :rtype: UserWithRemark
        """
        return self._creator

    @creator.setter
    def creator(self, creator):
        """Sets the creator of this ProjectsList.


        :param creator: The creator of this ProjectsList.  # noqa: E501
        :type: UserWithRemark
        """

        self._creator = creator

    @property
    def is_fork(self):
        """Gets the is_fork of this ProjectsList.  # noqa: E501

        是否是fork仓库  # noqa: E501

        :return: The is_fork of this ProjectsList.  # noqa: E501
        :rtype: bool
        """
        return self._is_fork

    @is_fork.setter
    def is_fork(self, is_fork):
        """Sets the is_fork of this ProjectsList.

        是否是fork仓库  # noqa: E501

        :param is_fork: The is_fork of this ProjectsList.  # noqa: E501
        :type: bool
        """

        self._is_fork = is_fork

    @property
    def parent_project(self):
        """Gets the parent_project of this ProjectsList.  # noqa: E501


        :return: The parent_project of this ProjectsList.  # noqa: E501
        :rtype: Project
        """
        return self._parent_project

    @parent_project.setter
    def parent_project(self, parent_project):
        """Sets the parent_project of this ProjectsList.


        :param parent_project: The parent_project of this ProjectsList.  # noqa: E501
        :type: Project
        """

        self._parent_project = parent_project

    @property
    def status(self):
        """Gets the status of this ProjectsList.  # noqa: E501

        状态值  # noqa: E501

        :return: The status of this ProjectsList.  # noqa: E501
        :rtype: int
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this ProjectsList.

        状态值  # noqa: E501

        :param status: The status of this ProjectsList.  # noqa: E501
        :type: int
        """

        self._status = status

    @property
    def status_name(self):
        """Gets the status_name of this ProjectsList.  # noqa: E501

        状态中文名称  # noqa: E501

        :return: The status_name of this ProjectsList.  # noqa: E501
        :rtype: int
        """
        return self._status_name

    @status_name.setter
    def status_name(self, status_name):
        """Sets the status_name of this ProjectsList.

        状态中文名称  # noqa: E501

        :param status_name: The status_name of this ProjectsList.  # noqa: E501
        :type: int
        """

        self._status_name = status_name

    @property
    def outsourced(self):
        """Gets the outsourced of this ProjectsList.  # noqa: E501

        是否外包  # noqa: E501

        :return: The outsourced of this ProjectsList.  # noqa: E501
        :rtype: bool
        """
        return self._outsourced

    @outsourced.setter
    def outsourced(self, outsourced):
        """Sets the outsourced of this ProjectsList.

        是否外包  # noqa: E501

        :param outsourced: The outsourced of this ProjectsList.  # noqa: E501
        :type: bool
        """

        self._outsourced = outsourced

    @property
    def repo_size(self):
        """Gets the repo_size of this ProjectsList.  # noqa: E501

        仓库大小  # noqa: E501

        :return: The repo_size of this ProjectsList.  # noqa: E501
        :rtype: int
        """
        return self._repo_size

    @repo_size.setter
    def repo_size(self, repo_size):
        """Sets the repo_size of this ProjectsList.

        仓库大小  # noqa: E501

        :param repo_size: The repo_size of this ProjectsList.  # noqa: E501
        :type: int
        """

        self._repo_size = repo_size

    @property
    def can_admin_project(self):
        """Gets the can_admin_project of this ProjectsList.  # noqa: E501

        能否操作当前仓库  # noqa: E501

        :return: The can_admin_project of this ProjectsList.  # noqa: E501
        :rtype: bool
        """
        return self._can_admin_project

    @can_admin_project.setter
    def can_admin_project(self, can_admin_project):
        """Sets the can_admin_project of this ProjectsList.

        能否操作当前仓库  # noqa: E501

        :param can_admin_project: The can_admin_project of this ProjectsList.  # noqa: E501
        :type: bool
        """

        self._can_admin_project = can_admin_project

    @property
    def members_count(self):
        """Gets the members_count of this ProjectsList.  # noqa: E501

        成员数  # noqa: E501

        :return: The members_count of this ProjectsList.  # noqa: E501
        :rtype: int
        """
        return self._members_count

    @members_count.setter
    def members_count(self, members_count):
        """Sets the members_count of this ProjectsList.

        成员数  # noqa: E501

        :param members_count: The members_count of this ProjectsList.  # noqa: E501
        :type: int
        """

        self._members_count = members_count

    @property
    def last_push_at(self):
        """Gets the last_push_at of this ProjectsList.  # noqa: E501

        最近push  # noqa: E501

        :return: The last_push_at of this ProjectsList.  # noqa: E501
        :rtype: bool
        """
        return self._last_push_at

    @last_push_at.setter
    def last_push_at(self, last_push_at):
        """Sets the last_push_at of this ProjectsList.

        最近push  # noqa: E501

        :param last_push_at: The last_push_at of this ProjectsList.  # noqa: E501
        :type: bool
        """

        self._last_push_at = last_push_at

    @property
    def watches_count(self):
        """Gets the watches_count of this ProjectsList.  # noqa: E501

        watches数  # noqa: E501

        :return: The watches_count of this ProjectsList.  # noqa: E501
        :rtype: int
        """
        return self._watches_count

    @watches_count.setter
    def watches_count(self, watches_count):
        """Sets the watches_count of this ProjectsList.

        watches数  # noqa: E501

        :param watches_count: The watches_count of this ProjectsList.  # noqa: E501
        :type: int
        """

        self._watches_count = watches_count

    @property
    def stars_count(self):
        """Gets the stars_count of this ProjectsList.  # noqa: E501

        stars数  # noqa: E501

        :return: The stars_count of this ProjectsList.  # noqa: E501
        :rtype: int
        """
        return self._stars_count

    @stars_count.setter
    def stars_count(self, stars_count):
        """Sets the stars_count of this ProjectsList.

        stars数  # noqa: E501

        :param stars_count: The stars_count of this ProjectsList.  # noqa: E501
        :type: int
        """

        self._stars_count = stars_count

    @property
    def forked_count(self):
        """Gets the forked_count of this ProjectsList.  # noqa: E501

        被fork数  # noqa: E501

        :return: The forked_count of this ProjectsList.  # noqa: E501
        :rtype: int
        """
        return self._forked_count

    @forked_count.setter
    def forked_count(self, forked_count):
        """Sets the forked_count of this ProjectsList.

        被fork数  # noqa: E501

        :param forked_count: The forked_count of this ProjectsList.  # noqa: E501
        :type: int
        """

        self._forked_count = forked_count

    @property
    def enable_backup(self):
        """Gets the enable_backup of this ProjectsList.  # noqa: E501

        是否开启备份  # noqa: E501

        :return: The enable_backup of this ProjectsList.  # noqa: E501
        :rtype: bool
        """
        return self._enable_backup

    @enable_backup.setter
    def enable_backup(self, enable_backup):
        """Sets the enable_backup of this ProjectsList.

        是否开启备份  # noqa: E501

        :param enable_backup: The enable_backup of this ProjectsList.  # noqa: E501
        :type: bool
        """

        self._enable_backup = enable_backup

    @property
    def has_backups(self):
        """Gets the has_backups of this ProjectsList.  # noqa: E501

        是否有备份  # noqa: E501

        :return: The has_backups of this ProjectsList.  # noqa: E501
        :rtype: bool
        """
        return self._has_backups

    @has_backups.setter
    def has_backups(self, has_backups):
        """Sets the has_backups of this ProjectsList.

        是否有备份  # noqa: E501

        :param has_backups: The has_backups of this ProjectsList.  # noqa: E501
        :type: bool
        """

        self._has_backups = has_backups

    @property
    def vip(self):
        """Gets the vip of this ProjectsList.  # noqa: E501

        是否vip  # noqa: E501

        :return: The vip of this ProjectsList.  # noqa: E501
        :rtype: bool
        """
        return self._vip

    @vip.setter
    def vip(self, vip):
        """Sets the vip of this ProjectsList.

        是否vip  # noqa: E501

        :param vip: The vip of this ProjectsList.  # noqa: E501
        :type: bool
        """

        self._vip = vip

    @property
    def recomm(self):
        """Gets the recomm of this ProjectsList.  # noqa: E501

        是否推荐  # noqa: E501

        :return: The recomm of this ProjectsList.  # noqa: E501
        :rtype: bool
        """
        return self._recomm

    @recomm.setter
    def recomm(self, recomm):
        """Sets the recomm of this ProjectsList.

        是否推荐  # noqa: E501

        :param recomm: The recomm of this ProjectsList.  # noqa: E501
        :type: bool
        """

        self._recomm = recomm

    @property
    def template(self):
        """Gets the template of this ProjectsList.  # noqa: E501


        :return: The template of this ProjectsList.  # noqa: E501
        :rtype: Project
        """
        return self._template

    @template.setter
    def template(self, template):
        """Sets the template of this ProjectsList.


        :param template: The template of this ProjectsList.  # noqa: E501
        :type: Project
        """

        self._template = template

    @property
    def template_enabled(self):
        """Gets the template_enabled of this ProjectsList.  # noqa: E501

        是否为模板仓库  # noqa: E501

        :return: The template_enabled of this ProjectsList.  # noqa: E501
        :rtype: bool
        """
        return self._template_enabled

    @template_enabled.setter
    def template_enabled(self, template_enabled):
        """Sets the template_enabled of this ProjectsList.

        是否为模板仓库  # noqa: E501

        :param template_enabled: The template_enabled of this ProjectsList.  # noqa: E501
        :type: bool
        """

        self._template_enabled = template_enabled

    @property
    def description(self):
        """Gets the description of this ProjectsList.  # noqa: E501

        仓库描述  # noqa: E501

        :return: The description of this ProjectsList.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this ProjectsList.

        仓库描述  # noqa: E501

        :param description: The description of this ProjectsList.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def get_default_branch(self):
        """Gets the get_default_branch of this ProjectsList.  # noqa: E501

        默认分支  # noqa: E501

        :return: The get_default_branch of this ProjectsList.  # noqa: E501
        :rtype: str
        """
        return self._get_default_branch

    @get_default_branch.setter
    def get_default_branch(self, get_default_branch):
        """Sets the get_default_branch of this ProjectsList.

        默认分支  # noqa: E501

        :param get_default_branch: The get_default_branch of this ProjectsList.  # noqa: E501
        :type: str
        """

        self._get_default_branch = get_default_branch

    @property
    def releases_count(self):
        """Gets the releases_count of this ProjectsList.  # noqa: E501

        发行版数  # noqa: E501

        :return: The releases_count of this ProjectsList.  # noqa: E501
        :rtype: int
        """
        return self._releases_count

    @releases_count.setter
    def releases_count(self, releases_count):
        """Sets the releases_count of this ProjectsList.

        发行版数  # noqa: E501

        :param releases_count: The releases_count of this ProjectsList.  # noqa: E501
        :type: int
        """

        self._releases_count = releases_count

    @property
    def total_pr_count(self):
        """Gets the total_pr_count of this ProjectsList.  # noqa: E501

        PR数  # noqa: E501

        :return: The total_pr_count of this ProjectsList.  # noqa: E501
        :rtype: int
        """
        return self._total_pr_count

    @total_pr_count.setter
    def total_pr_count(self, total_pr_count):
        """Sets the total_pr_count of this ProjectsList.

        PR数  # noqa: E501

        :param total_pr_count: The total_pr_count of this ProjectsList.  # noqa: E501
        :type: int
        """

        self._total_pr_count = total_pr_count

    @property
    def is_star(self):
        """Gets the is_star of this ProjectsList.  # noqa: E501

        是否收藏  # noqa: E501

        :return: The is_star of this ProjectsList.  # noqa: E501
        :rtype: bool
        """
        return self._is_star

    @is_star.setter
    def is_star(self, is_star):
        """Sets the is_star of this ProjectsList.

        是否收藏  # noqa: E501

        :param is_star: The is_star of this ProjectsList.  # noqa: E501
        :type: bool
        """

        self._is_star = is_star

    @property
    def used_template_count(self):
        """Gets the used_template_count of this ProjectsList.  # noqa: E501

        使用此仓库作为模板的仓库总数  # noqa: E501

        :return: The used_template_count of this ProjectsList.  # noqa: E501
        :rtype: int
        """
        return self._used_template_count

    @used_template_count.setter
    def used_template_count(self, used_template_count):
        """Sets the used_template_count of this ProjectsList.

        使用此仓库作为模板的仓库总数  # noqa: E501

        :param used_template_count: The used_template_count of this ProjectsList.  # noqa: E501
        :type: int
        """

        self._used_template_count = used_template_count

    @property
    def fork_enabled(self):
        """Gets the fork_enabled of this ProjectsList.  # noqa: E501

        是否允许被Fork  # noqa: E501

        :return: The fork_enabled of this ProjectsList.  # noqa: E501
        :rtype: bool
        """
        return self._fork_enabled

    @fork_enabled.setter
    def fork_enabled(self, fork_enabled):
        """Sets the fork_enabled of this ProjectsList.

        是否允许被Fork  # noqa: E501

        :param fork_enabled: The fork_enabled of this ProjectsList.  # noqa: E501
        :type: bool
        """

        self._fork_enabled = fork_enabled

    @property
    def pull_requests_enabled(self):
        """Gets the pull_requests_enabled of this ProjectsList.  # noqa: E501

        是否接受 PR  # noqa: E501

        :return: The pull_requests_enabled of this ProjectsList.  # noqa: E501
        :rtype: bool
        """
        return self._pull_requests_enabled

    @pull_requests_enabled.setter
    def pull_requests_enabled(self, pull_requests_enabled):
        """Sets the pull_requests_enabled of this ProjectsList.

        是否接受 PR  # noqa: E501

        :param pull_requests_enabled: The pull_requests_enabled of this ProjectsList.  # noqa: E501
        :type: bool
        """

        self._pull_requests_enabled = pull_requests_enabled

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ProjectsList, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ProjectsList):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

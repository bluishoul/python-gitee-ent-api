# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class Event(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'target_type': 'str',
        'ident': 'str',
        'is_proper': 'bool',
        'created_at': 'str',
        'action': 'str',
        'action_human_name': 'str',
        'author': 'UserWithRemark',
        'in_project': 'bool',
        'in_enterprise': 'bool',
        'project': 'Project',
        'enterprise': 'str',
        'payload': 'object'
    }

    attribute_map = {
        'id': 'id',
        'target_type': 'target_type',
        'ident': 'ident',
        'is_proper': 'is_proper',
        'created_at': 'created_at',
        'action': 'action',
        'action_human_name': 'action_human_name',
        'author': 'author',
        'in_project': 'in_project',
        'in_enterprise': 'in_enterprise',
        'project': 'project',
        'enterprise': 'enterprise',
        'payload': 'payload'
    }

    def __init__(self, id=None, target_type=None, ident=None, is_proper=None, created_at=None, action=None, action_human_name=None, author=None, in_project=None, in_enterprise=None, project=None, enterprise=None, payload=None):  # noqa: E501
        """Event - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._target_type = None
        self._ident = None
        self._is_proper = None
        self._created_at = None
        self._action = None
        self._action_human_name = None
        self._author = None
        self._in_project = None
        self._in_enterprise = None
        self._project = None
        self._enterprise = None
        self._payload = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if target_type is not None:
            self.target_type = target_type
        if ident is not None:
            self.ident = ident
        if is_proper is not None:
            self.is_proper = is_proper
        if created_at is not None:
            self.created_at = created_at
        if action is not None:
            self.action = action
        if action_human_name is not None:
            self.action_human_name = action_human_name
        if author is not None:
            self.author = author
        if in_project is not None:
            self.in_project = in_project
        if in_enterprise is not None:
            self.in_enterprise = in_enterprise
        if project is not None:
            self.project = project
        if enterprise is not None:
            self.enterprise = enterprise
        if payload is not None:
            self.payload = payload

    @property
    def id(self):
        """Gets the id of this Event.  # noqa: E501

        动态id  # noqa: E501

        :return: The id of this Event.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Event.

        动态id  # noqa: E501

        :param id: The id of this Event.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def target_type(self):
        """Gets the target_type of this Event.  # noqa: E501


        :return: The target_type of this Event.  # noqa: E501
        :rtype: str
        """
        return self._target_type

    @target_type.setter
    def target_type(self, target_type):
        """Sets the target_type of this Event.


        :param target_type: The target_type of this Event.  # noqa: E501
        :type: str
        """

        self._target_type = target_type

    @property
    def ident(self):
        """Gets the ident of this Event.  # noqa: E501

        动态标识  # noqa: E501

        :return: The ident of this Event.  # noqa: E501
        :rtype: str
        """
        return self._ident

    @ident.setter
    def ident(self, ident):
        """Sets the ident of this Event.

        动态标识  # noqa: E501

        :param ident: The ident of this Event.  # noqa: E501
        :type: str
        """

        self._ident = ident

    @property
    def is_proper(self):
        """Gets the is_proper of this Event.  # noqa: E501

        是否合规  # noqa: E501

        :return: The is_proper of this Event.  # noqa: E501
        :rtype: bool
        """
        return self._is_proper

    @is_proper.setter
    def is_proper(self, is_proper):
        """Sets the is_proper of this Event.

        是否合规  # noqa: E501

        :param is_proper: The is_proper of this Event.  # noqa: E501
        :type: bool
        """

        self._is_proper = is_proper

    @property
    def created_at(self):
        """Gets the created_at of this Event.  # noqa: E501

        动态产生的时间  # noqa: E501

        :return: The created_at of this Event.  # noqa: E501
        :rtype: str
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this Event.

        动态产生的时间  # noqa: E501

        :param created_at: The created_at of this Event.  # noqa: E501
        :type: str
        """

        self._created_at = created_at

    @property
    def action(self):
        """Gets the action of this Event.  # noqa: E501

        动作  # noqa: E501

        :return: The action of this Event.  # noqa: E501
        :rtype: str
        """
        return self._action

    @action.setter
    def action(self, action):
        """Sets the action of this Event.

        动作  # noqa: E501

        :param action: The action of this Event.  # noqa: E501
        :type: str
        """

        self._action = action

    @property
    def action_human_name(self):
        """Gets the action_human_name of this Event.  # noqa: E501

        动作名称  # noqa: E501

        :return: The action_human_name of this Event.  # noqa: E501
        :rtype: str
        """
        return self._action_human_name

    @action_human_name.setter
    def action_human_name(self, action_human_name):
        """Sets the action_human_name of this Event.

        动作名称  # noqa: E501

        :param action_human_name: The action_human_name of this Event.  # noqa: E501
        :type: str
        """

        self._action_human_name = action_human_name

    @property
    def author(self):
        """Gets the author of this Event.  # noqa: E501


        :return: The author of this Event.  # noqa: E501
        :rtype: UserWithRemark
        """
        return self._author

    @author.setter
    def author(self, author):
        """Sets the author of this Event.


        :param author: The author of this Event.  # noqa: E501
        :type: UserWithRemark
        """

        self._author = author

    @property
    def in_project(self):
        """Gets the in_project of this Event.  # noqa: E501

        是否在仓库  # noqa: E501

        :return: The in_project of this Event.  # noqa: E501
        :rtype: bool
        """
        return self._in_project

    @in_project.setter
    def in_project(self, in_project):
        """Sets the in_project of this Event.

        是否在仓库  # noqa: E501

        :param in_project: The in_project of this Event.  # noqa: E501
        :type: bool
        """

        self._in_project = in_project

    @property
    def in_enterprise(self):
        """Gets the in_enterprise of this Event.  # noqa: E501

        是否在企业  # noqa: E501

        :return: The in_enterprise of this Event.  # noqa: E501
        :rtype: bool
        """
        return self._in_enterprise

    @in_enterprise.setter
    def in_enterprise(self, in_enterprise):
        """Sets the in_enterprise of this Event.

        是否在企业  # noqa: E501

        :param in_enterprise: The in_enterprise of this Event.  # noqa: E501
        :type: bool
        """

        self._in_enterprise = in_enterprise

    @property
    def project(self):
        """Gets the project of this Event.  # noqa: E501


        :return: The project of this Event.  # noqa: E501
        :rtype: Project
        """
        return self._project

    @project.setter
    def project(self, project):
        """Sets the project of this Event.


        :param project: The project of this Event.  # noqa: E501
        :type: Project
        """

        self._project = project

    @property
    def enterprise(self):
        """Gets the enterprise of this Event.  # noqa: E501

        企业  # noqa: E501

        :return: The enterprise of this Event.  # noqa: E501
        :rtype: str
        """
        return self._enterprise

    @enterprise.setter
    def enterprise(self, enterprise):
        """Sets the enterprise of this Event.

        企业  # noqa: E501

        :param enterprise: The enterprise of this Event.  # noqa: E501
        :type: str
        """

        self._enterprise = enterprise

    @property
    def payload(self):
        """Gets the payload of this Event.  # noqa: E501

        不同类型动态的内容  # noqa: E501

        :return: The payload of this Event.  # noqa: E501
        :rtype: object
        """
        return self._payload

    @payload.setter
    def payload(self, payload):
        """Sets the payload of this Event.

        不同类型动态的内容  # noqa: E501

        :param payload: The payload of this Event.  # noqa: E501
        :type: object
        """

        self._payload = payload

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Event, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Event):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

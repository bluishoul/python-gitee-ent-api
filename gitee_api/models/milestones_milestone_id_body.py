# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class MilestonesMilestoneIdBody(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'access_token': 'str',
        'title': 'str',
        'due_date': 'str',
        'start_date': 'str',
        'state_event': 'str',
        'description': 'str',
        'program_id': 'int',
        'project_ids': 'str',
        'assignee_id': 'int',
        'top': 'int'
    }

    attribute_map = {
        'access_token': 'access_token',
        'title': 'title',
        'due_date': 'due_date',
        'start_date': 'start_date',
        'state_event': 'state_event',
        'description': 'description',
        'program_id': 'program_id',
        'project_ids': 'project_ids',
        'assignee_id': 'assignee_id',
        'top': 'top'
    }

    def __init__(self, access_token=None, title=None, due_date=None, start_date=None, state_event=None, description=None, program_id=None, project_ids=None, assignee_id=None, top=None):  # noqa: E501
        """MilestonesMilestoneIdBody - a model defined in Swagger"""  # noqa: E501
        self._access_token = None
        self._title = None
        self._due_date = None
        self._start_date = None
        self._state_event = None
        self._description = None
        self._program_id = None
        self._project_ids = None
        self._assignee_id = None
        self._top = None
        self.discriminator = None
        if access_token is not None:
            self.access_token = access_token
        if title is not None:
            self.title = title
        if due_date is not None:
            self.due_date = due_date
        if start_date is not None:
            self.start_date = start_date
        if state_event is not None:
            self.state_event = state_event
        if description is not None:
            self.description = description
        if program_id is not None:
            self.program_id = program_id
        if project_ids is not None:
            self.project_ids = project_ids
        if assignee_id is not None:
            self.assignee_id = assignee_id
        if top is not None:
            self.top = top

    @property
    def access_token(self):
        """Gets the access_token of this MilestonesMilestoneIdBody.  # noqa: E501

        用户授权码  # noqa: E501

        :return: The access_token of this MilestonesMilestoneIdBody.  # noqa: E501
        :rtype: str
        """
        return self._access_token

    @access_token.setter
    def access_token(self, access_token):
        """Sets the access_token of this MilestonesMilestoneIdBody.

        用户授权码  # noqa: E501

        :param access_token: The access_token of this MilestonesMilestoneIdBody.  # noqa: E501
        :type: str
        """

        self._access_token = access_token

    @property
    def title(self):
        """Gets the title of this MilestonesMilestoneIdBody.  # noqa: E501

        里程碑标题  # noqa: E501

        :return: The title of this MilestonesMilestoneIdBody.  # noqa: E501
        :rtype: str
        """
        return self._title

    @title.setter
    def title(self, title):
        """Sets the title of this MilestonesMilestoneIdBody.

        里程碑标题  # noqa: E501

        :param title: The title of this MilestonesMilestoneIdBody.  # noqa: E501
        :type: str
        """

        self._title = title

    @property
    def due_date(self):
        """Gets the due_date of this MilestonesMilestoneIdBody.  # noqa: E501

        里程碑结束日期如：2020-08-13  # noqa: E501

        :return: The due_date of this MilestonesMilestoneIdBody.  # noqa: E501
        :rtype: str
        """
        return self._due_date

    @due_date.setter
    def due_date(self, due_date):
        """Sets the due_date of this MilestonesMilestoneIdBody.

        里程碑结束日期如：2020-08-13  # noqa: E501

        :param due_date: The due_date of this MilestonesMilestoneIdBody.  # noqa: E501
        :type: str
        """

        self._due_date = due_date

    @property
    def start_date(self):
        """Gets the start_date of this MilestonesMilestoneIdBody.  # noqa: E501

        里程碑开始日期如：2020-08-13  # noqa: E501

        :return: The start_date of this MilestonesMilestoneIdBody.  # noqa: E501
        :rtype: str
        """
        return self._start_date

    @start_date.setter
    def start_date(self, start_date):
        """Sets the start_date of this MilestonesMilestoneIdBody.

        里程碑开始日期如：2020-08-13  # noqa: E501

        :param start_date: The start_date of this MilestonesMilestoneIdBody.  # noqa: E501
        :type: str
        """

        self._start_date = start_date

    @property
    def state_event(self):
        """Gets the state_event of this MilestonesMilestoneIdBody.  # noqa: E501

        关闭或重新开启里程碑  # noqa: E501

        :return: The state_event of this MilestonesMilestoneIdBody.  # noqa: E501
        :rtype: str
        """
        return self._state_event

    @state_event.setter
    def state_event(self, state_event):
        """Sets the state_event of this MilestonesMilestoneIdBody.

        关闭或重新开启里程碑  # noqa: E501

        :param state_event: The state_event of this MilestonesMilestoneIdBody.  # noqa: E501
        :type: str
        """
        allowed_values = ["activate", "close"]  # noqa: E501
        if state_event not in allowed_values:
            raise ValueError(
                "Invalid value for `state_event` ({0}), must be one of {1}"  # noqa: E501
                .format(state_event, allowed_values)
            )

        self._state_event = state_event

    @property
    def description(self):
        """Gets the description of this MilestonesMilestoneIdBody.  # noqa: E501

        里程碑描述  # noqa: E501

        :return: The description of this MilestonesMilestoneIdBody.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this MilestonesMilestoneIdBody.

        里程碑描述  # noqa: E501

        :param description: The description of this MilestonesMilestoneIdBody.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def program_id(self):
        """Gets the program_id of this MilestonesMilestoneIdBody.  # noqa: E501

        关联项目ID  # noqa: E501

        :return: The program_id of this MilestonesMilestoneIdBody.  # noqa: E501
        :rtype: int
        """
        return self._program_id

    @program_id.setter
    def program_id(self, program_id):
        """Sets the program_id of this MilestonesMilestoneIdBody.

        关联项目ID  # noqa: E501

        :param program_id: The program_id of this MilestonesMilestoneIdBody.  # noqa: E501
        :type: int
        """

        self._program_id = program_id

    @property
    def project_ids(self):
        """Gets the project_ids of this MilestonesMilestoneIdBody.  # noqa: E501

        关联仓库ID, 以,分隔的字符串  # noqa: E501

        :return: The project_ids of this MilestonesMilestoneIdBody.  # noqa: E501
        :rtype: str
        """
        return self._project_ids

    @project_ids.setter
    def project_ids(self, project_ids):
        """Sets the project_ids of this MilestonesMilestoneIdBody.

        关联仓库ID, 以,分隔的字符串  # noqa: E501

        :param project_ids: The project_ids of this MilestonesMilestoneIdBody.  # noqa: E501
        :type: str
        """

        self._project_ids = project_ids

    @property
    def assignee_id(self):
        """Gets the assignee_id of this MilestonesMilestoneIdBody.  # noqa: E501

        里程碑负责人ID  # noqa: E501

        :return: The assignee_id of this MilestonesMilestoneIdBody.  # noqa: E501
        :rtype: int
        """
        return self._assignee_id

    @assignee_id.setter
    def assignee_id(self, assignee_id):
        """Sets the assignee_id of this MilestonesMilestoneIdBody.

        里程碑负责人ID  # noqa: E501

        :param assignee_id: The assignee_id of this MilestonesMilestoneIdBody.  # noqa: E501
        :type: int
        """

        self._assignee_id = assignee_id

    @property
    def top(self):
        """Gets the top of this MilestonesMilestoneIdBody.  # noqa: E501

        是否置顶  # noqa: E501

        :return: The top of this MilestonesMilestoneIdBody.  # noqa: E501
        :rtype: int
        """
        return self._top

    @top.setter
    def top(self, top):
        """Sets the top of this MilestonesMilestoneIdBody.

        是否置顶  # noqa: E501

        :param top: The top of this MilestonesMilestoneIdBody.  # noqa: E501
        :type: int
        """
        allowed_values = [0, 1]  # noqa: E501
        if top not in allowed_values:
            raise ValueError(
                "Invalid value for `top` ({0}), must be one of {1}"  # noqa: E501
                .format(top, allowed_values)
            )

        self._top = top

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(MilestonesMilestoneIdBody, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, MilestonesMilestoneIdBody):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class IssueOperateLog(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'icon': 'str',
        'content': 'str',
        'user': 'UserWithRemark',
        'before_change': 'str',
        'after_change': 'str'
    }

    attribute_map = {
        'id': 'id',
        'icon': 'icon',
        'content': 'content',
        'user': 'user',
        'before_change': 'before_change',
        'after_change': 'after_change'
    }

    def __init__(self, id=None, icon=None, content=None, user=None, before_change=None, after_change=None):  # noqa: E501
        """IssueOperateLog - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._icon = None
        self._content = None
        self._user = None
        self._before_change = None
        self._after_change = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if icon is not None:
            self.icon = icon
        if content is not None:
            self.content = content
        if user is not None:
            self.user = user
        if before_change is not None:
            self.before_change = before_change
        if after_change is not None:
            self.after_change = after_change

    @property
    def id(self):
        """Gets the id of this IssueOperateLog.  # noqa: E501


        :return: The id of this IssueOperateLog.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this IssueOperateLog.


        :param id: The id of this IssueOperateLog.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def icon(self):
        """Gets the icon of this IssueOperateLog.  # noqa: E501

        图标  # noqa: E501

        :return: The icon of this IssueOperateLog.  # noqa: E501
        :rtype: str
        """
        return self._icon

    @icon.setter
    def icon(self, icon):
        """Sets the icon of this IssueOperateLog.

        图标  # noqa: E501

        :param icon: The icon of this IssueOperateLog.  # noqa: E501
        :type: str
        """

        self._icon = icon

    @property
    def content(self):
        """Gets the content of this IssueOperateLog.  # noqa: E501

        内容  # noqa: E501

        :return: The content of this IssueOperateLog.  # noqa: E501
        :rtype: str
        """
        return self._content

    @content.setter
    def content(self, content):
        """Sets the content of this IssueOperateLog.

        内容  # noqa: E501

        :param content: The content of this IssueOperateLog.  # noqa: E501
        :type: str
        """

        self._content = content

    @property
    def user(self):
        """Gets the user of this IssueOperateLog.  # noqa: E501


        :return: The user of this IssueOperateLog.  # noqa: E501
        :rtype: UserWithRemark
        """
        return self._user

    @user.setter
    def user(self, user):
        """Sets the user of this IssueOperateLog.


        :param user: The user of this IssueOperateLog.  # noqa: E501
        :type: UserWithRemark
        """

        self._user = user

    @property
    def before_change(self):
        """Gets the before_change of this IssueOperateLog.  # noqa: E501

        改变前的值  # noqa: E501

        :return: The before_change of this IssueOperateLog.  # noqa: E501
        :rtype: str
        """
        return self._before_change

    @before_change.setter
    def before_change(self, before_change):
        """Sets the before_change of this IssueOperateLog.

        改变前的值  # noqa: E501

        :param before_change: The before_change of this IssueOperateLog.  # noqa: E501
        :type: str
        """

        self._before_change = before_change

    @property
    def after_change(self):
        """Gets the after_change of this IssueOperateLog.  # noqa: E501

        改变后的值  # noqa: E501

        :return: The after_change of this IssueOperateLog.  # noqa: E501
        :rtype: str
        """
        return self._after_change

    @after_change.setter
    def after_change(self, after_change):
        """Sets the after_change of this IssueOperateLog.

        改变后的值  # noqa: E501

        :param after_change: The after_change of this IssueOperateLog.  # noqa: E501
        :type: str
        """

        self._after_change = after_change

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(IssueOperateLog, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, IssueOperateLog):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

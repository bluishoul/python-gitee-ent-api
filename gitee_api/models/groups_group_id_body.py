# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class GroupsGroupIdBody(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'access_token': 'str',
        'user_ids': 'str',
        'name': 'str',
        'description': 'str',
        'public': 'int',
        'owner_id': 'int'
    }

    attribute_map = {
        'access_token': 'access_token',
        'user_ids': 'user_ids',
        'name': 'name',
        'description': 'description',
        'public': 'public',
        'owner_id': 'owner_id'
    }

    def __init__(self, access_token=None, user_ids=None, name=None, description=None, public=None, owner_id=None):  # noqa: E501
        """GroupsGroupIdBody - a model defined in Swagger"""  # noqa: E501
        self._access_token = None
        self._user_ids = None
        self._name = None
        self._description = None
        self._public = None
        self._owner_id = None
        self.discriminator = None
        if access_token is not None:
            self.access_token = access_token
        if user_ids is not None:
            self.user_ids = user_ids
        if name is not None:
            self.name = name
        if description is not None:
            self.description = description
        if public is not None:
            self.public = public
        if owner_id is not None:
            self.owner_id = owner_id

    @property
    def access_token(self):
        """Gets the access_token of this GroupsGroupIdBody.  # noqa: E501

        用户授权码  # noqa: E501

        :return: The access_token of this GroupsGroupIdBody.  # noqa: E501
        :rtype: str
        """
        return self._access_token

    @access_token.setter
    def access_token(self, access_token):
        """Sets the access_token of this GroupsGroupIdBody.

        用户授权码  # noqa: E501

        :param access_token: The access_token of this GroupsGroupIdBody.  # noqa: E501
        :type: str
        """

        self._access_token = access_token

    @property
    def user_ids(self):
        """Gets the user_ids of this GroupsGroupIdBody.  # noqa: E501

        成员 id  # noqa: E501

        :return: The user_ids of this GroupsGroupIdBody.  # noqa: E501
        :rtype: str
        """
        return self._user_ids

    @user_ids.setter
    def user_ids(self, user_ids):
        """Sets the user_ids of this GroupsGroupIdBody.

        成员 id  # noqa: E501

        :param user_ids: The user_ids of this GroupsGroupIdBody.  # noqa: E501
        :type: str
        """

        self._user_ids = user_ids

    @property
    def name(self):
        """Gets the name of this GroupsGroupIdBody.  # noqa: E501

        名称  # noqa: E501

        :return: The name of this GroupsGroupIdBody.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this GroupsGroupIdBody.

        名称  # noqa: E501

        :param name: The name of this GroupsGroupIdBody.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def description(self):
        """Gets the description of this GroupsGroupIdBody.  # noqa: E501

        简介  # noqa: E501

        :return: The description of this GroupsGroupIdBody.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this GroupsGroupIdBody.

        简介  # noqa: E501

        :param description: The description of this GroupsGroupIdBody.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def public(self):
        """Gets the public of this GroupsGroupIdBody.  # noqa: E501

        类型, 0:内部，1:公开，2:外包  # noqa: E501

        :return: The public of this GroupsGroupIdBody.  # noqa: E501
        :rtype: int
        """
        return self._public

    @public.setter
    def public(self, public):
        """Sets the public of this GroupsGroupIdBody.

        类型, 0:内部，1:公开，2:外包  # noqa: E501

        :param public: The public of this GroupsGroupIdBody.  # noqa: E501
        :type: int
        """

        self._public = public

    @property
    def owner_id(self):
        """Gets the owner_id of this GroupsGroupIdBody.  # noqa: E501

        负责人 id  # noqa: E501

        :return: The owner_id of this GroupsGroupIdBody.  # noqa: E501
        :rtype: int
        """
        return self._owner_id

    @owner_id.setter
    def owner_id(self, owner_id):
        """Sets the owner_id of this GroupsGroupIdBody.

        负责人 id  # noqa: E501

        :param owner_id: The owner_id of this GroupsGroupIdBody.  # noqa: E501
        :type: int
        """

        self._owner_id = owner_id

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(GroupsGroupIdBody, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, GroupsGroupIdBody):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

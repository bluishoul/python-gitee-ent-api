# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class PrAssigner(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'assigners': 'str',
        'testers': 'str',
        'pr_assign_num': 'int',
        'pr_test_num': 'int'
    }

    attribute_map = {
        'assigners': 'assigners',
        'testers': 'testers',
        'pr_assign_num': 'pr_assign_num',
        'pr_test_num': 'pr_test_num'
    }

    def __init__(self, assigners=None, testers=None, pr_assign_num=None, pr_test_num=None):  # noqa: E501
        """PrAssigner - a model defined in Swagger"""  # noqa: E501
        self._assigners = None
        self._testers = None
        self._pr_assign_num = None
        self._pr_test_num = None
        self.discriminator = None
        if assigners is not None:
            self.assigners = assigners
        if testers is not None:
            self.testers = testers
        if pr_assign_num is not None:
            self.pr_assign_num = pr_assign_num
        if pr_test_num is not None:
            self.pr_test_num = pr_test_num

    @property
    def assigners(self):
        """Gets the assigners of this PrAssigner.  # noqa: E501

        审查人员id列表  # noqa: E501

        :return: The assigners of this PrAssigner.  # noqa: E501
        :rtype: str
        """
        return self._assigners

    @assigners.setter
    def assigners(self, assigners):
        """Sets the assigners of this PrAssigner.

        审查人员id列表  # noqa: E501

        :param assigners: The assigners of this PrAssigner.  # noqa: E501
        :type: str
        """

        self._assigners = assigners

    @property
    def testers(self):
        """Gets the testers of this PrAssigner.  # noqa: E501

        测试人员id列表  # noqa: E501

        :return: The testers of this PrAssigner.  # noqa: E501
        :rtype: str
        """
        return self._testers

    @testers.setter
    def testers(self, testers):
        """Sets the testers of this PrAssigner.

        测试人员id列表  # noqa: E501

        :param testers: The testers of this PrAssigner.  # noqa: E501
        :type: str
        """

        self._testers = testers

    @property
    def pr_assign_num(self):
        """Gets the pr_assign_num of this PrAssigner.  # noqa: E501

        可合并的审查人员门槛数  # noqa: E501

        :return: The pr_assign_num of this PrAssigner.  # noqa: E501
        :rtype: int
        """
        return self._pr_assign_num

    @pr_assign_num.setter
    def pr_assign_num(self, pr_assign_num):
        """Sets the pr_assign_num of this PrAssigner.

        可合并的审查人员门槛数  # noqa: E501

        :param pr_assign_num: The pr_assign_num of this PrAssigner.  # noqa: E501
        :type: int
        """

        self._pr_assign_num = pr_assign_num

    @property
    def pr_test_num(self):
        """Gets the pr_test_num of this PrAssigner.  # noqa: E501

        可合并的测试人员门槛数  # noqa: E501

        :return: The pr_test_num of this PrAssigner.  # noqa: E501
        :rtype: int
        """
        return self._pr_test_num

    @pr_test_num.setter
    def pr_test_num(self, pr_test_num):
        """Sets the pr_test_num of this PrAssigner.

        可合并的测试人员门槛数  # noqa: E501

        :param pr_test_num: The pr_test_num of this PrAssigner.  # noqa: E501
        :type: int
        """

        self._pr_test_num = pr_test_num

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(PrAssigner, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PrAssigner):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

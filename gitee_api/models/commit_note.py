# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class CommitNote(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'type': 'str',
        'author': 'UserWithRemark',
        'content': 'str',
        'content_html': 'str',
        'reactions': 'Reaction',
        'line_code': 'str'
    }

    attribute_map = {
        'id': 'id',
        'type': 'type',
        'author': 'author',
        'content': 'content',
        'content_html': 'content_html',
        'reactions': 'reactions',
        'line_code': 'line_code'
    }

    def __init__(self, id=None, type=None, author=None, content=None, content_html=None, reactions=None, line_code=None):  # noqa: E501
        """CommitNote - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._type = None
        self._author = None
        self._content = None
        self._content_html = None
        self._reactions = None
        self._line_code = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if type is not None:
            self.type = type
        if author is not None:
            self.author = author
        if content is not None:
            self.content = content
        if content_html is not None:
            self.content_html = content_html
        if reactions is not None:
            self.reactions = reactions
        if line_code is not None:
            self.line_code = line_code

    @property
    def id(self):
        """Gets the id of this CommitNote.  # noqa: E501

        评论的 id  # noqa: E501

        :return: The id of this CommitNote.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this CommitNote.

        评论的 id  # noqa: E501

        :param id: The id of this CommitNote.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def type(self):
        """Gets the type of this CommitNote.  # noqa: E501

        评论类型  # noqa: E501

        :return: The type of this CommitNote.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this CommitNote.

        评论类型  # noqa: E501

        :param type: The type of this CommitNote.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def author(self):
        """Gets the author of this CommitNote.  # noqa: E501


        :return: The author of this CommitNote.  # noqa: E501
        :rtype: UserWithRemark
        """
        return self._author

    @author.setter
    def author(self, author):
        """Sets the author of this CommitNote.


        :param author: The author of this CommitNote.  # noqa: E501
        :type: UserWithRemark
        """

        self._author = author

    @property
    def content(self):
        """Gets the content of this CommitNote.  # noqa: E501

        评论内容(markdown 格式)  # noqa: E501

        :return: The content of this CommitNote.  # noqa: E501
        :rtype: str
        """
        return self._content

    @content.setter
    def content(self, content):
        """Sets the content of this CommitNote.

        评论内容(markdown 格式)  # noqa: E501

        :param content: The content of this CommitNote.  # noqa: E501
        :type: str
        """

        self._content = content

    @property
    def content_html(self):
        """Gets the content_html of this CommitNote.  # noqa: E501

        评论内容(html 格式)  # noqa: E501

        :return: The content_html of this CommitNote.  # noqa: E501
        :rtype: str
        """
        return self._content_html

    @content_html.setter
    def content_html(self, content_html):
        """Sets the content_html of this CommitNote.

        评论内容(html 格式)  # noqa: E501

        :param content_html: The content_html of this CommitNote.  # noqa: E501
        :type: str
        """

        self._content_html = content_html

    @property
    def reactions(self):
        """Gets the reactions of this CommitNote.  # noqa: E501


        :return: The reactions of this CommitNote.  # noqa: E501
        :rtype: Reaction
        """
        return self._reactions

    @reactions.setter
    def reactions(self, reactions):
        """Sets the reactions of this CommitNote.


        :param reactions: The reactions of this CommitNote.  # noqa: E501
        :type: Reaction
        """

        self._reactions = reactions

    @property
    def line_code(self):
        """Gets the line_code of this CommitNote.  # noqa: E501

        代码行标记  # noqa: E501

        :return: The line_code of this CommitNote.  # noqa: E501
        :rtype: str
        """
        return self._line_code

    @line_code.setter
    def line_code(self, line_code):
        """Sets the line_code of this CommitNote.

        代码行标记  # noqa: E501

        :param line_code: The line_code of this CommitNote.  # noqa: E501
        :type: str
        """

        self._line_code = line_code

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(CommitNote, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, CommitNote):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

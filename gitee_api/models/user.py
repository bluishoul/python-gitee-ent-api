# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class User(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'username': 'str',
        'name': 'str',
        'avatar_url': 'str',
        'state': 'str',
        'user_color': 'int'
    }

    attribute_map = {
        'id': 'id',
        'username': 'username',
        'name': 'name',
        'avatar_url': 'avatar_url',
        'state': 'state',
        'user_color': 'user_color'
    }

    def __init__(self, id=None, username=None, name=None, avatar_url=None, state=None, user_color=None):  # noqa: E501
        """User - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._username = None
        self._name = None
        self._avatar_url = None
        self._state = None
        self._user_color = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if username is not None:
            self.username = username
        if name is not None:
            self.name = name
        if avatar_url is not None:
            self.avatar_url = avatar_url
        if state is not None:
            self.state = state
        if user_color is not None:
            self.user_color = user_color

    @property
    def id(self):
        """Gets the id of this User.  # noqa: E501

        用户 id  # noqa: E501

        :return: The id of this User.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this User.

        用户 id  # noqa: E501

        :param id: The id of this User.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def username(self):
        """Gets the username of this User.  # noqa: E501

        用户个性地址  # noqa: E501

        :return: The username of this User.  # noqa: E501
        :rtype: str
        """
        return self._username

    @username.setter
    def username(self, username):
        """Sets the username of this User.

        用户个性地址  # noqa: E501

        :param username: The username of this User.  # noqa: E501
        :type: str
        """

        self._username = username

    @property
    def name(self):
        """Gets the name of this User.  # noqa: E501

        用户名称  # noqa: E501

        :return: The name of this User.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this User.

        用户名称  # noqa: E501

        :param name: The name of this User.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def avatar_url(self):
        """Gets the avatar_url of this User.  # noqa: E501

        用户头像  # noqa: E501

        :return: The avatar_url of this User.  # noqa: E501
        :rtype: str
        """
        return self._avatar_url

    @avatar_url.setter
    def avatar_url(self, avatar_url):
        """Sets the avatar_url of this User.

        用户头像  # noqa: E501

        :param avatar_url: The avatar_url of this User.  # noqa: E501
        :type: str
        """

        self._avatar_url = avatar_url

    @property
    def state(self):
        """Gets the state of this User.  # noqa: E501

        账号是否可用(active: 可用 blocked: 已被系统屏蔽)  # noqa: E501

        :return: The state of this User.  # noqa: E501
        :rtype: str
        """
        return self._state

    @state.setter
    def state(self, state):
        """Sets the state of this User.

        账号是否可用(active: 可用 blocked: 已被系统屏蔽)  # noqa: E501

        :param state: The state of this User.  # noqa: E501
        :type: str
        """

        self._state = state

    @property
    def user_color(self):
        """Gets the user_color of this User.  # noqa: E501

        用户代码风格  # noqa: E501

        :return: The user_color of this User.  # noqa: E501
        :rtype: int
        """
        return self._user_color

    @user_color.setter
    def user_color(self, user_color):
        """Sets the user_color of this User.

        用户代码风格  # noqa: E501

        :param user_color: The user_color of this User.  # noqa: E501
        :type: int
        """

        self._user_color = user_color

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(User, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, User):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class EnterpriseDeployKey(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'title': 'str',
        'key': 'str',
        'sha256_fingerprint': 'str',
        'user': 'UserWithRemark',
        'projects_count': 'int',
        'projects': 'Project',
        'system': 'bool',
        'is_bae': 'bool',
        'is_pages': 'bool'
    }

    attribute_map = {
        'id': 'id',
        'title': 'title',
        'key': 'key',
        'sha256_fingerprint': 'sha256_fingerprint',
        'user': 'user',
        'projects_count': 'projects_count',
        'projects': 'projects',
        'system': 'system',
        'is_bae': 'is_bae',
        'is_pages': 'is_pages'
    }

    def __init__(self, id=None, title=None, key=None, sha256_fingerprint=None, user=None, projects_count=None, projects=None, system=None, is_bae=None, is_pages=None):  # noqa: E501
        """EnterpriseDeployKey - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._title = None
        self._key = None
        self._sha256_fingerprint = None
        self._user = None
        self._projects_count = None
        self._projects = None
        self._system = None
        self._is_bae = None
        self._is_pages = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if title is not None:
            self.title = title
        if key is not None:
            self.key = key
        if sha256_fingerprint is not None:
            self.sha256_fingerprint = sha256_fingerprint
        if user is not None:
            self.user = user
        if projects_count is not None:
            self.projects_count = projects_count
        if projects is not None:
            self.projects = projects
        if system is not None:
            self.system = system
        if is_bae is not None:
            self.is_bae = is_bae
        if is_pages is not None:
            self.is_pages = is_pages

    @property
    def id(self):
        """Gets the id of this EnterpriseDeployKey.  # noqa: E501

        部署公钥 id  # noqa: E501

        :return: The id of this EnterpriseDeployKey.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this EnterpriseDeployKey.

        部署公钥 id  # noqa: E501

        :param id: The id of this EnterpriseDeployKey.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def title(self):
        """Gets the title of this EnterpriseDeployKey.  # noqa: E501

        部署公钥 title  # noqa: E501

        :return: The title of this EnterpriseDeployKey.  # noqa: E501
        :rtype: str
        """
        return self._title

    @title.setter
    def title(self, title):
        """Sets the title of this EnterpriseDeployKey.

        部署公钥 title  # noqa: E501

        :param title: The title of this EnterpriseDeployKey.  # noqa: E501
        :type: str
        """

        self._title = title

    @property
    def key(self):
        """Gets the key of this EnterpriseDeployKey.  # noqa: E501

        部署公钥内容  # noqa: E501

        :return: The key of this EnterpriseDeployKey.  # noqa: E501
        :rtype: str
        """
        return self._key

    @key.setter
    def key(self, key):
        """Sets the key of this EnterpriseDeployKey.

        部署公钥内容  # noqa: E501

        :param key: The key of this EnterpriseDeployKey.  # noqa: E501
        :type: str
        """

        self._key = key

    @property
    def sha256_fingerprint(self):
        """Gets the sha256_fingerprint of this EnterpriseDeployKey.  # noqa: E501

        sha256 指纹  # noqa: E501

        :return: The sha256_fingerprint of this EnterpriseDeployKey.  # noqa: E501
        :rtype: str
        """
        return self._sha256_fingerprint

    @sha256_fingerprint.setter
    def sha256_fingerprint(self, sha256_fingerprint):
        """Sets the sha256_fingerprint of this EnterpriseDeployKey.

        sha256 指纹  # noqa: E501

        :param sha256_fingerprint: The sha256_fingerprint of this EnterpriseDeployKey.  # noqa: E501
        :type: str
        """

        self._sha256_fingerprint = sha256_fingerprint

    @property
    def user(self):
        """Gets the user of this EnterpriseDeployKey.  # noqa: E501


        :return: The user of this EnterpriseDeployKey.  # noqa: E501
        :rtype: UserWithRemark
        """
        return self._user

    @user.setter
    def user(self, user):
        """Sets the user of this EnterpriseDeployKey.


        :param user: The user of this EnterpriseDeployKey.  # noqa: E501
        :type: UserWithRemark
        """

        self._user = user

    @property
    def projects_count(self):
        """Gets the projects_count of this EnterpriseDeployKey.  # noqa: E501

        已部署仓库数量  # noqa: E501

        :return: The projects_count of this EnterpriseDeployKey.  # noqa: E501
        :rtype: int
        """
        return self._projects_count

    @projects_count.setter
    def projects_count(self, projects_count):
        """Sets the projects_count of this EnterpriseDeployKey.

        已部署仓库数量  # noqa: E501

        :param projects_count: The projects_count of this EnterpriseDeployKey.  # noqa: E501
        :type: int
        """

        self._projects_count = projects_count

    @property
    def projects(self):
        """Gets the projects of this EnterpriseDeployKey.  # noqa: E501


        :return: The projects of this EnterpriseDeployKey.  # noqa: E501
        :rtype: Project
        """
        return self._projects

    @projects.setter
    def projects(self, projects):
        """Sets the projects of this EnterpriseDeployKey.


        :param projects: The projects of this EnterpriseDeployKey.  # noqa: E501
        :type: Project
        """

        self._projects = projects

    @property
    def system(self):
        """Gets the system of this EnterpriseDeployKey.  # noqa: E501

        是否是系统公钥  # noqa: E501

        :return: The system of this EnterpriseDeployKey.  # noqa: E501
        :rtype: bool
        """
        return self._system

    @system.setter
    def system(self, system):
        """Sets the system of this EnterpriseDeployKey.

        是否是系统公钥  # noqa: E501

        :param system: The system of this EnterpriseDeployKey.  # noqa: E501
        :type: bool
        """

        self._system = system

    @property
    def is_bae(self):
        """Gets the is_bae of this EnterpriseDeployKey.  # noqa: E501

        是否是bae公钥  # noqa: E501

        :return: The is_bae of this EnterpriseDeployKey.  # noqa: E501
        :rtype: bool
        """
        return self._is_bae

    @is_bae.setter
    def is_bae(self, is_bae):
        """Sets the is_bae of this EnterpriseDeployKey.

        是否是bae公钥  # noqa: E501

        :param is_bae: The is_bae of this EnterpriseDeployKey.  # noqa: E501
        :type: bool
        """

        self._is_bae = is_bae

    @property
    def is_pages(self):
        """Gets the is_pages of this EnterpriseDeployKey.  # noqa: E501

        是否是pages公钥  # noqa: E501

        :return: The is_pages of this EnterpriseDeployKey.  # noqa: E501
        :rtype: bool
        """
        return self._is_pages

    @is_pages.setter
    def is_pages(self, is_pages):
        """Sets the is_pages of this EnterpriseDeployKey.

        是否是pages公钥  # noqa: E501

        :param is_pages: The is_pages of this EnterpriseDeployKey.  # noqa: E501
        :type: bool
        """

        self._is_pages = is_pages

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(EnterpriseDeployKey, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, EnterpriseDeployKey):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

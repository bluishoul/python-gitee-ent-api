# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class ProjectBase(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'name': 'str',
        'path': 'str',
        'path_with_namespace': 'str',
        'public': 'int',
        'is_gvp': 'bool',
        'is_empty_repo': 'bool',
        'fork_enabled': 'bool',
        'name_with_namespace': 'str'
    }

    attribute_map = {
        'id': 'id',
        'name': 'name',
        'path': 'path',
        'path_with_namespace': 'path_with_namespace',
        'public': 'public',
        'is_gvp': 'is_gvp',
        'is_empty_repo': 'is_empty_repo',
        'fork_enabled': 'fork_enabled',
        'name_with_namespace': 'name_with_namespace'
    }

    def __init__(self, id=None, name=None, path=None, path_with_namespace=None, public=None, is_gvp=None, is_empty_repo=None, fork_enabled=None, name_with_namespace=None):  # noqa: E501
        """ProjectBase - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._name = None
        self._path = None
        self._path_with_namespace = None
        self._public = None
        self._is_gvp = None
        self._is_empty_repo = None
        self._fork_enabled = None
        self._name_with_namespace = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if name is not None:
            self.name = name
        if path is not None:
            self.path = path
        if path_with_namespace is not None:
            self.path_with_namespace = path_with_namespace
        if public is not None:
            self.public = public
        if is_gvp is not None:
            self.is_gvp = is_gvp
        if is_empty_repo is not None:
            self.is_empty_repo = is_empty_repo
        if fork_enabled is not None:
            self.fork_enabled = fork_enabled
        if name_with_namespace is not None:
            self.name_with_namespace = name_with_namespace

    @property
    def id(self):
        """Gets the id of this ProjectBase.  # noqa: E501

        仓库ID  # noqa: E501

        :return: The id of this ProjectBase.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ProjectBase.

        仓库ID  # noqa: E501

        :param id: The id of this ProjectBase.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def name(self):
        """Gets the name of this ProjectBase.  # noqa: E501

        仓库名称  # noqa: E501

        :return: The name of this ProjectBase.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this ProjectBase.

        仓库名称  # noqa: E501

        :param name: The name of this ProjectBase.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def path(self):
        """Gets the path of this ProjectBase.  # noqa: E501

        仓库路径  # noqa: E501

        :return: The path of this ProjectBase.  # noqa: E501
        :rtype: str
        """
        return self._path

    @path.setter
    def path(self, path):
        """Sets the path of this ProjectBase.

        仓库路径  # noqa: E501

        :param path: The path of this ProjectBase.  # noqa: E501
        :type: str
        """

        self._path = path

    @property
    def path_with_namespace(self):
        """Gets the path_with_namespace of this ProjectBase.  # noqa: E501

        namespace/path  # noqa: E501

        :return: The path_with_namespace of this ProjectBase.  # noqa: E501
        :rtype: str
        """
        return self._path_with_namespace

    @path_with_namespace.setter
    def path_with_namespace(self, path_with_namespace):
        """Sets the path_with_namespace of this ProjectBase.

        namespace/path  # noqa: E501

        :param path_with_namespace: The path_with_namespace of this ProjectBase.  # noqa: E501
        :type: str
        """

        self._path_with_namespace = path_with_namespace

    @property
    def public(self):
        """Gets the public of this ProjectBase.  # noqa: E501

        仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开;  # noqa: E501

        :return: The public of this ProjectBase.  # noqa: E501
        :rtype: int
        """
        return self._public

    @public.setter
    def public(self, public):
        """Sets the public of this ProjectBase.

        仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开;  # noqa: E501

        :param public: The public of this ProjectBase.  # noqa: E501
        :type: int
        """

        self._public = public

    @property
    def is_gvp(self):
        """Gets the is_gvp of this ProjectBase.  # noqa: E501

        是否为GVP仓库  # noqa: E501

        :return: The is_gvp of this ProjectBase.  # noqa: E501
        :rtype: bool
        """
        return self._is_gvp

    @is_gvp.setter
    def is_gvp(self, is_gvp):
        """Sets the is_gvp of this ProjectBase.

        是否为GVP仓库  # noqa: E501

        :param is_gvp: The is_gvp of this ProjectBase.  # noqa: E501
        :type: bool
        """

        self._is_gvp = is_gvp

    @property
    def is_empty_repo(self):
        """Gets the is_empty_repo of this ProjectBase.  # noqa: E501

        是否为空仓库  # noqa: E501

        :return: The is_empty_repo of this ProjectBase.  # noqa: E501
        :rtype: bool
        """
        return self._is_empty_repo

    @is_empty_repo.setter
    def is_empty_repo(self, is_empty_repo):
        """Sets the is_empty_repo of this ProjectBase.

        是否为空仓库  # noqa: E501

        :param is_empty_repo: The is_empty_repo of this ProjectBase.  # noqa: E501
        :type: bool
        """

        self._is_empty_repo = is_empty_repo

    @property
    def fork_enabled(self):
        """Gets the fork_enabled of this ProjectBase.  # noqa: E501

        是否允许 fork  # noqa: E501

        :return: The fork_enabled of this ProjectBase.  # noqa: E501
        :rtype: bool
        """
        return self._fork_enabled

    @fork_enabled.setter
    def fork_enabled(self, fork_enabled):
        """Sets the fork_enabled of this ProjectBase.

        是否允许 fork  # noqa: E501

        :param fork_enabled: The fork_enabled of this ProjectBase.  # noqa: E501
        :type: bool
        """

        self._fork_enabled = fork_enabled

    @property
    def name_with_namespace(self):
        """Gets the name_with_namespace of this ProjectBase.  # noqa: E501

        namespace_name/path  # noqa: E501

        :return: The name_with_namespace of this ProjectBase.  # noqa: E501
        :rtype: str
        """
        return self._name_with_namespace

    @name_with_namespace.setter
    def name_with_namespace(self, name_with_namespace):
        """Sets the name_with_namespace of this ProjectBase.

        namespace_name/path  # noqa: E501

        :param name_with_namespace: The name_with_namespace of this ProjectBase.  # noqa: E501
        :type: str
        """

        self._name_with_namespace = name_with_namespace

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ProjectBase, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ProjectBase):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

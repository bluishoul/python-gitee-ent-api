# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class EnterpriseNavigate(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'name': 'str',
        'icon': 'str',
        'serial': 'int',
        'active': 'bool',
        'fixed': 'bool'
    }

    attribute_map = {
        'id': 'id',
        'name': 'name',
        'icon': 'icon',
        'serial': 'serial',
        'active': 'active',
        'fixed': 'fixed'
    }

    def __init__(self, id=None, name=None, icon=None, serial=None, active=None, fixed=None):  # noqa: E501
        """EnterpriseNavigate - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._name = None
        self._icon = None
        self._serial = None
        self._active = None
        self._fixed = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if name is not None:
            self.name = name
        if icon is not None:
            self.icon = icon
        if serial is not None:
            self.serial = serial
        if active is not None:
            self.active = active
        if fixed is not None:
            self.fixed = fixed

    @property
    def id(self):
        """Gets the id of this EnterpriseNavigate.  # noqa: E501

        导航卡 id  # noqa: E501

        :return: The id of this EnterpriseNavigate.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this EnterpriseNavigate.

        导航卡 id  # noqa: E501

        :param id: The id of this EnterpriseNavigate.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def name(self):
        """Gets the name of this EnterpriseNavigate.  # noqa: E501

        导航卡名称  # noqa: E501

        :return: The name of this EnterpriseNavigate.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this EnterpriseNavigate.

        导航卡名称  # noqa: E501

        :param name: The name of this EnterpriseNavigate.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def icon(self):
        """Gets the icon of this EnterpriseNavigate.  # noqa: E501

        导航卡图标  # noqa: E501

        :return: The icon of this EnterpriseNavigate.  # noqa: E501
        :rtype: str
        """
        return self._icon

    @icon.setter
    def icon(self, icon):
        """Sets the icon of this EnterpriseNavigate.

        导航卡图标  # noqa: E501

        :param icon: The icon of this EnterpriseNavigate.  # noqa: E501
        :type: str
        """

        self._icon = icon

    @property
    def serial(self):
        """Gets the serial of this EnterpriseNavigate.  # noqa: E501

        排序  # noqa: E501

        :return: The serial of this EnterpriseNavigate.  # noqa: E501
        :rtype: int
        """
        return self._serial

    @serial.setter
    def serial(self, serial):
        """Sets the serial of this EnterpriseNavigate.

        排序  # noqa: E501

        :param serial: The serial of this EnterpriseNavigate.  # noqa: E501
        :type: int
        """

        self._serial = serial

    @property
    def active(self):
        """Gets the active of this EnterpriseNavigate.  # noqa: E501

        是否显示  # noqa: E501

        :return: The active of this EnterpriseNavigate.  # noqa: E501
        :rtype: bool
        """
        return self._active

    @active.setter
    def active(self, active):
        """Sets the active of this EnterpriseNavigate.

        是否显示  # noqa: E501

        :param active: The active of this EnterpriseNavigate.  # noqa: E501
        :type: bool
        """

        self._active = active

    @property
    def fixed(self):
        """Gets the fixed of this EnterpriseNavigate.  # noqa: E501

        是否禁止修改  # noqa: E501

        :return: The fixed of this EnterpriseNavigate.  # noqa: E501
        :rtype: bool
        """
        return self._fixed

    @fixed.setter
    def fixed(self, fixed):
        """Sets the fixed of this EnterpriseNavigate.

        是否禁止修改  # noqa: E501

        :param fixed: The fixed of this EnterpriseNavigate.  # noqa: E501
        :type: bool
        """

        self._fixed = fixed

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(EnterpriseNavigate, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, EnterpriseNavigate):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other

# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from gitee_api.api_client import ApiClient


class LabelsApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def delete_enterprise_id_labels_label_id(self, enterprise_id, label_id, **kwargs):  # noqa: E501
        """删除标签  # noqa: E501

        删除标签  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_enterprise_id_labels_label_id(enterprise_id, label_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: (required)
        :param int label_id: (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.delete_enterprise_id_labels_label_id_with_http_info(enterprise_id, label_id, **kwargs)  # noqa: E501
        else:
            (data) = self.delete_enterprise_id_labels_label_id_with_http_info(enterprise_id, label_id, **kwargs)  # noqa: E501
            return data

    def delete_enterprise_id_labels_label_id_with_http_info(self, enterprise_id, label_id, **kwargs):  # noqa: E501
        """删除标签  # noqa: E501

        删除标签  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_enterprise_id_labels_label_id_with_http_info(enterprise_id, label_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: (required)
        :param int label_id: (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['enterprise_id', 'label_id', 'access_token']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_enterprise_id_labels_label_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `delete_enterprise_id_labels_label_id`")  # noqa: E501
        # verify the required parameter 'label_id' is set
        if ('label_id' not in params or
                params['label_id'] is None):
            raise ValueError("Missing the required parameter `label_id` when calling `delete_enterprise_id_labels_label_id`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501
        if 'label_id' in params:
            path_params['label_id'] = params['label_id']  # noqa: E501

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/labels/{label_id}', 'DELETE',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_enterprise_id_labels(self, enterprise_id, **kwargs):  # noqa: E501
        """获取标签列表  # noqa: E501

        获取标签列表  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_labels(enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str access_token: 用户授权码
        :param str sort: 排序字段(created_at、updated_at, serial)
        :param str direction: 排序方向(asc: 升序 desc: 倒序)
        :param str search: 搜索关键字
        :param str label_ids: 根据 id 返回标签列表
        :param int page: 当前的页码
        :param int per_page: 每页的数量，最大为 100
        :return: list[Label]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_enterprise_id_labels_with_http_info(enterprise_id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_enterprise_id_labels_with_http_info(enterprise_id, **kwargs)  # noqa: E501
            return data

    def get_enterprise_id_labels_with_http_info(self, enterprise_id, **kwargs):  # noqa: E501
        """获取标签列表  # noqa: E501

        获取标签列表  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_labels_with_http_info(enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str access_token: 用户授权码
        :param str sort: 排序字段(created_at、updated_at, serial)
        :param str direction: 排序方向(asc: 升序 desc: 倒序)
        :param str search: 搜索关键字
        :param str label_ids: 根据 id 返回标签列表
        :param int page: 当前的页码
        :param int per_page: 每页的数量，最大为 100
        :return: list[Label]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['enterprise_id', 'access_token', 'sort', 'direction', 'search', 'label_ids', 'page', 'per_page']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_enterprise_id_labels" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `get_enterprise_id_labels`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))  # noqa: E501
        if 'sort' in params:
            query_params.append(('sort', params['sort']))  # noqa: E501
        if 'direction' in params:
            query_params.append(('direction', params['direction']))  # noqa: E501
        if 'search' in params:
            query_params.append(('search', params['search']))  # noqa: E501
        if 'label_ids' in params:
            query_params.append(('label_ids', params['label_ids']))  # noqa: E501
        if 'page' in params:
            query_params.append(('page', params['page']))  # noqa: E501
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/labels', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='list[Label]',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def post_enterprise_id_labels(self, body, enterprise_id, **kwargs):  # noqa: E501
        """新增标签  # noqa: E501

        新增标签  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.post_enterprise_id_labels(body, enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param EnterpriseIdLabelsBody body: (required)
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :return: Label
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.post_enterprise_id_labels_with_http_info(body, enterprise_id, **kwargs)  # noqa: E501
        else:
            (data) = self.post_enterprise_id_labels_with_http_info(body, enterprise_id, **kwargs)  # noqa: E501
            return data

    def post_enterprise_id_labels_with_http_info(self, body, enterprise_id, **kwargs):  # noqa: E501
        """新增标签  # noqa: E501

        新增标签  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.post_enterprise_id_labels_with_http_info(body, enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param EnterpriseIdLabelsBody body: (required)
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :return: Label
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body', 'enterprise_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_enterprise_id_labels" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `post_enterprise_id_labels`")  # noqa: E501
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `post_enterprise_id_labels`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/labels', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='Label',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def put_enterprise_id_labels_change_serial(self, body, enterprise_id, **kwargs):  # noqa: E501
        """更新标签排序  # noqa: E501

        更新标签排序  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.put_enterprise_id_labels_change_serial(body, enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param LabelsChangeSerialBody body: (required)
        :param int enterprise_id: (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.put_enterprise_id_labels_change_serial_with_http_info(body, enterprise_id, **kwargs)  # noqa: E501
        else:
            (data) = self.put_enterprise_id_labels_change_serial_with_http_info(body, enterprise_id, **kwargs)  # noqa: E501
            return data

    def put_enterprise_id_labels_change_serial_with_http_info(self, body, enterprise_id, **kwargs):  # noqa: E501
        """更新标签排序  # noqa: E501

        更新标签排序  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.put_enterprise_id_labels_change_serial_with_http_info(body, enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param LabelsChangeSerialBody body: (required)
        :param int enterprise_id: (required)
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body', 'enterprise_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_enterprise_id_labels_change_serial" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `put_enterprise_id_labels_change_serial`")  # noqa: E501
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `put_enterprise_id_labels_change_serial`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/labels/change_serial', 'PUT',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def put_enterprise_id_labels_label_id(self, body, enterprise_id, label_id, **kwargs):  # noqa: E501
        """更新标签  # noqa: E501

        更新标签  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.put_enterprise_id_labels_label_id(body, enterprise_id, label_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param LabelsLabelIdBody body: (required)
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param int label_id: 标签 id (required)
        :return: Label
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.put_enterprise_id_labels_label_id_with_http_info(body, enterprise_id, label_id, **kwargs)  # noqa: E501
        else:
            (data) = self.put_enterprise_id_labels_label_id_with_http_info(body, enterprise_id, label_id, **kwargs)  # noqa: E501
            return data

    def put_enterprise_id_labels_label_id_with_http_info(self, body, enterprise_id, label_id, **kwargs):  # noqa: E501
        """更新标签  # noqa: E501

        更新标签  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.put_enterprise_id_labels_label_id_with_http_info(body, enterprise_id, label_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param LabelsLabelIdBody body: (required)
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param int label_id: 标签 id (required)
        :return: Label
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body', 'enterprise_id', 'label_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_enterprise_id_labels_label_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `put_enterprise_id_labels_label_id`")  # noqa: E501
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `put_enterprise_id_labels_label_id`")  # noqa: E501
        # verify the required parameter 'label_id' is set
        if ('label_id' not in params or
                params['label_id'] is None):
            raise ValueError("Missing the required parameter `label_id` when calling `put_enterprise_id_labels_label_id`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501
        if 'label_id' in params:
            path_params['label_id'] = params['label_id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/labels/{label_id}', 'PUT',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='Label',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

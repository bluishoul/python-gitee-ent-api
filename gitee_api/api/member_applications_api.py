# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from gitee_api.api_client import ApiClient


class MemberApplicationsApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def get_enterprise_id_member_applications(self, target_type, target_id, enterprise_id, **kwargs):  # noqa: E501
        """企业/仓库/团队的成员加入申请列表  # noqa: E501

        企业/仓库/团队的成员加入申请列表  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_member_applications(target_type, target_id, enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str target_type: 申请加入的类型: Enterprise,Project,Group (required)
        :param int target_id: 申请加入的主体 ID (required)
        :param int enterprise_id: (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量，最大为 100
        :return: list[MemberApplication]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_enterprise_id_member_applications_with_http_info(target_type, target_id, enterprise_id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_enterprise_id_member_applications_with_http_info(target_type, target_id, enterprise_id, **kwargs)  # noqa: E501
            return data

    def get_enterprise_id_member_applications_with_http_info(self, target_type, target_id, enterprise_id, **kwargs):  # noqa: E501
        """企业/仓库/团队的成员加入申请列表  # noqa: E501

        企业/仓库/团队的成员加入申请列表  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_member_applications_with_http_info(target_type, target_id, enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str target_type: 申请加入的类型: Enterprise,Project,Group (required)
        :param int target_id: 申请加入的主体 ID (required)
        :param int enterprise_id: (required)
        :param str access_token: 用户授权码
        :param int page: 当前的页码
        :param int per_page: 每页的数量，最大为 100
        :return: list[MemberApplication]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['target_type', 'target_id', 'enterprise_id', 'access_token', 'page', 'per_page']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_enterprise_id_member_applications" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'target_type' is set
        if ('target_type' not in params or
                params['target_type'] is None):
            raise ValueError("Missing the required parameter `target_type` when calling `get_enterprise_id_member_applications`")  # noqa: E501
        # verify the required parameter 'target_id' is set
        if ('target_id' not in params or
                params['target_id'] is None):
            raise ValueError("Missing the required parameter `target_id` when calling `get_enterprise_id_member_applications`")  # noqa: E501
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `get_enterprise_id_member_applications`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))  # noqa: E501
        if 'target_type' in params:
            query_params.append(('target_type', params['target_type']))  # noqa: E501
        if 'target_id' in params:
            query_params.append(('target_id', params['target_id']))  # noqa: E501
        if 'page' in params:
            query_params.append(('page', params['page']))  # noqa: E501
        if 'per_page' in params:
            query_params.append(('per_page', params['per_page']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/member_applications', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='list[MemberApplication]',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def post_enterprise_id_member_applications_bulk_operate(self, body, enterprise_id, **kwargs):  # noqa: E501
        """批量处理申请记录  # noqa: E501

        批量处理申请记录  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.post_enterprise_id_member_applications_bulk_operate(body, enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param MemberApplicationsBulkOperateBody body: (required)
        :param int enterprise_id: (required)
        :return: BulkResponseWithKey
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.post_enterprise_id_member_applications_bulk_operate_with_http_info(body, enterprise_id, **kwargs)  # noqa: E501
        else:
            (data) = self.post_enterprise_id_member_applications_bulk_operate_with_http_info(body, enterprise_id, **kwargs)  # noqa: E501
            return data

    def post_enterprise_id_member_applications_bulk_operate_with_http_info(self, body, enterprise_id, **kwargs):  # noqa: E501
        """批量处理申请记录  # noqa: E501

        批量处理申请记录  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.post_enterprise_id_member_applications_bulk_operate_with_http_info(body, enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param MemberApplicationsBulkOperateBody body: (required)
        :param int enterprise_id: (required)
        :return: BulkResponseWithKey
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body', 'enterprise_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_enterprise_id_member_applications_bulk_operate" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `post_enterprise_id_member_applications_bulk_operate`")  # noqa: E501
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `post_enterprise_id_member_applications_bulk_operate`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/member_applications/bulk_operate', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='BulkResponseWithKey',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def post_enterprise_id_member_applications_set_force_verify(self, body, enterprise_id, **kwargs):  # noqa: E501
        """强制审核开关  # noqa: E501

        强制审核开关  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.post_enterprise_id_member_applications_set_force_verify(body, enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param MemberApplicationsSetForceVerifyBody body: (required)
        :param int enterprise_id: (required)
        :return: Enterprise
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.post_enterprise_id_member_applications_set_force_verify_with_http_info(body, enterprise_id, **kwargs)  # noqa: E501
        else:
            (data) = self.post_enterprise_id_member_applications_set_force_verify_with_http_info(body, enterprise_id, **kwargs)  # noqa: E501
            return data

    def post_enterprise_id_member_applications_set_force_verify_with_http_info(self, body, enterprise_id, **kwargs):  # noqa: E501
        """强制审核开关  # noqa: E501

        强制审核开关  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.post_enterprise_id_member_applications_set_force_verify_with_http_info(body, enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param MemberApplicationsSetForceVerifyBody body: (required)
        :param int enterprise_id: (required)
        :return: Enterprise
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body', 'enterprise_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method post_enterprise_id_member_applications_set_force_verify" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `post_enterprise_id_member_applications_set_force_verify`")  # noqa: E501
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `post_enterprise_id_member_applications_set_force_verify`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/member_applications/set_force_verify', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='Enterprise',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def put_enterprise_id_member_applications_member_application_id(self, body, member_application_id, enterprise_id, **kwargs):  # noqa: E501
        """处理申请记录  # noqa: E501

        处理申请记录  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.put_enterprise_id_member_applications_member_application_id(body, member_application_id, enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param MemberApplicationsMemberApplicationIdBody body: (required)
        :param int member_application_id: 申请记录id (required)
        :param int enterprise_id: (required)
        :return: MemberApplication
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.put_enterprise_id_member_applications_member_application_id_with_http_info(body, member_application_id, enterprise_id, **kwargs)  # noqa: E501
        else:
            (data) = self.put_enterprise_id_member_applications_member_application_id_with_http_info(body, member_application_id, enterprise_id, **kwargs)  # noqa: E501
            return data

    def put_enterprise_id_member_applications_member_application_id_with_http_info(self, body, member_application_id, enterprise_id, **kwargs):  # noqa: E501
        """处理申请记录  # noqa: E501

        处理申请记录  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.put_enterprise_id_member_applications_member_application_id_with_http_info(body, member_application_id, enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param MemberApplicationsMemberApplicationIdBody body: (required)
        :param int member_application_id: 申请记录id (required)
        :param int enterprise_id: (required)
        :return: MemberApplication
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body', 'member_application_id', 'enterprise_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_enterprise_id_member_applications_member_application_id" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `put_enterprise_id_member_applications_member_application_id`")  # noqa: E501
        # verify the required parameter 'member_application_id' is set
        if ('member_application_id' not in params or
                params['member_application_id'] is None):
            raise ValueError("Missing the required parameter `member_application_id` when calling `put_enterprise_id_member_applications_member_application_id`")  # noqa: E501
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `put_enterprise_id_member_applications_member_application_id`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'member_application_id' in params:
            path_params['member_application_id'] = params['member_application_id']  # noqa: E501
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/member_applications/{member_application_id}', 'PUT',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='MemberApplication',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from gitee_api.api_client import ApiClient


class StatisticsApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def get_enterprise_id_dashboard_statistics_overview(self, enterprise_id, **kwargs):  # noqa: E501
        """获取企业概览数据  # noqa: E501

        获取企业概览数据  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_dashboard_statistics_overview(enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_enterprise_id_dashboard_statistics_overview_with_http_info(enterprise_id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_enterprise_id_dashboard_statistics_overview_with_http_info(enterprise_id, **kwargs)  # noqa: E501
            return data

    def get_enterprise_id_dashboard_statistics_overview_with_http_info(self, enterprise_id, **kwargs):  # noqa: E501
        """获取企业概览数据  # noqa: E501

        获取企业概览数据  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_dashboard_statistics_overview_with_http_info(enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['enterprise_id', 'access_token']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_enterprise_id_dashboard_statistics_overview" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `get_enterprise_id_dashboard_statistics_overview`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/dashboard_statistics/overview', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_enterprise_id_dashboard_statistics_user_dashboard(self, enterprise_id, **kwargs):  # noqa: E501
        """获取当前用户的统计信息  # noqa: E501

        获取当前用户的统计信息  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_dashboard_statistics_user_dashboard(enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_enterprise_id_dashboard_statistics_user_dashboard_with_http_info(enterprise_id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_enterprise_id_dashboard_statistics_user_dashboard_with_http_info(enterprise_id, **kwargs)  # noqa: E501
            return data

    def get_enterprise_id_dashboard_statistics_user_dashboard_with_http_info(self, enterprise_id, **kwargs):  # noqa: E501
        """获取当前用户的统计信息  # noqa: E501

        获取当前用户的统计信息  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_dashboard_statistics_user_dashboard_with_http_info(enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str access_token: 用户授权码
        :return: None
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['enterprise_id', 'access_token']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_enterprise_id_dashboard_statistics_user_dashboard" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `get_enterprise_id_dashboard_statistics_user_dashboard`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/dashboard_statistics/user_dashboard', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type=None,  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_enterprise_id_statistics_enterprise_os_project_list(self, enterprise_id, **kwargs):  # noqa: E501
        """获取企业开源仓库详情列表  # noqa: E501

        获取企业开源仓库详情列表  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_statistics_enterprise_os_project_list(enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str access_token: 用户授权码
        :param int project_id: 仓库id
        :param str start_date: 查询的起始时间。(格式：yyyy-mm-dd)
        :param str end_date: 查询的结束时间。(格式：yyyy-mm-dd)
        :return: OsProjectsList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_enterprise_id_statistics_enterprise_os_project_list_with_http_info(enterprise_id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_enterprise_id_statistics_enterprise_os_project_list_with_http_info(enterprise_id, **kwargs)  # noqa: E501
            return data

    def get_enterprise_id_statistics_enterprise_os_project_list_with_http_info(self, enterprise_id, **kwargs):  # noqa: E501
        """获取企业开源仓库详情列表  # noqa: E501

        获取企业开源仓库详情列表  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_statistics_enterprise_os_project_list_with_http_info(enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str access_token: 用户授权码
        :param int project_id: 仓库id
        :param str start_date: 查询的起始时间。(格式：yyyy-mm-dd)
        :param str end_date: 查询的结束时间。(格式：yyyy-mm-dd)
        :return: OsProjectsList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['enterprise_id', 'access_token', 'project_id', 'start_date', 'end_date']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_enterprise_id_statistics_enterprise_os_project_list" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `get_enterprise_id_statistics_enterprise_os_project_list`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))  # noqa: E501
        if 'project_id' in params:
            query_params.append(('project_id', params['project_id']))  # noqa: E501
        if 'start_date' in params:
            query_params.append(('start_date', params['start_date']))  # noqa: E501
        if 'end_date' in params:
            query_params.append(('end_date', params['end_date']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/statistics/enterprise_os_project_list', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='OsProjectsList',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_enterprise_id_statistics_enterprise_program_list(self, enterprise_id, **kwargs):  # noqa: E501
        """获取企业项目的统计信息  # noqa: E501

        获取企业项目的统计信息  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_statistics_enterprise_program_list(enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str access_token: 用户授权码
        :param int program_id: 项目id
        :param str start_date: 查询的起始时间。(格式：yyyy-mm-dd)
        :param str end_date: 查询的结束时间。(格式：yyyy-mm-dd)
        :return: EnterpriseProgramsList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_enterprise_id_statistics_enterprise_program_list_with_http_info(enterprise_id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_enterprise_id_statistics_enterprise_program_list_with_http_info(enterprise_id, **kwargs)  # noqa: E501
            return data

    def get_enterprise_id_statistics_enterprise_program_list_with_http_info(self, enterprise_id, **kwargs):  # noqa: E501
        """获取企业项目的统计信息  # noqa: E501

        获取企业项目的统计信息  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_statistics_enterprise_program_list_with_http_info(enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str access_token: 用户授权码
        :param int program_id: 项目id
        :param str start_date: 查询的起始时间。(格式：yyyy-mm-dd)
        :param str end_date: 查询的结束时间。(格式：yyyy-mm-dd)
        :return: EnterpriseProgramsList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['enterprise_id', 'access_token', 'program_id', 'start_date', 'end_date']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_enterprise_id_statistics_enterprise_program_list" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `get_enterprise_id_statistics_enterprise_program_list`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))  # noqa: E501
        if 'program_id' in params:
            query_params.append(('program_id', params['program_id']))  # noqa: E501
        if 'start_date' in params:
            query_params.append(('start_date', params['start_date']))  # noqa: E501
        if 'end_date' in params:
            query_params.append(('end_date', params['end_date']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/statistics/enterprise_program_list', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='EnterpriseProgramsList',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_enterprise_id_statistics_enterprise_project_list(self, enterprise_id, **kwargs):  # noqa: E501
        """获取企业仓库的统计信息  # noqa: E501

        获取企业仓库的统计信息  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_statistics_enterprise_project_list(enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str access_token: 用户授权码
        :param int project_id: 仓库id
        :param str start_date: 查询的起始时间。(格式：yyyy-mm-dd)
        :param str end_date: 查询的结束时间。(格式：yyyy-mm-dd)
        :return: EnterpriseProjectsList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_enterprise_id_statistics_enterprise_project_list_with_http_info(enterprise_id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_enterprise_id_statistics_enterprise_project_list_with_http_info(enterprise_id, **kwargs)  # noqa: E501
            return data

    def get_enterprise_id_statistics_enterprise_project_list_with_http_info(self, enterprise_id, **kwargs):  # noqa: E501
        """获取企业仓库的统计信息  # noqa: E501

        获取企业仓库的统计信息  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_statistics_enterprise_project_list_with_http_info(enterprise_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str access_token: 用户授权码
        :param int project_id: 仓库id
        :param str start_date: 查询的起始时间。(格式：yyyy-mm-dd)
        :param str end_date: 查询的结束时间。(格式：yyyy-mm-dd)
        :return: EnterpriseProjectsList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['enterprise_id', 'access_token', 'project_id', 'start_date', 'end_date']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_enterprise_id_statistics_enterprise_project_list" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `get_enterprise_id_statistics_enterprise_project_list`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))  # noqa: E501
        if 'project_id' in params:
            query_params.append(('project_id', params['project_id']))  # noqa: E501
        if 'start_date' in params:
            query_params.append(('start_date', params['start_date']))  # noqa: E501
        if 'end_date' in params:
            query_params.append(('end_date', params['end_date']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/statistics/enterprise_project_list', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='EnterpriseProjectsList',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_enterprise_id_statistics_user_statistic(self, enterprise_id, user_id, start_date, end_date, **kwargs):  # noqa: E501
        """获取成员的统计信息  # noqa: E501

        获取成员的统计信息  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_statistics_user_statistic(enterprise_id, user_id, start_date, end_date, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str user_id: 查找指定用户的数据。（可多选，用逗号分割） (required)
        :param str start_date: 统计的起始时间。(格式：yyyy-mm-dd) (required)
        :param str end_date: 统计的结束时间。(格式：yyyy-mm-dd) (required)
        :param str access_token: 用户授权码
        :param int project_id: 仓库 id
        :return: list[UserStatistic]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_enterprise_id_statistics_user_statistic_with_http_info(enterprise_id, user_id, start_date, end_date, **kwargs)  # noqa: E501
        else:
            (data) = self.get_enterprise_id_statistics_user_statistic_with_http_info(enterprise_id, user_id, start_date, end_date, **kwargs)  # noqa: E501
            return data

    def get_enterprise_id_statistics_user_statistic_with_http_info(self, enterprise_id, user_id, start_date, end_date, **kwargs):  # noqa: E501
        """获取成员的统计信息  # noqa: E501

        获取成员的统计信息  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_enterprise_id_statistics_user_statistic_with_http_info(enterprise_id, user_id, start_date, end_date, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int enterprise_id: 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) (required)
        :param str user_id: 查找指定用户的数据。（可多选，用逗号分割） (required)
        :param str start_date: 统计的起始时间。(格式：yyyy-mm-dd) (required)
        :param str end_date: 统计的结束时间。(格式：yyyy-mm-dd) (required)
        :param str access_token: 用户授权码
        :param int project_id: 仓库 id
        :return: list[UserStatistic]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['enterprise_id', 'user_id', 'start_date', 'end_date', 'access_token', 'project_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_enterprise_id_statistics_user_statistic" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'enterprise_id' is set
        if ('enterprise_id' not in params or
                params['enterprise_id'] is None):
            raise ValueError("Missing the required parameter `enterprise_id` when calling `get_enterprise_id_statistics_user_statistic`")  # noqa: E501
        # verify the required parameter 'user_id' is set
        if ('user_id' not in params or
                params['user_id'] is None):
            raise ValueError("Missing the required parameter `user_id` when calling `get_enterprise_id_statistics_user_statistic`")  # noqa: E501
        # verify the required parameter 'start_date' is set
        if ('start_date' not in params or
                params['start_date'] is None):
            raise ValueError("Missing the required parameter `start_date` when calling `get_enterprise_id_statistics_user_statistic`")  # noqa: E501
        # verify the required parameter 'end_date' is set
        if ('end_date' not in params or
                params['end_date'] is None):
            raise ValueError("Missing the required parameter `end_date` when calling `get_enterprise_id_statistics_user_statistic`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'enterprise_id' in params:
            path_params['enterprise_id'] = params['enterprise_id']  # noqa: E501

        query_params = []
        if 'access_token' in params:
            query_params.append(('access_token', params['access_token']))  # noqa: E501
        if 'user_id' in params:
            query_params.append(('user_id', params['user_id']))  # noqa: E501
        if 'project_id' in params:
            query_params.append(('project_id', params['project_id']))  # noqa: E501
        if 'start_date' in params:
            query_params.append(('start_date', params['start_date']))  # noqa: E501
        if 'end_date' in params:
            query_params.append(('end_date', params['end_date']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/{enterprise_id}/statistics/user_statistic', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='list[UserStatistic]',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

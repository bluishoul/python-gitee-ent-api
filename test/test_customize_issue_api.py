# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import gitee_api
from gitee_api.api.customize_issue_api import CustomizeIssueApi  # noqa: E501
from gitee_api.rest import ApiException


class TestCustomizeIssueApi(unittest.TestCase):
    """CustomizeIssueApi unit test stubs"""

    def setUp(self):
        self.api = CustomizeIssueApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_enterprise_id_customize_list(self):
        """Test case for get_enterprise_id_customize_list

        获取管理界面的字段列表  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()

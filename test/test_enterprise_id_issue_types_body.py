# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import gitee_api
from gitee_api.models.enterprise_id_issue_types_body import EnterpriseIdIssueTypesBody  # noqa: E501
from gitee_api.rest import ApiException


class TestEnterpriseIdIssueTypesBody(unittest.TestCase):
    """EnterpriseIdIssueTypesBody unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testEnterpriseIdIssueTypesBody(self):
        """Test EnterpriseIdIssueTypesBody"""
        # FIXME: construct object with mandatory attributes with example values
        # model = gitee_api.models.enterprise_id_issue_types_body.EnterpriseIdIssueTypesBody()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

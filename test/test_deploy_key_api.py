# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import gitee_api
from gitee_api.api.deploy_key_api import DeployKeyApi  # noqa: E501
from gitee_api.rest import ApiException


class TestDeployKeyApi(unittest.TestCase):
    """DeployKeyApi unit test stubs"""

    def setUp(self):
        self.api = DeployKeyApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_delete_enterprise_id_deploy_keys_deploy_key_id_projects(self):
        """Test case for delete_enterprise_id_deploy_keys_deploy_key_id_projects

        部署公钥移除仓库  # noqa: E501
        """
        pass

    def test_get_enterprise_id_deploy_keys(self):
        """Test case for get_enterprise_id_deploy_keys

        查看企业部署公钥  # noqa: E501
        """
        pass

    def test_get_enterprise_id_deploy_keys_deploy_key_id_projects(self):
        """Test case for get_enterprise_id_deploy_keys_deploy_key_id_projects

        查看公钥部署的仓库  # noqa: E501
        """
        pass

    def test_post_enterprise_id_deploy_keys(self):
        """Test case for post_enterprise_id_deploy_keys

        添加部署公钥  # noqa: E501
        """
        pass

    def test_post_enterprise_id_deploy_keys_deploy_key_id_projects(self):
        """Test case for post_enterprise_id_deploy_keys_deploy_key_id_projects

        部署公钥添加仓库  # noqa: E501
        """
        pass

    def test_put_enterprise_id_deploy_keys_deploy_key_id(self):
        """Test case for put_enterprise_id_deploy_keys_deploy_key_id

        修改部署公钥  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()

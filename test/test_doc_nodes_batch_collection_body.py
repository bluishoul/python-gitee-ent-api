# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import gitee_api
from gitee_api.models.doc_nodes_batch_collection_body import DocNodesBatchCollectionBody  # noqa: E501
from gitee_api.rest import ApiException


class TestDocNodesBatchCollectionBody(unittest.TestCase):
    """DocNodesBatchCollectionBody unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testDocNodesBatchCollectionBody(self):
        """Test DocNodesBatchCollectionBody"""
        # FIXME: construct object with mandatory attributes with example values
        # model = gitee_api.models.doc_nodes_batch_collection_body.DocNodesBatchCollectionBody()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import gitee_api
from gitee_api.api.git_data_api import GitDataApi  # noqa: E501
from gitee_api.rest import ApiException


class TestGitDataApi(unittest.TestCase):
    """GitDataApi unit test stubs"""

    def setUp(self):
        self.api = GitDataApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_delete_enterprise_id_projects_project_id_releases_release_id(self):
        """Test case for delete_enterprise_id_projects_project_id_releases_release_id

        删除发行版  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects(self):
        """Test case for get_enterprise_id_projects

        获取授权用户参与的仓库列表  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_for_pull_request(self):
        """Test case for get_enterprise_id_projects_for_pull_request

        获取当前用户有权限提pr的仓库  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_can_pull(self):
        """Test case for get_enterprise_id_projects_project_id_can_pull

        获取可创建 Pull Request 的仓库  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_check_releases(self):
        """Test case for get_enterprise_id_projects_project_id_check_releases

        检查发行版是否存在  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_contributors(self):
        """Test case for get_enterprise_id_projects_project_id_contributors

        获取仓库贡献者列表  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_events(self):
        """Test case for get_enterprise_id_projects_project_id_events

        获取仓库动态  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_issues(self):
        """Test case for get_enterprise_id_projects_project_id_issues

        获取仓库的任务列表  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_operate_auths(self):
        """Test case for get_enterprise_id_projects_project_id_operate_auths

        获取仓库的操作权限  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_programs(self):
        """Test case for get_enterprise_id_projects_project_id_programs

        获取仓库的关联项目列表  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_pull_requests(self):
        """Test case for get_enterprise_id_projects_project_id_pull_requests

        获取仓库的 Pull Request 列表  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_releases(self):
        """Test case for get_enterprise_id_projects_project_id_releases

        查看发行版列表  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_releases_tag_version(self):
        """Test case for get_enterprise_id_projects_project_id_releases_tag_version

        查看发行版详情  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_settings(self):
        """Test case for get_enterprise_id_projects_project_id_settings

        获取仓库基本设置  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_users(self):
        """Test case for get_enterprise_id_projects_project_id_users

        获取仓库的成员列表  # noqa: E501
        """
        pass

    def test_post_enterprise_id_projects(self):
        """Test case for post_enterprise_id_projects

        新建仓库  # noqa: E501
        """
        pass

    def test_post_enterprise_id_projects_check_project_can_import(self):
        """Test case for post_enterprise_id_projects_check_project_can_import

        新建仓库-导入仓库参数是否有效  # noqa: E501
        """
        pass

    def test_post_enterprise_id_projects_check_project_name(self):
        """Test case for post_enterprise_id_projects_check_project_name

        新建仓库-仓库名/路径是否已经存在  # noqa: E501
        """
        pass

    def test_post_enterprise_id_projects_project_id_releases(self):
        """Test case for post_enterprise_id_projects_project_id_releases

        新建发行版  # noqa: E501
        """
        pass

    def test_post_enterprise_id_projects_transfer_code(self):
        """Test case for post_enterprise_id_projects_transfer_code

        获取仓库转入-转移码  # noqa: E501
        """
        pass

    def test_put_enterprise_id_projects_project_id(self):
        """Test case for put_enterprise_id_projects_project_id

        更新仓库设置  # noqa: E501
        """
        pass

    def test_put_enterprise_id_projects_project_id_releases_release_id(self):
        """Test case for put_enterprise_id_projects_project_id_releases_release_id

        编辑发行版  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()

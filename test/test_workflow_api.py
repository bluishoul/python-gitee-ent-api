# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import gitee_api
from gitee_api.api.workflow_api import WorkflowApi  # noqa: E501
from gitee_api.rest import ApiException


class TestWorkflowApi(unittest.TestCase):
    """WorkflowApi unit test stubs"""

    def setUp(self):
        self.api = WorkflowApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_enterprise_id_workflow_list(self):
        """Test case for get_enterprise_id_workflow_list

        获取任务类型的工作流列表  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()

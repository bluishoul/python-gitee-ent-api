# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import gitee_api
from gitee_api.models.enterprise_projects_list import EnterpriseProjectsList  # noqa: E501
from gitee_api.rest import ApiException


class TestEnterpriseProjectsList(unittest.TestCase):
    """EnterpriseProjectsList unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testEnterpriseProjectsList(self):
        """Test EnterpriseProjectsList"""
        # FIXME: construct object with mandatory attributes with example values
        # model = gitee_api.models.enterprise_projects_list.EnterpriseProjectsList()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()

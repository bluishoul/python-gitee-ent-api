# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import gitee_api
from gitee_api.api.programs_api import ProgramsApi  # noqa: E501
from gitee_api.rest import ApiException


class TestProgramsApi(unittest.TestCase):
    """ProgramsApi unit test stubs"""

    def setUp(self):
        self.api = ProgramsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_delete_enterprise_id_programs_program_id(self):
        """Test case for delete_enterprise_id_programs_program_id

        删除项目  # noqa: E501
        """
        pass

    def test_delete_enterprise_id_programs_program_id_groups_group_id(self):
        """Test case for delete_enterprise_id_programs_program_id_groups_group_id

        移出项目下的团队  # noqa: E501
        """
        pass

    def test_delete_enterprise_id_programs_program_id_members_member_user_id(self):
        """Test case for delete_enterprise_id_programs_program_id_members_member_user_id

        移出项目下成员  # noqa: E501
        """
        pass

    def test_delete_enterprise_id_programs_program_id_projects_project_id(self):
        """Test case for delete_enterprise_id_programs_program_id_projects_project_id

        移出项目下仓库  # noqa: E501
        """
        pass

    def test_get_enterprise_id_programs(self):
        """Test case for get_enterprise_id_programs

        获取项目列表  # noqa: E501
        """
        pass

    def test_get_enterprise_id_programs_program_id(self):
        """Test case for get_enterprise_id_programs_program_id

        获取项目详情  # noqa: E501
        """
        pass

    def test_get_enterprise_id_programs_program_id_events(self):
        """Test case for get_enterprise_id_programs_program_id_events

        获取项目下动态列表  # noqa: E501
        """
        pass

    def test_get_enterprise_id_programs_program_id_groups(self):
        """Test case for get_enterprise_id_programs_program_id_groups

        获取项目下的团队  # noqa: E501
        """
        pass

    def test_get_enterprise_id_programs_program_id_issues(self):
        """Test case for get_enterprise_id_programs_program_id_issues

        获取项目下的任务列表  # noqa: E501
        """
        pass

    def test_get_enterprise_id_programs_program_id_members(self):
        """Test case for get_enterprise_id_programs_program_id_members

        获取项目下的成员列表  # noqa: E501
        """
        pass

    def test_get_enterprise_id_programs_program_id_operate_auths(self):
        """Test case for get_enterprise_id_programs_program_id_operate_auths

        获取项目的操作权限  # noqa: E501
        """
        pass

    def test_get_enterprise_id_programs_program_id_projects(self):
        """Test case for get_enterprise_id_programs_program_id_projects

        获取项目下的仓库列表  # noqa: E501
        """
        pass

    def test_get_enterprise_id_programs_program_id_pull_requests(self):
        """Test case for get_enterprise_id_programs_program_id_pull_requests

        获取项目下的 Pull Request  # noqa: E501
        """
        pass

    def test_get_enterprise_id_programs_unset(self):
        """Test case for get_enterprise_id_programs_unset

        获取未立项项目  # noqa: E501
        """
        pass

    def test_post_enterprise_id_programs(self):
        """Test case for post_enterprise_id_programs

        新建项目  # noqa: E501
        """
        pass

    def test_post_enterprise_id_programs_program_id_members(self):
        """Test case for post_enterprise_id_programs_program_id_members

        添加企业成员或团队进项目  # noqa: E501
        """
        pass

    def test_post_enterprise_id_programs_program_id_projects(self):
        """Test case for post_enterprise_id_programs_program_id_projects

        项目关联仓库  # noqa: E501
        """
        pass

    def test_put_enterprise_id_programs_program_id(self):
        """Test case for put_enterprise_id_programs_program_id

        更新项目  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()

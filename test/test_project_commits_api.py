# coding: utf-8

"""
    Gitee Open API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.1.306
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import gitee_api
from gitee_api.api.project_commits_api import ProjectCommitsApi  # noqa: E501
from gitee_api.rest import ApiException


class TestProjectCommitsApi(unittest.TestCase):
    """ProjectCommitsApi unit test stubs"""

    def setUp(self):
        self.api = ProjectCommitsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_enterprise_id_projects_project_id_commit_commit_id(self):
        """Test case for get_enterprise_id_projects_project_id_commit_commit_id

        commit详情  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_commit_commit_id_branches(self):
        """Test case for get_enterprise_id_projects_project_id_commit_commit_id_branches

        获取commit的分支和tag  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_commit_commit_id_diff_for_path(self):
        """Test case for get_enterprise_id_projects_project_id_commit_commit_id_diff_for_path

        获取 commit 详情中差异较大的文件内容  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_commit_commit_id_notes(self):
        """Test case for get_enterprise_id_projects_project_id_commit_commit_id_notes

        Commit 评论列表  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_commits_ref(self):
        """Test case for get_enterprise_id_projects_project_id_commits_ref

        commits列表  # noqa: E501
        """
        pass

    def test_get_enterprise_id_projects_project_id_compare_from___to(self):
        """Test case for get_enterprise_id_projects_project_id_compare_from___to

        对比commit  # noqa: E501
        """
        pass

    def test_post_enterprise_id_projects_project_id_commit_commit_id_notes(self):
        """Test case for post_enterprise_id_projects_project_id_commit_commit_id_notes

        评论 Commit  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()

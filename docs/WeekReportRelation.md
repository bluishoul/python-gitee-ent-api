# WeekReportRelation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issues** | [**IssueMain**](IssueMain.md) |  | [optional] 
**pull_requests** | [**PullRequest**](PullRequest.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


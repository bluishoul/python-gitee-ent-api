# OsProjectsList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**projects** | [**OsProject**](OsProject.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


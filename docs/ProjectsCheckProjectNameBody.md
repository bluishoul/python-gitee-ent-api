# ProjectsCheckProjectNameBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**name** | **str** | 名称。二选一检验某一项 | [optional] 
**path** | **str** | 路径。二选一检验某一项 | [optional] 
**namespace_path** | **str** | 归属路径 | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


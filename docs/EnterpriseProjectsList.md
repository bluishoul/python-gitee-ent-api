# EnterpriseProjectsList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**projects** | [**EnterpriseProject**](EnterpriseProject.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# gitee_api.AdminLogsStatisticApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_enterprise_id_log_statistics_deploy_key_logs**](AdminLogsStatisticApi.md#get_enterprise_id_log_statistics_deploy_key_logs) | **GET** /{enterprise_id}/log_statistics/deploy_key_logs | 企业部署公钥管理日志
[**get_enterprise_id_log_statistics_doc_node_logs**](AdminLogsStatisticApi.md#get_enterprise_id_log_statistics_doc_node_logs) | **GET** /{enterprise_id}/log_statistics/doc_node_logs | 企业文档管理日志
[**get_enterprise_id_log_statistics_enterprise_role_logs**](AdminLogsStatisticApi.md#get_enterprise_id_log_statistics_enterprise_role_logs) | **GET** /{enterprise_id}/log_statistics/enterprise_role_logs | 企业角色管理日志
[**get_enterprise_id_log_statistics_groups_log**](AdminLogsStatisticApi.md#get_enterprise_id_log_statistics_groups_log) | **GET** /{enterprise_id}/log_statistics/groups_log | 团队管理日志
[**get_enterprise_id_log_statistics_members_log**](AdminLogsStatisticApi.md#get_enterprise_id_log_statistics_members_log) | **GET** /{enterprise_id}/log_statistics/members_log | 成员管理日志
[**get_enterprise_id_log_statistics_programs_log**](AdminLogsStatisticApi.md#get_enterprise_id_log_statistics_programs_log) | **GET** /{enterprise_id}/log_statistics/programs_log | 项目管理日志
[**get_enterprise_id_log_statistics_projects_access_log**](AdminLogsStatisticApi.md#get_enterprise_id_log_statistics_projects_access_log) | **GET** /{enterprise_id}/log_statistics/projects_access_log | 仓库代码日志
[**get_enterprise_id_log_statistics_projects_log**](AdminLogsStatisticApi.md#get_enterprise_id_log_statistics_projects_log) | **GET** /{enterprise_id}/log_statistics/projects_log | 仓库管理日志
[**get_enterprise_id_log_statistics_security_setting_log**](AdminLogsStatisticApi.md#get_enterprise_id_log_statistics_security_setting_log) | **GET** /{enterprise_id}/log_statistics/security_setting_log | 安全与告警管理日志

# **get_enterprise_id_log_statistics_deploy_key_logs**
> list[DeployKeyLog] get_enterprise_id_log_statistics_deploy_key_logs(enterprise_id, access_token=access_token, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)

企业部署公钥管理日志

企业部署公钥管理日志

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.AdminLogsStatisticApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
member = 'member_example' # str | 成员username (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 企业部署公钥管理日志
    api_response = api_instance.get_enterprise_id_log_statistics_deploy_key_logs(enterprise_id, access_token=access_token, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AdminLogsStatisticApi->get_enterprise_id_log_statistics_deploy_key_logs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **member** | **str**| 成员username | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[DeployKeyLog]**](DeployKeyLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_log_statistics_doc_node_logs**
> list[DocNodeLog] get_enterprise_id_log_statistics_doc_node_logs(enterprise_id, access_token=access_token, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)

企业文档管理日志

企业文档管理日志

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.AdminLogsStatisticApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
member = 'member_example' # str | 成员username (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 企业文档管理日志
    api_response = api_instance.get_enterprise_id_log_statistics_doc_node_logs(enterprise_id, access_token=access_token, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AdminLogsStatisticApi->get_enterprise_id_log_statistics_doc_node_logs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **member** | **str**| 成员username | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[DocNodeLog]**](DocNodeLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_log_statistics_enterprise_role_logs**
> list[EnterpriseRoleLog] get_enterprise_id_log_statistics_enterprise_role_logs(enterprise_id, access_token=access_token, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)

企业角色管理日志

企业角色管理日志

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.AdminLogsStatisticApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
member = 'member_example' # str | 成员username (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 企业角色管理日志
    api_response = api_instance.get_enterprise_id_log_statistics_enterprise_role_logs(enterprise_id, access_token=access_token, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AdminLogsStatisticApi->get_enterprise_id_log_statistics_enterprise_role_logs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **member** | **str**| 成员username | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[EnterpriseRoleLog]**](EnterpriseRoleLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_log_statistics_groups_log**
> list[GroupManageLog] get_enterprise_id_log_statistics_groups_log(enterprise_id, access_token=access_token, group_name=group_name, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)

团队管理日志

团队管理日志

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.AdminLogsStatisticApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
group_name = 'group_name_example' # str | 团队名称 (optional)
member = 'member_example' # str | 成员username (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 团队管理日志
    api_response = api_instance.get_enterprise_id_log_statistics_groups_log(enterprise_id, access_token=access_token, group_name=group_name, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AdminLogsStatisticApi->get_enterprise_id_log_statistics_groups_log: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **group_name** | **str**| 团队名称 | [optional] 
 **member** | **str**| 成员username | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[GroupManageLog]**](GroupManageLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_log_statistics_members_log**
> list[MemberManageLog] get_enterprise_id_log_statistics_members_log(enterprise_id, access_token=access_token, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)

成员管理日志

成员管理日志

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.AdminLogsStatisticApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
member = 'member_example' # str | 成员username (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 成员管理日志
    api_response = api_instance.get_enterprise_id_log_statistics_members_log(enterprise_id, access_token=access_token, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AdminLogsStatisticApi->get_enterprise_id_log_statistics_members_log: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **member** | **str**| 成员username | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[MemberManageLog]**](MemberManageLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_log_statistics_programs_log**
> list[ProgramManageLog] get_enterprise_id_log_statistics_programs_log(enterprise_id, access_token=access_token, member=member, program_name=program_name, start_date=start_date, end_date=end_date, page=page, per_page=per_page)

项目管理日志

项目管理日志

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.AdminLogsStatisticApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
member = 'member_example' # str | 成员username (optional)
program_name = 'program_name_example' # str | 项目名称 (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 项目管理日志
    api_response = api_instance.get_enterprise_id_log_statistics_programs_log(enterprise_id, access_token=access_token, member=member, program_name=program_name, start_date=start_date, end_date=end_date, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AdminLogsStatisticApi->get_enterprise_id_log_statistics_programs_log: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **member** | **str**| 成员username | [optional] 
 **program_name** | **str**| 项目名称 | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[ProgramManageLog]**](ProgramManageLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_log_statistics_projects_access_log**
> list[ProjectAccessLog] get_enterprise_id_log_statistics_projects_access_log(enterprise_id, access_token=access_token, group_path=group_path, project_name=project_name, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)

仓库代码日志

仓库代码日志

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.AdminLogsStatisticApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
group_path = 'group_path_example' # str | 团队path (optional)
project_name = 'project_name_example' # str | 仓库path_with_namespace: namespace.path/project.path (optional)
member = 'member_example' # str | 成员username (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 仓库代码日志
    api_response = api_instance.get_enterprise_id_log_statistics_projects_access_log(enterprise_id, access_token=access_token, group_path=group_path, project_name=project_name, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AdminLogsStatisticApi->get_enterprise_id_log_statistics_projects_access_log: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **group_path** | **str**| 团队path | [optional] 
 **project_name** | **str**| 仓库path_with_namespace: namespace.path/project.path | [optional] 
 **member** | **str**| 成员username | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[ProjectAccessLog]**](ProjectAccessLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_log_statistics_projects_log**
> list[ProjectManageLog] get_enterprise_id_log_statistics_projects_log(enterprise_id, access_token=access_token, project_name=project_name, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)

仓库管理日志

仓库管理日志

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.AdminLogsStatisticApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
project_name = 'project_name_example' # str | 仓库path_with_namespace: namespace.path/project.path (optional)
member = 'member_example' # str | 成员username (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 仓库管理日志
    api_response = api_instance.get_enterprise_id_log_statistics_projects_log(enterprise_id, access_token=access_token, project_name=project_name, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AdminLogsStatisticApi->get_enterprise_id_log_statistics_projects_log: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **project_name** | **str**| 仓库path_with_namespace: namespace.path/project.path | [optional] 
 **member** | **str**| 成员username | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[ProjectManageLog]**](ProjectManageLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_log_statistics_security_setting_log**
> list[SecuritySettingLog] get_enterprise_id_log_statistics_security_setting_log(enterprise_id, access_token=access_token, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)

安全与告警管理日志

安全与告警管理日志

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.AdminLogsStatisticApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
member = 'member_example' # str | 成员username (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 安全与告警管理日志
    api_response = api_instance.get_enterprise_id_log_statistics_security_setting_log(enterprise_id, access_token=access_token, member=member, start_date=start_date, end_date=end_date, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AdminLogsStatisticApi->get_enterprise_id_log_statistics_security_setting_log: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **member** | **str**| 成员username | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[SecuritySettingLog]**](SecuritySettingLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


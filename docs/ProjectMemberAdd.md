# ProjectMemberAdd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 成员 id | [optional] 
**email** | **str** | 邮箱 | [optional] 
**name** | **str** | 成员名称 | [optional] 
**username** | **str** | 成员命名空间 | [optional] 
**msg** | **str** | 返回信息 | [optional] 
**success** | **bool** | 是否成功 | [optional] 
**no_register** | **bool** | 是否是注册用户 | [optional] 
**role_name** | **str** | 角色名称 | [optional] 
**access** | **str** | 角色级别 | [optional] 
**state** | **str** | 角色状态 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


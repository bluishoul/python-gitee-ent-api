# gitee_api.KooderApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_enterprise_id_kooder_code**](KooderApi.md#get_enterprise_id_kooder_code) | **GET** /{enterprise_id}/kooder/code | code 查询
[**get_enterprise_id_kooder_docs**](KooderApi.md#get_enterprise_id_kooder_docs) | **GET** /{enterprise_id}/kooder/docs | 知识库查询
[**get_enterprise_id_kooder_issue**](KooderApi.md#get_enterprise_id_kooder_issue) | **GET** /{enterprise_id}/kooder/issue | issue查询
[**get_enterprise_id_kooder_project**](KooderApi.md#get_enterprise_id_kooder_project) | **GET** /{enterprise_id}/kooder/project | 仓库查询

# **get_enterprise_id_kooder_code**
> get_enterprise_id_kooder_code(enterprise_id, search, access_token=access_token, page=page, per_page=per_page, language=language, project_id=project_id, visibility=visibility, scope=scope, scene=scene)

code 查询

code 查询

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.KooderApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
search = 'search_example' # str | 搜索内容
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)
language = 'language_example' # str | 仓库语言 (optional)
project_id = 'project_id_example' # str | 仓库id，用英文逗号分隔 (optional)
visibility = 'visibility_example' # str | 仓库开源属性，公开：0，私有：1，内源：2，可多选，用英文逗号分隔 (optional)
scope = 'scope_example' # str | 检索范围，null：默认，仓库文件名称、仓库文件内容， location：仓库文件名称 (optional)
scene = 56 # int | 触发场景，用户做分析统计 (optional)

try:
    # code 查询
    api_instance.get_enterprise_id_kooder_code(enterprise_id, search, access_token=access_token, page=page, per_page=per_page, language=language, project_id=project_id, visibility=visibility, scope=scope, scene=scene)
except ApiException as e:
    print("Exception when calling KooderApi->get_enterprise_id_kooder_code: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **search** | **str**| 搜索内容 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 
 **language** | **str**| 仓库语言 | [optional] 
 **project_id** | **str**| 仓库id，用英文逗号分隔 | [optional] 
 **visibility** | **str**| 仓库开源属性，公开：0，私有：1，内源：2，可多选，用英文逗号分隔 | [optional] 
 **scope** | **str**| 检索范围，null：默认，仓库文件名称、仓库文件内容， location：仓库文件名称 | [optional] 
 **scene** | **int**| 触发场景，用户做分析统计 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_kooder_docs**
> get_enterprise_id_kooder_docs(enterprise_id, search, access_token=access_token, page=page, per_page=per_page, program_id=program_id, scene=scene)

知识库查询

知识库查询

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.KooderApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
search = 'search_example' # str | 搜索内容
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)
program_id = 'program_id_example' # str | 项目id，用英文逗号分隔 (optional)
scene = 56 # int | 触发场景，用户做分析统计 (optional)

try:
    # 知识库查询
    api_instance.get_enterprise_id_kooder_docs(enterprise_id, search, access_token=access_token, page=page, per_page=per_page, program_id=program_id, scene=scene)
except ApiException as e:
    print("Exception when calling KooderApi->get_enterprise_id_kooder_docs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **search** | **str**| 搜索内容 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 
 **program_id** | **str**| 项目id，用英文逗号分隔 | [optional] 
 **scene** | **int**| 触发场景，用户做分析统计 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_kooder_issue**
> Issue get_enterprise_id_kooder_issue(enterprise_id, search, access_token=access_token, page=page, per_page=per_page, issue_type_ids=issue_type_ids, project_id=project_id, program_id=program_id, scope=scope, scene=scene)

issue查询

issue查询

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.KooderApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
search = 'search_example' # str | 搜索内容
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)
issue_type_ids = 'issue_type_ids_example' # str | 任务类型id，用英文逗号分隔 (optional)
project_id = 'project_id_example' # str | 仓库id，用英文逗号分隔 (optional)
program_id = 'program_id_example' # str | 项目id，用英文逗号分隔 (optional)
scope = 'scope_example' # str | 检索范围，null：默认，任务标题、任务描述， title：任务标题 (optional)
scene = 56 # int | 触发场景，用户做分析统计 (optional)

try:
    # issue查询
    api_response = api_instance.get_enterprise_id_kooder_issue(enterprise_id, search, access_token=access_token, page=page, per_page=per_page, issue_type_ids=issue_type_ids, project_id=project_id, program_id=program_id, scope=scope, scene=scene)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling KooderApi->get_enterprise_id_kooder_issue: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **search** | **str**| 搜索内容 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 
 **issue_type_ids** | **str**| 任务类型id，用英文逗号分隔 | [optional] 
 **project_id** | **str**| 仓库id，用英文逗号分隔 | [optional] 
 **program_id** | **str**| 项目id，用英文逗号分隔 | [optional] 
 **scope** | **str**| 检索范围，null：默认，任务标题、任务描述， title：任务标题 | [optional] 
 **scene** | **int**| 触发场景，用户做分析统计 | [optional] 

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_kooder_project**
> Project get_enterprise_id_kooder_project(enterprise_id, search, access_token=access_token, page=page, per_page=per_page, language=language, project_ids=project_ids, namespace_ids=namespace_ids, visibility=visibility, scope=scope, scene=scene)

仓库查询

仓库查询

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.KooderApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
search = 'search_example' # str | 搜索内容
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)
language = 'language_example' # str | 仓库语言 (optional)
project_ids = 'project_ids_example' # str | 仓库id，用英文逗号分隔 (optional)
namespace_ids = 'namespace_ids_example' # str | 仓库命名空间id，用英文逗号分隔 (optional)
visibility = 'visibility_example' # str | 仓库开源属性，公开：0，私有：1，内源：2，可多选，用英文逗号分隔 (optional)
scope = 'scope_example' # str | 检索范围，null：默认，仓库名、仓库描述， human_name：仓库名称 (optional)
scene = 56 # int | 触发场景，用户做分析统计 (optional)

try:
    # 仓库查询
    api_response = api_instance.get_enterprise_id_kooder_project(enterprise_id, search, access_token=access_token, page=page, per_page=per_page, language=language, project_ids=project_ids, namespace_ids=namespace_ids, visibility=visibility, scope=scope, scene=scene)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling KooderApi->get_enterprise_id_kooder_project: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **search** | **str**| 搜索内容 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 
 **language** | **str**| 仓库语言 | [optional] 
 **project_ids** | **str**| 仓库id，用英文逗号分隔 | [optional] 
 **namespace_ids** | **str**| 仓库命名空间id，用英文逗号分隔 | [optional] 
 **visibility** | **str**| 仓库开源属性，公开：0，私有：1，内源：2，可多选，用英文逗号分隔 | [optional] 
 **scope** | **str**| 检索范围，null：默认，仓库名、仓库描述， human_name：仓库名称 | [optional] 
 **scene** | **int**| 触发场景，用户做分析统计 | [optional] 

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


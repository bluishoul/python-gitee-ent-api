# gitee_api.ProjectEnvApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_projects_project_id_env_variables_env_variable_id**](ProjectEnvApi.md#delete_enterprise_id_projects_project_id_env_variables_env_variable_id) | **DELETE** /{enterprise_id}/projects/{project_id}/env_variables/{env_variable_id} | 删除环境变量
[**get_enterprise_id_projects_project_id_env_variables**](ProjectEnvApi.md#get_enterprise_id_projects_project_id_env_variables) | **GET** /{enterprise_id}/projects/{project_id}/env_variables | 环境变量列表
[**post_enterprise_id_projects_project_id_env_variables**](ProjectEnvApi.md#post_enterprise_id_projects_project_id_env_variables) | **POST** /{enterprise_id}/projects/{project_id}/env_variables | 新建环境变量
[**put_enterprise_id_projects_project_id_env_variables_env_variable_id**](ProjectEnvApi.md#put_enterprise_id_projects_project_id_env_variables_env_variable_id) | **PUT** /{enterprise_id}/projects/{project_id}/env_variables/{env_variable_id} | 修改环境变量

# **delete_enterprise_id_projects_project_id_env_variables_env_variable_id**
> delete_enterprise_id_projects_project_id_env_variables_env_variable_id(enterprise_id, project_id, env_variable_id, access_token=access_token, qt=qt)

删除环境变量

删除环境变量

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectEnvApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
env_variable_id = 56 # int | 变量id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 删除环境变量
    api_instance.delete_enterprise_id_projects_project_id_env_variables_env_variable_id(enterprise_id, project_id, env_variable_id, access_token=access_token, qt=qt)
except ApiException as e:
    print("Exception when calling ProjectEnvApi->delete_enterprise_id_projects_project_id_env_variables_env_variable_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **env_variable_id** | **int**| 变量id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_env_variables**
> EnvVariable get_enterprise_id_projects_project_id_env_variables(enterprise_id, project_id, access_token=access_token, qt=qt, query=query, read_only=read_only, sort=sort, direction=direction, page=page, per_page=per_page)

环境变量列表

环境变量列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectEnvApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
query = 'query_example' # str | 搜索字符串 (optional)
read_only = 56 # int | 1为只读，其他情况为全部 (optional)
sort = 'sort_example' # str | 排序字段(created_at: 创建时间 updated_at: 更新时间 name: 名称) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 环境变量列表
    api_response = api_instance.get_enterprise_id_projects_project_id_env_variables(enterprise_id, project_id, access_token=access_token, qt=qt, query=query, read_only=read_only, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectEnvApi->get_enterprise_id_projects_project_id_env_variables: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **query** | **str**| 搜索字符串 | [optional] 
 **read_only** | **int**| 1为只读，其他情况为全部 | [optional] 
 **sort** | **str**| 排序字段(created_at: 创建时间 updated_at: 更新时间 name: 名称) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**EnvVariable**](EnvVariable.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_env_variables**
> EnvVariable post_enterprise_id_projects_project_id_env_variables(body, enterprise_id, project_id)

新建环境变量

新建环境变量

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectEnvApi()
body = gitee_api.ProjectIdEnvVariablesBody() # ProjectIdEnvVariablesBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path

try:
    # 新建环境变量
    api_response = api_instance.post_enterprise_id_projects_project_id_env_variables(body, enterprise_id, project_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectEnvApi->post_enterprise_id_projects_project_id_env_variables: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdEnvVariablesBody**](ProjectIdEnvVariablesBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 

### Return type

[**EnvVariable**](EnvVariable.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_projects_project_id_env_variables_env_variable_id**
> EnvVariable put_enterprise_id_projects_project_id_env_variables_env_variable_id(enterprise_id, project_id, env_variable_id, body=body)

修改环境变量

修改环境变量

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectEnvApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
env_variable_id = 56 # int | 变量id
body = gitee_api.EnvVariablesEnvVariableIdBody() # EnvVariablesEnvVariableIdBody |  (optional)

try:
    # 修改环境变量
    api_response = api_instance.put_enterprise_id_projects_project_id_env_variables_env_variable_id(enterprise_id, project_id, env_variable_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectEnvApi->put_enterprise_id_projects_project_id_env_variables_env_variable_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **env_variable_id** | **int**| 变量id | 
 **body** | [**EnvVariablesEnvVariableIdBody**](EnvVariablesEnvVariableIdBody.md)|  | [optional] 

### Return type

[**EnvVariable**](EnvVariable.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


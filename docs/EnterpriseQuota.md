# EnterpriseQuota

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groups_count** | **int** | 当前企业的团队总数 | [optional] 
**members_count** | **int** | 当前企业的成员总数（不包含观察者） | [optional] 
**viewers_count** | **int** | 当前企业的观察者总数 | [optional] 
**projects_count** | **int** | 当前企业的仓库总数 | [optional] 
**programs_count** | **int** | 当前企业的项目总数 | [optional] 
**project_quota** | **int** | 在企业内能创建的仓库数的总限制 | [optional] 
**space_quota** | **int** | 当前企业套餐的仓库容量大小配额。单位：G | [optional] 
**member_quota** | **int** | 当前企业套餐的成员数配额 | [optional] 
**viewer_quota** | **int** | 当前企业套餐的观察者数配额 | [optional] 
**attach_file_quota** | **int** | 当前企业套餐的附件配额。单位：G | [optional] 
**lfs_quota** | **int** | 当前企业套餐的 LFS 套餐配额。单位：G | [optional] 
**attach_file_used_space_quota** | **int** | 企业附件已占用的容量。单位: 兆 | [optional] 
**lfs_space_used_quota** | **int** | 企业 LFS 已使用的容量。单位: 兆 | [optional] 
**project_used_space_quota** | **int** | 企业仓库已占用的容量。单位: 兆 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


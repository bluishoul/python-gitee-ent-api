# gitee_api.MemberApplicationsApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_enterprise_id_member_applications**](MemberApplicationsApi.md#get_enterprise_id_member_applications) | **GET** /{enterprise_id}/member_applications | 企业/仓库/团队的成员加入申请列表
[**post_enterprise_id_member_applications_bulk_operate**](MemberApplicationsApi.md#post_enterprise_id_member_applications_bulk_operate) | **POST** /{enterprise_id}/member_applications/bulk_operate | 批量处理申请记录
[**post_enterprise_id_member_applications_set_force_verify**](MemberApplicationsApi.md#post_enterprise_id_member_applications_set_force_verify) | **POST** /{enterprise_id}/member_applications/set_force_verify | 强制审核开关
[**put_enterprise_id_member_applications_member_application_id**](MemberApplicationsApi.md#put_enterprise_id_member_applications_member_application_id) | **PUT** /{enterprise_id}/member_applications/{member_application_id} | 处理申请记录

# **get_enterprise_id_member_applications**
> list[MemberApplication] get_enterprise_id_member_applications(target_type, target_id, enterprise_id, access_token=access_token, page=page, per_page=per_page)

企业/仓库/团队的成员加入申请列表

企业/仓库/团队的成员加入申请列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MemberApplicationsApi()
target_type = 'target_type_example' # str | 申请加入的类型: Enterprise,Project,Group
target_id = 56 # int | 申请加入的主体 ID
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 企业/仓库/团队的成员加入申请列表
    api_response = api_instance.get_enterprise_id_member_applications(target_type, target_id, enterprise_id, access_token=access_token, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MemberApplicationsApi->get_enterprise_id_member_applications: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **target_type** | **str**| 申请加入的类型: Enterprise,Project,Group | 
 **target_id** | **int**| 申请加入的主体 ID | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[MemberApplication]**](MemberApplication.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_member_applications_bulk_operate**
> BulkResponseWithKey post_enterprise_id_member_applications_bulk_operate(body, enterprise_id)

批量处理申请记录

批量处理申请记录

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MemberApplicationsApi()
body = gitee_api.MemberApplicationsBulkOperateBody() # MemberApplicationsBulkOperateBody | 
enterprise_id = 56 # int | 

try:
    # 批量处理申请记录
    api_response = api_instance.post_enterprise_id_member_applications_bulk_operate(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MemberApplicationsApi->post_enterprise_id_member_applications_bulk_operate: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MemberApplicationsBulkOperateBody**](MemberApplicationsBulkOperateBody.md)|  | 
 **enterprise_id** | **int**|  | 

### Return type

[**BulkResponseWithKey**](BulkResponseWithKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_member_applications_set_force_verify**
> Enterprise post_enterprise_id_member_applications_set_force_verify(body, enterprise_id)

强制审核开关

强制审核开关

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MemberApplicationsApi()
body = gitee_api.MemberApplicationsSetForceVerifyBody() # MemberApplicationsSetForceVerifyBody | 
enterprise_id = 56 # int | 

try:
    # 强制审核开关
    api_response = api_instance.post_enterprise_id_member_applications_set_force_verify(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MemberApplicationsApi->post_enterprise_id_member_applications_set_force_verify: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MemberApplicationsSetForceVerifyBody**](MemberApplicationsSetForceVerifyBody.md)|  | 
 **enterprise_id** | **int**|  | 

### Return type

[**Enterprise**](Enterprise.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_member_applications_member_application_id**
> MemberApplication put_enterprise_id_member_applications_member_application_id(body, member_application_id, enterprise_id)

处理申请记录

处理申请记录

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MemberApplicationsApi()
body = gitee_api.MemberApplicationsMemberApplicationIdBody() # MemberApplicationsMemberApplicationIdBody | 
member_application_id = 56 # int | 申请记录id
enterprise_id = 56 # int | 

try:
    # 处理申请记录
    api_response = api_instance.put_enterprise_id_member_applications_member_application_id(body, member_application_id, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MemberApplicationsApi->put_enterprise_id_member_applications_member_application_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MemberApplicationsMemberApplicationIdBody**](MemberApplicationsMemberApplicationIdBody.md)|  | 
 **member_application_id** | **int**| 申请记录id | 
 **enterprise_id** | **int**|  | 

### Return type

[**MemberApplication**](MemberApplication.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# CherryPick

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cherry_branch** | **str** | 自动创建的 cherry pick 分支名 | [optional] 
**status** | **str** | 创建状态 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


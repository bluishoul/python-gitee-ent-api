# AttachFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 附件 id | [optional] 
**name** | **str** | 附件名称 | [optional] 
**size** | **int** | 附件的大小 | [optional] 
**file_type** | **str** | 附件的类型 | [optional] 
**preview_url** | **str** | 预览链接 | [optional] 
**creator** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**can_delete** | **bool** | 能否删除附件 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


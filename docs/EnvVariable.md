# EnvVariable

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 环境变量id | [optional] 
**project_id** | **int** | 仓库id | [optional] 
**name** | **str** | 环境变量名称 | [optional] 
**value** | **str** | 环境变量值 | [optional] 
**remark** | **str** | 环境变量备注 | [optional] 
**updated_at** | **str** | 修改时间 | [optional] 
**read_only** | **int** | 是否只读,1:true,0:false | [optional] 
**is_secret** | **int** | 是否密钥,1:true,0:false | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


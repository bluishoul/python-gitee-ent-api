# ProjectBaseSetting

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 仓库ID | [optional] 
**name** | **str** | 仓库名称 | [optional] 
**path** | **str** | 仓库路径 | [optional] 
**path_with_namespace** | **str** | namespace/path | [optional] 
**public** | **str** | 仓库开源属性，0:私有，1:开源，2:内部开源 | [optional] 
**is_gvp** | **bool** | 是否为GVP仓库 | [optional] 
**is_empty_repo** | **bool** | 是否为空仓库 | [optional] 
**fork_enabled** | **bool** | 是否允许仓库被Fork | [optional] 
**name_with_namespace** | **str** | namespace_name/path | [optional] 
**description** | **str** | 仓库介绍 | [optional] 
**homepage** | **str** | 仓库主页 | [optional] 
**lang_id** | **str** | 仓库语言 | [optional] 
**status** | **str** | 仓库状态(已关闭/开发中/已完结/维护中) | [optional] 
**default_branch** | **str** | 默认分支 | [optional] 
**outsourced** | **bool** | 仓库类型，0内部，1外包 | [optional] 
**creator** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**programs** | [**Program**](Program.md) |  | [optional] 
**can_comment** | **bool** | 允许用户对仓库进行评论 | [optional] 
**issue_comment** | **bool** | 允许用户对\&quot;关闭\&quot;状态的Issues进行评论 | [optional] 
**issues_enabled** | **bool** | 轻量级的issue跟踪系统 | [optional] 
**security_hole_enabled** | **bool** | 允许用户创建涉及敏感信息的Issue，提交后不公开此Issue（可见范围：仓库成员、企业成员） | [optional] 
**online_edit_enabled** | **bool** | 是否允许仓库文件在线编辑 | [optional] 
**pull_requests_enabled** | **bool** | 接受pull request，协作开发 | [optional] 
**wiki_enabled** | **bool** | 可以编写文档 | [optional] 
**lightweight_pr_enabled** | **bool** | 接受轻量级 Pull Request（用户可以发起轻量级 Pull Request 而无需 Fork 仓库） | [optional] 
**pr_master_only** | **bool** | 开启的Pull Request，仅管理员、审查者、测试者可见 | [optional] 
**forbid_force_push** | **bool** | 禁止强制推送 | [optional] 
**import_url** | **bool** | 仓库远程地址(用于强制同步) | [optional] 
**forbid_force_sync** | **bool** | 禁止仓库同步(禁止从仓库远程地址或Fork的源仓库强制同步代码，禁止后将关闭同步按钮) | [optional] 
**svn_enabled** | **bool** | 使用SVN管理您的仓库 | [optional] 
**can_readonly** | **bool** | 开启文件/文件夹只读功能(只读文件和SVN支持无法同时启用) | [optional] 
**enterprise_forbids_svn** | **bool** | 企业是否禁用SVN | [optional] 
**parent** | [**Project**](Project.md) |  | [optional] 
**template_enabled** | **bool** | 是否为模板仓库 | [optional] 
**gitee_go_enabled** | **bool** | 仓库是启用了GiteeGo | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


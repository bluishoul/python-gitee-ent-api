# WikiSort

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 文件 id | [optional] 
**name** | **str** | 文件名称 | [optional] 
**parent_id** | **int** | 父级 id | [optional] 
**creator** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**editor** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**file_type** | **int** | 文件类型(文件: 1; 文件夹: 2) | [optional] 
**version** | **str** | 文件的最后一次版本号 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


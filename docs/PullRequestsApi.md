# gitee_api.PullRequestsApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues_link_issue_id**](PullRequestsApi.md#delete_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues_link_issue_id) | **DELETE** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/link_issues/{link_issue_id} | PR 取消 关联任务
[**delete_enterprise_id_projects_project_id_pull_requests_pull_request_id_reactions_current_user_reaction_id**](PullRequestsApi.md#delete_enterprise_id_projects_project_id_pull_requests_pull_request_id_reactions_current_user_reaction_id) | **DELETE** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/reactions/{current_user_reaction_id} | 取消PR表态
[**get_enterprise_id_projects_project_id_pull_requests_new_commits**](PullRequestsApi.md#get_enterprise_id_projects_project_id_pull_requests_new_commits) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests/new/commits | 获取分支对比的 Commit 列表信息
[**get_enterprise_id_projects_project_id_pull_requests_new_files**](PullRequestsApi.md#get_enterprise_id_projects_project_id_pull_requests_new_files) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests/new/files | 获取分支对比的文件改动列表
[**get_enterprise_id_projects_project_id_pull_requests_pull_request_id**](PullRequestsApi.md#get_enterprise_id_projects_project_id_pull_requests_pull_request_id) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id} | 获取 Pull Request 详情
[**get_enterprise_id_projects_project_id_pull_requests_pull_request_id_auths**](PullRequestsApi.md#get_enterprise_id_projects_project_id_pull_requests_pull_request_id_auths) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/auths | 获取授权用户对 PR 的权限
[**get_enterprise_id_projects_project_id_pull_requests_pull_request_id_commits**](PullRequestsApi.md#get_enterprise_id_projects_project_id_pull_requests_pull_request_id_commits) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/commits | 获取某Pull Request的所有Commit信息
[**get_enterprise_id_projects_project_id_pull_requests_pull_request_id_commits_commit_id_files**](PullRequestsApi.md#get_enterprise_id_projects_project_id_pull_requests_pull_request_id_commits_commit_id_files) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/commits/{commit_id}/files | 获取 Commit 下的 diffs
[**get_enterprise_id_projects_project_id_pull_requests_pull_request_id_diff_for_path**](PullRequestsApi.md#get_enterprise_id_projects_project_id_pull_requests_pull_request_id_diff_for_path) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/diff_for_path | 获取 PullRequest 中差异较大的文件内容
[**get_enterprise_id_projects_project_id_pull_requests_pull_request_id_diff_position_context**](PullRequestsApi.md#get_enterprise_id_projects_project_id_pull_requests_pull_request_id_diff_position_context) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/diff_position_context | 获取 Pull Request 评论引用的代码片段
[**get_enterprise_id_projects_project_id_pull_requests_pull_request_id_files**](PullRequestsApi.md#get_enterprise_id_projects_project_id_pull_requests_pull_request_id_files) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/files | Pull Request Commit 文件列表。最多显示100条diff
[**get_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues**](PullRequestsApi.md#get_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/link_issues | PR 关联任务任务列表
[**get_enterprise_id_projects_project_id_pull_requests_pull_request_id_notes**](PullRequestsApi.md#get_enterprise_id_projects_project_id_pull_requests_pull_request_id_notes) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/notes | 获取 Pull Request 评论列表
[**get_enterprise_id_projects_project_id_pull_requests_pull_request_id_operate_logs**](PullRequestsApi.md#get_enterprise_id_projects_project_id_pull_requests_pull_request_id_operate_logs) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/operate_logs | 获取 Pull Request 操作日志
[**get_enterprise_id_projects_project_id_pull_requests_pull_request_id_scan_reports**](PullRequestsApi.md#get_enterprise_id_projects_project_id_pull_requests_pull_request_id_scan_reports) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/scan_reports | 获取 PullRequest 的 GiteeScan 报告
[**get_enterprise_id_pull_requests**](PullRequestsApi.md#get_enterprise_id_pull_requests) | **GET** /{enterprise_id}/pull_requests | 获取企业下的 Pull Request 列表
[**get_enterprise_id_pull_requests_pull_request_stats**](PullRequestsApi.md#get_enterprise_id_pull_requests_pull_request_stats) | **GET** /{enterprise_id}/pull_requests/pull_request_stats | 获取企业下用户 Pull Request 相关数量数据
[**post_enterprise_id_projects_project_id_pull_requests**](PullRequestsApi.md#post_enterprise_id_projects_project_id_pull_requests) | **POST** /{enterprise_id}/projects/{project_id}/pull_requests | 创建 PR
[**post_enterprise_id_projects_project_id_pull_requests_pull_request_id_cherry_pick**](PullRequestsApi.md#post_enterprise_id_projects_project_id_pull_requests_pull_request_id_cherry_pick) | **POST** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/cherry_pick | 创建 Cherry Pick
[**post_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues**](PullRequestsApi.md#post_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues) | **POST** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/link_issues | PR 添加 关联任务
[**post_enterprise_id_projects_project_id_pull_requests_pull_request_id_merge**](PullRequestsApi.md#post_enterprise_id_projects_project_id_pull_requests_pull_request_id_merge) | **POST** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/merge | 合并 PR
[**post_enterprise_id_projects_project_id_pull_requests_pull_request_id_notes**](PullRequestsApi.md#post_enterprise_id_projects_project_id_pull_requests_pull_request_id_notes) | **POST** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/notes | 评论 Pull Request
[**post_enterprise_id_projects_project_id_pull_requests_pull_request_id_reactions**](PullRequestsApi.md#post_enterprise_id_projects_project_id_pull_requests_pull_request_id_reactions) | **POST** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/reactions | 新增PR表态
[**put_enterprise_id_projects_project_id_pull_requests_pull_request_id**](PullRequestsApi.md#put_enterprise_id_projects_project_id_pull_requests_pull_request_id) | **PUT** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id} | 更新 Pull Request
[**put_enterprise_id_projects_project_id_pull_requests_pull_request_id_assign**](PullRequestsApi.md#put_enterprise_id_projects_project_id_pull_requests_pull_request_id_assign) | **PUT** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/assign | PR 审查通过
[**put_enterprise_id_projects_project_id_pull_requests_pull_request_id_test**](PullRequestsApi.md#put_enterprise_id_projects_project_id_pull_requests_pull_request_id_test) | **PUT** /{enterprise_id}/projects/{project_id}/pull_requests/{pull_request_id}/test | PR 测试通过

# **delete_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues_link_issue_id**
> delete_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues_link_issue_id(enterprise_id, project_id, link_issue_id, pull_request_id, access_token=access_token, qt=qt)

PR 取消 关联任务

PR 取消 关联任务

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
link_issue_id = 56 # int | 任务id
pull_request_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # PR 取消 关联任务
    api_instance.delete_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues_link_issue_id(enterprise_id, project_id, link_issue_id, pull_request_id, access_token=access_token, qt=qt)
except ApiException as e:
    print("Exception when calling PullRequestsApi->delete_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues_link_issue_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **link_issue_id** | **int**| 任务id | 
 **pull_request_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_projects_project_id_pull_requests_pull_request_id_reactions_current_user_reaction_id**
> IssueReaction delete_enterprise_id_projects_project_id_pull_requests_pull_request_id_reactions_current_user_reaction_id(enterprise_id, project_id, pull_request_id, current_user_reaction_id, access_token=access_token, qt=qt)

取消PR表态

取消PR表态

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
current_user_reaction_id = 56 # int | 表态id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 取消PR表态
    api_response = api_instance.delete_enterprise_id_projects_project_id_pull_requests_pull_request_id_reactions_current_user_reaction_id(enterprise_id, project_id, pull_request_id, current_user_reaction_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->delete_enterprise_id_projects_project_id_pull_requests_pull_request_id_reactions_current_user_reaction_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **current_user_reaction_id** | **int**| 表态id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**IssueReaction**](IssueReaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests_new_commits**
> list[CommitList] get_enterprise_id_projects_project_id_pull_requests_new_commits(enterprise_id, project_id, source_namespace, source_branch, target_namespace, target_branch, access_token=access_token, qt=qt, page=page, per_page=per_page)

获取分支对比的 Commit 列表信息

获取分支对比的 Commit 列表信息

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
source_namespace = 'source_namespace_example' # str | 源仓库所属空间地址
source_branch = 'source_branch_example' # str | 源仓库的分支名称
target_namespace = 'target_namespace_example' # str | 目标仓库所属空间地址
target_branch = 'target_branch_example' # str | 目标仓库的分支名称
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取分支对比的 Commit 列表信息
    api_response = api_instance.get_enterprise_id_projects_project_id_pull_requests_new_commits(enterprise_id, project_id, source_namespace, source_branch, target_namespace, target_branch, access_token=access_token, qt=qt, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_projects_project_id_pull_requests_new_commits: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **source_namespace** | **str**| 源仓库所属空间地址 | 
 **source_branch** | **str**| 源仓库的分支名称 | 
 **target_namespace** | **str**| 目标仓库所属空间地址 | 
 **target_branch** | **str**| 目标仓库的分支名称 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[CommitList]**](CommitList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests_new_files**
> list[CommitList] get_enterprise_id_projects_project_id_pull_requests_new_files(enterprise_id, project_id, source_namespace, source_branch, target_namespace, target_branch, access_token=access_token, qt=qt, page=page, per_page=per_page)

获取分支对比的文件改动列表

获取分支对比的文件改动列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
source_namespace = 'source_namespace_example' # str | 源仓库所属空间地址
source_branch = 'source_branch_example' # str | 源仓库的分支名称
target_namespace = 'target_namespace_example' # str | 目标仓库所属空间地址
target_branch = 'target_branch_example' # str | 目标仓库的分支名称
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取分支对比的文件改动列表
    api_response = api_instance.get_enterprise_id_projects_project_id_pull_requests_new_files(enterprise_id, project_id, source_namespace, source_branch, target_namespace, target_branch, access_token=access_token, qt=qt, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_projects_project_id_pull_requests_new_files: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **source_namespace** | **str**| 源仓库所属空间地址 | 
 **source_branch** | **str**| 源仓库的分支名称 | 
 **target_namespace** | **str**| 目标仓库所属空间地址 | 
 **target_branch** | **str**| 目标仓库的分支名称 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[CommitList]**](CommitList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests_pull_request_id**
> PullRequestDetail get_enterprise_id_projects_project_id_pull_requests_pull_request_id(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt)

获取 Pull Request 详情

获取 Pull Request 详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 获取 Pull Request 详情
    api_response = api_instance.get_enterprise_id_projects_project_id_pull_requests_pull_request_id(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_projects_project_id_pull_requests_pull_request_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**PullRequestDetail**](PullRequestDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests_pull_request_id_auths**
> PullRequestAuth get_enterprise_id_projects_project_id_pull_requests_pull_request_id_auths(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt)

获取授权用户对 PR 的权限

获取授权用户对 PR 的权限

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 获取授权用户对 PR 的权限
    api_response = api_instance.get_enterprise_id_projects_project_id_pull_requests_pull_request_id_auths(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_projects_project_id_pull_requests_pull_request_id_auths: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**PullRequestAuth**](PullRequestAuth.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests_pull_request_id_commits**
> list[CommitList] get_enterprise_id_projects_project_id_pull_requests_pull_request_id_commits(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt, page=page, per_page=per_page)

获取某Pull Request的所有Commit信息

获取某Pull Request的所有Commit信息

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取某Pull Request的所有Commit信息
    api_response = api_instance.get_enterprise_id_projects_project_id_pull_requests_pull_request_id_commits(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_projects_project_id_pull_requests_pull_request_id_commits: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[CommitList]**](CommitList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests_pull_request_id_commits_commit_id_files**
> list[PullRequestFiles] get_enterprise_id_projects_project_id_pull_requests_pull_request_id_commits_commit_id_files(enterprise_id, project_id, pull_request_id, commit_id, access_token=access_token, qt=qt, page=page, per_page=per_page)

获取 Commit 下的 diffs

获取 Commit 下的 diffs

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
commit_id = 'commit_id_example' # str | Commit id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取 Commit 下的 diffs
    api_response = api_instance.get_enterprise_id_projects_project_id_pull_requests_pull_request_id_commits_commit_id_files(enterprise_id, project_id, pull_request_id, commit_id, access_token=access_token, qt=qt, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_projects_project_id_pull_requests_pull_request_id_commits_commit_id_files: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **commit_id** | **str**| Commit id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[PullRequestFiles]**](PullRequestFiles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests_pull_request_id_diff_for_path**
> get_enterprise_id_projects_project_id_pull_requests_pull_request_id_diff_for_path(enterprise_id, project_id, pull_request_id, file_identifier, new_path, old_path, access_token=access_token, qt=qt)

获取 PullRequest 中差异较大的文件内容

获取 PullRequest 中差异较大的文件内容

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
file_identifier = 'file_identifier_example' # str | git 文件标识符
new_path = 'new_path_example' # str | 旧路径
old_path = 'old_path_example' # str | 新路径
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 获取 PullRequest 中差异较大的文件内容
    api_instance.get_enterprise_id_projects_project_id_pull_requests_pull_request_id_diff_for_path(enterprise_id, project_id, pull_request_id, file_identifier, new_path, old_path, access_token=access_token, qt=qt)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_projects_project_id_pull_requests_pull_request_id_diff_for_path: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **file_identifier** | **str**| git 文件标识符 | 
 **new_path** | **str**| 旧路径 | 
 **old_path** | **str**| 新路径 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests_pull_request_id_diff_position_context**
> list[DiffPosition] get_enterprise_id_projects_project_id_pull_requests_pull_request_id_diff_position_context(enterprise_id, project_id, ids, pull_request_id, access_token=access_token, qt=qt)

获取 Pull Request 评论引用的代码片段

获取 Pull Request 评论引用的代码片段

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
ids = 'ids_example' # str | diff_position_id，使用英文逗号分隔
pull_request_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 获取 Pull Request 评论引用的代码片段
    api_response = api_instance.get_enterprise_id_projects_project_id_pull_requests_pull_request_id_diff_position_context(enterprise_id, project_id, ids, pull_request_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_projects_project_id_pull_requests_pull_request_id_diff_position_context: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **ids** | **str**| diff_position_id，使用英文逗号分隔 | 
 **pull_request_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**list[DiffPosition]**](DiffPosition.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests_pull_request_id_files**
> list[PullRequestFiles] get_enterprise_id_projects_project_id_pull_requests_pull_request_id_files(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt, page=page, per_page=per_page, head_sha=head_sha, base_sha=base_sha)

Pull Request Commit 文件列表。最多显示100条diff

Pull Request Commit 文件列表。最多显示100条diff

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)
head_sha = 'head_sha_example' # str | head sha (optional)
base_sha = 'base_sha_example' # str | base sha (optional)

try:
    # Pull Request Commit 文件列表。最多显示100条diff
    api_response = api_instance.get_enterprise_id_projects_project_id_pull_requests_pull_request_id_files(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt, page=page, per_page=per_page, head_sha=head_sha, base_sha=base_sha)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_projects_project_id_pull_requests_pull_request_id_files: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 
 **head_sha** | **str**| head sha | [optional] 
 **base_sha** | **str**| base sha | [optional] 

### Return type

[**list[PullRequestFiles]**](PullRequestFiles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues**
> Issue get_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt, page=page, per_page=per_page)

PR 关联任务任务列表

PR 关联任务任务列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # PR 关联任务任务列表
    api_response = api_instance.get_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests_pull_request_id_notes**
> PullRequestNote get_enterprise_id_projects_project_id_pull_requests_pull_request_id_notes(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt, commit_id=commit_id, page=page, per_page=per_page)

获取 Pull Request 评论列表

获取 Pull Request 评论列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
commit_id = 'commit_id_example' # str | commit sha (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取 Pull Request 评论列表
    api_response = api_instance.get_enterprise_id_projects_project_id_pull_requests_pull_request_id_notes(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt, commit_id=commit_id, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_projects_project_id_pull_requests_pull_request_id_notes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **commit_id** | **str**| commit sha | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**PullRequestNote**](PullRequestNote.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests_pull_request_id_operate_logs**
> PrOperateLog get_enterprise_id_projects_project_id_pull_requests_pull_request_id_operate_logs(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt, sort=sort, direction=direction, page=page, per_page=per_page)

获取 Pull Request 操作日志

获取 Pull Request 操作日志

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
sort = 'sort_example' # str | 排序字段(created_at: 创建时间 updated_at: 更新时间) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取 Pull Request 操作日志
    api_response = api_instance.get_enterprise_id_projects_project_id_pull_requests_pull_request_id_operate_logs(enterprise_id, project_id, pull_request_id, access_token=access_token, qt=qt, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_projects_project_id_pull_requests_pull_request_id_operate_logs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **sort** | **str**| 排序字段(created_at: 创建时间 updated_at: 更新时间) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**PrOperateLog**](PrOperateLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests_pull_request_id_scan_reports**
> get_enterprise_id_projects_project_id_pull_requests_pull_request_id_scan_reports(enterprise_id, project_id, pull_request_id, task_id, type, access_token=access_token, qt=qt, bug_level=bug_level, bug_type=bug_type, page=page, per_page=per_page)

获取 PullRequest 的 GiteeScan 报告

获取 PullRequest 的 GiteeScan 报告

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
task_id = 56 # int | GiteeScan 扫描任务 id
type = 'type_example' # str | 扫描类型，bug：缺陷扫描、style：规范扫描、cve：依赖项漏洞扫描
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
bug_level = 'bug_level_example' # str | 缺陷等级，高危：1、中危：2 (optional)
bug_type = 'bug_type_example' # str | 缺陷类型，BUG：0、CodeSmell：1、Security：2 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取 PullRequest 的 GiteeScan 报告
    api_instance.get_enterprise_id_projects_project_id_pull_requests_pull_request_id_scan_reports(enterprise_id, project_id, pull_request_id, task_id, type, access_token=access_token, qt=qt, bug_level=bug_level, bug_type=bug_type, page=page, per_page=per_page)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_projects_project_id_pull_requests_pull_request_id_scan_reports: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **task_id** | **int**| GiteeScan 扫描任务 id | 
 **type** | **str**| 扫描类型，bug：缺陷扫描、style：规范扫描、cve：依赖项漏洞扫描 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **bug_level** | **str**| 缺陷等级，高危：1、中危：2 | [optional] 
 **bug_type** | **str**| 缺陷类型，BUG：0、CodeSmell：1、Security：2 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_pull_requests**
> list[PullRequest] get_enterprise_id_pull_requests(enterprise_id, access_token=access_token, state=state, scope=scope, author_id=author_id, assignee_id=assignee_id, tester_id=tester_id, search=search, sort=sort, direction=direction, group_id=group_id, milestone_id=milestone_id, labels=labels, label_ids=label_ids, can_be_merged=can_be_merged, project_id=project_id, need_state_count=need_state_count, public_or_internal_open_only=public_or_internal_open_only, page=page, per_page=per_page)

获取企业下的 Pull Request 列表

获取企业下的 Pull Request 列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
state = 'state_example' # str | PR 状态 (optional)
scope = 'scope_example' # str | 范围筛选。指派我的: assigned_or_test，我创建或指派给我的: related_to_me，我参与仓库的PR: participate_in (optional)
author_id = 'author_id_example' # str | 筛选 PR 创建者 (optional)
assignee_id = 'assignee_id_example' # str | 筛选 PR 审查者 (optional)
tester_id = 'tester_id_example' # str | 筛选 PR 测试人员 (optional)
search = 'search_example' # str | 搜索参数 (optional)
sort = 'sort_example' # str | 排序字段(created_at、closed_at、priority) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
group_id = 56 # int | 团队 id (optional)
milestone_id = 56 # int | 里程碑 id (optional)
labels = 'labels_example' # str | 标签名称。多个标签逗号(,)隔开 (optional)
label_ids = 'label_ids_example' # str | 标签ID,多个标签逗号(,)隔开 (optional)
can_be_merged = 56 # int | 是否可合并 (optional)
project_id = 56 # int | 仓库 id (optional)
need_state_count = 56 # int | 是否需要状态统计数 (optional)
public_or_internal_open_only = 56 # int | 仅列出内部公开和外部公开的 PR (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取企业下的 Pull Request 列表
    api_response = api_instance.get_enterprise_id_pull_requests(enterprise_id, access_token=access_token, state=state, scope=scope, author_id=author_id, assignee_id=assignee_id, tester_id=tester_id, search=search, sort=sort, direction=direction, group_id=group_id, milestone_id=milestone_id, labels=labels, label_ids=label_ids, can_be_merged=can_be_merged, project_id=project_id, need_state_count=need_state_count, public_or_internal_open_only=public_or_internal_open_only, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_pull_requests: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **state** | **str**| PR 状态 | [optional] 
 **scope** | **str**| 范围筛选。指派我的: assigned_or_test，我创建或指派给我的: related_to_me，我参与仓库的PR: participate_in | [optional] 
 **author_id** | **str**| 筛选 PR 创建者 | [optional] 
 **assignee_id** | **str**| 筛选 PR 审查者 | [optional] 
 **tester_id** | **str**| 筛选 PR 测试人员 | [optional] 
 **search** | **str**| 搜索参数 | [optional] 
 **sort** | **str**| 排序字段(created_at、closed_at、priority) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **group_id** | **int**| 团队 id | [optional] 
 **milestone_id** | **int**| 里程碑 id | [optional] 
 **labels** | **str**| 标签名称。多个标签逗号(,)隔开 | [optional] 
 **label_ids** | **str**| 标签ID,多个标签逗号(,)隔开 | [optional] 
 **can_be_merged** | **int**| 是否可合并 | [optional] 
 **project_id** | **int**| 仓库 id | [optional] 
 **need_state_count** | **int**| 是否需要状态统计数 | [optional] 
 **public_or_internal_open_only** | **int**| 仅列出内部公开和外部公开的 PR | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[PullRequest]**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_pull_requests_pull_request_stats**
> get_enterprise_id_pull_requests_pull_request_stats(enterprise_id, access_token=access_token)

获取企业下用户 Pull Request 相关数量数据

获取企业下用户 Pull Request 相关数量数据

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取企业下用户 Pull Request 相关数量数据
    api_instance.get_enterprise_id_pull_requests_pull_request_stats(enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling PullRequestsApi->get_enterprise_id_pull_requests_pull_request_stats: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_pull_requests**
> list[PullRequestDetail] post_enterprise_id_projects_project_id_pull_requests(body, enterprise_id, project_id)

创建 PR

创建 PR

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
body = gitee_api.ProjectIdPullRequestsBody() # ProjectIdPullRequestsBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path

try:
    # 创建 PR
    api_response = api_instance.post_enterprise_id_projects_project_id_pull_requests(body, enterprise_id, project_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->post_enterprise_id_projects_project_id_pull_requests: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdPullRequestsBody**](ProjectIdPullRequestsBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 

### Return type

[**list[PullRequestDetail]**](PullRequestDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_pull_requests_pull_request_id_cherry_pick**
> CherryPick post_enterprise_id_projects_project_id_pull_requests_pull_request_id_cherry_pick(body, enterprise_id, project_id, pull_request_id)

创建 Cherry Pick

创建 Cherry Pick

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
body = gitee_api.PullRequestIdCherryPickBody() # PullRequestIdCherryPickBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | 

try:
    # 创建 Cherry Pick
    api_response = api_instance.post_enterprise_id_projects_project_id_pull_requests_pull_request_id_cherry_pick(body, enterprise_id, project_id, pull_request_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->post_enterprise_id_projects_project_id_pull_requests_pull_request_id_cherry_pick: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PullRequestIdCherryPickBody**](PullRequestIdCherryPickBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**|  | 

### Return type

[**CherryPick**](CherryPick.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues**
> Issue post_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues(body, enterprise_id, project_id, pull_request_id)

PR 添加 关联任务

PR 添加 关联任务

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
body = gitee_api.PullRequestIdLinkIssuesBody() # PullRequestIdLinkIssuesBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | 

try:
    # PR 添加 关联任务
    api_response = api_instance.post_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues(body, enterprise_id, project_id, pull_request_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->post_enterprise_id_projects_project_id_pull_requests_pull_request_id_link_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PullRequestIdLinkIssuesBody**](PullRequestIdLinkIssuesBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**|  | 

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_pull_requests_pull_request_id_merge**
> post_enterprise_id_projects_project_id_pull_requests_pull_request_id_merge(enterprise_id, project_id, pull_request_id, body=body)

合并 PR

合并 PR

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
body = gitee_api.PullRequestIdMergeBody() # PullRequestIdMergeBody |  (optional)

try:
    # 合并 PR
    api_instance.post_enterprise_id_projects_project_id_pull_requests_pull_request_id_merge(enterprise_id, project_id, pull_request_id, body=body)
except ApiException as e:
    print("Exception when calling PullRequestsApi->post_enterprise_id_projects_project_id_pull_requests_pull_request_id_merge: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **body** | [**PullRequestIdMergeBody**](PullRequestIdMergeBody.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_pull_requests_pull_request_id_notes**
> PullRequestNote post_enterprise_id_projects_project_id_pull_requests_pull_request_id_notes(body, enterprise_id, project_id, pull_request_id)

评论 Pull Request

评论 Pull Request

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
body = gitee_api.PullRequestIdNotesBody() # PullRequestIdNotesBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id

try:
    # 评论 Pull Request
    api_response = api_instance.post_enterprise_id_projects_project_id_pull_requests_pull_request_id_notes(body, enterprise_id, project_id, pull_request_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->post_enterprise_id_projects_project_id_pull_requests_pull_request_id_notes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PullRequestIdNotesBody**](PullRequestIdNotesBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 

### Return type

[**PullRequestNote**](PullRequestNote.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_pull_requests_pull_request_id_reactions**
> IssueReaction post_enterprise_id_projects_project_id_pull_requests_pull_request_id_reactions(body, enterprise_id, project_id, pull_request_id)

新增PR表态

新增PR表态

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
body = gitee_api.PullRequestIdReactionsBody() # PullRequestIdReactionsBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id

try:
    # 新增PR表态
    api_response = api_instance.post_enterprise_id_projects_project_id_pull_requests_pull_request_id_reactions(body, enterprise_id, project_id, pull_request_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->post_enterprise_id_projects_project_id_pull_requests_pull_request_id_reactions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PullRequestIdReactionsBody**](PullRequestIdReactionsBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 

### Return type

[**IssueReaction**](IssueReaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_projects_project_id_pull_requests_pull_request_id**
> PullRequestDetail put_enterprise_id_projects_project_id_pull_requests_pull_request_id(enterprise_id, project_id, pull_request_id, body=body)

更新 Pull Request

更新 Pull Request

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
body = gitee_api.PullRequestsPullRequestIdBody() # PullRequestsPullRequestIdBody |  (optional)

try:
    # 更新 Pull Request
    api_response = api_instance.put_enterprise_id_projects_project_id_pull_requests_pull_request_id(enterprise_id, project_id, pull_request_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PullRequestsApi->put_enterprise_id_projects_project_id_pull_requests_pull_request_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **body** | [**PullRequestsPullRequestIdBody**](PullRequestsPullRequestIdBody.md)|  | [optional] 

### Return type

[**PullRequestDetail**](PullRequestDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_projects_project_id_pull_requests_pull_request_id_assign**
> put_enterprise_id_projects_project_id_pull_requests_pull_request_id_assign(enterprise_id, project_id, pull_request_id, body=body)

PR 审查通过

PR 审查通过

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
body = gitee_api.PullRequestIdAssignBody() # PullRequestIdAssignBody |  (optional)

try:
    # PR 审查通过
    api_instance.put_enterprise_id_projects_project_id_pull_requests_pull_request_id_assign(enterprise_id, project_id, pull_request_id, body=body)
except ApiException as e:
    print("Exception when calling PullRequestsApi->put_enterprise_id_projects_project_id_pull_requests_pull_request_id_assign: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **body** | [**PullRequestIdAssignBody**](PullRequestIdAssignBody.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_projects_project_id_pull_requests_pull_request_id_test**
> put_enterprise_id_projects_project_id_pull_requests_pull_request_id_test(enterprise_id, project_id, pull_request_id, body=body)

PR 测试通过

PR 测试通过

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.PullRequestsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
pull_request_id = 56 # int | PR id
body = gitee_api.PullRequestIdTestBody() # PullRequestIdTestBody |  (optional)

try:
    # PR 测试通过
    api_instance.put_enterprise_id_projects_project_id_pull_requests_pull_request_id_test(enterprise_id, project_id, pull_request_id, body=body)
except ApiException as e:
    print("Exception when calling PullRequestsApi->put_enterprise_id_projects_project_id_pull_requests_pull_request_id_test: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **pull_request_id** | **int**| PR id | 
 **body** | [**PullRequestIdTestBody**](PullRequestIdTestBody.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


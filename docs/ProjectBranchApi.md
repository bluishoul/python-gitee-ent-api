# gitee_api.ProjectBranchApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_projects_project_id_branches_batch_destroy**](ProjectBranchApi.md#delete_enterprise_id_projects_project_id_branches_batch_destroy) | **DELETE** /{enterprise_id}/projects/{project_id}/branches/batch_destroy | 批量删除分支
[**delete_enterprise_id_projects_project_id_branches_name**](ProjectBranchApi.md#delete_enterprise_id_projects_project_id_branches_name) | **DELETE** /{enterprise_id}/projects/{project_id}/branches/{name} | 删除分支
[**get_enterprise_id_projects_project_id_branches**](ProjectBranchApi.md#get_enterprise_id_projects_project_id_branches) | **GET** /{enterprise_id}/projects/{project_id}/branches | 获取仓库的分支列表
[**post_enterprise_id_projects_project_id_branches**](ProjectBranchApi.md#post_enterprise_id_projects_project_id_branches) | **POST** /{enterprise_id}/projects/{project_id}/branches | 添加分支
[**put_enterprise_id_projects_project_id_branches_name**](ProjectBranchApi.md#put_enterprise_id_projects_project_id_branches_name) | **PUT** /{enterprise_id}/projects/{project_id}/branches/{name} | 编辑分支状态
[**put_enterprise_id_projects_project_id_branches_name_set_default**](ProjectBranchApi.md#put_enterprise_id_projects_project_id_branches_name_set_default) | **PUT** /{enterprise_id}/projects/{project_id}/branches/{name}/set_default | 设为默认分支

# **delete_enterprise_id_projects_project_id_branches_batch_destroy**
> delete_enterprise_id_projects_project_id_branches_batch_destroy(names, enterprise_id, project_id, access_token=access_token, qt=qt)

批量删除分支

批量删除分支

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectBranchApi()
names = ['names_example'] # list[str] | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 批量删除分支
    api_instance.delete_enterprise_id_projects_project_id_branches_batch_destroy(names, enterprise_id, project_id, access_token=access_token, qt=qt)
except ApiException as e:
    print("Exception when calling ProjectBranchApi->delete_enterprise_id_projects_project_id_branches_batch_destroy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **names** | [**list[str]**](str.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_projects_project_id_branches_name**
> delete_enterprise_id_projects_project_id_branches_name(enterprise_id, project_id, name, access_token=access_token, qt=qt)

删除分支

删除分支

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectBranchApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
name = 'name_example' # str | 分支名
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 删除分支
    api_instance.delete_enterprise_id_projects_project_id_branches_name(enterprise_id, project_id, name, access_token=access_token, qt=qt)
except ApiException as e:
    print("Exception when calling ProjectBranchApi->delete_enterprise_id_projects_project_id_branches_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **name** | **str**| 分支名 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_branches**
> Branch get_enterprise_id_projects_project_id_branches(enterprise_id, project_id, access_token=access_token, qt=qt, search=search, page=page, per_page=per_page)

获取仓库的分支列表

获取仓库的分支列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectBranchApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
search = 'search_example' # str | 搜索关键字 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取仓库的分支列表
    api_response = api_instance.get_enterprise_id_projects_project_id_branches(enterprise_id, project_id, access_token=access_token, qt=qt, search=search, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectBranchApi->get_enterprise_id_projects_project_id_branches: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **search** | **str**| 搜索关键字 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**Branch**](Branch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_branches**
> Branch post_enterprise_id_projects_project_id_branches(body, enterprise_id, project_id)

添加分支

添加分支

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectBranchApi()
body = gitee_api.ProjectIdBranchesBody() # ProjectIdBranchesBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path

try:
    # 添加分支
    api_response = api_instance.post_enterprise_id_projects_project_id_branches(body, enterprise_id, project_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectBranchApi->post_enterprise_id_projects_project_id_branches: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdBranchesBody**](ProjectIdBranchesBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 

### Return type

[**Branch**](Branch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_projects_project_id_branches_name**
> Branch put_enterprise_id_projects_project_id_branches_name(body, enterprise_id, project_id, name)

编辑分支状态

编辑分支状态

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectBranchApi()
body = gitee_api.BranchesNameBody() # BranchesNameBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
name = 'name_example' # str | 分支名

try:
    # 编辑分支状态
    api_response = api_instance.put_enterprise_id_projects_project_id_branches_name(body, enterprise_id, project_id, name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectBranchApi->put_enterprise_id_projects_project_id_branches_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**BranchesNameBody**](BranchesNameBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **name** | **str**| 分支名 | 

### Return type

[**Branch**](Branch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_projects_project_id_branches_name_set_default**
> put_enterprise_id_projects_project_id_branches_name_set_default(enterprise_id, project_id, name, body=body)

设为默认分支

设为默认分支

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectBranchApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
name = 'name_example' # str | 分支名
body = gitee_api.NameSetDefaultBody() # NameSetDefaultBody |  (optional)

try:
    # 设为默认分支
    api_instance.put_enterprise_id_projects_project_id_branches_name_set_default(enterprise_id, project_id, name, body=body)
except ApiException as e:
    print("Exception when calling ProjectBranchApi->put_enterprise_id_projects_project_id_branches_name_set_default: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **name** | **str**| 分支名 | 
 **body** | [**NameSetDefaultBody**](NameSetDefaultBody.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


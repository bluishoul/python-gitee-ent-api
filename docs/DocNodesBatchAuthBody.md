# DocNodesBatchAuthBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**doc_node_ids** | **str** | doc_node 节点 id，使用,，隔开 | 
**guess_access** | **int** | 访客的访问权限; (无权限: 0; 只读: 1;) | [optional] 
**enterprise_access** | **int** | 企业成员的访问权限(无权限: 0; 只读: 1; 读写: 2) | [optional] 
**access_target_type** | **list[str]** | 关联类型(target_type: program|group|user) | [optional] 
**access_target_id** | **list[int]** | 关联类型的 id(如 target_type &#x3D; program，则此处应为对应 program 的 id) | [optional] 
**access_access_level** | **list[int]** | 访问属性（无权限: 0; 只读: 1; 读写: 2） | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


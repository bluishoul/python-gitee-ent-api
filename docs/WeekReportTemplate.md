# WeekReportTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 模版ID | [optional] 
**content** | **str** | 模版内容 | [optional] 
**is_default** | **bool** | 是否是默认模版 | [optional] 
**name** | **str** | 模版名称 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# EnterpriseProgram

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assignee_id** | **int** | 所有者id | [optional] 
**id** | **int** | 项目id | [optional] 
**name** | **str** | 项目名称 | [optional] 
**path** | **str** | 项目path | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


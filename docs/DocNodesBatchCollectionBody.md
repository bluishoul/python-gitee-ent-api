# DocNodesBatchCollectionBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**doc_node_ids** | **str** | 文件 id，使用,，隔开 | 
**operate_type** | **str** | 收藏: set; 取消收藏: unset | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


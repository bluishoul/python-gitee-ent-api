# gitee_api.ProjectAdminApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_enterprise_id_projects_project_id_availabel_keys**](ProjectAdminApi.md#get_enterprise_id_projects_project_id_availabel_keys) | **GET** /{enterprise_id}/projects/{project_id}/availabel_keys | 可部署公钥
[**get_enterprise_id_projects_project_id_deploy_keys**](ProjectAdminApi.md#get_enterprise_id_projects_project_id_deploy_keys) | **GET** /{enterprise_id}/projects/{project_id}/deploy_keys | 已启用公钥
[**get_enterprise_id_projects_project_id_deploy_keys_deploy_key_id**](ProjectAdminApi.md#get_enterprise_id_projects_project_id_deploy_keys_deploy_key_id) | **GET** /{enterprise_id}/projects/{project_id}/deploy_keys/{deploy_key_id} | 公钥详情
[**get_enterprise_id_projects_project_id_pr_assigner**](ProjectAdminApi.md#get_enterprise_id_projects_project_id_pr_assigner) | **GET** /{enterprise_id}/projects/{project_id}/pr_assigner | 查看代码审查设置
[**post_enterprise_id_projects_project_id_deploy_keys**](ProjectAdminApi.md#post_enterprise_id_projects_project_id_deploy_keys) | **POST** /{enterprise_id}/projects/{project_id}/deploy_keys | 添加部署公钥
[**post_enterprise_id_projects_project_id_initialiaze_readme**](ProjectAdminApi.md#post_enterprise_id_projects_project_id_initialiaze_readme) | **POST** /{enterprise_id}/projects/{project_id}/initialiaze_readme | 初始化README
[**put_enterprise_id_projects_project_id_deploy_keys_deploy_key_id**](ProjectAdminApi.md#put_enterprise_id_projects_project_id_deploy_keys_deploy_key_id) | **PUT** /{enterprise_id}/projects/{project_id}/deploy_keys/{deploy_key_id} | 启用/停用公钥
[**put_enterprise_id_projects_project_id_pr_assigner**](ProjectAdminApi.md#put_enterprise_id_projects_project_id_pr_assigner) | **PUT** /{enterprise_id}/projects/{project_id}/pr_assigner | 指派审查、测试人员
[**put_enterprise_id_projects_project_id_status**](ProjectAdminApi.md#put_enterprise_id_projects_project_id_status) | **PUT** /{enterprise_id}/projects/{project_id}/status | 仓库归档(状态管理)

# **get_enterprise_id_projects_project_id_availabel_keys**
> EnterpriseDeployKey get_enterprise_id_projects_project_id_availabel_keys(enterprise_id, project_id, access_token=access_token, qt=qt, page=page, per_page=per_page)

可部署公钥

可部署公钥

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectAdminApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 可部署公钥
    api_response = api_instance.get_enterprise_id_projects_project_id_availabel_keys(enterprise_id, project_id, access_token=access_token, qt=qt, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectAdminApi->get_enterprise_id_projects_project_id_availabel_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**EnterpriseDeployKey**](EnterpriseDeployKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_deploy_keys**
> EnterpriseDeployKey get_enterprise_id_projects_project_id_deploy_keys(enterprise_id, project_id, access_token=access_token, qt=qt, page=page, per_page=per_page)

已启用公钥

已启用公钥

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectAdminApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 已启用公钥
    api_response = api_instance.get_enterprise_id_projects_project_id_deploy_keys(enterprise_id, project_id, access_token=access_token, qt=qt, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectAdminApi->get_enterprise_id_projects_project_id_deploy_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**EnterpriseDeployKey**](EnterpriseDeployKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_deploy_keys_deploy_key_id**
> EnterpriseDeployKey get_enterprise_id_projects_project_id_deploy_keys_deploy_key_id(enterprise_id, project_id, deploy_key_id, access_token=access_token, qt=qt)

公钥详情

公钥详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectAdminApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
deploy_key_id = 56 # int | 公钥id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 公钥详情
    api_response = api_instance.get_enterprise_id_projects_project_id_deploy_keys_deploy_key_id(enterprise_id, project_id, deploy_key_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectAdminApi->get_enterprise_id_projects_project_id_deploy_keys_deploy_key_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **deploy_key_id** | **int**| 公钥id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**EnterpriseDeployKey**](EnterpriseDeployKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pr_assigner**
> PrAssigner get_enterprise_id_projects_project_id_pr_assigner(enterprise_id, project_id, access_token=access_token, qt=qt)

查看代码审查设置

查看代码审查设置

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectAdminApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 查看代码审查设置
    api_response = api_instance.get_enterprise_id_projects_project_id_pr_assigner(enterprise_id, project_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectAdminApi->get_enterprise_id_projects_project_id_pr_assigner: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**PrAssigner**](PrAssigner.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_deploy_keys**
> EnterpriseDeployKey post_enterprise_id_projects_project_id_deploy_keys(body, enterprise_id, project_id)

添加部署公钥

添加部署公钥

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectAdminApi()
body = gitee_api.ProjectIdDeployKeysBody() # ProjectIdDeployKeysBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path

try:
    # 添加部署公钥
    api_response = api_instance.post_enterprise_id_projects_project_id_deploy_keys(body, enterprise_id, project_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectAdminApi->post_enterprise_id_projects_project_id_deploy_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdDeployKeysBody**](ProjectIdDeployKeysBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 

### Return type

[**EnterpriseDeployKey**](EnterpriseDeployKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_initialiaze_readme**
> post_enterprise_id_projects_project_id_initialiaze_readme(enterprise_id, project_id, body=body)

初始化README

初始化README

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectAdminApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
body = gitee_api.ProjectIdInitialiazeReadmeBody() # ProjectIdInitialiazeReadmeBody |  (optional)

try:
    # 初始化README
    api_instance.post_enterprise_id_projects_project_id_initialiaze_readme(enterprise_id, project_id, body=body)
except ApiException as e:
    print("Exception when calling ProjectAdminApi->post_enterprise_id_projects_project_id_initialiaze_readme: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **body** | [**ProjectIdInitialiazeReadmeBody**](ProjectIdInitialiazeReadmeBody.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_projects_project_id_deploy_keys_deploy_key_id**
> put_enterprise_id_projects_project_id_deploy_keys_deploy_key_id(body, enterprise_id, project_id, deploy_key_id)

启用/停用公钥

启用/停用公钥

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectAdminApi()
body = gitee_api.DeployKeysDeployKeyIdBody() # DeployKeysDeployKeyIdBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
deploy_key_id = 56 # int | 公钥id

try:
    # 启用/停用公钥
    api_instance.put_enterprise_id_projects_project_id_deploy_keys_deploy_key_id(body, enterprise_id, project_id, deploy_key_id)
except ApiException as e:
    print("Exception when calling ProjectAdminApi->put_enterprise_id_projects_project_id_deploy_keys_deploy_key_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DeployKeysDeployKeyIdBody**](DeployKeysDeployKeyIdBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **deploy_key_id** | **int**| 公钥id | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_projects_project_id_pr_assigner**
> PrAssigner put_enterprise_id_projects_project_id_pr_assigner(enterprise_id, project_id, body=body)

指派审查、测试人员

指派审查、测试人员

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectAdminApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
body = gitee_api.ProjectIdPrAssignerBody() # ProjectIdPrAssignerBody |  (optional)

try:
    # 指派审查、测试人员
    api_response = api_instance.put_enterprise_id_projects_project_id_pr_assigner(enterprise_id, project_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectAdminApi->put_enterprise_id_projects_project_id_pr_assigner: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **body** | [**ProjectIdPrAssignerBody**](ProjectIdPrAssignerBody.md)|  | [optional] 

### Return type

[**PrAssigner**](PrAssigner.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_projects_project_id_status**
> put_enterprise_id_projects_project_id_status(body, enterprise_id, project_id)

仓库归档(状态管理)

仓库归档(状态管理)

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectAdminApi()
body = gitee_api.ProjectIdStatusBody() # ProjectIdStatusBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path

try:
    # 仓库归档(状态管理)
    api_instance.put_enterprise_id_projects_project_id_status(body, enterprise_id, project_id)
except ApiException as e:
    print("Exception when calling ProjectAdminApi->put_enterprise_id_projects_project_id_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdStatusBody**](ProjectIdStatusBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# ProjectAccessLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | [optional] 
**uuid** | **str** | uuid | [optional] 
**user** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**ip** | **str** | ip | [optional] 
**ip_filter** | **str** | 过滤后IP | [optional] 
**stat_type** | **str** | 操作: {0&#x3D;&gt;\&quot;HTTP_ACCESS\&quot;, 1&#x3D;&gt;\&quot;SSH_PULL\&quot;, 2&#x3D;&gt;\&quot;SVN_PULL\&quot;, 3&#x3D;&gt;\&quot;HTTP_PULL\&quot;, 4&#x3D;&gt;\&quot;SSH_PUSH\&quot;, 5&#x3D;&gt;\&quot;SVN_PUSH\&quot;, 6&#x3D;&gt;\&quot;HTTP_PUSH\&quot;, 7&#x3D;&gt;\&quot;DOWNLOAD_ZIP\&quot;} | [optional] 
**stat_type_cn** | **str** | 操作 | [optional] 
**project** | [**Project**](Project.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# DocNodeIdBackupBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**name** | **str** | 文档名称 | 
**parent_id** | **int** | 所属文件夹的 doc_node 的 id，默认为 0 | [optional] 
**program_id** | **int** | 项目 id | [optional] 
**auth_type** | **str** | 权限; 继承: inherit; 私有: private; 只读 read_only; 读写: read_write | [optional] [default to 'inherit']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


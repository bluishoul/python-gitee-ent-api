# FileIdMoveBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**parent_id** | **int** | 文件移动后的父级的 file_id | 
**prev_id** | **int** | 移动后的同级上一个节点的 file_id（默认：0） | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


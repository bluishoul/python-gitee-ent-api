# ProgramIdProjectsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**add_project_ids** | **str** | 仓库ids | 
**import_project_users** | **int** | 是否导入仓库成员 | [optional] [default to Import_project_usersEnum._1]
**import_project_issues** | **int** | 是否导入仓库任务 | [optional] [default to Import_project_issuesEnum._0]
**import_project_milestones** | **int** | 是否导入仓库里程碑 | [optional] [default to Import_project_milestonesEnum._0]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# EnterpriseIdProgramsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**name** | **str** | 项目名称 | 
**description** | **str** | 项目简介 | [optional] 
**assignee_id** | **str** | 负责人ID | [optional] 
**outsourced** | **bool** | 项目类型:内部(false)/外包(true)项目 | [optional] 
**status** | **int** | 项目状态:（0:开始 1:暂停 2:关闭） | [optional] 
**color** | **str** | 颜色 | [optional] 
**ident** | **str** | 项目编号 | [optional] 
**category** | **str** | 项目类型(standard、scrum、kanban) | [optional] 
**project_ids** | **str** | 关联仓库ids，逗号隔开 | [optional] 
**import_project_users** | **bool** | 是否导入仓库成员 | [optional] 
**import_project_issues** | **bool** | 是否导入仓库任务 | [optional] 
**import_project_milestones** | **bool** | 是否导入仓库里程碑 | [optional] 
**user_ids** | **str** | 成员ids，逗号隔开 | [optional] 
**group_ids** | **str** | 团队ids，逗号隔开 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


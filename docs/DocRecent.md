# DocRecent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 文件 id | [optional] 
**name** | **str** | 文件名称 | [optional] 
**parent_id** | **int** | 父级 id | [optional] 
**editor** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**editing_user** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**creator** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**wiki_info** | [**WikiInfo**](WikiInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


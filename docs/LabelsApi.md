# gitee_api.LabelsApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_labels_label_id**](LabelsApi.md#delete_enterprise_id_labels_label_id) | **DELETE** /{enterprise_id}/labels/{label_id} | 删除标签
[**get_enterprise_id_labels**](LabelsApi.md#get_enterprise_id_labels) | **GET** /{enterprise_id}/labels | 获取标签列表
[**post_enterprise_id_labels**](LabelsApi.md#post_enterprise_id_labels) | **POST** /{enterprise_id}/labels | 新增标签
[**put_enterprise_id_labels_change_serial**](LabelsApi.md#put_enterprise_id_labels_change_serial) | **PUT** /{enterprise_id}/labels/change_serial | 更新标签排序
[**put_enterprise_id_labels_label_id**](LabelsApi.md#put_enterprise_id_labels_label_id) | **PUT** /{enterprise_id}/labels/{label_id} | 更新标签

# **delete_enterprise_id_labels_label_id**
> delete_enterprise_id_labels_label_id(enterprise_id, label_id, access_token=access_token)

删除标签

删除标签

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.LabelsApi()
enterprise_id = 56 # int | 
label_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 删除标签
    api_instance.delete_enterprise_id_labels_label_id(enterprise_id, label_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling LabelsApi->delete_enterprise_id_labels_label_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**|  | 
 **label_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_labels**
> list[Label] get_enterprise_id_labels(enterprise_id, access_token=access_token, sort=sort, direction=direction, search=search, label_ids=label_ids, page=page, per_page=per_page)

获取标签列表

获取标签列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.LabelsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
sort = 'sort_example' # str | 排序字段(created_at、updated_at, serial) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
search = 'search_example' # str | 搜索关键字 (optional)
label_ids = 'label_ids_example' # str | 根据 id 返回标签列表 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取标签列表
    api_response = api_instance.get_enterprise_id_labels(enterprise_id, access_token=access_token, sort=sort, direction=direction, search=search, label_ids=label_ids, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LabelsApi->get_enterprise_id_labels: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **sort** | **str**| 排序字段(created_at、updated_at, serial) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **search** | **str**| 搜索关键字 | [optional] 
 **label_ids** | **str**| 根据 id 返回标签列表 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[Label]**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_labels**
> Label post_enterprise_id_labels(body, enterprise_id)

新增标签

新增标签

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.LabelsApi()
body = gitee_api.EnterpriseIdLabelsBody() # EnterpriseIdLabelsBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 新增标签
    api_response = api_instance.post_enterprise_id_labels(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LabelsApi->post_enterprise_id_labels: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnterpriseIdLabelsBody**](EnterpriseIdLabelsBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_labels_change_serial**
> put_enterprise_id_labels_change_serial(body, enterprise_id)

更新标签排序

更新标签排序

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.LabelsApi()
body = gitee_api.LabelsChangeSerialBody() # LabelsChangeSerialBody | 
enterprise_id = 56 # int | 

try:
    # 更新标签排序
    api_instance.put_enterprise_id_labels_change_serial(body, enterprise_id)
except ApiException as e:
    print("Exception when calling LabelsApi->put_enterprise_id_labels_change_serial: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**LabelsChangeSerialBody**](LabelsChangeSerialBody.md)|  | 
 **enterprise_id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_labels_label_id**
> Label put_enterprise_id_labels_label_id(body, enterprise_id, label_id)

更新标签

更新标签

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.LabelsApi()
body = gitee_api.LabelsLabelIdBody() # LabelsLabelIdBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
label_id = 56 # int | 标签 id

try:
    # 更新标签
    api_response = api_instance.put_enterprise_id_labels_label_id(body, enterprise_id, label_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LabelsApi->put_enterprise_id_labels_label_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**LabelsLabelIdBody**](LabelsLabelIdBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **label_id** | **int**| 标签 id | 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# TemplatesTemplateIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**name** | **str** | 模版名称 | [optional] 
**content** | **str** | 模版内容 | [optional] 
**is_default** | **int** | 是否设为默认模版。0(否)或1(是)，默认0(否) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


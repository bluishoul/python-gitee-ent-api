# PullRequestsPullRequestIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**qt** | **str** | path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
**milestone_id** | **int** | 关联的里程碑 id | [optional] 
**related_issue_id** | **str** | 关联的任务 id。如有多个，用英文逗号分隔。eg: 1,2,3 | [optional] 
**title** | **str** | PR 标题 | [optional] 
**body** | **str** | PR 正文内容 | [optional] 
**priority** | **int** | 优先级(0: 不指定 1: 不重要 2: 次要 3: 主要 4: 严重) | [optional] 
**state_event** | **str** | 关闭 PR | [optional] 
**label_ids** | **str** | 关联的标签 id。如有多个，用英文逗号分隔。eg: 1,2,3 | [optional] 
**close_related_issue** | **int** | 是否需要在合并 PR 后关闭关联的任务. 0: 否 1: 是 | [optional] 
**prune_branch** | **int** | 是否需要在合并 PR 后删除提交分支. 0: 否 1: 是 | [optional] 
**assignee_id** | **str** | 审查人员的用户 id。如有多个，用英文逗号分隔。eg: 1,2,3 | [optional] 
**pr_assign_num** | **int** | 至少需要{pr_assign_num}名审查人员审查通过后可合并 | [optional] 
**tester_id** | **str** | 测试人员的用户 id。如有多个，用英文逗号分隔。eg: 1,2,3 | [optional] 
**pr_test_num** | **int** | 至少需要{pr_assign_num}名测试人员测试通过后可合并 | [optional] 
**check_state** | **int** | 审查状态. 0: 非必须审查 1: 必须审查 | [optional] 
**test_state** | **int** | 测试状态. 0: 非必须测试 1: 必须测试 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# DocNodesAttachFileBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**file** | **str** | 上传的文件 | 
**program_id** | **int** | 项目 id | [optional] 
**parent_id** | **int** | 父级目录 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


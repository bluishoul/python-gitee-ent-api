# ScrumVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 版本ID | [optional] 
**enterprise_id** | **int** | 企业ID | [optional] 
**program_id** | **int** | 项目ID | [optional] 
**number** | **str** | 版本号 | [optional] 
**plan_released_at** | **datetime** | 计划发版时间 | [optional] 
**released_at** | **datetime** | 实际发版时间 | [optional] 
**title** | **str** | 标题 | [optional] 
**description** | **str** | 描述 | [optional] 
**state** | **str** | 状态 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# gitee_api.IssuesApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_issues_issue_id**](IssuesApi.md#delete_enterprise_id_issues_issue_id) | **DELETE** /{enterprise_id}/issues/{issue_id} | 删除任务
[**delete_enterprise_id_issues_issue_id_attach_files_attach_file_id**](IssuesApi.md#delete_enterprise_id_issues_issue_id_attach_files_attach_file_id) | **DELETE** /{enterprise_id}/issues/{issue_id}/attach_files/{attach_file_id} | 删除任务附件
[**delete_enterprise_id_issues_issue_id_link_pull_request_pull_request_id**](IssuesApi.md#delete_enterprise_id_issues_issue_id_link_pull_request_pull_request_id) | **DELETE** /{enterprise_id}/issues/{issue_id}/link_pull_request/{pull_request_id} | 解除任务与 Pull Request 的关联
[**delete_enterprise_id_issues_issue_id_notes_note_id**](IssuesApi.md#delete_enterprise_id_issues_issue_id_notes_note_id) | **DELETE** /{enterprise_id}/issues/{issue_id}/notes/{note_id} | 删除某任务下评论
[**delete_enterprise_id_issues_issue_id_related_issue_link_issue_id**](IssuesApi.md#delete_enterprise_id_issues_issue_id_related_issue_link_issue_id) | **DELETE** /{enterprise_id}/issues/{issue_id}/related_issue/{link_issue_id} | 移除任务的关联关系
[**delete_enterprise_id_issues_issue_id_star**](IssuesApi.md#delete_enterprise_id_issues_issue_id_star) | **DELETE** /{enterprise_id}/issues/{issue_id}/star | 取消星标任务
[**get_enterprise_id_issues**](IssuesApi.md#get_enterprise_id_issues) | **GET** /{enterprise_id}/issues | 获取任务列表
[**get_enterprise_id_issues_board_members**](IssuesApi.md#get_enterprise_id_issues_board_members) | **GET** /{enterprise_id}/issues/board_members | 获取成员看板的成员列表
[**get_enterprise_id_issues_issue_id**](IssuesApi.md#get_enterprise_id_issues_issue_id) | **GET** /{enterprise_id}/issues/{issue_id} | 获取任务详情
[**get_enterprise_id_issues_issue_id_attach_files**](IssuesApi.md#get_enterprise_id_issues_issue_id_attach_files) | **GET** /{enterprise_id}/issues/{issue_id}/attach_files | 获取任务附件
[**get_enterprise_id_issues_issue_id_auths**](IssuesApi.md#get_enterprise_id_issues_issue_id_auths) | **GET** /{enterprise_id}/issues/{issue_id}/auths | 获取授权用户对任务的权限
[**get_enterprise_id_issues_issue_id_issue_states**](IssuesApi.md#get_enterprise_id_issues_issue_id_issue_states) | **GET** /{enterprise_id}/issues/{issue_id}/issue_states | 获取当前任务可流转的下一状态
[**get_enterprise_id_issues_issue_id_link_issues**](IssuesApi.md#get_enterprise_id_issues_issue_id_link_issues) | **GET** /{enterprise_id}/issues/{issue_id}/link_issues | 获取任务的关联任务
[**get_enterprise_id_issues_issue_id_link_pull_request**](IssuesApi.md#get_enterprise_id_issues_issue_id_link_pull_request) | **GET** /{enterprise_id}/issues/{issue_id}/link_pull_request | 获取任务关联的 Pull Request
[**get_enterprise_id_issues_issue_id_notes**](IssuesApi.md#get_enterprise_id_issues_issue_id_notes) | **GET** /{enterprise_id}/issues/{issue_id}/notes | 获取任务下的评论列表
[**get_enterprise_id_issues_issue_id_operate_logs**](IssuesApi.md#get_enterprise_id_issues_issue_id_operate_logs) | **GET** /{enterprise_id}/issues/{issue_id}/operate_logs | 获取任务的操作日志列表
[**get_enterprise_id_issues_issue_stats**](IssuesApi.md#get_enterprise_id_issues_issue_stats) | **GET** /{enterprise_id}/issues/issue_stats | 获取企业下用户任务相关数量数据
[**get_enterprise_id_issues_link_issues**](IssuesApi.md#get_enterprise_id_issues_link_issues) | **GET** /{enterprise_id}/issues/link_issues | 可选的关联任务集
[**get_enterprise_id_issues_member_select**](IssuesApi.md#get_enterprise_id_issues_member_select) | **GET** /{enterprise_id}/issues/member_select | 获取任务指派的成员列表
[**post_enterprise_id_issues**](IssuesApi.md#post_enterprise_id_issues) | **POST** /{enterprise_id}/issues | 新建任务
[**post_enterprise_id_issues_filter**](IssuesApi.md#post_enterprise_id_issues_filter) | **POST** /{enterprise_id}/issues/filter | 获取任务列表-筛选器查询
[**post_enterprise_id_issues_issue_id_link_pull_request_pull_request_id**](IssuesApi.md#post_enterprise_id_issues_issue_id_link_pull_request_pull_request_id) | **POST** /{enterprise_id}/issues/{issue_id}/link_pull_request/{pull_request_id} | 任务关联 Pull Request
[**post_enterprise_id_issues_issue_id_notes**](IssuesApi.md#post_enterprise_id_issues_issue_id_notes) | **POST** /{enterprise_id}/issues/{issue_id}/notes | 评论任务
[**post_enterprise_id_issues_issue_id_reaction**](IssuesApi.md#post_enterprise_id_issues_issue_id_reaction) | **POST** /{enterprise_id}/issues/{issue_id}/reaction | 新增任务表态
[**post_enterprise_id_issues_issue_id_related_issue**](IssuesApi.md#post_enterprise_id_issues_issue_id_related_issue) | **POST** /{enterprise_id}/issues/{issue_id}/related_issue | 为指定任务添加关联任务
[**post_enterprise_id_issues_issue_id_star**](IssuesApi.md#post_enterprise_id_issues_issue_id_star) | **POST** /{enterprise_id}/issues/{issue_id}/star | 星标任务
[**put_enterprise_id_issues_issue_id**](IssuesApi.md#put_enterprise_id_issues_issue_id) | **PUT** /{enterprise_id}/issues/{issue_id} | 更新任务
[**put_enterprise_id_issues_issue_id_notes_note_id**](IssuesApi.md#put_enterprise_id_issues_issue_id_notes_note_id) | **PUT** /{enterprise_id}/issues/{issue_id}/notes/{note_id} | 修改任务下的评论
[**put_enterprise_id_issues_issue_id_related_issue_link_issue_id**](IssuesApi.md#put_enterprise_id_issues_issue_id_related_issue_link_issue_id) | **PUT** /{enterprise_id}/issues/{issue_id}/related_issue/{link_issue_id} | 更新任务的关联关系

# **delete_enterprise_id_issues_issue_id**
> delete_enterprise_id_issues_issue_id(enterprise_id, issue_id, access_token=access_token)

删除任务

删除任务

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 删除任务
    api_instance.delete_enterprise_id_issues_issue_id(enterprise_id, issue_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssuesApi->delete_enterprise_id_issues_issue_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_issues_issue_id_attach_files_attach_file_id**
> delete_enterprise_id_issues_issue_id_attach_files_attach_file_id(enterprise_id, issue_id, attach_file_id, access_token=access_token)

删除任务附件

删除任务附件

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务的 id
attach_file_id = 56 # int | 附件 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 删除任务附件
    api_instance.delete_enterprise_id_issues_issue_id_attach_files_attach_file_id(enterprise_id, issue_id, attach_file_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssuesApi->delete_enterprise_id_issues_issue_id_attach_files_attach_file_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务的 id | 
 **attach_file_id** | **int**| 附件 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_issues_issue_id_link_pull_request_pull_request_id**
> delete_enterprise_id_issues_issue_id_link_pull_request_pull_request_id(enterprise_id, issue_id, pull_request_id, access_token=access_token)

解除任务与 Pull Request 的关联

解除任务与 Pull Request 的关联

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id
pull_request_id = 56 # int | Pull Request id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 解除任务与 Pull Request 的关联
    api_instance.delete_enterprise_id_issues_issue_id_link_pull_request_pull_request_id(enterprise_id, issue_id, pull_request_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssuesApi->delete_enterprise_id_issues_issue_id_link_pull_request_pull_request_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 
 **pull_request_id** | **int**| Pull Request id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_issues_issue_id_notes_note_id**
> delete_enterprise_id_issues_issue_id_notes_note_id(enterprise_id, issue_id, note_id, access_token=access_token)

删除某任务下评论

删除某任务下评论

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id
note_id = 56 # int | 评论的 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 删除某任务下评论
    api_instance.delete_enterprise_id_issues_issue_id_notes_note_id(enterprise_id, issue_id, note_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssuesApi->delete_enterprise_id_issues_issue_id_notes_note_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 
 **note_id** | **int**| 评论的 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_issues_issue_id_related_issue_link_issue_id**
> delete_enterprise_id_issues_issue_id_related_issue_link_issue_id(enterprise_id, issue_id, link_issue_id, access_token=access_token)

移除任务的关联关系

移除任务的关联关系

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id
link_issue_id = 56 # int | 待删除的关联的任务id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 移除任务的关联关系
    api_instance.delete_enterprise_id_issues_issue_id_related_issue_link_issue_id(enterprise_id, issue_id, link_issue_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssuesApi->delete_enterprise_id_issues_issue_id_related_issue_link_issue_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 
 **link_issue_id** | **int**| 待删除的关联的任务id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_issues_issue_id_star**
> delete_enterprise_id_issues_issue_id_star(enterprise_id, issue_id, access_token=access_token)

取消星标任务

取消星标任务

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 取消星标任务
    api_instance.delete_enterprise_id_issues_issue_id_star(enterprise_id, issue_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssuesApi->delete_enterprise_id_issues_issue_id_star: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issues**
> list[Issue] get_enterprise_id_issues(enterprise_id, access_token=access_token, project_id=project_id, program_id=program_id, milestone_id=milestone_id, state=state, only_related_me=only_related_me, assignee_id=assignee_id, author_id=author_id, collaborator_ids=collaborator_ids, created_at=created_at, finished_at=finished_at, plan_started_at=plan_started_at, deadline=deadline, search=search, filter_child=filter_child, issue_state_ids=issue_state_ids, issue_type_id=issue_type_id, label_ids=label_ids, priority=priority, scrum_sprint_ids=scrum_sprint_ids, scrum_version_ids=scrum_version_ids, kanban_ids=kanban_ids, kanban_column_ids=kanban_column_ids, sort=sort, direction=direction, page=page, per_page=per_page)

获取任务列表

获取任务列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
project_id = 'project_id_example' # str | 仓库 id (optional)
program_id = 'program_id_example' # str | 项目 id (optional)
milestone_id = 'milestone_id_example' # str | 里程碑 id (optional)
state = 'state_example' # str | 任务状态属性，可多选，用逗号分隔。（开启：open 关闭：closed 拒绝：rejected 进行中: progressing） (optional)
only_related_me = 'only_related_me_example' # str | 是否仅列出与授权用户相关的任务（0: 否 1: 是） (optional)
assignee_id = 'assignee_id_example' # str | 负责人 id (optional)
author_id = 'author_id_example' # str | 创建者 id (optional)
collaborator_ids = 'collaborator_ids_example' # str | 协作者。(,分隔的id字符串) (optional)
created_at = 'created_at_example' # str | 创建时间，格式：(区间)yyyymmddTHH:MM:SS+08:00-yyyymmddTHH:MM:SS+08:00，（指定某日期）yyyymmddTHH:MM:SS+08:00，（小于指定日期）<yyyymmddTHH:MM:SS+08:00，（大于指定日期）>yyyymmddTHH:MM:SS+08:00 (optional)
finished_at = 'finished_at_example' # str | 任务完成日期，格式同上 (optional)
plan_started_at = 'plan_started_at_example' # str | 计划开始时间，(格式：yyyy-mm-dd) (optional)
deadline = 'deadline_example' # str | 任务截止日期，(格式：yyyy-mm-dd) (optional)
search = 'search_example' # str | 搜索参数 (optional)
filter_child = 'filter_child_example' # str | 是否过滤子任务(0: 否, 1: 是) (optional)
issue_state_ids = 'issue_state_ids_example' # str | 任务状态id，多选，用逗号分隔。 (optional)
issue_type_id = 'issue_type_id_example' # str | 任务类型 (optional)
label_ids = 'label_ids_example' # str | 标签 id（可多选，用逗号分隔） (optional)
priority = 'priority_example' # str | 优先级（可多选，用逗号分隔） (optional)
scrum_sprint_ids = 'scrum_sprint_ids_example' # str | 迭代id(可多选，用逗号分隔) (optional)
scrum_version_ids = 'scrum_version_ids_example' # str | 版本id(可多选，用逗号分隔) (optional)
kanban_ids = 'kanban_ids_example' # str | 看板id(可多选，用逗号分隔) (optional)
kanban_column_ids = 'kanban_column_ids_example' # str | 看板栏id(可多选，用逗号分隔) (optional)
sort = 'sort_example' # str | 排序字段(created_at、updated_at、deadline、priority) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取任务列表
    api_response = api_instance.get_enterprise_id_issues(enterprise_id, access_token=access_token, project_id=project_id, program_id=program_id, milestone_id=milestone_id, state=state, only_related_me=only_related_me, assignee_id=assignee_id, author_id=author_id, collaborator_ids=collaborator_ids, created_at=created_at, finished_at=finished_at, plan_started_at=plan_started_at, deadline=deadline, search=search, filter_child=filter_child, issue_state_ids=issue_state_ids, issue_type_id=issue_type_id, label_ids=label_ids, priority=priority, scrum_sprint_ids=scrum_sprint_ids, scrum_version_ids=scrum_version_ids, kanban_ids=kanban_ids, kanban_column_ids=kanban_column_ids, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_enterprise_id_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **project_id** | **str**| 仓库 id | [optional] 
 **program_id** | **str**| 项目 id | [optional] 
 **milestone_id** | **str**| 里程碑 id | [optional] 
 **state** | **str**| 任务状态属性，可多选，用逗号分隔。（开启：open 关闭：closed 拒绝：rejected 进行中: progressing） | [optional] 
 **only_related_me** | **str**| 是否仅列出与授权用户相关的任务（0: 否 1: 是） | [optional] 
 **assignee_id** | **str**| 负责人 id | [optional] 
 **author_id** | **str**| 创建者 id | [optional] 
 **collaborator_ids** | **str**| 协作者。(,分隔的id字符串) | [optional] 
 **created_at** | **str**| 创建时间，格式：(区间)yyyymmddTHH:MM:SS+08:00-yyyymmddTHH:MM:SS+08:00，（指定某日期）yyyymmddTHH:MM:SS+08:00，（小于指定日期）&lt;yyyymmddTHH:MM:SS+08:00，（大于指定日期）&gt;yyyymmddTHH:MM:SS+08:00 | [optional] 
 **finished_at** | **str**| 任务完成日期，格式同上 | [optional] 
 **plan_started_at** | **str**| 计划开始时间，(格式：yyyy-mm-dd) | [optional] 
 **deadline** | **str**| 任务截止日期，(格式：yyyy-mm-dd) | [optional] 
 **search** | **str**| 搜索参数 | [optional] 
 **filter_child** | **str**| 是否过滤子任务(0: 否, 1: 是) | [optional] 
 **issue_state_ids** | **str**| 任务状态id，多选，用逗号分隔。 | [optional] 
 **issue_type_id** | **str**| 任务类型 | [optional] 
 **label_ids** | **str**| 标签 id（可多选，用逗号分隔） | [optional] 
 **priority** | **str**| 优先级（可多选，用逗号分隔） | [optional] 
 **scrum_sprint_ids** | **str**| 迭代id(可多选，用逗号分隔) | [optional] 
 **scrum_version_ids** | **str**| 版本id(可多选，用逗号分隔) | [optional] 
 **kanban_ids** | **str**| 看板id(可多选，用逗号分隔) | [optional] 
 **kanban_column_ids** | **str**| 看板栏id(可多选，用逗号分隔) | [optional] 
 **sort** | **str**| 排序字段(created_at、updated_at、deadline、priority) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[Issue]**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issues_board_members**
> list[UserWithRemark] get_enterprise_id_issues_board_members(enterprise_id, access_token=access_token, assignee_ids=assignee_ids, project_id=project_id, group_ids=group_ids, program_id=program_id, milestone_id=milestone_id, issue_type_id=issue_type_id, page=page, per_page=per_page)

获取成员看板的成员列表

获取成员看板的成员列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业ID
access_token = 'access_token_example' # str | 用户授权码 (optional)
assignee_ids = 'assignee_ids_example' # str | 以,分隔的负责人ID字符串, 如: 23,12 (optional)
project_id = 56 # int | 仓库ID (optional)
group_ids = 'group_ids_example' # str | 以,分隔的组织ID字符串 (optional)
program_id = 56 # int | 项目ID (optional)
milestone_id = 56 # int | 里程碑ID (optional)
issue_type_id = 56 # int | 任务类型ID (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取成员看板的成员列表
    api_response = api_instance.get_enterprise_id_issues_board_members(enterprise_id, access_token=access_token, assignee_ids=assignee_ids, project_id=project_id, group_ids=group_ids, program_id=program_id, milestone_id=milestone_id, issue_type_id=issue_type_id, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_enterprise_id_issues_board_members: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业ID | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **assignee_ids** | **str**| 以,分隔的负责人ID字符串, 如: 23,12 | [optional] 
 **project_id** | **int**| 仓库ID | [optional] 
 **group_ids** | **str**| 以,分隔的组织ID字符串 | [optional] 
 **program_id** | **int**| 项目ID | [optional] 
 **milestone_id** | **int**| 里程碑ID | [optional] 
 **issue_type_id** | **int**| 任务类型ID | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[UserWithRemark]**](UserWithRemark.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issues_issue_id**
> IssueDetail get_enterprise_id_issues_issue_id(enterprise_id, issue_id, access_token=access_token, qt=qt)

获取任务详情

获取任务详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务的 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | 查询方式(ident/id) (optional)

try:
    # 获取任务详情
    api_response = api_instance.get_enterprise_id_issues_issue_id(enterprise_id, issue_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_enterprise_id_issues_issue_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务的 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| 查询方式(ident/id) | [optional] 

### Return type

[**IssueDetail**](IssueDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issues_issue_id_attach_files**
> AttachFile get_enterprise_id_issues_issue_id_attach_files(enterprise_id, issue_id, access_token=access_token)

获取任务附件

获取任务附件

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务的 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取任务附件
    api_response = api_instance.get_enterprise_id_issues_issue_id_attach_files(enterprise_id, issue_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_enterprise_id_issues_issue_id_attach_files: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务的 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**AttachFile**](AttachFile.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issues_issue_id_auths**
> IssueAuth get_enterprise_id_issues_issue_id_auths(enterprise_id, issue_id, access_token=access_token)

获取授权用户对任务的权限

获取授权用户对任务的权限

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务的 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取授权用户对任务的权限
    api_response = api_instance.get_enterprise_id_issues_issue_id_auths(enterprise_id, issue_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_enterprise_id_issues_issue_id_auths: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务的 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**IssueAuth**](IssueAuth.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issues_issue_id_issue_states**
> IssueTypeStateRef get_enterprise_id_issues_issue_id_issue_states(enterprise_id, issue_id, access_token=access_token)

获取当前任务可流转的下一状态

获取当前任务可流转的下一状态

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务的 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取当前任务可流转的下一状态
    api_response = api_instance.get_enterprise_id_issues_issue_id_issue_states(enterprise_id, issue_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_enterprise_id_issues_issue_id_issue_states: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务的 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**IssueTypeStateRef**](IssueTypeStateRef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issues_issue_id_link_issues**
> LinkIssue get_enterprise_id_issues_issue_id_link_issues(enterprise_id, issue_id, access_token=access_token)

获取任务的关联任务

获取任务的关联任务

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取任务的关联任务
    api_response = api_instance.get_enterprise_id_issues_issue_id_link_issues(enterprise_id, issue_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_enterprise_id_issues_issue_id_link_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**LinkIssue**](LinkIssue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issues_issue_id_link_pull_request**
> get_enterprise_id_issues_issue_id_link_pull_request(enterprise_id, issue_id, access_token=access_token, page=page, per_page=per_page)

获取任务关联的 Pull Request

获取任务关联的 Pull Request

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取任务关联的 Pull Request
    api_instance.get_enterprise_id_issues_issue_id_link_pull_request(enterprise_id, issue_id, access_token=access_token, page=page, per_page=per_page)
except ApiException as e:
    print("Exception when calling IssuesApi->get_enterprise_id_issues_issue_id_link_pull_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issues_issue_id_notes**
> IssueNote get_enterprise_id_issues_issue_id_notes(enterprise_id, issue_id, access_token=access_token, sort=sort, direction=direction, page=page, per_page=per_page)

获取任务下的评论列表

获取任务下的评论列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
sort = 'sort_example' # str | 排序字段(name || created_at) (optional)
direction = 'desc' # str | 可选。升序/降序 (optional) (default to desc)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取任务下的评论列表
    api_response = api_instance.get_enterprise_id_issues_issue_id_notes(enterprise_id, issue_id, access_token=access_token, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_enterprise_id_issues_issue_id_notes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **sort** | **str**| 排序字段(name || created_at) | [optional] 
 **direction** | **str**| 可选。升序/降序 | [optional] [default to desc]
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**IssueNote**](IssueNote.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issues_issue_id_operate_logs**
> IssueOperateLog get_enterprise_id_issues_issue_id_operate_logs(enterprise_id, issue_id, access_token=access_token, prev_id=prev_id, last_id=last_id, limit=limit)

获取任务的操作日志列表

获取任务的操作日志列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务的 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
prev_id = 56 # int | 前一个日志 id, 用作分页 (optional)
last_id = 56 # int | 后一个日志 id, 用作分页 (optional)
limit = 56 # int | 限制多少个日志记录 (optional)

try:
    # 获取任务的操作日志列表
    api_response = api_instance.get_enterprise_id_issues_issue_id_operate_logs(enterprise_id, issue_id, access_token=access_token, prev_id=prev_id, last_id=last_id, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_enterprise_id_issues_issue_id_operate_logs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务的 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **prev_id** | **int**| 前一个日志 id, 用作分页 | [optional] 
 **last_id** | **int**| 后一个日志 id, 用作分页 | [optional] 
 **limit** | **int**| 限制多少个日志记录 | [optional] 

### Return type

[**IssueOperateLog**](IssueOperateLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issues_issue_stats**
> get_enterprise_id_issues_issue_stats(enterprise_id, access_token=access_token)

获取企业下用户任务相关数量数据

获取企业下用户任务相关数量数据

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取企业下用户任务相关数量数据
    api_instance.get_enterprise_id_issues_issue_stats(enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssuesApi->get_enterprise_id_issues_issue_stats: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issues_link_issues**
> list[Issue] get_enterprise_id_issues_link_issues(enterprise_id, access_token=access_token, issue_id=issue_id, program_id=program_id, search=search, page=page, per_page=per_page)

可选的关联任务集

可选的关联任务集

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
issue_id = 56 # int | 任务 id (optional)
program_id = 56 # int | 项目 id (optional)
search = 'search_example' # str | 搜索关键字 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 可选的关联任务集
    api_response = api_instance.get_enterprise_id_issues_link_issues(enterprise_id, access_token=access_token, issue_id=issue_id, program_id=program_id, search=search, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_enterprise_id_issues_link_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **issue_id** | **int**| 任务 id | [optional] 
 **program_id** | **int**| 项目 id | [optional] 
 **search** | **str**| 搜索关键字 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[Issue]**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issues_member_select**
> list[IssueMemberSelect] get_enterprise_id_issues_member_select(enterprise_id, access_token=access_token, program_id=program_id, project_id=project_id)

获取任务指派的成员列表

获取任务指派的成员列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
program_id = 'program_id_example' # str | 项目 ID (optional)
project_id = 'project_id_example' # str | 仓库 ID (optional)

try:
    # 获取任务指派的成员列表
    api_response = api_instance.get_enterprise_id_issues_member_select(enterprise_id, access_token=access_token, program_id=program_id, project_id=project_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->get_enterprise_id_issues_member_select: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **program_id** | **str**| 项目 ID | [optional] 
 **project_id** | **str**| 仓库 ID | [optional] 

### Return type

[**list[IssueMemberSelect]**](IssueMemberSelect.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_issues**
> IssueDetail post_enterprise_id_issues(body, enterprise_id)

新建任务

新建任务

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
body = gitee_api.EnterpriseIdIssuesBody() # EnterpriseIdIssuesBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 新建任务
    api_response = api_instance.post_enterprise_id_issues(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->post_enterprise_id_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnterpriseIdIssuesBody**](EnterpriseIdIssuesBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

[**IssueDetail**](IssueDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_issues_filter**
> list[Issue] post_enterprise_id_issues_filter(body, enterprise_id)

获取任务列表-筛选器查询

获取任务列表-筛选器查询

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
body = gitee_api.IssuesFilterBody() # IssuesFilterBody | 
enterprise_id = 56 # int | 

try:
    # 获取任务列表-筛选器查询
    api_response = api_instance.post_enterprise_id_issues_filter(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->post_enterprise_id_issues_filter: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**IssuesFilterBody**](IssuesFilterBody.md)|  | 
 **enterprise_id** | **int**|  | 

### Return type

[**list[Issue]**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_issues_issue_id_link_pull_request_pull_request_id**
> post_enterprise_id_issues_issue_id_link_pull_request_pull_request_id(enterprise_id, issue_id, pull_request_id, body=body)

任务关联 Pull Request

任务关联 Pull Request

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id
pull_request_id = 56 # int | Pull Request id
body = gitee_api.LinkPullRequestPullRequestIdBody() # LinkPullRequestPullRequestIdBody |  (optional)

try:
    # 任务关联 Pull Request
    api_instance.post_enterprise_id_issues_issue_id_link_pull_request_pull_request_id(enterprise_id, issue_id, pull_request_id, body=body)
except ApiException as e:
    print("Exception when calling IssuesApi->post_enterprise_id_issues_issue_id_link_pull_request_pull_request_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 
 **pull_request_id** | **int**| Pull Request id | 
 **body** | [**LinkPullRequestPullRequestIdBody**](LinkPullRequestPullRequestIdBody.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_issues_issue_id_notes**
> IssueNote post_enterprise_id_issues_issue_id_notes(body, enterprise_id, issue_id)

评论任务

评论任务

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
body = gitee_api.IssueIdNotesBody() # IssueIdNotesBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id

try:
    # 评论任务
    api_response = api_instance.post_enterprise_id_issues_issue_id_notes(body, enterprise_id, issue_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->post_enterprise_id_issues_issue_id_notes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**IssueIdNotesBody**](IssueIdNotesBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 

### Return type

[**IssueNote**](IssueNote.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_issues_issue_id_reaction**
> IssueReaction post_enterprise_id_issues_issue_id_reaction(body, enterprise_id, issue_id)

新增任务表态

新增任务表态

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
body = gitee_api.IssueIdReactionBody() # IssueIdReactionBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务的 id

try:
    # 新增任务表态
    api_response = api_instance.post_enterprise_id_issues_issue_id_reaction(body, enterprise_id, issue_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->post_enterprise_id_issues_issue_id_reaction: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**IssueIdReactionBody**](IssueIdReactionBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务的 id | 

### Return type

[**IssueReaction**](IssueReaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_issues_issue_id_related_issue**
> IssueDetail post_enterprise_id_issues_issue_id_related_issue(body, enterprise_id, issue_id)

为指定任务添加关联任务

为指定任务添加关联任务

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
body = gitee_api.IssueIdRelatedIssueBody() # IssueIdRelatedIssueBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id

try:
    # 为指定任务添加关联任务
    api_response = api_instance.post_enterprise_id_issues_issue_id_related_issue(body, enterprise_id, issue_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->post_enterprise_id_issues_issue_id_related_issue: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**IssueIdRelatedIssueBody**](IssueIdRelatedIssueBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 

### Return type

[**IssueDetail**](IssueDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_issues_issue_id_star**
> post_enterprise_id_issues_issue_id_star(enterprise_id, issue_id, body=body)

星标任务

星标任务

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id
body = gitee_api.IssueIdStarBody() # IssueIdStarBody |  (optional)

try:
    # 星标任务
    api_instance.post_enterprise_id_issues_issue_id_star(enterprise_id, issue_id, body=body)
except ApiException as e:
    print("Exception when calling IssuesApi->post_enterprise_id_issues_issue_id_star: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 
 **body** | [**IssueIdStarBody**](IssueIdStarBody.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_issues_issue_id**
> IssueDetail put_enterprise_id_issues_issue_id(enterprise_id, issue_id, body=body)

更新任务

更新任务

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id
body = gitee_api.IssuesIssueIdBody() # IssuesIssueIdBody |  (optional)

try:
    # 更新任务
    api_response = api_instance.put_enterprise_id_issues_issue_id(enterprise_id, issue_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->put_enterprise_id_issues_issue_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 
 **body** | [**IssuesIssueIdBody**](IssuesIssueIdBody.md)|  | [optional] 

### Return type

[**IssueDetail**](IssueDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_issues_issue_id_notes_note_id**
> IssueNote put_enterprise_id_issues_issue_id_notes_note_id(body, enterprise_id, issue_id, note_id)

修改任务下的评论

修改任务下的评论

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
body = gitee_api.NotesNoteIdBody() # NotesNoteIdBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id
note_id = 56 # int | 

try:
    # 修改任务下的评论
    api_response = api_instance.put_enterprise_id_issues_issue_id_notes_note_id(body, enterprise_id, issue_id, note_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->put_enterprise_id_issues_issue_id_notes_note_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**NotesNoteIdBody**](NotesNoteIdBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 
 **note_id** | **int**|  | 

### Return type

[**IssueNote**](IssueNote.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_issues_issue_id_related_issue_link_issue_id**
> IssueDetail put_enterprise_id_issues_issue_id_related_issue_link_issue_id(enterprise_id, issue_id, link_issue_id, body=body)

更新任务的关联关系

更新任务的关联关系

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssuesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_id = 'issue_id_example' # str | 任务 id
link_issue_id = 56 # int | 需要关联的任务 id
body = gitee_api.RelatedIssueLinkIssueIdBody() # RelatedIssueLinkIssueIdBody |  (optional)

try:
    # 更新任务的关联关系
    api_response = api_instance.put_enterprise_id_issues_issue_id_related_issue_link_issue_id(enterprise_id, issue_id, link_issue_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssuesApi->put_enterprise_id_issues_issue_id_related_issue_link_issue_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_id** | **str**| 任务 id | 
 **link_issue_id** | **int**| 需要关联的任务 id | 
 **body** | [**RelatedIssueLinkIssueIdBody**](RelatedIssueLinkIssueIdBody.md)|  | [optional] 

### Return type

[**IssueDetail**](IssueDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


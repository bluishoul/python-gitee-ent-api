# MemberApplicationsMemberApplicationIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**role_id** | **int** | 角色id。企业角色列表 scope: can_invite | [optional] 
**state** | **str** | 通过/拒绝 | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


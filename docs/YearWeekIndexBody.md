# YearWeekIndexBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**content** | **str** | 周报内容 | 
**pull_request_ids** | **list[int]** | 周报关联PR | [optional] 
**issue_ids** | **list[int]** | 周报关联issue | [optional] 
**event_ids** | **list[int]** | 关联动态 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


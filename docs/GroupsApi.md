# gitee_api.GroupsApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_groups_group_id**](GroupsApi.md#delete_enterprise_id_groups_group_id) | **DELETE** /{enterprise_id}/groups/{group_id} | 删除企业团队
[**delete_enterprise_id_groups_group_id_members**](GroupsApi.md#delete_enterprise_id_groups_group_id_members) | **DELETE** /{enterprise_id}/groups/{group_id}/members | 移除团队成员
[**get_enterprise_id_groups**](GroupsApi.md#get_enterprise_id_groups) | **GET** /{enterprise_id}/groups | 获取企业团队列表
[**get_enterprise_id_groups_group_id**](GroupsApi.md#get_enterprise_id_groups_group_id) | **GET** /{enterprise_id}/groups/{group_id} | 获取团队详情
[**get_enterprise_id_groups_group_id_members**](GroupsApi.md#get_enterprise_id_groups_group_id_members) | **GET** /{enterprise_id}/groups/{group_id}/members | 获取企业团队下的成员列表
[**get_enterprise_id_groups_group_id_projects**](GroupsApi.md#get_enterprise_id_groups_group_id_projects) | **GET** /{enterprise_id}/groups/{group_id}/projects | 企业团队下仓库列表
[**post_enterprise_id_groups**](GroupsApi.md#post_enterprise_id_groups) | **POST** /{enterprise_id}/groups | 新建企业团队
[**put_enterprise_id_groups_group_id**](GroupsApi.md#put_enterprise_id_groups_group_id) | **PUT** /{enterprise_id}/groups/{group_id} | 更新企业团队

# **delete_enterprise_id_groups_group_id**
> delete_enterprise_id_groups_group_id(enterprise_id, group_id, access_token=access_token, sms_captcha=sms_captcha, password=password, validate_type=validate_type)

删除企业团队

删除企业团队

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GroupsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
group_id = 56 # int | 团队 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
sms_captcha = 'sms_captcha_example' # str | 短信验证码 (optional)
password = 'password_example' # str | 用户密码 (optional)
validate_type = 'validate_type_example' # str | 验证方式 (optional)

try:
    # 删除企业团队
    api_instance.delete_enterprise_id_groups_group_id(enterprise_id, group_id, access_token=access_token, sms_captcha=sms_captcha, password=password, validate_type=validate_type)
except ApiException as e:
    print("Exception when calling GroupsApi->delete_enterprise_id_groups_group_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **group_id** | **int**| 团队 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **sms_captcha** | **str**| 短信验证码 | [optional] 
 **password** | **str**| 用户密码 | [optional] 
 **validate_type** | **str**| 验证方式 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_groups_group_id_members**
> delete_enterprise_id_groups_group_id_members(enterprise_id, group_id, access_token=access_token, user_ids=user_ids)

移除团队成员

移除团队成员

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GroupsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
group_id = 56 # int | 团队 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
user_ids = 'user_ids_example' # str | 成员 id，多个成员id通过英文逗号分隔 (optional)

try:
    # 移除团队成员
    api_instance.delete_enterprise_id_groups_group_id_members(enterprise_id, group_id, access_token=access_token, user_ids=user_ids)
except ApiException as e:
    print("Exception when calling GroupsApi->delete_enterprise_id_groups_group_id_members: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **group_id** | **int**| 团队 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **user_ids** | **str**| 成员 id，多个成员id通过英文逗号分隔 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_groups**
> list[Group] get_enterprise_id_groups(enterprise_id, access_token=access_token, qt=qt, sort=sort, direction=direction, search=search, scope=scope, page=page, per_page=per_page)

获取企业团队列表

获取企业团队列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GroupsApi()
enterprise_id = 56 # int | 企业id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
sort = 'sort_example' # str | 排序字段(created_at: 创建时间 updated_at: 更新时间) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
search = 'search_example' # str | 搜索字符串 (optional)
scope = 'scope_example' # str | admin: 获取用户管理的团队 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取企业团队列表
    api_response = api_instance.get_enterprise_id_groups(enterprise_id, access_token=access_token, qt=qt, sort=sort, direction=direction, search=search, scope=scope, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GroupsApi->get_enterprise_id_groups: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **sort** | **str**| 排序字段(created_at: 创建时间 updated_at: 更新时间) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **search** | **str**| 搜索字符串 | [optional] 
 **scope** | **str**| admin: 获取用户管理的团队 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[Group]**](Group.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_groups_group_id**
> Group get_enterprise_id_groups_group_id(enterprise_id, group_id, access_token=access_token, qt=qt)

获取团队详情

获取团队详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GroupsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
group_id = 'group_id_example' # str | 团队id/path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 获取团队详情
    api_response = api_instance.get_enterprise_id_groups_group_id(enterprise_id, group_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GroupsApi->get_enterprise_id_groups_group_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **group_id** | **str**| 团队id/path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**Group**](Group.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_groups_group_id_members**
> list[Member] get_enterprise_id_groups_group_id_members(enterprise_id, group_id, access_token=access_token, qt=qt, search=search, sort=sort, direction=direction, page=page, per_page=per_page)

获取企业团队下的成员列表

获取企业团队下的成员列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GroupsApi()
enterprise_id = 56 # int | 企业id
group_id = 'group_id_example' # str | 团队id/path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
search = 'search_example' # str | 搜索关键字 (optional)
sort = 'sort_example' # str | 排序字段(created_at: 创建时间 remark: 在企业的备注) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取企业团队下的成员列表
    api_response = api_instance.get_enterprise_id_groups_group_id_members(enterprise_id, group_id, access_token=access_token, qt=qt, search=search, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GroupsApi->get_enterprise_id_groups_group_id_members: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **group_id** | **str**| 团队id/path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **search** | **str**| 搜索关键字 | [optional] 
 **sort** | **str**| 排序字段(created_at: 创建时间 remark: 在企业的备注) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[Member]**](Member.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_groups_group_id_projects**
> list[Project] get_enterprise_id_groups_group_id_projects(enterprise_id, group_id, access_token=access_token, qt=qt, scope=scope, search=search, type=type, status=status, creator_id=creator_id, parent_id=parent_id, fork_filter=fork_filter, outsourced=outsourced, sort=sort, direction=direction, page=page, per_page=per_page)

企业团队下仓库列表

企业团队下仓库列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GroupsApi()
enterprise_id = 56 # int | 企业id
group_id = 'group_id_example' # str | 团队id/path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
scope = 'scope_example' # str | 范围筛选 (optional)
search = 'search_example' # str | 搜索参数 (optional)
type = 'type_example' # str | 与我相关，created：我创建的，joined：我参与的，star: 我收藏的，template：模版仓库 (optional)
status = 56 # int | 状态: 0: 开始，1: 暂停，2: 关闭  (optional)
creator_id = 56 # int | 负责人 (optional)
parent_id = 56 # int | form_from仓库id (optional)
fork_filter = 'fork_filter_example' # str | 非fork的(not_fork), 只看fork的(only_fork) (optional)
outsourced = 56 # int | 是否外包：0：内部，1：外包 (optional)
sort = 'sort_example' # str | 排序字段(created_at: 创建时间 last_push_at: 最近push时间) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 企业团队下仓库列表
    api_response = api_instance.get_enterprise_id_groups_group_id_projects(enterprise_id, group_id, access_token=access_token, qt=qt, scope=scope, search=search, type=type, status=status, creator_id=creator_id, parent_id=parent_id, fork_filter=fork_filter, outsourced=outsourced, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GroupsApi->get_enterprise_id_groups_group_id_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **group_id** | **str**| 团队id/path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **scope** | **str**| 范围筛选 | [optional] 
 **search** | **str**| 搜索参数 | [optional] 
 **type** | **str**| 与我相关，created：我创建的，joined：我参与的，star: 我收藏的，template：模版仓库 | [optional] 
 **status** | **int**| 状态: 0: 开始，1: 暂停，2: 关闭  | [optional] 
 **creator_id** | **int**| 负责人 | [optional] 
 **parent_id** | **int**| form_from仓库id | [optional] 
 **fork_filter** | **str**| 非fork的(not_fork), 只看fork的(only_fork) | [optional] 
 **outsourced** | **int**| 是否外包：0：内部，1：外包 | [optional] 
 **sort** | **str**| 排序字段(created_at: 创建时间 last_push_at: 最近push时间) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[Project]**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_groups**
> Group post_enterprise_id_groups(body, enterprise_id)

新建企业团队

新建企业团队

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GroupsApi()
body = gitee_api.EnterpriseIdGroupsBody() # EnterpriseIdGroupsBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 新建企业团队
    api_response = api_instance.post_enterprise_id_groups(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GroupsApi->post_enterprise_id_groups: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnterpriseIdGroupsBody**](EnterpriseIdGroupsBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

[**Group**](Group.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_groups_group_id**
> Group put_enterprise_id_groups_group_id(enterprise_id, group_id, body=body)

更新企业团队

更新企业团队

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GroupsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
group_id = 56 # int | 团队 id
body = gitee_api.GroupsGroupIdBody() # GroupsGroupIdBody |  (optional)

try:
    # 更新企业团队
    api_response = api_instance.put_enterprise_id_groups_group_id(enterprise_id, group_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GroupsApi->put_enterprise_id_groups_group_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **group_id** | **int**| 团队 id | 
 **body** | [**GroupsGroupIdBody**](GroupsGroupIdBody.md)|  | [optional] 

### Return type

[**Group**](Group.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


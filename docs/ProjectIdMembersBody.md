# ProjectIdMembersBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**qt** | **str** | path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
**users** | **str** | 要添加的成员信息,例如[{\&quot;id\&quot;:\&quot;13\&quot;, \&quot;access\&quot;:\&quot;30\&quot;, \&quot;name\&quot;:\&quot;真喜洋洋 (xiyangyang)\&quot;, \&quot;username\&quot;:\&quot;xiyangyang\&quot;}] | [optional] 
**groups** | **str** | 授权团队的id，多个ID通过英文逗号分隔 | [optional] 
**access_level** | **int** | 授权团队的成员在仓库的权限 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# PullRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | PR 的 id | [optional] 
**iid** | **int** | 仓库内唯一的 PR id 标识符 | [optional] 
**title** | **str** | PR 的标题 | [optional] 
**project_id** | **int** | 仓库 id | [optional] 
**state** | **str** | PR 的状态(opened: 开启; reopened: 关闭后重开; closed: 关闭; merged: 已合并;) | [optional] 
**check_state** | **int** | PR的审查状态(0: 不需要审查; 1: 待审查; 2: 审查已全部通过;) | [optional] 
**test_state** | **int** | PR的测试状态(0: 不需要测试; 1: 待测试; 2: 测试已全部通过;) | [optional] 
**priority** | **str** | PR 的优先级。(0: 不指定; 1: 不重要; 2: 次要; 3: 主要; 4: 严重;) | [optional] 
**lightweight** | **bool** | 是否轻量级 PR | [optional] 
**labels** | [**Label**](Label.md) |  | [optional] 
**conflict** | **bool** | 是否存在冲突 | [optional] 
**project** | [**Project**](Project.md) |  | [optional] 
**source_branch** | **object** | 源分支 | [optional] 
**target_branch** | **object** | 目标分支 | [optional] 
**can_merge** | **str** | 是否可合并 | [optional] 
**assignees** | [**PrAssign**](PrAssign.md) |  | [optional] 
**pr_assign_num** | **int** | 最少审查人数 | [optional] 
**testers** | [**PrAssign**](PrAssign.md) |  | [optional] 
**close_related_issue** | **int** | 合并 PR 后关闭关联的任务 | [optional] 
**prune_branch** | **int** | 合并 PR 后删除关联分支 | [optional] 
**pr_test_num** | **int** | 最少测试人数 | [optional] 
**latest_scan_task** | **object** | 最后一次gitee scan扫描结果 | [optional] 
**gitee_go_enabled** | **bool** | 所属仓库GiteeGo服务是否可用 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


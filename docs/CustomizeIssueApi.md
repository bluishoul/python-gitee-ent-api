# gitee_api.CustomizeIssueApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_enterprise_id_customize_list**](CustomizeIssueApi.md#get_enterprise_id_customize_list) | **GET** /{enterprise_id}/customize/list | 获取管理界面的字段列表

# **get_enterprise_id_customize_list**
> IssueField get_enterprise_id_customize_list(enterprise_id, access_token=access_token, page=page, per_page=per_page, sort=sort, issue_type_id=issue_type_id, type=type)

获取管理界面的字段列表

获取管理界面的字段列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.CustomizeIssueApi()
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)
sort = 56 # int | 是否排序（被选的字段排后面） (optional)
issue_type_id = 56 # int | 任务类型 id (optional)
type = 'type_example' # str | 获取字段列表来源（导出/企业管理） (optional)

try:
    # 获取管理界面的字段列表
    api_response = api_instance.get_enterprise_id_customize_list(enterprise_id, access_token=access_token, page=page, per_page=per_page, sort=sort, issue_type_id=issue_type_id, type=type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CustomizeIssueApi->get_enterprise_id_customize_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 
 **sort** | **int**| 是否排序（被选的字段排后面） | [optional] 
 **issue_type_id** | **int**| 任务类型 id | [optional] 
 **type** | **str**| 获取字段列表来源（导出/企业管理） | [optional] 

### Return type

[**IssueField**](IssueField.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


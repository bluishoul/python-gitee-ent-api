# DocNodesBatchMoveBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**doc_node_ids** | **str** | 文件 id，使用,，隔开 | 
**parent_id** | **int** | 挂载到文件下（此处填入上级的 doc_node 节点的 id; 若移动到根目录，parent_id: 0） | 
**program_id** | **int** | 挂载到某项目下（此处填入项目 id） | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


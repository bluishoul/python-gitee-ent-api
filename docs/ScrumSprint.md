# ScrumSprint

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 迭代ID | [optional] 
**title** | **str** | 迭代标题 | [optional] 
**enterprise_id** | **int** | 企业ID | [optional] 
**program_id** | **int** | 项目ID | [optional] 
**state** | **str** | 迭代状态 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# Diff

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha** | **str** | Commit ID | [optional] 
**file_path_sha** | **str** | 文件路径 sha 值 | [optional] 
**filename** | **str** | 文件名 | [optional] 
**status** | **str** | 文件改动类型。added: 新增 renamed: 重命名 deleted: 删除 | [optional] 
**additions** | **int** | 新增代码行数 | [optional] 
**deletions** | **int** | 删除代码行数 | [optional] 
**diff_file_loc** | **int** | diff文件代码行数 | [optional] 
**statistic** | **str** | diff 统计 | [optional] 
**head** | **str** | diff head | [optional] 
**content** | **str** | diff 内容 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


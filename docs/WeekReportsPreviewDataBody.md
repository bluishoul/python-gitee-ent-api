# WeekReportsPreviewDataBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**content** | **str** | 需要预览的周报内容 | 
**event_ids** | **str** | 关联动态ids，以逗号分隔，如：3241,2324 | [optional] 
**issue_ids** | **str** | 关联issue ids，以逗号分隔，如：3241,2324 | [optional] 
**pull_request_ids** | **str** | 关联pull request ids，以逗号分隔，如：3241,2324 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


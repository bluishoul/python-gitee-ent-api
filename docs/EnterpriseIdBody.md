# EnterpriseIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**name** | **str** | 企业名称 | [optional] 
**public** | **str** | 企业属性 (0: 不公开 1: 公开） | [optional] 
**email** | **str** | 企业邮箱 | [optional] 
**website** | **str** | 企业网站 | [optional] 
**description** | **str** | 企业简介 | [optional] 
**phone** | **str** | 手机号码 | [optional] 
**watermark** | **bool** | 企业水印 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


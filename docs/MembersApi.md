# gitee_api.MembersApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_members**](MembersApi.md#delete_enterprise_id_members) | **DELETE** /{enterprise_id}/members | 成员批量移出企业
[**delete_enterprise_id_members_user_id**](MembersApi.md#delete_enterprise_id_members_user_id) | **DELETE** /{enterprise_id}/members/{user_id} | 把成员移出企业
[**delete_enterprise_id_members_user_id_groups_group_id**](MembersApi.md#delete_enterprise_id_members_user_id_groups_group_id) | **DELETE** /{enterprise_id}/members/{user_id}/groups/{group_id} | 把成员移出团队
[**delete_enterprise_id_members_user_id_projects_project_id**](MembersApi.md#delete_enterprise_id_members_user_id_projects_project_id) | **DELETE** /{enterprise_id}/members/{user_id}/projects/{project_id} | 把成员移出仓库
[**delete_enterprise_id_outside_members**](MembersApi.md#delete_enterprise_id_outside_members) | **DELETE** /{enterprise_id}/outside-members | 企业外的成员-从企业组织或仓库中移除(单个/批量)
[**get_enterprise_id_members**](MembersApi.md#get_enterprise_id_members) | **GET** /{enterprise_id}/members | 获取企业成员列表
[**get_enterprise_id_members_by_email**](MembersApi.md#get_enterprise_id_members_by_email) | **GET** /{enterprise_id}/members/by_email | 通过邮箱获取成员详情
[**get_enterprise_id_members_user_id**](MembersApi.md#get_enterprise_id_members_user_id) | **GET** /{enterprise_id}/members/{user_id} | 获取企业成员详情
[**get_enterprise_id_members_user_id_events**](MembersApi.md#get_enterprise_id_members_user_id_events) | **GET** /{enterprise_id}/members/{user_id}/events | 企业成员动态
[**get_enterprise_id_members_user_id_fork_projects**](MembersApi.md#get_enterprise_id_members_user_id_fork_projects) | **GET** /{enterprise_id}/members/{user_id}/fork_projects | 获取成员fork的企业仓库列表
[**get_enterprise_id_members_user_id_groups**](MembersApi.md#get_enterprise_id_members_user_id_groups) | **GET** /{enterprise_id}/members/{user_id}/groups | 获取成员加入的团队列表
[**get_enterprise_id_members_user_id_programs**](MembersApi.md#get_enterprise_id_members_user_id_programs) | **GET** /{enterprise_id}/members/{user_id}/programs | 获取成员加入的项目列表
[**get_enterprise_id_members_user_id_projects**](MembersApi.md#get_enterprise_id_members_user_id_projects) | **GET** /{enterprise_id}/members/{user_id}/projects | 获取成员加入的仓库列表
[**post_enterprise_id_members_email_invitation**](MembersApi.md#post_enterprise_id_members_email_invitation) | **POST** /{enterprise_id}/members/email_invitation | 添加成员到企业-邮件邀请
[**post_enterprise_id_members_user_id_reset_password_email**](MembersApi.md#post_enterprise_id_members_user_id_reset_password_email) | **POST** /{enterprise_id}/members/{user_id}/reset_password_email | 发送密码重置邮件
[**put_enterprise_id_members_user_id**](MembersApi.md#put_enterprise_id_members_user_id) | **PUT** /{enterprise_id}/members/{user_id} | 更新企业成员信息
[**put_enterprise_id_members_user_id_block**](MembersApi.md#put_enterprise_id_members_user_id_block) | **PUT** /{enterprise_id}/members/{user_id}/block | 锁定/解除锁定用户
[**put_enterprise_id_members_user_id_member_groups**](MembersApi.md#put_enterprise_id_members_user_id_member_groups) | **PUT** /{enterprise_id}/members/{user_id}/member_groups | 给成员添加/移出团队
[**put_enterprise_id_members_user_id_member_programs**](MembersApi.md#put_enterprise_id_members_user_id_member_programs) | **PUT** /{enterprise_id}/members/{user_id}/member_programs | 给成员添加/移出项目
[**put_enterprise_id_members_user_id_member_projects**](MembersApi.md#put_enterprise_id_members_user_id_member_projects) | **PUT** /{enterprise_id}/members/{user_id}/member_projects | 给成员添加/移出仓库

# **delete_enterprise_id_members**
> delete_enterprise_id_members(enterprise_id, access_token=access_token, user_ids=user_ids, sms_captcha=sms_captcha, password=password, validate_type=validate_type)

成员批量移出企业

成员批量移出企业

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
user_ids = 'user_ids_example' # str | 成员 ids, 逗号(,)隔开空 (optional)
sms_captcha = 'sms_captcha_example' # str | 短信验证码 (optional)
password = 'password_example' # str | 用户密码 (optional)
validate_type = 'validate_type_example' # str | 验证方式 (optional)

try:
    # 成员批量移出企业
    api_instance.delete_enterprise_id_members(enterprise_id, access_token=access_token, user_ids=user_ids, sms_captcha=sms_captcha, password=password, validate_type=validate_type)
except ApiException as e:
    print("Exception when calling MembersApi->delete_enterprise_id_members: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **user_ids** | **str**| 成员 ids, 逗号(,)隔开空 | [optional] 
 **sms_captcha** | **str**| 短信验证码 | [optional] 
 **password** | **str**| 用户密码 | [optional] 
 **validate_type** | **str**| 验证方式 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_members_user_id**
> delete_enterprise_id_members_user_id(enterprise_id, user_id, access_token=access_token, sms_captcha=sms_captcha, password=password, validate_type=validate_type)

把成员移出企业

把成员移出企业

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 56 # int | 成员 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
sms_captcha = 'sms_captcha_example' # str | 短信验证码 (optional)
password = 'password_example' # str | 用户密码 (optional)
validate_type = 'validate_type_example' # str | 验证方式 (optional)

try:
    # 把成员移出企业
    api_instance.delete_enterprise_id_members_user_id(enterprise_id, user_id, access_token=access_token, sms_captcha=sms_captcha, password=password, validate_type=validate_type)
except ApiException as e:
    print("Exception when calling MembersApi->delete_enterprise_id_members_user_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **int**| 成员 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **sms_captcha** | **str**| 短信验证码 | [optional] 
 **password** | **str**| 用户密码 | [optional] 
 **validate_type** | **str**| 验证方式 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_members_user_id_groups_group_id**
> delete_enterprise_id_members_user_id_groups_group_id(enterprise_id, user_id, group_id, access_token=access_token)

把成员移出团队

把成员移出团队

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 56 # int | 成员 id
group_id = 56 # int | 团队 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 把成员移出团队
    api_instance.delete_enterprise_id_members_user_id_groups_group_id(enterprise_id, user_id, group_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling MembersApi->delete_enterprise_id_members_user_id_groups_group_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **int**| 成员 id | 
 **group_id** | **int**| 团队 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_members_user_id_projects_project_id**
> delete_enterprise_id_members_user_id_projects_project_id(enterprise_id, user_id, project_id, access_token=access_token)

把成员移出仓库

把成员移出仓库

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 56 # int | 成员 id
project_id = 56 # int | 仓库 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 把成员移出仓库
    api_instance.delete_enterprise_id_members_user_id_projects_project_id(enterprise_id, user_id, project_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling MembersApi->delete_enterprise_id_members_user_id_projects_project_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **int**| 成员 id | 
 **project_id** | **int**| 仓库 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_outside_members**
> list[User] delete_enterprise_id_outside_members(enterprise_id, user_ids, access_token=access_token)

企业外的成员-从企业组织或仓库中移除(单个/批量)

企业外的成员-从企业组织或仓库中移除(单个/批量)

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_ids = 'user_ids_example' # str | 企业外成员用户id，多个id逗号(,)隔开
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 企业外的成员-从企业组织或仓库中移除(单个/批量)
    api_response = api_instance.delete_enterprise_id_outside_members(enterprise_id, user_ids, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->delete_enterprise_id_outside_members: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_ids** | **str**| 企业外成员用户id，多个id逗号(,)隔开 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**list[User]**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_members**
> list[Member] get_enterprise_id_members(enterprise_id, access_token=access_token, ident=ident, is_block=is_block, group_id=group_id, role_id=role_id, search=search, sort=sort, direction=direction, page=page, per_page=per_page)

获取企业成员列表

获取企业成员列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
ident = 'ident_example' # str | 角色类型. (viewer: 观察者; member: 普通成员; outsourced_member: 外包成员; human_resources: 人事管理员; admin: 管理员; super_admin: 超级管理员; enterprise_owner: 企业拥有者;) (optional)
is_block = 0 # int | 1: 筛选已锁定的用户。 (optional) (default to 0)
group_id = 56 # int | 筛选团队的成员列表 (optional)
role_id = 56 # int | 筛选指定角色的成员列表 (optional)
search = 'search_example' # str | 搜索关键字 (optional)
sort = 'sort_example' # str | 排序字段(created_at: 创建时间 remark: 在企业的备注) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取企业成员列表
    api_response = api_instance.get_enterprise_id_members(enterprise_id, access_token=access_token, ident=ident, is_block=is_block, group_id=group_id, role_id=role_id, search=search, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->get_enterprise_id_members: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **ident** | **str**| 角色类型. (viewer: 观察者; member: 普通成员; outsourced_member: 外包成员; human_resources: 人事管理员; admin: 管理员; super_admin: 超级管理员; enterprise_owner: 企业拥有者;) | [optional] 
 **is_block** | **int**| 1: 筛选已锁定的用户。 | [optional] [default to 0]
 **group_id** | **int**| 筛选团队的成员列表 | [optional] 
 **role_id** | **int**| 筛选指定角色的成员列表 | [optional] 
 **search** | **str**| 搜索关键字 | [optional] 
 **sort** | **str**| 排序字段(created_at: 创建时间 remark: 在企业的备注) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[Member]**](Member.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_members_by_email**
> Member get_enterprise_id_members_by_email(email, enterprise_id, access_token=access_token)

通过邮箱获取成员详情

通过邮箱获取成员详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
email = 'email_example' # str | 用户邮箱
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 通过邮箱获取成员详情
    api_response = api_instance.get_enterprise_id_members_by_email(email, enterprise_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->get_enterprise_id_members_by_email: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **str**| 用户邮箱 | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**Member**](Member.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_members_user_id**
> list[Member] get_enterprise_id_members_user_id(enterprise_id, user_id, access_token=access_token, qt=qt)

获取企业成员详情

获取企业成员详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 'user_id_example' # str | 用户id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 获取企业成员详情
    api_response = api_instance.get_enterprise_id_members_user_id(enterprise_id, user_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->get_enterprise_id_members_user_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **str**| 用户id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**list[Member]**](Member.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_members_user_id_events**
> list[Event] get_enterprise_id_members_user_id_events(enterprise_id, user_id, access_token=access_token, qt=qt, start_date=start_date, end_date=end_date, page=page, per_page=per_page, prev_id=prev_id, limit=limit)

企业成员动态

企业成员动态

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 'user_id_example' # str | 用户id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)
prev_id = 56 # int | 上一次动态列表中最小动态 ID (返回列表不包含该ID记录) (optional)
limit = 56 # int | 每次获取动态的条数 (optional)

try:
    # 企业成员动态
    api_response = api_instance.get_enterprise_id_members_user_id_events(enterprise_id, user_id, access_token=access_token, qt=qt, start_date=start_date, end_date=end_date, page=page, per_page=per_page, prev_id=prev_id, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->get_enterprise_id_members_user_id_events: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **str**| 用户id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 
 **prev_id** | **int**| 上一次动态列表中最小动态 ID (返回列表不包含该ID记录) | [optional] 
 **limit** | **int**| 每次获取动态的条数 | [optional] 

### Return type

[**list[Event]**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_members_user_id_fork_projects**
> list[Project] get_enterprise_id_members_user_id_fork_projects(enterprise_id, user_id, access_token=access_token, qt=qt)

获取成员fork的企业仓库列表

获取成员fork的企业仓库列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 'user_id_example' # str | 用户id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 获取成员fork的企业仓库列表
    api_response = api_instance.get_enterprise_id_members_user_id_fork_projects(enterprise_id, user_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->get_enterprise_id_members_user_id_fork_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **str**| 用户id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**list[Project]**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_members_user_id_groups**
> list[GroupWithAuth] get_enterprise_id_members_user_id_groups(enterprise_id, user_id, access_token=access_token, qt=qt, page=page, per_page=per_page, scope=scope, search=search)

获取成员加入的团队列表

获取成员加入的团队列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 'user_id_example' # str | 用户id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)
scope = 'scope_example' # str | 筛选项(not_joined: 未加入的团队列表, admin: 成员管理的团队列表) (optional)
search = 'search_example' # str | 搜索关键字 (optional)

try:
    # 获取成员加入的团队列表
    api_response = api_instance.get_enterprise_id_members_user_id_groups(enterprise_id, user_id, access_token=access_token, qt=qt, page=page, per_page=per_page, scope=scope, search=search)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->get_enterprise_id_members_user_id_groups: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **str**| 用户id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 
 **scope** | **str**| 筛选项(not_joined: 未加入的团队列表, admin: 成员管理的团队列表) | [optional] 
 **search** | **str**| 搜索关键字 | [optional] 

### Return type

[**list[GroupWithAuth]**](GroupWithAuth.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_members_user_id_programs**
> list[ProgramWithAuth] get_enterprise_id_members_user_id_programs(enterprise_id, user_id, access_token=access_token, qt=qt, page=page, per_page=per_page, scope=scope, search=search)

获取成员加入的项目列表

获取成员加入的项目列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 'user_id_example' # str | 用户id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)
scope = 'scope_example' # str | 筛选项(not_joined: 未加入的项目列表) (optional)
search = 'search_example' # str | 搜索关键字 (optional)

try:
    # 获取成员加入的项目列表
    api_response = api_instance.get_enterprise_id_members_user_id_programs(enterprise_id, user_id, access_token=access_token, qt=qt, page=page, per_page=per_page, scope=scope, search=search)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->get_enterprise_id_members_user_id_programs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **str**| 用户id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 
 **scope** | **str**| 筛选项(not_joined: 未加入的项目列表) | [optional] 
 **search** | **str**| 搜索关键字 | [optional] 

### Return type

[**list[ProgramWithAuth]**](ProgramWithAuth.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_members_user_id_projects**
> list[ProjectWithAuth] get_enterprise_id_members_user_id_projects(enterprise_id, user_id, access_token=access_token, qt=qt, not_fork=not_fork, scope=scope, search=search, page=page, per_page=per_page)

获取成员加入的仓库列表

获取成员加入的仓库列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 'user_id_example' # str | 用户id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
not_fork = true # bool | true: 非fork仓库, false: 不过滤 (optional)
scope = 'scope_example' # str | 筛选项(not_joined: 未加入的仓库列表) (optional)
search = 'search_example' # str | 搜索关键字 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取成员加入的仓库列表
    api_response = api_instance.get_enterprise_id_members_user_id_projects(enterprise_id, user_id, access_token=access_token, qt=qt, not_fork=not_fork, scope=scope, search=search, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->get_enterprise_id_members_user_id_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **str**| 用户id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **not_fork** | **bool**| true: 非fork仓库, false: 不过滤 | [optional] 
 **scope** | **str**| 筛选项(not_joined: 未加入的仓库列表) | [optional] 
 **search** | **str**| 搜索关键字 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[ProjectWithAuth]**](ProjectWithAuth.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_members_email_invitation**
> EmailInvitation post_enterprise_id_members_email_invitation(body, enterprise_id)

添加成员到企业-邮件邀请

添加成员到企业-邮件邀请

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
body = gitee_api.MembersEmailInvitationBody() # MembersEmailInvitationBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 添加成员到企业-邮件邀请
    api_response = api_instance.post_enterprise_id_members_email_invitation(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->post_enterprise_id_members_email_invitation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MembersEmailInvitationBody**](MembersEmailInvitationBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

[**EmailInvitation**](EmailInvitation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_members_user_id_reset_password_email**
> post_enterprise_id_members_user_id_reset_password_email(enterprise_id, user_id, body=body)

发送密码重置邮件

发送密码重置邮件

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 56 # int | 成员 id
body = gitee_api.UserIdResetPasswordEmailBody() # UserIdResetPasswordEmailBody |  (optional)

try:
    # 发送密码重置邮件
    api_instance.post_enterprise_id_members_user_id_reset_password_email(enterprise_id, user_id, body=body)
except ApiException as e:
    print("Exception when calling MembersApi->post_enterprise_id_members_user_id_reset_password_email: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **int**| 成员 id | 
 **body** | [**UserIdResetPasswordEmailBody**](UserIdResetPasswordEmailBody.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_members_user_id**
> list[Member] put_enterprise_id_members_user_id(enterprise_id, user_id, body=body)

更新企业成员信息

更新企业成员信息

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 56 # int | 成员 id
body = gitee_api.MembersUserIdBody() # MembersUserIdBody |  (optional)

try:
    # 更新企业成员信息
    api_response = api_instance.put_enterprise_id_members_user_id(enterprise_id, user_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->put_enterprise_id_members_user_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **int**| 成员 id | 
 **body** | [**MembersUserIdBody**](MembersUserIdBody.md)|  | [optional] 

### Return type

[**list[Member]**](Member.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_members_user_id_block**
> list[Member] put_enterprise_id_members_user_id_block(body, enterprise_id, user_id)

锁定/解除锁定用户

锁定/解除锁定用户

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
body = gitee_api.UserIdBlockBody() # UserIdBlockBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 56 # int | 成员 id

try:
    # 锁定/解除锁定用户
    api_response = api_instance.put_enterprise_id_members_user_id_block(body, enterprise_id, user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->put_enterprise_id_members_user_id_block: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserIdBlockBody**](UserIdBlockBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **int**| 成员 id | 

### Return type

[**list[Member]**](Member.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_members_user_id_member_groups**
> MemberBulkModify put_enterprise_id_members_user_id_member_groups(enterprise_id, user_id, body=body)

给成员添加/移出团队

给成员添加/移出团队

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 56 # int | 成员 id
body = gitee_api.UserIdMemberGroupsBody() # UserIdMemberGroupsBody |  (optional)

try:
    # 给成员添加/移出团队
    api_response = api_instance.put_enterprise_id_members_user_id_member_groups(enterprise_id, user_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->put_enterprise_id_members_user_id_member_groups: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **int**| 成员 id | 
 **body** | [**UserIdMemberGroupsBody**](UserIdMemberGroupsBody.md)|  | [optional] 

### Return type

[**MemberBulkModify**](MemberBulkModify.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_members_user_id_member_programs**
> MemberBulkModify put_enterprise_id_members_user_id_member_programs(enterprise_id, user_id, body=body)

给成员添加/移出项目

给成员添加/移出项目

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 56 # int | 成员 id
body = gitee_api.UserIdMemberProgramsBody() # UserIdMemberProgramsBody |  (optional)

try:
    # 给成员添加/移出项目
    api_response = api_instance.put_enterprise_id_members_user_id_member_programs(enterprise_id, user_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->put_enterprise_id_members_user_id_member_programs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **int**| 成员 id | 
 **body** | [**UserIdMemberProgramsBody**](UserIdMemberProgramsBody.md)|  | [optional] 

### Return type

[**MemberBulkModify**](MemberBulkModify.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_members_user_id_member_projects**
> MemberBulkModify put_enterprise_id_members_user_id_member_projects(enterprise_id, user_id, body=body)

给成员添加/移出仓库

给成员添加/移出仓库

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MembersApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 56 # int | 成员 id
body = gitee_api.UserIdMemberProjectsBody() # UserIdMemberProjectsBody |  (optional)

try:
    # 给成员添加/移出仓库
    api_response = api_instance.put_enterprise_id_members_user_id_member_projects(enterprise_id, user_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MembersApi->put_enterprise_id_members_user_id_member_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **int**| 成员 id | 
 **body** | [**UserIdMemberProjectsBody**](UserIdMemberProjectsBody.md)|  | [optional] 

### Return type

[**MemberBulkModify**](MemberBulkModify.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


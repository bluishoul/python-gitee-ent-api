# DocNodesDirectoryBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**name** | **str** | 文件夹名称 | 
**parent_id** | **int** | 父级 id （默认：0） | [optional] 
**program_id** | **int** | 归属的项目 id | [optional] 
**auth_type** | **str** | 权限; 继承: inherit; 私有: private; 只读 read_only; 读写: read_write | [optional] [default to 'inherit']
**scrum_sprint_id** | **str** | 迭代 ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


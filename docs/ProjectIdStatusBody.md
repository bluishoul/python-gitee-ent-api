# ProjectIdStatusBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**qt** | **str** | path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
**status** | **int** | 仓库状态，0：开始，1：暂停，2：关闭 | 
**sms_captcha** | **str** | 短信验证码 | [optional] 
**password** | **str** | 用户密码 | [optional] 
**validate_type** | **str** | 验证方式 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


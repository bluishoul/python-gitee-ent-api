# gitee_api.ProgramsApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_programs_program_id**](ProgramsApi.md#delete_enterprise_id_programs_program_id) | **DELETE** /{enterprise_id}/programs/{program_id} | 删除项目
[**delete_enterprise_id_programs_program_id_groups_group_id**](ProgramsApi.md#delete_enterprise_id_programs_program_id_groups_group_id) | **DELETE** /{enterprise_id}/programs/{program_id}/groups/{group_id} | 移出项目下的团队
[**delete_enterprise_id_programs_program_id_members_member_user_id**](ProgramsApi.md#delete_enterprise_id_programs_program_id_members_member_user_id) | **DELETE** /{enterprise_id}/programs/{program_id}/members/{member_user_id} | 移出项目下成员
[**delete_enterprise_id_programs_program_id_projects_project_id**](ProgramsApi.md#delete_enterprise_id_programs_program_id_projects_project_id) | **DELETE** /{enterprise_id}/programs/{program_id}/projects/{project_id} | 移出项目下仓库
[**get_enterprise_id_programs**](ProgramsApi.md#get_enterprise_id_programs) | **GET** /{enterprise_id}/programs | 获取项目列表
[**get_enterprise_id_programs_program_id**](ProgramsApi.md#get_enterprise_id_programs_program_id) | **GET** /{enterprise_id}/programs/{program_id} | 获取项目详情
[**get_enterprise_id_programs_program_id_events**](ProgramsApi.md#get_enterprise_id_programs_program_id_events) | **GET** /{enterprise_id}/programs/{program_id}/events | 获取项目下动态列表
[**get_enterprise_id_programs_program_id_groups**](ProgramsApi.md#get_enterprise_id_programs_program_id_groups) | **GET** /{enterprise_id}/programs/{program_id}/groups | 获取项目下的团队
[**get_enterprise_id_programs_program_id_issues**](ProgramsApi.md#get_enterprise_id_programs_program_id_issues) | **GET** /{enterprise_id}/programs/{program_id}/issues | 获取项目下的任务列表
[**get_enterprise_id_programs_program_id_members**](ProgramsApi.md#get_enterprise_id_programs_program_id_members) | **GET** /{enterprise_id}/programs/{program_id}/members | 获取项目下的成员列表
[**get_enterprise_id_programs_program_id_operate_auths**](ProgramsApi.md#get_enterprise_id_programs_program_id_operate_auths) | **GET** /{enterprise_id}/programs/{program_id}/operate_auths | 获取项目的操作权限
[**get_enterprise_id_programs_program_id_projects**](ProgramsApi.md#get_enterprise_id_programs_program_id_projects) | **GET** /{enterprise_id}/programs/{program_id}/projects | 获取项目下的仓库列表
[**get_enterprise_id_programs_program_id_pull_requests**](ProgramsApi.md#get_enterprise_id_programs_program_id_pull_requests) | **GET** /{enterprise_id}/programs/{program_id}/pull_requests | 获取项目下的 Pull Request
[**get_enterprise_id_programs_unset**](ProgramsApi.md#get_enterprise_id_programs_unset) | **GET** /{enterprise_id}/programs/unset | 获取未立项项目
[**post_enterprise_id_programs**](ProgramsApi.md#post_enterprise_id_programs) | **POST** /{enterprise_id}/programs | 新建项目
[**post_enterprise_id_programs_program_id_members**](ProgramsApi.md#post_enterprise_id_programs_program_id_members) | **POST** /{enterprise_id}/programs/{program_id}/members | 添加企业成员或团队进项目
[**post_enterprise_id_programs_program_id_projects**](ProgramsApi.md#post_enterprise_id_programs_program_id_projects) | **POST** /{enterprise_id}/programs/{program_id}/projects | 项目关联仓库
[**put_enterprise_id_programs_program_id**](ProgramsApi.md#put_enterprise_id_programs_program_id) | **PUT** /{enterprise_id}/programs/{program_id} | 更新项目

# **delete_enterprise_id_programs_program_id**
> delete_enterprise_id_programs_program_id(program_id, enterprise_id, access_token=access_token, delete_milestones=delete_milestones, delete_issues=delete_issues, sms_captcha=sms_captcha, password=password, validate_type=validate_type)

删除项目

删除项目

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
program_id = 56 # int | 项目 id
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
delete_milestones = true # bool | 是否删除里程碑 (optional)
delete_issues = true # bool | 是否删除任务 (optional)
sms_captcha = 'sms_captcha_example' # str | 短信验证码 (optional)
password = 'password_example' # str | 用户密码 (optional)
validate_type = 'validate_type_example' # str | 验证方式 (optional)

try:
    # 删除项目
    api_instance.delete_enterprise_id_programs_program_id(program_id, enterprise_id, access_token=access_token, delete_milestones=delete_milestones, delete_issues=delete_issues, sms_captcha=sms_captcha, password=password, validate_type=validate_type)
except ApiException as e:
    print("Exception when calling ProgramsApi->delete_enterprise_id_programs_program_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **program_id** | **int**| 项目 id | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **delete_milestones** | **bool**| 是否删除里程碑 | [optional] 
 **delete_issues** | **bool**| 是否删除任务 | [optional] 
 **sms_captcha** | **str**| 短信验证码 | [optional] 
 **password** | **str**| 用户密码 | [optional] 
 **validate_type** | **str**| 验证方式 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_programs_program_id_groups_group_id**
> delete_enterprise_id_programs_program_id_groups_group_id(program_id, group_id, enterprise_id, access_token=access_token)

移出项目下的团队

移出项目下的团队

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
program_id = 56 # int | 项目 id
group_id = 56 # int | 团队 id
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 移出项目下的团队
    api_instance.delete_enterprise_id_programs_program_id_groups_group_id(program_id, group_id, enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling ProgramsApi->delete_enterprise_id_programs_program_id_groups_group_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **program_id** | **int**| 项目 id | 
 **group_id** | **int**| 团队 id | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_programs_program_id_members_member_user_id**
> delete_enterprise_id_programs_program_id_members_member_user_id(program_id, member_user_id, enterprise_id, access_token=access_token)

移出项目下成员

移出项目下成员

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
program_id = 56 # int | 项目 id
member_user_id = 56 # int | 成员id
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 移出项目下成员
    api_instance.delete_enterprise_id_programs_program_id_members_member_user_id(program_id, member_user_id, enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling ProgramsApi->delete_enterprise_id_programs_program_id_members_member_user_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **program_id** | **int**| 项目 id | 
 **member_user_id** | **int**| 成员id | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_programs_program_id_projects_project_id**
> delete_enterprise_id_programs_program_id_projects_project_id(program_id, project_id, enterprise_id, access_token=access_token)

移出项目下仓库

移出项目下仓库

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
program_id = 56 # int | 项目 id
project_id = 56 # int | 仓库id
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 移出项目下仓库
    api_instance.delete_enterprise_id_programs_program_id_projects_project_id(program_id, project_id, enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling ProgramsApi->delete_enterprise_id_programs_program_id_projects_project_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **program_id** | **int**| 项目 id | 
 **project_id** | **int**| 仓库id | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_programs**
> list[ProgramList] get_enterprise_id_programs(enterprise_id, access_token=access_token, type=type, sort=sort, direction=direction, status=status, category=category, page=page, per_page=per_page)

获取项目列表

获取项目列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
type = 'type_example' # str | 筛选不同类型的项目列表。我参与的: joined; 我负责的: assigned; 我创建的: created; 我星标的: only_star (optional)
sort = 'sort_example' # str | 排序字段(created_at: 创建时间 updated_at: 更新时间 users_count: 成员数 projects_count: 仓库数 issues_count: 任务数) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
status = 'status_example' # str | 项目状态（0:开始 1:暂停 2:关闭）, 逗号分隔,如: 0,1 (optional)
category = 'category_example' # str | 项目类别（all: 所有，common: 普通，kanban: 看板）, 支持多种类型，以,分隔，如：common,kanban (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取项目列表
    api_response = api_instance.get_enterprise_id_programs(enterprise_id, access_token=access_token, type=type, sort=sort, direction=direction, status=status, category=category, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProgramsApi->get_enterprise_id_programs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **type** | **str**| 筛选不同类型的项目列表。我参与的: joined; 我负责的: assigned; 我创建的: created; 我星标的: only_star | [optional] 
 **sort** | **str**| 排序字段(created_at: 创建时间 updated_at: 更新时间 users_count: 成员数 projects_count: 仓库数 issues_count: 任务数) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **status** | **str**| 项目状态（0:开始 1:暂停 2:关闭）, 逗号分隔,如: 0,1 | [optional] 
 **category** | **str**| 项目类别（all: 所有，common: 普通，kanban: 看板）, 支持多种类型，以,分隔，如：common,kanban | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[ProgramList]**](ProgramList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_programs_program_id**
> list[Program] get_enterprise_id_programs_program_id(program_id, enterprise_id, access_token=access_token)

获取项目详情

获取项目详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
program_id = 56 # int | 项目 id
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取项目详情
    api_response = api_instance.get_enterprise_id_programs_program_id(program_id, enterprise_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProgramsApi->get_enterprise_id_programs_program_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **program_id** | **int**| 项目 id | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**list[Program]**](Program.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_programs_program_id_events**
> list[Event] get_enterprise_id_programs_program_id_events(program_id, enterprise_id, access_token=access_token, start_date=start_date, end_date=end_date, scope=scope, page=page, per_page=per_page, prev_id=prev_id, limit=limit)

获取项目下动态列表

获取项目下动态列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
program_id = 56 # int | 项目 id
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)
scope = 'scope_example' # str | 项目范围：所有，仓库，任务，外部，我的 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)
prev_id = 56 # int | 上一次动态列表中最小动态 ID (返回列表不包含该ID记录) (optional)
limit = 56 # int | 每次获取动态的条数 (optional)

try:
    # 获取项目下动态列表
    api_response = api_instance.get_enterprise_id_programs_program_id_events(program_id, enterprise_id, access_token=access_token, start_date=start_date, end_date=end_date, scope=scope, page=page, per_page=per_page, prev_id=prev_id, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProgramsApi->get_enterprise_id_programs_program_id_events: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **program_id** | **int**| 项目 id | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 
 **scope** | **str**| 项目范围：所有，仓库，任务，外部，我的 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 
 **prev_id** | **int**| 上一次动态列表中最小动态 ID (返回列表不包含该ID记录) | [optional] 
 **limit** | **int**| 每次获取动态的条数 | [optional] 

### Return type

[**list[Event]**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_programs_program_id_groups**
> list[Group] get_enterprise_id_programs_program_id_groups(program_id, enterprise_id, access_token=access_token, page=page, per_page=per_page)

获取项目下的团队

获取项目下的团队

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
program_id = 56 # int | 项目 id
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取项目下的团队
    api_response = api_instance.get_enterprise_id_programs_program_id_groups(program_id, enterprise_id, access_token=access_token, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProgramsApi->get_enterprise_id_programs_program_id_groups: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **program_id** | **int**| 项目 id | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[Group]**](Group.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_programs_program_id_issues**
> list[Issue] get_enterprise_id_programs_program_id_issues(program_id, enterprise_id, access_token=access_token, kanban_column_id=kanban_column_id, kanban_ids=kanban_ids, project_id=project_id, milestone_id=milestone_id, state=state, only_related_me=only_related_me, assignee_id=assignee_id, author_id=author_id, collaborator_ids=collaborator_ids, created_at=created_at, finished_at=finished_at, plan_started_at=plan_started_at, deadline=deadline, search=search, filter_child=filter_child, issue_state_ids=issue_state_ids, issue_type_id=issue_type_id, label_ids=label_ids, priority=priority, scrum_sprint_ids=scrum_sprint_ids, scrum_version_ids=scrum_version_ids, kanban_column_ids=kanban_column_ids, sort=sort, direction=direction, page=page, per_page=per_page)

获取项目下的任务列表

获取项目下的任务列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
program_id = 'program_id_example' # str | 项目 id
enterprise_id = 56 # int | 企业 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
kanban_column_id = 56 # int | 看板栏ID (optional)
kanban_ids = 'kanban_ids_example' # str | 看板id(可多选，用逗号分隔) (optional)
project_id = 'project_id_example' # str | 仓库 id (optional)
milestone_id = 'milestone_id_example' # str | 里程碑 id (optional)
state = 'state_example' # str | 任务状态属性，可多选，用逗号分隔。（开启：open 关闭：closed 拒绝：rejected 进行中: progressing） (optional)
only_related_me = 'only_related_me_example' # str | 是否仅列出与授权用户相关的任务（0: 否 1: 是） (optional)
assignee_id = 'assignee_id_example' # str | 负责人 id (optional)
author_id = 'author_id_example' # str | 创建者 id (optional)
collaborator_ids = 'collaborator_ids_example' # str | 协作者。(,分隔的id字符串) (optional)
created_at = 'created_at_example' # str | 创建时间，格式：(区间)yyyymmddTHH:MM:SS+08:00-yyyymmddTHH:MM:SS+08:00，（指定某日期）yyyymmddTHH:MM:SS+08:00，（小于指定日期）<yyyymmddTHH:MM:SS+08:00，（大于指定日期）>yyyymmddTHH:MM:SS+08:00 (optional)
finished_at = 'finished_at_example' # str | 任务完成日期，格式同上 (optional)
plan_started_at = 'plan_started_at_example' # str | 计划开始时间，(格式：yyyy-mm-dd) (optional)
deadline = 'deadline_example' # str | 任务截止日期，(格式：yyyy-mm-dd) (optional)
search = 'search_example' # str | 搜索参数 (optional)
filter_child = 'filter_child_example' # str | 是否过滤子任务(0: 否, 1: 是) (optional)
issue_state_ids = 'issue_state_ids_example' # str | 任务状态id，多选，用逗号分隔。 (optional)
issue_type_id = 'issue_type_id_example' # str | 任务类型 (optional)
label_ids = 'label_ids_example' # str | 标签 id（可多选，用逗号分隔） (optional)
priority = 'priority_example' # str | 优先级（可多选，用逗号分隔） (optional)
scrum_sprint_ids = 'scrum_sprint_ids_example' # str | 迭代id(可多选，用逗号分隔) (optional)
scrum_version_ids = 'scrum_version_ids_example' # str | 版本id(可多选，用逗号分隔) (optional)
kanban_column_ids = 'kanban_column_ids_example' # str | 看板栏id(可多选，用逗号分隔) (optional)
sort = 'sort_example' # str | 排序字段(created_at、updated_at、deadline、priority) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取项目下的任务列表
    api_response = api_instance.get_enterprise_id_programs_program_id_issues(program_id, enterprise_id, access_token=access_token, kanban_column_id=kanban_column_id, kanban_ids=kanban_ids, project_id=project_id, milestone_id=milestone_id, state=state, only_related_me=only_related_me, assignee_id=assignee_id, author_id=author_id, collaborator_ids=collaborator_ids, created_at=created_at, finished_at=finished_at, plan_started_at=plan_started_at, deadline=deadline, search=search, filter_child=filter_child, issue_state_ids=issue_state_ids, issue_type_id=issue_type_id, label_ids=label_ids, priority=priority, scrum_sprint_ids=scrum_sprint_ids, scrum_version_ids=scrum_version_ids, kanban_column_ids=kanban_column_ids, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProgramsApi->get_enterprise_id_programs_program_id_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **program_id** | **str**| 项目 id | 
 **enterprise_id** | **int**| 企业 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **kanban_column_id** | **int**| 看板栏ID | [optional] 
 **kanban_ids** | **str**| 看板id(可多选，用逗号分隔) | [optional] 
 **project_id** | **str**| 仓库 id | [optional] 
 **milestone_id** | **str**| 里程碑 id | [optional] 
 **state** | **str**| 任务状态属性，可多选，用逗号分隔。（开启：open 关闭：closed 拒绝：rejected 进行中: progressing） | [optional] 
 **only_related_me** | **str**| 是否仅列出与授权用户相关的任务（0: 否 1: 是） | [optional] 
 **assignee_id** | **str**| 负责人 id | [optional] 
 **author_id** | **str**| 创建者 id | [optional] 
 **collaborator_ids** | **str**| 协作者。(,分隔的id字符串) | [optional] 
 **created_at** | **str**| 创建时间，格式：(区间)yyyymmddTHH:MM:SS+08:00-yyyymmddTHH:MM:SS+08:00，（指定某日期）yyyymmddTHH:MM:SS+08:00，（小于指定日期）&lt;yyyymmddTHH:MM:SS+08:00，（大于指定日期）&gt;yyyymmddTHH:MM:SS+08:00 | [optional] 
 **finished_at** | **str**| 任务完成日期，格式同上 | [optional] 
 **plan_started_at** | **str**| 计划开始时间，(格式：yyyy-mm-dd) | [optional] 
 **deadline** | **str**| 任务截止日期，(格式：yyyy-mm-dd) | [optional] 
 **search** | **str**| 搜索参数 | [optional] 
 **filter_child** | **str**| 是否过滤子任务(0: 否, 1: 是) | [optional] 
 **issue_state_ids** | **str**| 任务状态id，多选，用逗号分隔。 | [optional] 
 **issue_type_id** | **str**| 任务类型 | [optional] 
 **label_ids** | **str**| 标签 id（可多选，用逗号分隔） | [optional] 
 **priority** | **str**| 优先级（可多选，用逗号分隔） | [optional] 
 **scrum_sprint_ids** | **str**| 迭代id(可多选，用逗号分隔) | [optional] 
 **scrum_version_ids** | **str**| 版本id(可多选，用逗号分隔) | [optional] 
 **kanban_column_ids** | **str**| 看板栏id(可多选，用逗号分隔) | [optional] 
 **sort** | **str**| 排序字段(created_at、updated_at、deadline、priority) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[Issue]**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_programs_program_id_members**
> list[Member] get_enterprise_id_programs_program_id_members(program_id, enterprise_id, access_token=access_token, search=search, sort=sort, direction=direction, page=page, per_page=per_page)

获取项目下的成员列表

获取项目下的成员列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
program_id = 56 # int | 项目 id
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
search = 'search_example' # str | 搜索关键字 (optional)
sort = 'sort_example' # str | 排序字段(created_at: 创建时间 remark: 在企业的备注) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取项目下的成员列表
    api_response = api_instance.get_enterprise_id_programs_program_id_members(program_id, enterprise_id, access_token=access_token, search=search, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProgramsApi->get_enterprise_id_programs_program_id_members: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **program_id** | **int**| 项目 id | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **search** | **str**| 搜索关键字 | [optional] 
 **sort** | **str**| 排序字段(created_at: 创建时间 remark: 在企业的备注) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[Member]**](Member.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_programs_program_id_operate_auths**
> get_enterprise_id_programs_program_id_operate_auths(program_id, enterprise_id, access_token=access_token)

获取项目的操作权限

获取项目的操作权限

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
program_id = 56 # int | 项目 id
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取项目的操作权限
    api_instance.get_enterprise_id_programs_program_id_operate_auths(program_id, enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling ProgramsApi->get_enterprise_id_programs_program_id_operate_auths: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **program_id** | **int**| 项目 id | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_programs_program_id_projects**
> list[ProjectsList] get_enterprise_id_programs_program_id_projects(program_id, enterprise_id, access_token=access_token, scope=scope, search=search, type=type, status=status, creator_id=creator_id, parent_id=parent_id, fork_filter=fork_filter, outsourced=outsourced, group_id=group_id, sort=sort, direction=direction, page=page, per_page=per_page)

获取项目下的仓库列表

获取项目下的仓库列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
program_id = 56 # int | 项目 id
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
scope = 'scope_example' # str | 范围筛选 (optional)
search = 'search_example' # str | 搜索参数 (optional)
type = 'type_example' # str | 与我相关，created：我创建的，joined：我参与的，star: 我收藏的，template：模版仓库 (optional)
status = 56 # int | 状态: 0: 开始，1: 暂停，2: 关闭  (optional)
creator_id = 56 # int | 负责人 (optional)
parent_id = 56 # int | form_from仓库id (optional)
fork_filter = 'fork_filter_example' # str | 非fork的(not_fork), 只看fork的(only_fork) (optional)
outsourced = 56 # int | 是否外包：0：内部，1：外包 (optional)
group_id = 56 # int | 团队id (optional)
sort = 'sort_example' # str | 排序字段(created_at、last_push_at) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取项目下的仓库列表
    api_response = api_instance.get_enterprise_id_programs_program_id_projects(program_id, enterprise_id, access_token=access_token, scope=scope, search=search, type=type, status=status, creator_id=creator_id, parent_id=parent_id, fork_filter=fork_filter, outsourced=outsourced, group_id=group_id, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProgramsApi->get_enterprise_id_programs_program_id_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **program_id** | **int**| 项目 id | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **scope** | **str**| 范围筛选 | [optional] 
 **search** | **str**| 搜索参数 | [optional] 
 **type** | **str**| 与我相关，created：我创建的，joined：我参与的，star: 我收藏的，template：模版仓库 | [optional] 
 **status** | **int**| 状态: 0: 开始，1: 暂停，2: 关闭  | [optional] 
 **creator_id** | **int**| 负责人 | [optional] 
 **parent_id** | **int**| form_from仓库id | [optional] 
 **fork_filter** | **str**| 非fork的(not_fork), 只看fork的(only_fork) | [optional] 
 **outsourced** | **int**| 是否外包：0：内部，1：外包 | [optional] 
 **group_id** | **int**| 团队id | [optional] 
 **sort** | **str**| 排序字段(created_at、last_push_at) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[ProjectsList]**](ProjectsList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_programs_program_id_pull_requests**
> list[Issue] get_enterprise_id_programs_program_id_pull_requests(program_id, enterprise_id, access_token=access_token, state=state, scope=scope, author_id=author_id, assignee_id=assignee_id, tester_id=tester_id, search=search, sort=sort, direction=direction, group_id=group_id, milestone_id=milestone_id, labels=labels, label_ids=label_ids, can_be_merged=can_be_merged, project_id=project_id, need_state_count=need_state_count, page=page, per_page=per_page)

获取项目下的 Pull Request

获取项目下的 Pull Request

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
program_id = 56 # int | 项目 id
enterprise_id = 56 # int | 企业 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
state = 'state_example' # str | PR 状态 (optional)
scope = 'scope_example' # str | 范围筛选。指派我的: assigned_or_test，我创建或指派给我的: related_to_me，我参与仓库的PR: participate_in (optional)
author_id = 'author_id_example' # str | 筛选 PR 创建者 (optional)
assignee_id = 'assignee_id_example' # str | 筛选 PR 审查者 (optional)
tester_id = 'tester_id_example' # str | 筛选 PR 测试人员 (optional)
search = 'search_example' # str | 搜索参数 (optional)
sort = 'sort_example' # str | 排序字段(created_at、closed_at、priority) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
group_id = 56 # int | 团队 id (optional)
milestone_id = 56 # int | 里程碑 id (optional)
labels = 'labels_example' # str | 标签名称。多个标签逗号(,)隔开 (optional)
label_ids = 'label_ids_example' # str | 标签ID,多个标签逗号(,)隔开 (optional)
can_be_merged = 56 # int | 是否可合并 (optional)
project_id = 56 # int | 仓库 id (optional)
need_state_count = 56 # int | 是否需要状态统计数 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取项目下的 Pull Request
    api_response = api_instance.get_enterprise_id_programs_program_id_pull_requests(program_id, enterprise_id, access_token=access_token, state=state, scope=scope, author_id=author_id, assignee_id=assignee_id, tester_id=tester_id, search=search, sort=sort, direction=direction, group_id=group_id, milestone_id=milestone_id, labels=labels, label_ids=label_ids, can_be_merged=can_be_merged, project_id=project_id, need_state_count=need_state_count, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProgramsApi->get_enterprise_id_programs_program_id_pull_requests: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **program_id** | **int**| 项目 id | 
 **enterprise_id** | **int**| 企业 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **state** | **str**| PR 状态 | [optional] 
 **scope** | **str**| 范围筛选。指派我的: assigned_or_test，我创建或指派给我的: related_to_me，我参与仓库的PR: participate_in | [optional] 
 **author_id** | **str**| 筛选 PR 创建者 | [optional] 
 **assignee_id** | **str**| 筛选 PR 审查者 | [optional] 
 **tester_id** | **str**| 筛选 PR 测试人员 | [optional] 
 **search** | **str**| 搜索参数 | [optional] 
 **sort** | **str**| 排序字段(created_at、closed_at、priority) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **group_id** | **int**| 团队 id | [optional] 
 **milestone_id** | **int**| 里程碑 id | [optional] 
 **labels** | **str**| 标签名称。多个标签逗号(,)隔开 | [optional] 
 **label_ids** | **str**| 标签ID,多个标签逗号(,)隔开 | [optional] 
 **can_be_merged** | **int**| 是否可合并 | [optional] 
 **project_id** | **int**| 仓库 id | [optional] 
 **need_state_count** | **int**| 是否需要状态统计数 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[Issue]**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_programs_unset**
> get_enterprise_id_programs_unset(enterprise_id, access_token=access_token)

获取未立项项目

获取未立项项目

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取未立项项目
    api_instance.get_enterprise_id_programs_unset(enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling ProgramsApi->get_enterprise_id_programs_unset: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_programs**
> Program post_enterprise_id_programs(body, enterprise_id)

新建项目

新建项目

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
body = gitee_api.EnterpriseIdProgramsBody() # EnterpriseIdProgramsBody | 
enterprise_id = 56 # int | 

try:
    # 新建项目
    api_response = api_instance.post_enterprise_id_programs(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProgramsApi->post_enterprise_id_programs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnterpriseIdProgramsBody**](EnterpriseIdProgramsBody.md)|  | 
 **enterprise_id** | **int**|  | 

### Return type

[**Program**](Program.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_programs_program_id_members**
> Member post_enterprise_id_programs_program_id_members(program_id, enterprise_id, body=body)

添加企业成员或团队进项目

添加企业成员或团队进项目

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
program_id = 56 # int | 项目 id
enterprise_id = 56 # int | 
body = gitee_api.ProgramIdMembersBody() # ProgramIdMembersBody |  (optional)

try:
    # 添加企业成员或团队进项目
    api_response = api_instance.post_enterprise_id_programs_program_id_members(program_id, enterprise_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProgramsApi->post_enterprise_id_programs_program_id_members: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **program_id** | **int**| 项目 id | 
 **enterprise_id** | **int**|  | 
 **body** | [**ProgramIdMembersBody**](ProgramIdMembersBody.md)|  | [optional] 

### Return type

[**Member**](Member.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_programs_program_id_projects**
> Project post_enterprise_id_programs_program_id_projects(body, program_id, enterprise_id)

项目关联仓库

项目关联仓库

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
body = gitee_api.ProgramIdProjectsBody() # ProgramIdProjectsBody | 
program_id = 56 # int | 项目 id
enterprise_id = 56 # int | 

try:
    # 项目关联仓库
    api_response = api_instance.post_enterprise_id_programs_program_id_projects(body, program_id, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProgramsApi->post_enterprise_id_programs_program_id_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProgramIdProjectsBody**](ProgramIdProjectsBody.md)|  | 
 **program_id** | **int**| 项目 id | 
 **enterprise_id** | **int**|  | 

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_programs_program_id**
> Program put_enterprise_id_programs_program_id(body, program_id, enterprise_id)

更新项目

更新项目

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProgramsApi()
body = gitee_api.ProgramsProgramIdBody() # ProgramsProgramIdBody | 
program_id = 56 # int | 项目 id
enterprise_id = 56 # int | 

try:
    # 更新项目
    api_response = api_instance.put_enterprise_id_programs_program_id(body, program_id, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProgramsApi->put_enterprise_id_programs_program_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProgramsProgramIdBody**](ProgramsProgramIdBody.md)|  | 
 **program_id** | **int**| 项目 id | 
 **enterprise_id** | **int**|  | 

### Return type

[**Program**](Program.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


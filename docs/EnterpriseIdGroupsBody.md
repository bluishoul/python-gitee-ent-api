# EnterpriseIdGroupsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**path** | **str** | 团队 path | 
**name** | **str** | 名称 | 
**description** | **str** | 简介 | [optional] 
**public** | **int** | 类型, 0:内部，1:公开，2:外包 | 
**owner_id** | **int** | 负责人 id | 
**user_ids** | **str** | 成员ids, 逗号隔开：1,2,3 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


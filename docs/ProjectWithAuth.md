# ProjectWithAuth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 仓库ID | [optional] 
**name** | **str** | 仓库名称 | [optional] 
**path** | **str** | 仓库路径 | [optional] 
**public** | **int** | 仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开; | [optional] 
**enterprise_id** | **int** | 企业 id | [optional] 
**security_hole_enabled** | **bool** | 是否允许用户创建涉及敏感信息的任务 | [optional] 
**namespace** | [**Namespace**](Namespace.md) |  | [optional] 
**creator** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**can_quit** | **bool** | 能否编辑退出 | [optional] 
**project_access** | **int** | 当前成员在该仓库的角色 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# gitee_api.WeekReportsApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_week_reports_report_id_comment_comment_id**](WeekReportsApi.md#delete_enterprise_id_week_reports_report_id_comment_comment_id) | **DELETE** /{enterprise_id}/week_reports/{report_id}/comment/{comment_id} | 删除周报某个评论
[**delete_enterprise_id_week_reports_templates_template_id**](WeekReportsApi.md#delete_enterprise_id_week_reports_templates_template_id) | **DELETE** /{enterprise_id}/week_reports/templates/{template_id} | 删除周报模版
[**get_enterprise_id_week_reports_member_reports**](WeekReportsApi.md#get_enterprise_id_week_reports_member_reports) | **GET** /{enterprise_id}/week_reports/member_reports | 成员周报列表
[**get_enterprise_id_week_reports_my_reports**](WeekReportsApi.md#get_enterprise_id_week_reports_my_reports) | **GET** /{enterprise_id}/week_reports/my_reports | 获取授权用户的周报列表
[**get_enterprise_id_week_reports_not_submit_users**](WeekReportsApi.md#get_enterprise_id_week_reports_not_submit_users) | **GET** /{enterprise_id}/week_reports/not_submit_users | 获取未提交周报的用户列表
[**get_enterprise_id_week_reports_relate_events**](WeekReportsApi.md#get_enterprise_id_week_reports_relate_events) | **GET** /{enterprise_id}/week_reports/relate_events | 周报可关联动态列表
[**get_enterprise_id_week_reports_relations**](WeekReportsApi.md#get_enterprise_id_week_reports_relations) | **GET** /{enterprise_id}/week_reports/relations | 获取周报可关联issue和pull request
[**get_enterprise_id_week_reports_report_id_comments**](WeekReportsApi.md#get_enterprise_id_week_reports_report_id_comments) | **GET** /{enterprise_id}/week_reports/{report_id}/comments | 某个周报评论列表
[**get_enterprise_id_week_reports_templates**](WeekReportsApi.md#get_enterprise_id_week_reports_templates) | **GET** /{enterprise_id}/week_reports/templates | 周报模版列表
[**get_enterprise_id_week_reports_templates_template_id**](WeekReportsApi.md#get_enterprise_id_week_reports_templates_template_id) | **GET** /{enterprise_id}/week_reports/templates/{template_id} | 查看周报模版详情
[**get_enterprise_id_week_reports_username_year_week_index**](WeekReportsApi.md#get_enterprise_id_week_reports_username_year_week_index) | **GET** /{enterprise_id}/week_reports/{username}/{year}/{week_index} | 获取某人某年某周的周报详情
[**get_enterprise_id_week_reports_year_weeks**](WeekReportsApi.md#get_enterprise_id_week_reports_year_weeks) | **GET** /{enterprise_id}/week_reports/{year}/weeks | 获取企业某年存在周报的周
[**get_enterprise_id_week_reports_years**](WeekReportsApi.md#get_enterprise_id_week_reports_years) | **GET** /{enterprise_id}/week_reports/years | 获取企业存在周报的年份
[**post_enterprise_id_week_reports_preview_data**](WeekReportsApi.md#post_enterprise_id_week_reports_preview_data) | **POST** /{enterprise_id}/week_reports/preview_data | 预览周报
[**post_enterprise_id_week_reports_report_id_comment**](WeekReportsApi.md#post_enterprise_id_week_reports_report_id_comment) | **POST** /{enterprise_id}/week_reports/{report_id}/comment | 评论周报
[**post_enterprise_id_week_reports_templates**](WeekReportsApi.md#post_enterprise_id_week_reports_templates) | **POST** /{enterprise_id}/week_reports/templates | 新增周报模版
[**put_enterprise_id_week_reports_templates_template_id**](WeekReportsApi.md#put_enterprise_id_week_reports_templates_template_id) | **PUT** /{enterprise_id}/week_reports/templates/{template_id} | 编辑周报模版
[**put_enterprise_id_week_reports_username_year_week_index**](WeekReportsApi.md#put_enterprise_id_week_reports_username_year_week_index) | **PUT** /{enterprise_id}/week_reports/{username}/{year}/{week_index} | 新建/编辑周报

# **delete_enterprise_id_week_reports_report_id_comment_comment_id**
> delete_enterprise_id_week_reports_report_id_comment_comment_id(enterprise_id, report_id, comment_id, access_token=access_token)

删除周报某个评论

删除周报某个评论

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
report_id = 56 # int | 周报的ID
comment_id = 56 # int | 评论ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 删除周报某个评论
    api_instance.delete_enterprise_id_week_reports_report_id_comment_comment_id(enterprise_id, report_id, comment_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling WeekReportsApi->delete_enterprise_id_week_reports_report_id_comment_comment_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **report_id** | **int**| 周报的ID | 
 **comment_id** | **int**| 评论ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_week_reports_templates_template_id**
> delete_enterprise_id_week_reports_templates_template_id(enterprise_id, template_id, access_token=access_token)

删除周报模版

删除周报模版

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
template_id = 56 # int | 模版ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 删除周报模版
    api_instance.delete_enterprise_id_week_reports_templates_template_id(enterprise_id, template_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling WeekReportsApi->delete_enterprise_id_week_reports_templates_template_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **template_id** | **int**| 模版ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_week_reports_member_reports**
> MemberWeekReport get_enterprise_id_week_reports_member_reports(enterprise_id, access_token=access_token, year=year, week_index=week_index, program_id=program_id, group_id=group_id, member_ids=member_ids, _date=_date, page=page, per_page=per_page)

成员周报列表

成员周报列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
year = 2022 # int | 年份 (optional) (default to 2022)
week_index = 56 # int | 周报所属周 (optional)
program_id = 56 # int | 项目ID (optional)
group_id = 56 # int | 团队ID (optional)
member_ids = 'member_ids_example' # str | 成员ID，逗号分隔，如：1,2 (optional)
_date = '_date_example' # str | 周报日期(格式：2019-03-25) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 成员周报列表
    api_response = api_instance.get_enterprise_id_week_reports_member_reports(enterprise_id, access_token=access_token, year=year, week_index=week_index, program_id=program_id, group_id=group_id, member_ids=member_ids, _date=_date, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->get_enterprise_id_week_reports_member_reports: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **year** | **int**| 年份 | [optional] [default to 2022]
 **week_index** | **int**| 周报所属周 | [optional] 
 **program_id** | **int**| 项目ID | [optional] 
 **group_id** | **int**| 团队ID | [optional] 
 **member_ids** | **str**| 成员ID，逗号分隔，如：1,2 | [optional] 
 **_date** | **str**| 周报日期(格式：2019-03-25) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**MemberWeekReport**](MemberWeekReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_week_reports_my_reports**
> MyWeekReport get_enterprise_id_week_reports_my_reports(enterprise_id, access_token=access_token, year=year)

获取授权用户的周报列表

获取授权用户的周报列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
year = 2022 # int | 年份 (optional) (default to 2022)

try:
    # 获取授权用户的周报列表
    api_response = api_instance.get_enterprise_id_week_reports_my_reports(enterprise_id, access_token=access_token, year=year)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->get_enterprise_id_week_reports_my_reports: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **year** | **int**| 年份 | [optional] [default to 2022]

### Return type

[**MyWeekReport**](MyWeekReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_week_reports_not_submit_users**
> MyWeekReport get_enterprise_id_week_reports_not_submit_users(enterprise_id, year, access_token=access_token, week_index=week_index, program_id=program_id)

获取未提交周报的用户列表

获取未提交周报的用户列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
year = 2022 # int | 年份 (default to 2022)
access_token = 'access_token_example' # str | 用户授权码 (optional)
week_index = 56 # int | 当前周数（默认当前周） (optional)
program_id = 56 # int | 项目的 id (optional)

try:
    # 获取未提交周报的用户列表
    api_response = api_instance.get_enterprise_id_week_reports_not_submit_users(enterprise_id, year, access_token=access_token, week_index=week_index, program_id=program_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->get_enterprise_id_week_reports_not_submit_users: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **year** | **int**| 年份 | [default to 2022]
 **access_token** | **str**| 用户授权码 | [optional] 
 **week_index** | **int**| 当前周数（默认当前周） | [optional] 
 **program_id** | **int**| 项目的 id | [optional] 

### Return type

[**MyWeekReport**](MyWeekReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_week_reports_relate_events**
> Event get_enterprise_id_week_reports_relate_events(enterprise_id, year, week_index, access_token=access_token, event_ids=event_ids, page=page, per_page=per_page)

周报可关联动态列表

周报可关联动态列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
year = 56 # int | 周报所属年
week_index = 56 # int | 周报所属周
access_token = 'access_token_example' # str | 用户授权码 (optional)
event_ids = 56 # int | 一关联的动态ids， 用逗号分隔，如：1,2,3 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 周报可关联动态列表
    api_response = api_instance.get_enterprise_id_week_reports_relate_events(enterprise_id, year, week_index, access_token=access_token, event_ids=event_ids, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->get_enterprise_id_week_reports_relate_events: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **year** | **int**| 周报所属年 | 
 **week_index** | **int**| 周报所属周 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **event_ids** | **int**| 一关联的动态ids， 用逗号分隔，如：1,2,3 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**Event**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_week_reports_relations**
> WeekReportRelation get_enterprise_id_week_reports_relations(enterprise_id, year, week_index, access_token=access_token)

获取周报可关联issue和pull request

获取周报可关联issue和pull request

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
year = 56 # int | 周报所属年
week_index = 56 # int | 周报所属周
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取周报可关联issue和pull request
    api_response = api_instance.get_enterprise_id_week_reports_relations(enterprise_id, year, week_index, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->get_enterprise_id_week_reports_relations: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **year** | **int**| 周报所属年 | 
 **week_index** | **int**| 周报所属周 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**WeekReportRelation**](WeekReportRelation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_week_reports_report_id_comments**
> WeekReportNote get_enterprise_id_week_reports_report_id_comments(enterprise_id, report_id, access_token=access_token)

某个周报评论列表

某个周报评论列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
report_id = 56 # int | 周报的ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 某个周报评论列表
    api_response = api_instance.get_enterprise_id_week_reports_report_id_comments(enterprise_id, report_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->get_enterprise_id_week_reports_report_id_comments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **report_id** | **int**| 周报的ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**WeekReportNote**](WeekReportNote.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_week_reports_templates**
> WeekReportTemplate get_enterprise_id_week_reports_templates(enterprise_id, access_token=access_token, page=page, per_page=per_page)

周报模版列表

周报模版列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 周报模版列表
    api_response = api_instance.get_enterprise_id_week_reports_templates(enterprise_id, access_token=access_token, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->get_enterprise_id_week_reports_templates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**WeekReportTemplate**](WeekReportTemplate.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_week_reports_templates_template_id**
> WeekReportTemplate get_enterprise_id_week_reports_templates_template_id(enterprise_id, template_id, access_token=access_token)

查看周报模版详情

查看周报模版详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
template_id = 56 # int | 模版ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 查看周报模版详情
    api_response = api_instance.get_enterprise_id_week_reports_templates_template_id(enterprise_id, template_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->get_enterprise_id_week_reports_templates_template_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **template_id** | **int**| 模版ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**WeekReportTemplate**](WeekReportTemplate.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_week_reports_username_year_week_index**
> WeekReportDetail get_enterprise_id_week_reports_username_year_week_index(enterprise_id, username, year, week_index, access_token=access_token)

获取某人某年某周的周报详情

获取某人某年某周的周报详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
username = 'username_example' # str | 用户个性域名
year = 56 # int | 周报所处年
week_index = 56 # int | 周报所处周
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取某人某年某周的周报详情
    api_response = api_instance.get_enterprise_id_week_reports_username_year_week_index(enterprise_id, username, year, week_index, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->get_enterprise_id_week_reports_username_year_week_index: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **username** | **str**| 用户个性域名 | 
 **year** | **int**| 周报所处年 | 
 **week_index** | **int**| 周报所处周 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**WeekReportDetail**](WeekReportDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_week_reports_year_weeks**
> get_enterprise_id_week_reports_year_weeks(enterprise_id, year, access_token=access_token)

获取企业某年存在周报的周

获取企业某年存在周报的周

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
year = 56 # int | 周报所属年
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取企业某年存在周报的周
    api_instance.get_enterprise_id_week_reports_year_weeks(enterprise_id, year, access_token=access_token)
except ApiException as e:
    print("Exception when calling WeekReportsApi->get_enterprise_id_week_reports_year_weeks: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **year** | **int**| 周报所属年 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_week_reports_years**
> get_enterprise_id_week_reports_years(enterprise_id, access_token=access_token)

获取企业存在周报的年份

获取企业存在周报的年份

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取企业存在周报的年份
    api_instance.get_enterprise_id_week_reports_years(enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling WeekReportsApi->get_enterprise_id_week_reports_years: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_week_reports_preview_data**
> WeekReportPreview post_enterprise_id_week_reports_preview_data(body, enterprise_id)

预览周报

预览周报

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
body = gitee_api.WeekReportsPreviewDataBody() # WeekReportsPreviewDataBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 预览周报
    api_response = api_instance.post_enterprise_id_week_reports_preview_data(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->post_enterprise_id_week_reports_preview_data: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WeekReportsPreviewDataBody**](WeekReportsPreviewDataBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

[**WeekReportPreview**](WeekReportPreview.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_week_reports_report_id_comment**
> WeekReportNote post_enterprise_id_week_reports_report_id_comment(body, enterprise_id, report_id)

评论周报

评论周报

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
body = gitee_api.ReportIdCommentBody() # ReportIdCommentBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
report_id = 56 # int | 周报的ID

try:
    # 评论周报
    api_response = api_instance.post_enterprise_id_week_reports_report_id_comment(body, enterprise_id, report_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->post_enterprise_id_week_reports_report_id_comment: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ReportIdCommentBody**](ReportIdCommentBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **report_id** | **int**| 周报的ID | 

### Return type

[**WeekReportNote**](WeekReportNote.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_week_reports_templates**
> WeekReportTemplate post_enterprise_id_week_reports_templates(body, enterprise_id)

新增周报模版

新增周报模版

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
body = gitee_api.WeekReportsTemplatesBody() # WeekReportsTemplatesBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 新增周报模版
    api_response = api_instance.post_enterprise_id_week_reports_templates(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->post_enterprise_id_week_reports_templates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WeekReportsTemplatesBody**](WeekReportsTemplatesBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

[**WeekReportTemplate**](WeekReportTemplate.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_week_reports_templates_template_id**
> WeekReportTemplate put_enterprise_id_week_reports_templates_template_id(enterprise_id, template_id, body=body)

编辑周报模版

编辑周报模版

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
template_id = 56 # int | 模版ID
body = gitee_api.TemplatesTemplateIdBody() # TemplatesTemplateIdBody |  (optional)

try:
    # 编辑周报模版
    api_response = api_instance.put_enterprise_id_week_reports_templates_template_id(enterprise_id, template_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->put_enterprise_id_week_reports_templates_template_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **template_id** | **int**| 模版ID | 
 **body** | [**TemplatesTemplateIdBody**](TemplatesTemplateIdBody.md)|  | [optional] 

### Return type

[**WeekReportTemplate**](WeekReportTemplate.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_week_reports_username_year_week_index**
> WeekReportDetail put_enterprise_id_week_reports_username_year_week_index(body, enterprise_id, year, week_index, username)

新建/编辑周报

新建/编辑周报

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WeekReportsApi()
body = gitee_api.YearWeekIndexBody() # YearWeekIndexBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
year = 56 # int | 周报所属年
week_index = 56 # int | 周报所属周
username = 'username_example' # str | 用户名(username/login)

try:
    # 新建/编辑周报
    api_response = api_instance.put_enterprise_id_week_reports_username_year_week_index(body, enterprise_id, year, week_index, username)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeekReportsApi->put_enterprise_id_week_reports_username_year_week_index: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**YearWeekIndexBody**](YearWeekIndexBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **year** | **int**| 周报所属年 | 
 **week_index** | **int**| 周报所属周 | 
 **username** | **str**| 用户名(username/login) | 

### Return type

[**WeekReportDetail**](WeekReportDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


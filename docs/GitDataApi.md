# gitee_api.GitDataApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_projects_project_id_releases_release_id**](GitDataApi.md#delete_enterprise_id_projects_project_id_releases_release_id) | **DELETE** /{enterprise_id}/projects/{project_id}/releases/{release_id} | 删除发行版
[**get_enterprise_id_projects**](GitDataApi.md#get_enterprise_id_projects) | **GET** /{enterprise_id}/projects | 获取授权用户参与的仓库列表
[**get_enterprise_id_projects_for_pull_request**](GitDataApi.md#get_enterprise_id_projects_for_pull_request) | **GET** /{enterprise_id}/projects/for_pull_request | 获取当前用户有权限提pr的仓库
[**get_enterprise_id_projects_project_id_can_pull**](GitDataApi.md#get_enterprise_id_projects_project_id_can_pull) | **GET** /{enterprise_id}/projects/{project_id}/can_pull | 获取可创建 Pull Request 的仓库
[**get_enterprise_id_projects_project_id_check_releases**](GitDataApi.md#get_enterprise_id_projects_project_id_check_releases) | **GET** /{enterprise_id}/projects/{project_id}/check_releases | 检查发行版是否存在
[**get_enterprise_id_projects_project_id_contributors**](GitDataApi.md#get_enterprise_id_projects_project_id_contributors) | **GET** /{enterprise_id}/projects/{project_id}/contributors | 获取仓库贡献者列表
[**get_enterprise_id_projects_project_id_events**](GitDataApi.md#get_enterprise_id_projects_project_id_events) | **GET** /{enterprise_id}/projects/{project_id}/events | 获取仓库动态
[**get_enterprise_id_projects_project_id_issues**](GitDataApi.md#get_enterprise_id_projects_project_id_issues) | **GET** /{enterprise_id}/projects/{project_id}/issues | 获取仓库的任务列表
[**get_enterprise_id_projects_project_id_operate_auths**](GitDataApi.md#get_enterprise_id_projects_project_id_operate_auths) | **GET** /{enterprise_id}/projects/{project_id}/operate_auths | 获取仓库的操作权限
[**get_enterprise_id_projects_project_id_programs**](GitDataApi.md#get_enterprise_id_projects_project_id_programs) | **GET** /{enterprise_id}/projects/{project_id}/programs | 获取仓库的关联项目列表
[**get_enterprise_id_projects_project_id_pull_requests**](GitDataApi.md#get_enterprise_id_projects_project_id_pull_requests) | **GET** /{enterprise_id}/projects/{project_id}/pull_requests | 获取仓库的 Pull Request 列表
[**get_enterprise_id_projects_project_id_releases**](GitDataApi.md#get_enterprise_id_projects_project_id_releases) | **GET** /{enterprise_id}/projects/{project_id}/releases | 查看发行版列表
[**get_enterprise_id_projects_project_id_releases_tag_version**](GitDataApi.md#get_enterprise_id_projects_project_id_releases_tag_version) | **GET** /{enterprise_id}/projects/{project_id}/releases/{tag_version} | 查看发行版详情
[**get_enterprise_id_projects_project_id_settings**](GitDataApi.md#get_enterprise_id_projects_project_id_settings) | **GET** /{enterprise_id}/projects/{project_id}/settings | 获取仓库基本设置
[**get_enterprise_id_projects_project_id_users**](GitDataApi.md#get_enterprise_id_projects_project_id_users) | **GET** /{enterprise_id}/projects/{project_id}/users | 获取仓库的成员列表
[**post_enterprise_id_projects**](GitDataApi.md#post_enterprise_id_projects) | **POST** /{enterprise_id}/projects | 新建仓库
[**post_enterprise_id_projects_check_project_can_import**](GitDataApi.md#post_enterprise_id_projects_check_project_can_import) | **POST** /{enterprise_id}/projects/check_project_can_import | 新建仓库-导入仓库参数是否有效
[**post_enterprise_id_projects_check_project_name**](GitDataApi.md#post_enterprise_id_projects_check_project_name) | **POST** /{enterprise_id}/projects/check_project_name | 新建仓库-仓库名/路径是否已经存在
[**post_enterprise_id_projects_project_id_releases**](GitDataApi.md#post_enterprise_id_projects_project_id_releases) | **POST** /{enterprise_id}/projects/{project_id}/releases | 新建发行版
[**post_enterprise_id_projects_transfer_code**](GitDataApi.md#post_enterprise_id_projects_transfer_code) | **POST** /{enterprise_id}/projects/transfer_code | 获取仓库转入-转移码
[**put_enterprise_id_projects_project_id**](GitDataApi.md#put_enterprise_id_projects_project_id) | **PUT** /{enterprise_id}/projects/{project_id} | 更新仓库设置
[**put_enterprise_id_projects_project_id_releases_release_id**](GitDataApi.md#put_enterprise_id_projects_project_id_releases_release_id) | **PUT** /{enterprise_id}/projects/{project_id}/releases/{release_id} | 编辑发行版

# **delete_enterprise_id_projects_project_id_releases_release_id**
> Release delete_enterprise_id_projects_project_id_releases_release_id(enterprise_id, project_id, release_id, access_token=access_token, qt=qt)

删除发行版

删除发行版

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
release_id = 56 # int | 发行版 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 删除发行版
    api_response = api_instance.delete_enterprise_id_projects_project_id_releases_release_id(enterprise_id, project_id, release_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->delete_enterprise_id_projects_project_id_releases_release_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **release_id** | **int**| 发行版 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects**
> ProjectDetail get_enterprise_id_projects(enterprise_id, access_token=access_token, scope=scope, search=search, type=type, status=status, creator_id=creator_id, parent_id=parent_id, fork_filter=fork_filter, outsourced=outsourced, group_id=group_id, sort=sort, direction=direction, page=page, per_page=per_page)

获取授权用户参与的仓库列表

获取授权用户参与的仓库列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
scope = 'scope_example' # str | 范围筛选 (optional)
search = 'search_example' # str | 搜索参数 (optional)
type = 'type_example' # str | 与我相关，created：我创建的，joined：我参与的，star: 我收藏的，template：模版仓库 (optional)
status = 56 # int | 状态: 0: 开始，1: 暂停，2: 关闭  (optional)
creator_id = 56 # int | 负责人 (optional)
parent_id = 56 # int | form_from仓库id (optional)
fork_filter = 'fork_filter_example' # str | 非fork的(not_fork), 只看fork的(only_fork) (optional)
outsourced = 56 # int | 是否外包：0：内部，1：外包 (optional)
group_id = 56 # int | 团队id (optional)
sort = 'sort_example' # str | 排序字段(created_at、last_push_at) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取授权用户参与的仓库列表
    api_response = api_instance.get_enterprise_id_projects(enterprise_id, access_token=access_token, scope=scope, search=search, type=type, status=status, creator_id=creator_id, parent_id=parent_id, fork_filter=fork_filter, outsourced=outsourced, group_id=group_id, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **scope** | **str**| 范围筛选 | [optional] 
 **search** | **str**| 搜索参数 | [optional] 
 **type** | **str**| 与我相关，created：我创建的，joined：我参与的，star: 我收藏的，template：模版仓库 | [optional] 
 **status** | **int**| 状态: 0: 开始，1: 暂停，2: 关闭  | [optional] 
 **creator_id** | **int**| 负责人 | [optional] 
 **parent_id** | **int**| form_from仓库id | [optional] 
 **fork_filter** | **str**| 非fork的(not_fork), 只看fork的(only_fork) | [optional] 
 **outsourced** | **int**| 是否外包：0：内部，1：外包 | [optional] 
 **group_id** | **int**| 团队id | [optional] 
 **sort** | **str**| 排序字段(created_at、last_push_at) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**ProjectDetail**](ProjectDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_for_pull_request**
> ProjectDetail get_enterprise_id_projects_for_pull_request(enterprise_id, access_token=access_token, search=search, page=page, per_page=per_page)

获取当前用户有权限提pr的仓库

获取当前用户有权限提pr的仓库

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
search = 'search_example' # str | 搜索字符串 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取当前用户有权限提pr的仓库
    api_response = api_instance.get_enterprise_id_projects_for_pull_request(enterprise_id, access_token=access_token, search=search, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects_for_pull_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **search** | **str**| 搜索字符串 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**ProjectDetail**](ProjectDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_can_pull**
> Project get_enterprise_id_projects_project_id_can_pull(enterprise_id, project_id, access_token=access_token, qt=qt)

获取可创建 Pull Request 的仓库

获取可创建 Pull Request 的仓库

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 获取可创建 Pull Request 的仓库
    api_response = api_instance.get_enterprise_id_projects_project_id_can_pull(enterprise_id, project_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects_project_id_can_pull: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_check_releases**
> get_enterprise_id_projects_project_id_check_releases(enterprise_id, project_id, tag_version, access_token=access_token, qt=qt)

检查发行版是否存在

检查发行版是否存在

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
tag_version = 'tag_version_example' # str | 发行版本
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 检查发行版是否存在
    api_instance.get_enterprise_id_projects_project_id_check_releases(enterprise_id, project_id, tag_version, access_token=access_token, qt=qt)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects_project_id_check_releases: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **tag_version** | **str**| 发行版本 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_contributors**
> list[ProjectContributor] get_enterprise_id_projects_project_id_contributors(enterprise_id, project_id, access_token=access_token, qt=qt, ref=ref, page=page, per_page=per_page)

获取仓库贡献者列表

获取仓库贡献者列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
ref = 'ref_example' # str | 分支, 不传则取仓库的默认分支 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取仓库贡献者列表
    api_response = api_instance.get_enterprise_id_projects_project_id_contributors(enterprise_id, project_id, access_token=access_token, qt=qt, ref=ref, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects_project_id_contributors: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **ref** | **str**| 分支, 不传则取仓库的默认分支 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[ProjectContributor]**](ProjectContributor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_events**
> Event get_enterprise_id_projects_project_id_events(enterprise_id, project_id, access_token=access_token, qt=qt, start_date=start_date, end_date=end_date, page=page, per_page=per_page, prev_id=prev_id, limit=limit)

获取仓库动态

获取仓库动态

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)
prev_id = 56 # int | 上一次动态列表中最小动态 ID (返回列表不包含该ID记录) (optional)
limit = 56 # int | 每次获取动态的条数 (optional)

try:
    # 获取仓库动态
    api_response = api_instance.get_enterprise_id_projects_project_id_events(enterprise_id, project_id, access_token=access_token, qt=qt, start_date=start_date, end_date=end_date, page=page, per_page=per_page, prev_id=prev_id, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects_project_id_events: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 
 **prev_id** | **int**| 上一次动态列表中最小动态 ID (返回列表不包含该ID记录) | [optional] 
 **limit** | **int**| 每次获取动态的条数 | [optional] 

### Return type

[**Event**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_issues**
> list[Issue] get_enterprise_id_projects_project_id_issues(enterprise_id, project_id, access_token=access_token, qt=qt, program_id=program_id, milestone_id=milestone_id, state=state, only_related_me=only_related_me, assignee_id=assignee_id, author_id=author_id, collaborator_ids=collaborator_ids, created_at=created_at, finished_at=finished_at, plan_started_at=plan_started_at, deadline=deadline, search=search, filter_child=filter_child, issue_state_ids=issue_state_ids, issue_type_id=issue_type_id, label_ids=label_ids, priority=priority, scrum_sprint_ids=scrum_sprint_ids, scrum_version_ids=scrum_version_ids, kanban_ids=kanban_ids, kanban_column_ids=kanban_column_ids, sort=sort, direction=direction, page=page, per_page=per_page)

获取仓库的任务列表

获取仓库的任务列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业 id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
program_id = 'program_id_example' # str | 项目 id (optional)
milestone_id = 'milestone_id_example' # str | 里程碑 id (optional)
state = 'state_example' # str | 任务状态属性，可多选，用逗号分隔。（开启：open 关闭：closed 拒绝：rejected 进行中: progressing） (optional)
only_related_me = 'only_related_me_example' # str | 是否仅列出与授权用户相关的任务（0: 否 1: 是） (optional)
assignee_id = 'assignee_id_example' # str | 负责人 id (optional)
author_id = 'author_id_example' # str | 创建者 id (optional)
collaborator_ids = 'collaborator_ids_example' # str | 协作者。(,分隔的id字符串) (optional)
created_at = 'created_at_example' # str | 创建时间，格式：(区间)yyyymmddTHH:MM:SS+08:00-yyyymmddTHH:MM:SS+08:00，（指定某日期）yyyymmddTHH:MM:SS+08:00，（小于指定日期）<yyyymmddTHH:MM:SS+08:00，（大于指定日期）>yyyymmddTHH:MM:SS+08:00 (optional)
finished_at = 'finished_at_example' # str | 任务完成日期，格式同上 (optional)
plan_started_at = 'plan_started_at_example' # str | 计划开始时间，(格式：yyyy-mm-dd) (optional)
deadline = 'deadline_example' # str | 任务截止日期，(格式：yyyy-mm-dd) (optional)
search = 'search_example' # str | 搜索参数 (optional)
filter_child = 'filter_child_example' # str | 是否过滤子任务(0: 否, 1: 是) (optional)
issue_state_ids = 'issue_state_ids_example' # str | 任务状态id，多选，用逗号分隔。 (optional)
issue_type_id = 'issue_type_id_example' # str | 任务类型 (optional)
label_ids = 'label_ids_example' # str | 标签 id（可多选，用逗号分隔） (optional)
priority = 'priority_example' # str | 优先级（可多选，用逗号分隔） (optional)
scrum_sprint_ids = 'scrum_sprint_ids_example' # str | 迭代id(可多选，用逗号分隔) (optional)
scrum_version_ids = 'scrum_version_ids_example' # str | 版本id(可多选，用逗号分隔) (optional)
kanban_ids = 'kanban_ids_example' # str | 看板id(可多选，用逗号分隔) (optional)
kanban_column_ids = 'kanban_column_ids_example' # str | 看板栏id(可多选，用逗号分隔) (optional)
sort = 'sort_example' # str | 排序字段(created_at、updated_at、deadline、priority) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取仓库的任务列表
    api_response = api_instance.get_enterprise_id_projects_project_id_issues(enterprise_id, project_id, access_token=access_token, qt=qt, program_id=program_id, milestone_id=milestone_id, state=state, only_related_me=only_related_me, assignee_id=assignee_id, author_id=author_id, collaborator_ids=collaborator_ids, created_at=created_at, finished_at=finished_at, plan_started_at=plan_started_at, deadline=deadline, search=search, filter_child=filter_child, issue_state_ids=issue_state_ids, issue_type_id=issue_type_id, label_ids=label_ids, priority=priority, scrum_sprint_ids=scrum_sprint_ids, scrum_version_ids=scrum_version_ids, kanban_ids=kanban_ids, kanban_column_ids=kanban_column_ids, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects_project_id_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业 id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **program_id** | **str**| 项目 id | [optional] 
 **milestone_id** | **str**| 里程碑 id | [optional] 
 **state** | **str**| 任务状态属性，可多选，用逗号分隔。（开启：open 关闭：closed 拒绝：rejected 进行中: progressing） | [optional] 
 **only_related_me** | **str**| 是否仅列出与授权用户相关的任务（0: 否 1: 是） | [optional] 
 **assignee_id** | **str**| 负责人 id | [optional] 
 **author_id** | **str**| 创建者 id | [optional] 
 **collaborator_ids** | **str**| 协作者。(,分隔的id字符串) | [optional] 
 **created_at** | **str**| 创建时间，格式：(区间)yyyymmddTHH:MM:SS+08:00-yyyymmddTHH:MM:SS+08:00，（指定某日期）yyyymmddTHH:MM:SS+08:00，（小于指定日期）&lt;yyyymmddTHH:MM:SS+08:00，（大于指定日期）&gt;yyyymmddTHH:MM:SS+08:00 | [optional] 
 **finished_at** | **str**| 任务完成日期，格式同上 | [optional] 
 **plan_started_at** | **str**| 计划开始时间，(格式：yyyy-mm-dd) | [optional] 
 **deadline** | **str**| 任务截止日期，(格式：yyyy-mm-dd) | [optional] 
 **search** | **str**| 搜索参数 | [optional] 
 **filter_child** | **str**| 是否过滤子任务(0: 否, 1: 是) | [optional] 
 **issue_state_ids** | **str**| 任务状态id，多选，用逗号分隔。 | [optional] 
 **issue_type_id** | **str**| 任务类型 | [optional] 
 **label_ids** | **str**| 标签 id（可多选，用逗号分隔） | [optional] 
 **priority** | **str**| 优先级（可多选，用逗号分隔） | [optional] 
 **scrum_sprint_ids** | **str**| 迭代id(可多选，用逗号分隔) | [optional] 
 **scrum_version_ids** | **str**| 版本id(可多选，用逗号分隔) | [optional] 
 **kanban_ids** | **str**| 看板id(可多选，用逗号分隔) | [optional] 
 **kanban_column_ids** | **str**| 看板栏id(可多选，用逗号分隔) | [optional] 
 **sort** | **str**| 排序字段(created_at、updated_at、deadline、priority) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[Issue]**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_operate_auths**
> UsersProjects get_enterprise_id_projects_project_id_operate_auths(enterprise_id, project_id, access_token=access_token, qt=qt)

获取仓库的操作权限

获取仓库的操作权限

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 获取仓库的操作权限
    api_response = api_instance.get_enterprise_id_projects_project_id_operate_auths(enterprise_id, project_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects_project_id_operate_auths: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**UsersProjects**](UsersProjects.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_programs**
> Program get_enterprise_id_projects_project_id_programs(enterprise_id, project_id, access_token=access_token, qt=qt, scope=scope, page=page, per_page=per_page)

获取仓库的关联项目列表

获取仓库的关联项目列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
scope = 'scope_example' # str | not_joined (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取仓库的关联项目列表
    api_response = api_instance.get_enterprise_id_projects_project_id_programs(enterprise_id, project_id, access_token=access_token, qt=qt, scope=scope, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects_project_id_programs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **scope** | **str**| not_joined | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**Program**](Program.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_pull_requests**
> PullRequest get_enterprise_id_projects_project_id_pull_requests(enterprise_id, project_id, access_token=access_token, qt=qt, state=state, sort=sort, direction=direction, page=page, per_page=per_page)

获取仓库的 Pull Request 列表

获取仓库的 Pull Request 列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
state = 'state_example' # str | PR 状态 (optional)
sort = 'sort_example' # str | 排序字段(created_at、updated_at) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取仓库的 Pull Request 列表
    api_response = api_instance.get_enterprise_id_projects_project_id_pull_requests(enterprise_id, project_id, access_token=access_token, qt=qt, state=state, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects_project_id_pull_requests: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **state** | **str**| PR 状态 | [optional] 
 **sort** | **str**| 排序字段(created_at、updated_at) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_releases**
> Release get_enterprise_id_projects_project_id_releases(enterprise_id, project_id, access_token=access_token, qt=qt, page=page, per_page=per_page)

查看发行版列表

查看发行版列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 查看发行版列表
    api_response = api_instance.get_enterprise_id_projects_project_id_releases(enterprise_id, project_id, access_token=access_token, qt=qt, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects_project_id_releases: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_releases_tag_version**
> Release get_enterprise_id_projects_project_id_releases_tag_version(enterprise_id, project_id, tag_version, access_token=access_token, qt=qt)

查看发行版详情

查看发行版详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
tag_version = 'tag_version_example' # str | 发行版版本
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 查看发行版详情
    api_response = api_instance.get_enterprise_id_projects_project_id_releases_tag_version(enterprise_id, project_id, tag_version, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects_project_id_releases_tag_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **tag_version** | **str**| 发行版版本 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_settings**
> ProjectBaseSetting get_enterprise_id_projects_project_id_settings(enterprise_id, project_id, access_token=access_token, qt=qt)

获取仓库基本设置

获取仓库基本设置

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 获取仓库基本设置
    api_response = api_instance.get_enterprise_id_projects_project_id_settings(enterprise_id, project_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects_project_id_settings: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**ProjectBaseSetting**](ProjectBaseSetting.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_users**
> UsersProjects get_enterprise_id_projects_project_id_users(enterprise_id, project_id, access_token=access_token, qt=qt, search=search, page=page, per_page=per_page)

获取仓库的成员列表

获取仓库的成员列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
search = 'search_example' # str | 搜索字符串 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取仓库的成员列表
    api_response = api_instance.get_enterprise_id_projects_project_id_users(enterprise_id, project_id, access_token=access_token, qt=qt, search=search, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->get_enterprise_id_projects_project_id_users: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **search** | **str**| 搜索字符串 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**UsersProjects**](UsersProjects.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects**
> ProjectDetail post_enterprise_id_projects(body, enterprise_id)

新建仓库

新建仓库

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
body = gitee_api.EnterpriseIdProjectsBody() # EnterpriseIdProjectsBody | 
enterprise_id = 56 # int | 

try:
    # 新建仓库
    api_response = api_instance.post_enterprise_id_projects(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->post_enterprise_id_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnterpriseIdProjectsBody**](EnterpriseIdProjectsBody.md)|  | 
 **enterprise_id** | **int**|  | 

### Return type

[**ProjectDetail**](ProjectDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_check_project_can_import**
> ResultResponse post_enterprise_id_projects_check_project_can_import(body, enterprise_id)

新建仓库-导入仓库参数是否有效

新建仓库-导入仓库参数是否有效

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
body = gitee_api.ProjectsCheckProjectCanImportBody() # ProjectsCheckProjectCanImportBody | 
enterprise_id = 56 # int | 

try:
    # 新建仓库-导入仓库参数是否有效
    api_response = api_instance.post_enterprise_id_projects_check_project_can_import(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->post_enterprise_id_projects_check_project_can_import: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectsCheckProjectCanImportBody**](ProjectsCheckProjectCanImportBody.md)|  | 
 **enterprise_id** | **int**|  | 

### Return type

[**ResultResponse**](ResultResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_check_project_name**
> post_enterprise_id_projects_check_project_name(body, enterprise_id)

新建仓库-仓库名/路径是否已经存在

新建仓库-仓库名/路径是否已经存在

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
body = gitee_api.ProjectsCheckProjectNameBody() # ProjectsCheckProjectNameBody | 
enterprise_id = 56 # int | 

try:
    # 新建仓库-仓库名/路径是否已经存在
    api_instance.post_enterprise_id_projects_check_project_name(body, enterprise_id)
except ApiException as e:
    print("Exception when calling GitDataApi->post_enterprise_id_projects_check_project_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectsCheckProjectNameBody**](ProjectsCheckProjectNameBody.md)|  | 
 **enterprise_id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_releases**
> Release post_enterprise_id_projects_project_id_releases(body, enterprise_id, project_id)

新建发行版

新建发行版

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
body = gitee_api.ProjectIdReleasesBody() # ProjectIdReleasesBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path

try:
    # 新建发行版
    api_response = api_instance.post_enterprise_id_projects_project_id_releases(body, enterprise_id, project_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->post_enterprise_id_projects_project_id_releases: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdReleasesBody**](ProjectIdReleasesBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_transfer_code**
> post_enterprise_id_projects_transfer_code(enterprise_id, body=body)

获取仓库转入-转移码

获取仓库转入-转移码

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 
body = gitee_api.ProjectsTransferCodeBody() # ProjectsTransferCodeBody |  (optional)

try:
    # 获取仓库转入-转移码
    api_instance.post_enterprise_id_projects_transfer_code(enterprise_id, body=body)
except ApiException as e:
    print("Exception when calling GitDataApi->post_enterprise_id_projects_transfer_code: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**|  | 
 **body** | [**ProjectsTransferCodeBody**](ProjectsTransferCodeBody.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_projects_project_id**
> ProjectBaseSetting put_enterprise_id_projects_project_id(enterprise_id, project_id, body=body)

更新仓库设置

更新仓库设置

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
body = gitee_api.ProjectsProjectIdBody() # ProjectsProjectIdBody |  (optional)

try:
    # 更新仓库设置
    api_response = api_instance.put_enterprise_id_projects_project_id(enterprise_id, project_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->put_enterprise_id_projects_project_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **body** | [**ProjectsProjectIdBody**](ProjectsProjectIdBody.md)|  | [optional] 

### Return type

[**ProjectBaseSetting**](ProjectBaseSetting.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_projects_project_id_releases_release_id**
> Release put_enterprise_id_projects_project_id_releases_release_id(body, enterprise_id, project_id, release_id)

编辑发行版

编辑发行版

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.GitDataApi()
body = gitee_api.ReleasesReleaseIdBody() # ReleasesReleaseIdBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
release_id = 56 # int | 发行版 id

try:
    # 编辑发行版
    api_response = api_instance.put_enterprise_id_projects_project_id_releases_release_id(body, enterprise_id, project_id, release_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GitDataApi->put_enterprise_id_projects_project_id_releases_release_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ReleasesReleaseIdBody**](ReleasesReleaseIdBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **release_id** | **int**| 发行版 id | 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


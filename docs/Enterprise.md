# Enterprise

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 企业 id | [optional] 
**name** | **str** | 企业名称 | [optional] 
**path** | **str** | 企业路径 | [optional] 
**description** | **str** | 企业简介 | [optional] 
**public** | **str** | 企业类型(0: 未公开 1: 公开) | [optional] 
**website** | **str** | 企业官网 | [optional] 
**email** | **str** | 企业邮箱 | [optional] 
**phone** | **str** | 企业手机号码 | [optional] 
**avatar_url** | **str** | 企业头像 | [optional] 
**level** | **int** | 企业套餐所属级别 | [optional] 
**title** | **str** | 企业套餐名称 | [optional] 
**notice** | **str** | 企业公告 | [optional] 
**welcome_message** | **str** | 企业欢迎私信内容 | [optional] 
**force_verify** | **bool** | 是否开启强制审核 | [optional] 
**default_role_id** | **int** | 企业默认角色ID | [optional] 
**open_application** | **int** | 是否允许外部成员申请加入 | [optional] 
**version** | **int** |  | [optional] 
**overdue** | **bool** | 是否过期企业 | [optional] 
**charged** | **bool** | 是否付费企业 | [optional] 
**watermark** | **bool** | 是否开启水印 | [optional] 
**gitee_search_status** | **str** | 是否开启 Gitee Search, not_open、opening、success、failed | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


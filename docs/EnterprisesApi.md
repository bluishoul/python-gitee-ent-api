# gitee_api.EnterprisesApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_enterprise_id**](EnterprisesApi.md#get_enterprise_id) | **GET** /{enterprise_id} | 获取企业基础信息
[**get_enterprise_id_auths**](EnterprisesApi.md#get_enterprise_id_auths) | **GET** /{enterprise_id}/auths | 获取授权用户在企业拥有的权限
[**get_enterprise_id_navigates**](EnterprisesApi.md#get_enterprise_id_navigates) | **GET** /{enterprise_id}/navigates | 企业导航栏设置
[**get_enterprise_id_quota**](EnterprisesApi.md#get_enterprise_id_quota) | **GET** /{enterprise_id}/quota | 获取当前企业的配额信息
[**get_list**](EnterprisesApi.md#get_list) | **GET** /list | 获取授权用户的企业列表
[**put_enterprise_id**](EnterprisesApi.md#put_enterprise_id) | **PUT** /{enterprise_id} | 更新企业基础信息
[**put_enterprise_id_navigates**](EnterprisesApi.md#put_enterprise_id_navigates) | **PUT** /{enterprise_id}/navigates | 更新企业导航栏设置
[**put_enterprise_id_notices**](EnterprisesApi.md#put_enterprise_id_notices) | **PUT** /{enterprise_id}/notices | 更新企业公告内容

# **get_enterprise_id**
> Enterprise get_enterprise_id(enterprise_id, access_token=access_token)

获取企业基础信息

获取企业基础信息

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.EnterprisesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取企业基础信息
    api_response = api_instance.get_enterprise_id(enterprise_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnterprisesApi->get_enterprise_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**Enterprise**](Enterprise.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_auths**
> EnterpriseAuth get_enterprise_id_auths(enterprise_id, access_token=access_token)

获取授权用户在企业拥有的权限

获取授权用户在企业拥有的权限

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.EnterprisesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取授权用户在企业拥有的权限
    api_response = api_instance.get_enterprise_id_auths(enterprise_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnterprisesApi->get_enterprise_id_auths: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**EnterpriseAuth**](EnterpriseAuth.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_navigates**
> list[EnterpriseNavigate] get_enterprise_id_navigates(enterprise_id, access_token=access_token)

企业导航栏设置

企业导航栏设置

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.EnterprisesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 企业导航栏设置
    api_response = api_instance.get_enterprise_id_navigates(enterprise_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnterprisesApi->get_enterprise_id_navigates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**list[EnterpriseNavigate]**](EnterpriseNavigate.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_quota**
> EnterpriseQuota get_enterprise_id_quota(enterprise_id, access_token=access_token)

获取当前企业的配额信息

获取当前企业的配额信息

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.EnterprisesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取当前企业的配额信息
    api_response = api_instance.get_enterprise_id_quota(enterprise_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnterprisesApi->get_enterprise_id_quota: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**EnterpriseQuota**](EnterpriseQuota.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_list**
> list[Enterprise] get_list(access_token=access_token, sort=sort, direction=direction, page=page, per_page=per_page)

获取授权用户的企业列表

获取授权用户的企业列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.EnterprisesApi()
access_token = 'access_token_example' # str | 用户授权码 (optional)
sort = 'sort_example' # str | 排序字段(name || created_at) (optional)
direction = 'desc' # str | 可选。升序/降序 (optional) (default to desc)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取授权用户的企业列表
    api_response = api_instance.get_list(access_token=access_token, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnterprisesApi->get_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_token** | **str**| 用户授权码 | [optional] 
 **sort** | **str**| 排序字段(name || created_at) | [optional] 
 **direction** | **str**| 可选。升序/降序 | [optional] [default to desc]
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[Enterprise]**](Enterprise.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id**
> Enterprise put_enterprise_id(enterprise_id, body=body)

更新企业基础信息

更新企业基础信息

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.EnterprisesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
body = gitee_api.EnterpriseIdBody() # EnterpriseIdBody |  (optional)

try:
    # 更新企业基础信息
    api_response = api_instance.put_enterprise_id(enterprise_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnterprisesApi->put_enterprise_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **body** | [**EnterpriseIdBody**](EnterpriseIdBody.md)|  | [optional] 

### Return type

[**Enterprise**](Enterprise.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_navigates**
> list[EnterpriseNavigate] put_enterprise_id_navigates(enterprise_id, body=body)

更新企业导航栏设置

更新企业导航栏设置

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.EnterprisesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
body = gitee_api.EnterpriseIdNavigatesBody() # EnterpriseIdNavigatesBody |  (optional)

try:
    # 更新企业导航栏设置
    api_response = api_instance.put_enterprise_id_navigates(enterprise_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnterprisesApi->put_enterprise_id_navigates: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **body** | [**EnterpriseIdNavigatesBody**](EnterpriseIdNavigatesBody.md)|  | [optional] 

### Return type

[**list[EnterpriseNavigate]**](EnterpriseNavigate.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_notices**
> Enterprise put_enterprise_id_notices(body, enterprise_id)

更新企业公告内容

更新企业公告内容

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.EnterprisesApi()
body = gitee_api.EnterpriseIdNoticesBody() # EnterpriseIdNoticesBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 更新企业公告内容
    api_response = api_instance.put_enterprise_id_notices(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnterprisesApi->put_enterprise_id_notices: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnterpriseIdNoticesBody**](EnterpriseIdNoticesBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

[**Enterprise**](Enterprise.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


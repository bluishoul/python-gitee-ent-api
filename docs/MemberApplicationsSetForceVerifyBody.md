# MemberApplicationsSetForceVerifyBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**on** | **bool** | 是否开启强制审核: 开启此选项后，所有邀请(包括之前已生成的邀请)都将需要管理员审核通过后才可加入企业 | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# CommitDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Commit ID | [optional] 
**short_id** | **int** | Commit Short ID | [optional] 
**title** | **int** | Commit Title | [optional] 
**title_html** | **int** | Commit Html Title | [optional] 
**description** | **int** | Commit Description | [optional] 
**description_html** | **int** | Commit Html Description | [optional] 
**message** | **int** | Commit Message | [optional] 
**message_html** | **int** | Commit Html Message | [optional] 
**author** | **int** | Commit 作者 | [optional] 
**committer** | **int** | Commit 提交人 | [optional] 
**authored_date** | **int** | 推送时间 | [optional] 
**committed_date** | **int** | 提交时间 | [optional] 
**signature** | [**CommitSignature**](CommitSignature.md) |  | [optional] 
**build_state** | **int** | Gitee Go构建状态 | [optional] 
**diff_files_size** | **int** | diff 文件大小 | [optional] 
**limit_diff_files_size** | **int** | 可渲染的diff文件大小 | [optional] 
**is_overflow** | **bool** | diff大小是否超出限制 | [optional] 
**is_change_to_large** | **bool** | diff是否过大 | [optional] 
**added_lines** | **int** | 新增行数 | [optional] 
**removed_lines** | **int** | 删除行数 | [optional] 
**diffs** | [**Diff**](Diff.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


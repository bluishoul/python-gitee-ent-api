# IssueNote

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 评论的 id | [optional] 
**type** | **str** | 评论类型 | [optional] 
**author** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**content** | **str** | 评论内容(markdown 格式) | [optional] 
**content_html** | **str** | 评论内容(html 格式) | [optional] 
**reactions** | [**Reaction**](Reaction.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# gitee_api.StatisticsApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_enterprise_id_dashboard_statistics_overview**](StatisticsApi.md#get_enterprise_id_dashboard_statistics_overview) | **GET** /{enterprise_id}/dashboard_statistics/overview | 获取企业概览数据
[**get_enterprise_id_dashboard_statistics_user_dashboard**](StatisticsApi.md#get_enterprise_id_dashboard_statistics_user_dashboard) | **GET** /{enterprise_id}/dashboard_statistics/user_dashboard | 获取当前用户的统计信息
[**get_enterprise_id_statistics_enterprise_os_project_list**](StatisticsApi.md#get_enterprise_id_statistics_enterprise_os_project_list) | **GET** /{enterprise_id}/statistics/enterprise_os_project_list | 获取企业开源仓库详情列表
[**get_enterprise_id_statistics_enterprise_program_list**](StatisticsApi.md#get_enterprise_id_statistics_enterprise_program_list) | **GET** /{enterprise_id}/statistics/enterprise_program_list | 获取企业项目的统计信息
[**get_enterprise_id_statistics_enterprise_project_list**](StatisticsApi.md#get_enterprise_id_statistics_enterprise_project_list) | **GET** /{enterprise_id}/statistics/enterprise_project_list | 获取企业仓库的统计信息
[**get_enterprise_id_statistics_user_statistic**](StatisticsApi.md#get_enterprise_id_statistics_user_statistic) | **GET** /{enterprise_id}/statistics/user_statistic | 获取成员的统计信息

# **get_enterprise_id_dashboard_statistics_overview**
> get_enterprise_id_dashboard_statistics_overview(enterprise_id, access_token=access_token)

获取企业概览数据

获取企业概览数据

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.StatisticsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取企业概览数据
    api_instance.get_enterprise_id_dashboard_statistics_overview(enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling StatisticsApi->get_enterprise_id_dashboard_statistics_overview: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_dashboard_statistics_user_dashboard**
> get_enterprise_id_dashboard_statistics_user_dashboard(enterprise_id, access_token=access_token)

获取当前用户的统计信息

获取当前用户的统计信息

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.StatisticsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取当前用户的统计信息
    api_instance.get_enterprise_id_dashboard_statistics_user_dashboard(enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling StatisticsApi->get_enterprise_id_dashboard_statistics_user_dashboard: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_statistics_enterprise_os_project_list**
> OsProjectsList get_enterprise_id_statistics_enterprise_os_project_list(enterprise_id, access_token=access_token, project_id=project_id, start_date=start_date, end_date=end_date)

获取企业开源仓库详情列表

获取企业开源仓库详情列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.StatisticsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
project_id = 56 # int | 仓库id (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)

try:
    # 获取企业开源仓库详情列表
    api_response = api_instance.get_enterprise_id_statistics_enterprise_os_project_list(enterprise_id, access_token=access_token, project_id=project_id, start_date=start_date, end_date=end_date)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling StatisticsApi->get_enterprise_id_statistics_enterprise_os_project_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **project_id** | **int**| 仓库id | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 

### Return type

[**OsProjectsList**](OsProjectsList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_statistics_enterprise_program_list**
> EnterpriseProgramsList get_enterprise_id_statistics_enterprise_program_list(enterprise_id, access_token=access_token, program_id=program_id, start_date=start_date, end_date=end_date)

获取企业项目的统计信息

获取企业项目的统计信息

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.StatisticsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
program_id = 56 # int | 项目id (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)

try:
    # 获取企业项目的统计信息
    api_response = api_instance.get_enterprise_id_statistics_enterprise_program_list(enterprise_id, access_token=access_token, program_id=program_id, start_date=start_date, end_date=end_date)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling StatisticsApi->get_enterprise_id_statistics_enterprise_program_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **program_id** | **int**| 项目id | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 

### Return type

[**EnterpriseProgramsList**](EnterpriseProgramsList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_statistics_enterprise_project_list**
> EnterpriseProjectsList get_enterprise_id_statistics_enterprise_project_list(enterprise_id, access_token=access_token, project_id=project_id, start_date=start_date, end_date=end_date)

获取企业仓库的统计信息

获取企业仓库的统计信息

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.StatisticsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
project_id = 56 # int | 仓库id (optional)
start_date = 'start_date_example' # str | 查询的起始时间。(格式：yyyy-mm-dd) (optional)
end_date = 'end_date_example' # str | 查询的结束时间。(格式：yyyy-mm-dd) (optional)

try:
    # 获取企业仓库的统计信息
    api_response = api_instance.get_enterprise_id_statistics_enterprise_project_list(enterprise_id, access_token=access_token, project_id=project_id, start_date=start_date, end_date=end_date)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling StatisticsApi->get_enterprise_id_statistics_enterprise_project_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **project_id** | **int**| 仓库id | [optional] 
 **start_date** | **str**| 查询的起始时间。(格式：yyyy-mm-dd) | [optional] 
 **end_date** | **str**| 查询的结束时间。(格式：yyyy-mm-dd) | [optional] 

### Return type

[**EnterpriseProjectsList**](EnterpriseProjectsList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_statistics_user_statistic**
> list[UserStatistic] get_enterprise_id_statistics_user_statistic(enterprise_id, user_id, start_date, end_date, access_token=access_token, project_id=project_id)

获取成员的统计信息

获取成员的统计信息

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.StatisticsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
user_id = 'user_id_example' # str | 查找指定用户的数据。（可多选，用逗号分割）
start_date = 'start_date_example' # str | 统计的起始时间。(格式：yyyy-mm-dd)
end_date = 'end_date_example' # str | 统计的结束时间。(格式：yyyy-mm-dd)
access_token = 'access_token_example' # str | 用户授权码 (optional)
project_id = 56 # int | 仓库 id (optional)

try:
    # 获取成员的统计信息
    api_response = api_instance.get_enterprise_id_statistics_user_statistic(enterprise_id, user_id, start_date, end_date, access_token=access_token, project_id=project_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling StatisticsApi->get_enterprise_id_statistics_user_statistic: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **user_id** | **str**| 查找指定用户的数据。（可多选，用逗号分割） | 
 **start_date** | **str**| 统计的起始时间。(格式：yyyy-mm-dd) | 
 **end_date** | **str**| 统计的结束时间。(格式：yyyy-mm-dd) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **project_id** | **int**| 仓库 id | [optional] 

### Return type

[**list[UserStatistic]**](UserStatistic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


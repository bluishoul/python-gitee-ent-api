# gitee_api.ProjectTagsApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_projects_project_id_tags_destroy**](ProjectTagsApi.md#delete_enterprise_id_projects_project_id_tags_destroy) | **DELETE** /{enterprise_id}/projects/{project_id}/tags/destroy | 删除标签
[**get_enterprise_id_projects_project_id_tags**](ProjectTagsApi.md#get_enterprise_id_projects_project_id_tags) | **GET** /{enterprise_id}/projects/{project_id}/tags | 获取仓库的标签列表
[**post_enterprise_id_projects_project_id_tags**](ProjectTagsApi.md#post_enterprise_id_projects_project_id_tags) | **POST** /{enterprise_id}/projects/{project_id}/tags | 新建标签

# **delete_enterprise_id_projects_project_id_tags_destroy**
> delete_enterprise_id_projects_project_id_tags_destroy(enterprise_id, project_id, name, access_token=access_token, qt=qt)

删除标签

删除标签

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectTagsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
name = 'name_example' # str | 标签名称
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 删除标签
    api_instance.delete_enterprise_id_projects_project_id_tags_destroy(enterprise_id, project_id, name, access_token=access_token, qt=qt)
except ApiException as e:
    print("Exception when calling ProjectTagsApi->delete_enterprise_id_projects_project_id_tags_destroy: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **name** | **str**| 标签名称 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_tags**
> Tag get_enterprise_id_projects_project_id_tags(enterprise_id, project_id, access_token=access_token, qt=qt, search=search, page=page, per_page=per_page)

获取仓库的标签列表

获取仓库的标签列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectTagsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
search = 'search_example' # str | 搜索关键字 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取仓库的标签列表
    api_response = api_instance.get_enterprise_id_projects_project_id_tags(enterprise_id, project_id, access_token=access_token, qt=qt, search=search, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectTagsApi->get_enterprise_id_projects_project_id_tags: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **search** | **str**| 搜索关键字 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**Tag**](Tag.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_tags**
> Tag post_enterprise_id_projects_project_id_tags(body, enterprise_id, project_id)

新建标签

新建标签

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectTagsApi()
body = gitee_api.ProjectIdTagsBody() # ProjectIdTagsBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path

try:
    # 新建标签
    api_response = api_instance.post_enterprise_id_projects_project_id_tags(body, enterprise_id, project_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectTagsApi->post_enterprise_id_projects_project_id_tags: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectIdTagsBody**](ProjectIdTagsBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 

### Return type

[**Tag**](Tag.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


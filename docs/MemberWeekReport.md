# MemberWeekReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 周报的 id | [optional] 
**year** | **int** | 周报所属年 | [optional] 
**month** | **int** | 周报所属月份 | [optional] 
**week_index** | **int** | 处于本年的第几周 | [optional] 
**content** | **str** | 周报内容 | [optional] 
**content_html** | **str** | 周报(html 格式) | [optional] 
**user** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


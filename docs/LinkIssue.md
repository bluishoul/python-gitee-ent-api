# LinkIssue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 任务 ID | [optional] 
**root_id** | **int** | 根结点 ID | [optional] 
**parent_id** | **int** | 父任务 ID | [optional] 
**ident** | **str** | 任务全局唯一标识符 | [optional] 
**title** | **str** | 任务标题 | [optional] 
**state** | **str** | 任务状态标识符: open, progressing, closed, rejected | [optional] 
**comments_count** | **int** | 评论数量 | [optional] 
**priority** | **int** | 优先级标识符 | [optional] 
**branch** | **str** | 关联的分支名 | [optional] 
**priority_human** | **str** | 优先级中文名称 | [optional] 
**duration** | **int** | 预计工时。（单位：分钟） | [optional] 
**collaborators** | **list** | 任务协作者 | [optional] 
**author** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**milestone** | [**Milestone**](Milestone.md) |  | [optional] 
**issue_state** | [**IssueState**](IssueState.md) |  | [optional] 
**issue_type** | [**IssueType**](IssueType.md) |  | [optional] 
**labels** | [**Label**](Label.md) |  | [optional] 
**security_hole** | **bool** | 是否是私有Issue | [optional] 
**is_star** | **bool** | 是否星标任务 | [optional] 
**ref_type** | **str** | 关联关系 | [optional] 
**direction** | **str** | 关联顺序 | [optional] 
**direction_cn** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# gitee_api.ReactionApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_reactions_reaction_id**](ReactionApi.md#delete_enterprise_id_reactions_reaction_id) | **DELETE** /{enterprise_id}/reactions/{reaction_id} | 删除表态
[**post_enterprise_id_reactions**](ReactionApi.md#post_enterprise_id_reactions) | **POST** /{enterprise_id}/reactions | 新增表态

# **delete_enterprise_id_reactions_reaction_id**
> delete_enterprise_id_reactions_reaction_id(reaction_id, enterprise_id, access_token=access_token)

删除表态

删除表态

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ReactionApi()
reaction_id = 56 # int | 表态的 id
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 删除表态
    api_instance.delete_enterprise_id_reactions_reaction_id(reaction_id, enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling ReactionApi->delete_enterprise_id_reactions_reaction_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reaction_id** | **int**| 表态的 id | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_reactions**
> list[Reaction] post_enterprise_id_reactions(body, enterprise_id)

新增表态

新增表态

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ReactionApi()
body = gitee_api.EnterpriseIdReactionsBody() # EnterpriseIdReactionsBody | 
enterprise_id = 56 # int | 

try:
    # 新增表态
    api_response = api_instance.post_enterprise_id_reactions(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReactionApi->post_enterprise_id_reactions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnterpriseIdReactionsBody**](EnterpriseIdReactionsBody.md)|  | 
 **enterprise_id** | **int**|  | 

### Return type

[**list[Reaction]**](Reaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


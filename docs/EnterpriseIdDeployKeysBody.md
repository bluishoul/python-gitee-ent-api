# EnterpriseIdDeployKeysBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**title** | **str** | 标题 | 
**key** | **str** | 公钥 | 
**project_ids** | **str** | 部署仓库id列表，用英文逗号分隔 | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


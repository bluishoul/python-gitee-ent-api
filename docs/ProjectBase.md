# ProjectBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 仓库ID | [optional] 
**name** | **str** | 仓库名称 | [optional] 
**path** | **str** | 仓库路径 | [optional] 
**path_with_namespace** | **str** | namespace/path | [optional] 
**public** | **int** | 仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开; | [optional] 
**is_gvp** | **bool** | 是否为GVP仓库 | [optional] 
**is_empty_repo** | **bool** | 是否为空仓库 | [optional] 
**fork_enabled** | **bool** | 是否允许 fork | [optional] 
**name_with_namespace** | **str** | namespace_name/path | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


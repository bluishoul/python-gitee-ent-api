# WikiInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 文档 id | [optional] 
**name** | **str** | 文档名称 | [optional] 
**path** | **str** | 文档路径 | [optional] 
**creator** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**project** | [**Project**](Project.md) |  | [optional] 
**doc_node** | [**DocNode**](DocNode.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


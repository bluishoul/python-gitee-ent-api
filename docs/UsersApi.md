# gitee_api.UsersApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_users**](UsersApi.md#get_users) | **GET** /users | 获取授权用户的资料

# **get_users**
> User get_users(access_token=access_token)

获取授权用户的资料

获取授权用户的资料

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.UsersApi()
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取授权用户的资料
    api_response = api_instance.get_users(access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UsersApi->get_users: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


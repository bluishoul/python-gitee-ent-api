# CommitSignature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_verified** | **bool** | 是否验证 | [optional] 
**title** | **str** | 状态文案 | [optional] 
**gpg_key_primary_keyid** | **str** | GPG key ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


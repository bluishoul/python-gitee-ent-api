# gitee_api.DocsApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_docs_doc_id_files_file_id**](DocsApi.md#delete_enterprise_id_docs_doc_id_files_file_id) | **DELETE** /{enterprise_id}/docs/{doc_id}/files/{file_id} | 删除文件
[**get_enterprise_id_docs_doc_id_backup_progress**](DocsApi.md#get_enterprise_id_docs_doc_id_backup_progress) | **GET** /{enterprise_id}/docs/{doc_id}/backup_progress | 获取文档备份进度
[**get_enterprise_id_docs_doc_id_commit**](DocsApi.md#get_enterprise_id_docs_doc_id_commit) | **GET** /{enterprise_id}/docs/{doc_id}/commit | 获取版本历史详情
[**get_enterprise_id_docs_doc_id_files**](DocsApi.md#get_enterprise_id_docs_doc_id_files) | **GET** /{enterprise_id}/docs/{doc_id}/files | 获取文档下的文件列表
[**get_enterprise_id_docs_doc_id_files_file_id**](DocsApi.md#get_enterprise_id_docs_doc_id_files_file_id) | **GET** /{enterprise_id}/docs/{doc_id}/files/{file_id} | 获取文件详情
[**get_enterprise_id_docs_doc_id_files_file_id_versions**](DocsApi.md#get_enterprise_id_docs_doc_id_files_file_id_versions) | **GET** /{enterprise_id}/docs/{doc_id}/files/{file_id}/versions | 获取文件的历史版本列表
[**get_enterprise_id_docs_doc_id_versions**](DocsApi.md#get_enterprise_id_docs_doc_id_versions) | **GET** /{enterprise_id}/docs/{doc_id}/versions | 获取文档历史版本
[**post_enterprise_id_docs_doc_id_create_file**](DocsApi.md#post_enterprise_id_docs_doc_id_create_file) | **POST** /{enterprise_id}/docs/{doc_id}/create_file | 创建文件
[**post_enterprise_id_docs_doc_id_create_folder**](DocsApi.md#post_enterprise_id_docs_doc_id_create_folder) | **POST** /{enterprise_id}/docs/{doc_id}/create_folder | 创建文件夹
[**put_enterprise_id_docs_doc_id_files_file_id**](DocsApi.md#put_enterprise_id_docs_doc_id_files_file_id) | **PUT** /{enterprise_id}/docs/{doc_id}/files/{file_id} | 更新文件
[**put_enterprise_id_docs_doc_id_files_file_id_move**](DocsApi.md#put_enterprise_id_docs_doc_id_files_file_id_move) | **PUT** /{enterprise_id}/docs/{doc_id}/files/{file_id}/move | 移动文件
[**put_enterprise_id_docs_doc_id_files_file_id_rename**](DocsApi.md#put_enterprise_id_docs_doc_id_files_file_id_rename) | **PUT** /{enterprise_id}/docs/{doc_id}/files/{file_id}/rename | 更改文件夹名称

# **delete_enterprise_id_docs_doc_id_files_file_id**
> delete_enterprise_id_docs_doc_id_files_file_id(doc_id, file_id, enterprise_id, access_token=access_token)

删除文件

删除文件

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocsApi()
doc_id = 56 # int | 文档 id(doc_nodes 文档列表接口的 file_id 字段)
file_id = 56 # int | 文件 id(docs 获取文件列表的 id 字段)
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 删除文件
    api_instance.delete_enterprise_id_docs_doc_id_files_file_id(doc_id, file_id, enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling DocsApi->delete_enterprise_id_docs_doc_id_files_file_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **doc_id** | **int**| 文档 id(doc_nodes 文档列表接口的 file_id 字段) | 
 **file_id** | **int**| 文件 id(docs 获取文件列表的 id 字段) | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_docs_doc_id_backup_progress**
> get_enterprise_id_docs_doc_id_backup_progress(enterprise_id, doc_id, access_token=access_token)

获取文档备份进度

获取文档备份进度

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocsApi()
enterprise_id = 56 # int | 
doc_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取文档备份进度
    api_instance.get_enterprise_id_docs_doc_id_backup_progress(enterprise_id, doc_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling DocsApi->get_enterprise_id_docs_doc_id_backup_progress: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**|  | 
 **doc_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_docs_doc_id_commit**
> get_enterprise_id_docs_doc_id_commit(doc_id, commit_id, enterprise_id, access_token=access_token)

获取版本历史详情

获取版本历史详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocsApi()
doc_id = 56 # int | 文档 id(doc_nodes 文档列表接口的 file_id 字段)
commit_id = 'commit_id_example' # str | 文件历史
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取版本历史详情
    api_instance.get_enterprise_id_docs_doc_id_commit(doc_id, commit_id, enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling DocsApi->get_enterprise_id_docs_doc_id_commit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **doc_id** | **int**| 文档 id(doc_nodes 文档列表接口的 file_id 字段) | 
 **commit_id** | **str**| 文件历史 | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_docs_doc_id_files**
> list[WikiSort] get_enterprise_id_docs_doc_id_files(enterprise_id, doc_id, access_token=access_token)

获取文档下的文件列表

获取文档下的文件列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocsApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_id = 56 # int | 文档 id(doc_nodes 文档列表接口的 file_id 字段)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取文档下的文件列表
    api_response = api_instance.get_enterprise_id_docs_doc_id_files(enterprise_id, doc_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocsApi->get_enterprise_id_docs_doc_id_files: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_id** | **int**| 文档 id(doc_nodes 文档列表接口的 file_id 字段) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**list[WikiSort]**](WikiSort.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_docs_doc_id_files_file_id**
> WikiSortDetail get_enterprise_id_docs_doc_id_files_file_id(doc_id, file_id, enterprise_id, access_token=access_token, version_id=version_id)

获取文件详情

获取文件详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocsApi()
doc_id = 56 # int | 文档 id(doc_nodes 文档列表接口的 file_id 字段)
file_id = 56 # int | 文件 id(docs 获取文件列表的 id 字段)
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
version_id = 'version_id_example' # str | 指定历史版本 (optional)

try:
    # 获取文件详情
    api_response = api_instance.get_enterprise_id_docs_doc_id_files_file_id(doc_id, file_id, enterprise_id, access_token=access_token, version_id=version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocsApi->get_enterprise_id_docs_doc_id_files_file_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **doc_id** | **int**| 文档 id(doc_nodes 文档列表接口的 file_id 字段) | 
 **file_id** | **int**| 文件 id(docs 获取文件列表的 id 字段) | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **version_id** | **str**| 指定历史版本 | [optional] 

### Return type

[**WikiSortDetail**](WikiSortDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_docs_doc_id_files_file_id_versions**
> FileVersion get_enterprise_id_docs_doc_id_files_file_id_versions(doc_id, file_id, enterprise_id, access_token=access_token)

获取文件的历史版本列表

获取文件的历史版本列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocsApi()
doc_id = 56 # int | 文档 id(doc_nodes 文档列表接口的 file_id 字段)
file_id = 56 # int | 文件 id(docs 获取文件列表的 id 字段)
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取文件的历史版本列表
    api_response = api_instance.get_enterprise_id_docs_doc_id_files_file_id_versions(doc_id, file_id, enterprise_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocsApi->get_enterprise_id_docs_doc_id_files_file_id_versions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **doc_id** | **int**| 文档 id(doc_nodes 文档列表接口的 file_id 字段) | 
 **file_id** | **int**| 文件 id(docs 获取文件列表的 id 字段) | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**FileVersion**](FileVersion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_docs_doc_id_versions**
> get_enterprise_id_docs_doc_id_versions(doc_id, enterprise_id, access_token=access_token)

获取文档历史版本

获取文档历史版本

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocsApi()
doc_id = 56 # int | 文档 id(doc_nodes 文档列表接口的 file_id 字段)
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取文档历史版本
    api_instance.get_enterprise_id_docs_doc_id_versions(doc_id, enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling DocsApi->get_enterprise_id_docs_doc_id_versions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **doc_id** | **int**| 文档 id(doc_nodes 文档列表接口的 file_id 字段) | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_docs_doc_id_create_file**
> list[WikiSortDetail] post_enterprise_id_docs_doc_id_create_file(doc_id, enterprise_id, body=body)

创建文件

创建文件

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocsApi()
doc_id = 56 # int | 文档 id(doc_nodes 文档列表接口的 file_id 字段)
enterprise_id = 56 # int | 
body = gitee_api.DocIdCreateFileBody() # DocIdCreateFileBody |  (optional)

try:
    # 创建文件
    api_response = api_instance.post_enterprise_id_docs_doc_id_create_file(doc_id, enterprise_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocsApi->post_enterprise_id_docs_doc_id_create_file: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **doc_id** | **int**| 文档 id(doc_nodes 文档列表接口的 file_id 字段) | 
 **enterprise_id** | **int**|  | 
 **body** | [**DocIdCreateFileBody**](DocIdCreateFileBody.md)|  | [optional] 

### Return type

[**list[WikiSortDetail]**](WikiSortDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_docs_doc_id_create_folder**
> list[WikiSortDetail] post_enterprise_id_docs_doc_id_create_folder(body, doc_id, enterprise_id)

创建文件夹

创建文件夹

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocsApi()
body = gitee_api.DocIdCreateFolderBody() # DocIdCreateFolderBody | 
doc_id = 56 # int | 文档 id(doc_nodes 文档列表接口的 file_id 字段)
enterprise_id = 56 # int | 

try:
    # 创建文件夹
    api_response = api_instance.post_enterprise_id_docs_doc_id_create_folder(body, doc_id, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocsApi->post_enterprise_id_docs_doc_id_create_folder: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocIdCreateFolderBody**](DocIdCreateFolderBody.md)|  | 
 **doc_id** | **int**| 文档 id(doc_nodes 文档列表接口的 file_id 字段) | 
 **enterprise_id** | **int**|  | 

### Return type

[**list[WikiSortDetail]**](WikiSortDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_docs_doc_id_files_file_id**
> WikiSortDetail put_enterprise_id_docs_doc_id_files_file_id(doc_id, file_id, enterprise_id, body=body)

更新文件

更新文件

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocsApi()
doc_id = 56 # int | 文档 id(doc_nodes 文档列表接口的 file_id 字段)
file_id = 56 # int | 文件 id(docs 获取文件列表的 id 字段)
enterprise_id = 56 # int | 
body = gitee_api.FilesFileIdBody() # FilesFileIdBody |  (optional)

try:
    # 更新文件
    api_response = api_instance.put_enterprise_id_docs_doc_id_files_file_id(doc_id, file_id, enterprise_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocsApi->put_enterprise_id_docs_doc_id_files_file_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **doc_id** | **int**| 文档 id(doc_nodes 文档列表接口的 file_id 字段) | 
 **file_id** | **int**| 文件 id(docs 获取文件列表的 id 字段) | 
 **enterprise_id** | **int**|  | 
 **body** | [**FilesFileIdBody**](FilesFileIdBody.md)|  | [optional] 

### Return type

[**WikiSortDetail**](WikiSortDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_docs_doc_id_files_file_id_move**
> WikiSort put_enterprise_id_docs_doc_id_files_file_id_move(body, doc_id, file_id, enterprise_id)

移动文件

移动文件

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocsApi()
body = gitee_api.FileIdMoveBody() # FileIdMoveBody | 
doc_id = 56 # int | 文档 id(doc_nodes 文档列表接口的 file_id 字段)
file_id = 56 # int | 文件 id(docs 获取文件列表的 id 字段)
enterprise_id = 56 # int | 

try:
    # 移动文件
    api_response = api_instance.put_enterprise_id_docs_doc_id_files_file_id_move(body, doc_id, file_id, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocsApi->put_enterprise_id_docs_doc_id_files_file_id_move: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**FileIdMoveBody**](FileIdMoveBody.md)|  | 
 **doc_id** | **int**| 文档 id(doc_nodes 文档列表接口的 file_id 字段) | 
 **file_id** | **int**| 文件 id(docs 获取文件列表的 id 字段) | 
 **enterprise_id** | **int**|  | 

### Return type

[**WikiSort**](WikiSort.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_docs_doc_id_files_file_id_rename**
> put_enterprise_id_docs_doc_id_files_file_id_rename(body, doc_id, file_id, enterprise_id)

更改文件夹名称

更改文件夹名称

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocsApi()
body = gitee_api.FileIdRenameBody() # FileIdRenameBody | 
doc_id = 56 # int | 文档 id(doc_nodes 文档列表接口的 file_id 字段)
file_id = 56 # int | 文件 id(docs 获取文件列表的 id 字段)
enterprise_id = 56 # int | 

try:
    # 更改文件夹名称
    api_instance.put_enterprise_id_docs_doc_id_files_file_id_rename(body, doc_id, file_id, enterprise_id)
except ApiException as e:
    print("Exception when calling DocsApi->put_enterprise_id_docs_doc_id_files_file_id_rename: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**FileIdRenameBody**](FileIdRenameBody.md)|  | 
 **doc_id** | **int**| 文档 id(doc_nodes 文档列表接口的 file_id 字段) | 
 **file_id** | **int**| 文件 id(docs 获取文件列表的 id 字段) | 
 **enterprise_id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


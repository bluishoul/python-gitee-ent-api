# EnterpriseDeployKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 部署公钥 id | [optional] 
**title** | **str** | 部署公钥 title | [optional] 
**key** | **str** | 部署公钥内容 | [optional] 
**sha256_fingerprint** | **str** | sha256 指纹 | [optional] 
**user** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**projects_count** | **int** | 已部署仓库数量 | [optional] 
**projects** | [**Project**](Project.md) |  | [optional] 
**system** | **bool** | 是否是系统公钥 | [optional] 
**is_bae** | **bool** | 是否是bae公钥 | [optional] 
**is_pages** | **bool** | 是否是pages公钥 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


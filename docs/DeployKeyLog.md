# DeployKeyLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID | [optional] 
**user_id** | **int** | 用户ID | [optional] 
**user** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**ip** | **str** | ip | [optional] 
**target_id** | **int** | 目标操作对象ID | [optional] 
**title** | **str** | 操作对象名称 | [optional] 
**type** | **str** | 操作对象类型 | [optional] 
**operating** | **str** | 操作 | [optional] 
**target_project** | [**Project**](Project.md) |  | [optional] 
**target_project_name** | **str** | 仓库名称 | [optional] 
**target_deploy_key_title** | **str** | 公钥名称 | [optional] 
**target_deploy_key** | [**DeployKey**](DeployKey.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# MilestoneList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 里程碑 ID | [optional] 
**title** | **str** | 里程碑标题名称 | [optional] 
**state** | **str** | 里程碑状态 | [optional] 
**description** | **str** | 描述 | [optional] 
**description_html** | **str** | 描述(html格式) | [optional] 
**assignee** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**author** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**issue_all_count** | **int** | 里程碑issues数量 | [optional] 
**issue_complete_count** | **int** | 里程碑完成issue数量 | [optional] 
**pr_all_count** | **int** | 里程碑pr数量 | [optional] 
**pr_complete_count** | **int** | 里程碑完成pr数量 | [optional] 
**projects** | [**Project**](Project.md) |  | [optional] 
**program** | [**Program**](Program.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


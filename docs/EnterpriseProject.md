# EnterpriseProject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enterprise_project_path** | **str** | 企业仓库路径 | [optional] 
**id** | **int** | 仓库id | [optional] 
**name** | **str** | 仓库名称 | [optional] 
**path** | **str** | 路径 | [optional] 
**public** | **int** | 仓库公开与否 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# DocNodeLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID | [optional] 
**user_id** | **int** | 用户ID | [optional] 
**user** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**ip** | **str** | ip | [optional] 
**target_id** | **int** | 目标操作对象ID | [optional] 
**title** | **str** | 操作对象名称 | [optional] 
**operating** | **str** | 操作 | [optional] 
**target_doc** | **str** | 操作对象 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# IssueStatesIssueStateIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**title** | **str** | 任务状态名称 | 
**icon** | **str** | 任务状态图标 | 
**command** | **str** | 任务状态指令 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


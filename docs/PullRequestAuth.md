# PullRequestAuth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**read** | **bool** | 是否可查看此 PR | [optional] 
**create_note** | **bool** | 是否可评论此 PR | [optional] 
**update** | **bool** | 是否可更新 PR 的信息 | [optional] 
**merge_status_check** | **bool** | PR 当前的状态是否处于可合并 | [optional] 
**can_check** | **bool** | 授权用户是否可以让此 PR 审查通过 | [optional] 
**can_test** | **bool** | 授权用户是否可以让此 PR 测试通过 | [optional] 
**can_merge** | **bool** | 授权用户是否有权限可以合并此 PR | [optional] 
**force_checked** | **bool** | 授权用户是否可把 PR 的审查状态强制设置为已通过 | [optional] 
**force_tested** | **bool** | 授权用户是否可把 PR 的测试状态强制设置为已通过 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


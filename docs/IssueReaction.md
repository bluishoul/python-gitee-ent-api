# IssueReaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | [optional] 
**target_id** | **int** | 表态对象id | [optional] 
**target_type** | **str** | 表态对象类型 | [optional] 
**text** | **str** | 表态 | [optional] 
**user_id** | **int** | 表态人id | [optional] 
**user_name** | **str** | 表态人名 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


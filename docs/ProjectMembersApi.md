# gitee_api.ProjectMembersApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_projects_project_id_members_apply_apply_id**](ProjectMembersApi.md#delete_enterprise_id_projects_project_id_members_apply_apply_id) | **DELETE** /{enterprise_id}/projects/{project_id}/members/apply/{apply_id} | 删除仓库成员申请
[**delete_enterprise_id_projects_project_id_members_member_id**](ProjectMembersApi.md#delete_enterprise_id_projects_project_id_members_member_id) | **DELETE** /{enterprise_id}/projects/{project_id}/members/{member_id} | 移除仓库成员
[**get_enterprise_id_projects_project_id_members**](ProjectMembersApi.md#get_enterprise_id_projects_project_id_members) | **GET** /{enterprise_id}/projects/{project_id}/members | 仓库成员
[**get_enterprise_id_projects_project_id_members_apply_list**](ProjectMembersApi.md#get_enterprise_id_projects_project_id_members_apply_list) | **GET** /{enterprise_id}/projects/{project_id}/members/apply_list | 仓库成员申请列表
[**get_enterprise_id_projects_project_id_members_overview**](ProjectMembersApi.md#get_enterprise_id_projects_project_id_members_overview) | **GET** /{enterprise_id}/projects/{project_id}/members/overview | 成员概览
[**get_enterprise_id_projects_project_id_members_roles**](ProjectMembersApi.md#get_enterprise_id_projects_project_id_members_roles) | **GET** /{enterprise_id}/projects/{project_id}/members/roles | 获取仓库角色
[**post_enterprise_id_projects_project_id_members**](ProjectMembersApi.md#post_enterprise_id_projects_project_id_members) | **POST** /{enterprise_id}/projects/{project_id}/members | 添加仓库成员
[**put_enterprise_id_projects_project_id_members_member_id**](ProjectMembersApi.md#put_enterprise_id_projects_project_id_members_member_id) | **PUT** /{enterprise_id}/projects/{project_id}/members/{member_id} | 修改仓库成员权限

# **delete_enterprise_id_projects_project_id_members_apply_apply_id**
> delete_enterprise_id_projects_project_id_members_apply_apply_id(enterprise_id, project_id, apply_id, access_token=access_token, qt=qt)

删除仓库成员申请

删除仓库成员申请

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectMembersApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
apply_id = 56 # int | 成员申请id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 删除仓库成员申请
    api_instance.delete_enterprise_id_projects_project_id_members_apply_apply_id(enterprise_id, project_id, apply_id, access_token=access_token, qt=qt)
except ApiException as e:
    print("Exception when calling ProjectMembersApi->delete_enterprise_id_projects_project_id_members_apply_apply_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **apply_id** | **int**| 成员申请id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_projects_project_id_members_member_id**
> delete_enterprise_id_projects_project_id_members_member_id(enterprise_id, project_id, member_id, access_token=access_token, qt=qt)

移除仓库成员

移除仓库成员

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectMembersApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
member_id = 56 # int | 成员id
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 移除仓库成员
    api_instance.delete_enterprise_id_projects_project_id_members_member_id(enterprise_id, project_id, member_id, access_token=access_token, qt=qt)
except ApiException as e:
    print("Exception when calling ProjectMembersApi->delete_enterprise_id_projects_project_id_members_member_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **member_id** | **int**| 成员id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_members**
> ProjectMember get_enterprise_id_projects_project_id_members(enterprise_id, project_id, access_token=access_token, qt=qt, access_level=access_level, scope=scope, search=search, page=page, per_page=per_page)

仓库成员

仓库成员

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectMembersApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
access_level = 'access_level_example' # str | reporter:报告者, viewer:观察者, developer:开发者, master:管理员 (optional)
scope = 'scope_example' # str | not_in:获取不在本仓库的企业成员 (optional)
search = 'search_example' # str | 成员搜索，邮箱、username、name、remark (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 仓库成员
    api_response = api_instance.get_enterprise_id_projects_project_id_members(enterprise_id, project_id, access_token=access_token, qt=qt, access_level=access_level, scope=scope, search=search, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectMembersApi->get_enterprise_id_projects_project_id_members: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **access_level** | **str**| reporter:报告者, viewer:观察者, developer:开发者, master:管理员 | [optional] 
 **scope** | **str**| not_in:获取不在本仓库的企业成员 | [optional] 
 **search** | **str**| 成员搜索，邮箱、username、name、remark | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**ProjectMember**](ProjectMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_members_apply_list**
> MemberApplication get_enterprise_id_projects_project_id_members_apply_list(enterprise_id, project_id, access_token=access_token, qt=qt, query=query, status=status, page=page, per_page=per_page)

仓库成员申请列表

仓库成员申请列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectMembersApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
query = 'query_example' # str | 搜索字符串：申请人姓名或个人空间地址 (optional)
status = 'status_example' # str | 状态[approved:已同意,pending:待审核,refused:已拒绝,overdue:已过期,invite_pass:已接受邀请] (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 仓库成员申请列表
    api_response = api_instance.get_enterprise_id_projects_project_id_members_apply_list(enterprise_id, project_id, access_token=access_token, qt=qt, query=query, status=status, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectMembersApi->get_enterprise_id_projects_project_id_members_apply_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **query** | **str**| 搜索字符串：申请人姓名或个人空间地址 | [optional] 
 **status** | **str**| 状态[approved:已同意,pending:待审核,refused:已拒绝,overdue:已过期,invite_pass:已接受邀请] | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**MemberApplication**](MemberApplication.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_members_overview**
> ProjectMemberOverview get_enterprise_id_projects_project_id_members_overview(enterprise_id, project_id, access_token=access_token, qt=qt)

成员概览

成员概览

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectMembersApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 成员概览
    api_response = api_instance.get_enterprise_id_projects_project_id_members_overview(enterprise_id, project_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectMembersApi->get_enterprise_id_projects_project_id_members_overview: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**ProjectMemberOverview**](ProjectMemberOverview.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_members_roles**
> ProjectRole get_enterprise_id_projects_project_id_members_roles(enterprise_id, project_id, access_token=access_token, qt=qt)

获取仓库角色

获取仓库角色

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectMembersApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 获取仓库角色
    api_response = api_instance.get_enterprise_id_projects_project_id_members_roles(enterprise_id, project_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectMembersApi->get_enterprise_id_projects_project_id_members_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**ProjectRole**](ProjectRole.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_members**
> ProjectMemberAdd post_enterprise_id_projects_project_id_members(enterprise_id, project_id, body=body)

添加仓库成员

添加仓库成员

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectMembersApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
body = gitee_api.ProjectIdMembersBody() # ProjectIdMembersBody |  (optional)

try:
    # 添加仓库成员
    api_response = api_instance.post_enterprise_id_projects_project_id_members(enterprise_id, project_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectMembersApi->post_enterprise_id_projects_project_id_members: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **body** | [**ProjectIdMembersBody**](ProjectIdMembersBody.md)|  | [optional] 

### Return type

[**ProjectMemberAdd**](ProjectMemberAdd.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_projects_project_id_members_member_id**
> Member put_enterprise_id_projects_project_id_members_member_id(body, enterprise_id, project_id, member_id)

修改仓库成员权限

修改仓库成员权限

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectMembersApi()
body = gitee_api.MembersMemberIdBody() # MembersMemberIdBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
member_id = 56 # int | 成员id

try:
    # 修改仓库成员权限
    api_response = api_instance.put_enterprise_id_projects_project_id_members_member_id(body, enterprise_id, project_id, member_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectMembersApi->put_enterprise_id_projects_project_id_members_member_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MembersMemberIdBody**](MembersMemberIdBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **member_id** | **int**| 成员id | 

### Return type

[**Member**](Member.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


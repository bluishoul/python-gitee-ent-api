# PrAssigner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assigners** | **str** | 审查人员id列表 | [optional] 
**testers** | **str** | 测试人员id列表 | [optional] 
**pr_assign_num** | **int** | 可合并的审查人员门槛数 | [optional] 
**pr_test_num** | **int** | 可合并的测试人员门槛数 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


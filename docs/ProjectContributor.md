# ProjectContributor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | 姓名 | [optional] 
**committer_name** | **str** | 贡献者名称 | [optional] 
**email** | **str** | 邮箱 | [optional] 
**username** | **str** | 用户名 | [optional] 
**is_gitee_user** | **bool** | 是否为Gitee用户 | [optional] 
**image_path** | **str** | 头像 | [optional] 
**commits_count** | **int** | commit数 | [optional] 
**enterprise_remark** | **str** | 企业备注 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


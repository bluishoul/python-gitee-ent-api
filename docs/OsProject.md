# OsProject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 仓库id | [optional] 
**enterprise_project_path** | **str** | 企业仓库路径 | [optional] 
**name** | **str** | 仓库名称 | [optional] 
**name_with_namespace** | **str** | 带有命名空间的仓库名 | [optional] 
**path** | **str** | 路径 | [optional] 
**public** | **int** | 仓库公开与否。0：闭源、1：开源、2：内部开源 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


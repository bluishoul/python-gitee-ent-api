# UserIdMemberGroupsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**add_ids** | **str** | 加入ids, 逗号(,)隔开。add_ids，remove_ids 至少填一项，不能都为空 | [optional] 
**remove_ids** | **str** | 退出ids, 逗号(,)隔开 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


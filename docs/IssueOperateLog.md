# IssueOperateLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**icon** | **str** | 图标 | [optional] 
**content** | **str** | 内容 | [optional] 
**user** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**before_change** | **str** | 改变前的值 | [optional] 
**after_change** | **str** | 改变后的值 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


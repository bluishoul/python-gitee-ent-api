# IssueField

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 字段 id | [optional] 
**name** | **str** | 字段名称 | [optional] 
**field_type** | **str** | 字段类型 | [optional] 
**options** | **str** | 字段可选值 | [optional] 
**is_system** | **bool** | 是否为系统字段 | [optional] 
**system_name** | **str** | 系统字段英文名 | [optional] 
**description** | **str** | 字段描述 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# EnterpriseIdNavigatesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**navigates_id** | **list[int]** | 导航卡id | [optional] 
**navigates_icon** | **list[int]** | 导航卡icon | [optional] 
**navigates_active** | **list[bool]** | 是否显示 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# Milestone

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 里程碑 ID | [optional] 
**title** | **str** | 里程碑标题名称 | [optional] 
**state** | **str** | 里程碑状态 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


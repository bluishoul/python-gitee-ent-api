# PullRequestNote

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 评论的 id | [optional] 
**type** | **str** | 评论类型 | [optional] 
**author** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**content** | **str** | 评论内容(markdown 格式) | [optional] 
**content_html** | **str** | 评论内容(html 格式) | [optional] 
**reactions** | [**Reaction**](Reaction.md) |  | [optional] 
**line_code** | **str** | 代码行标记 | [optional] 
**diff_position_id** | **int** | 评论引用的代码块 id | [optional] 
**outdated** | **bool** | 代码评论是否过期 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


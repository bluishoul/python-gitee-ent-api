# ProjectIdEnvVariablesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**qt** | **str** | path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
**name** | **str** | 变量名称 | 
**value** | **str** | 变量值 | 
**read_only** | **int** | 是否只读,1为true,0为false | [optional] 
**remark** | **str** | 备注 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


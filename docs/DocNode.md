# DocNode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | [optional] 
**parent_id** | **int** | 父层级的 id | [optional] 
**name** | **str** | 名称 | [optional] 
**public** | **str** | 权限值 | [optional] 
**public_name** | **str** | 权限名称 | [optional] 
**program** | [**Program**](Program.md) |  | [optional] 
**file_type** | **str** | 关联类型。(目录：DocDirectory，文档：WikiInfo，附件：AttachFile) | [optional] 
**file_id** | **str** | 关联类型的 id | [optional] 
**file_versions** | **bool** | 是否查看附件历史版本 | [optional] 
**creator** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**is_favour** | **bool** | 是否已收藏 | [optional] 
**attach_file** | [**DocAttachFile**](DocAttachFile.md) |  | [optional] 
**is_wiki** | **bool** | 是否 wiki | [optional] 
**need_password** | **bool** | 需要密码访问 | [optional] 
**is_top** | **bool** | 是否置顶 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# ProjectManageLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID | [optional] 
**user_id** | **int** | 用户ID | [optional] 
**user** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**ip** | **str** | ip | [optional] 
**target_id** | **int** | 目标操作对象ID | [optional] 
**title** | **str** | 操作原始记录 | [optional] 
**target_project** | [**ProjectBase**](ProjectBase.md) |  | [optional] 
**operating** | **str** | 操作 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# gitee_api.MilestonesApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_milestones_milestone_id**](MilestonesApi.md#delete_enterprise_id_milestones_milestone_id) | **DELETE** /{enterprise_id}/milestones/{milestone_id} | 删除里程碑
[**get_enterprise_id_milestones**](MilestonesApi.md#get_enterprise_id_milestones) | **GET** /{enterprise_id}/milestones | 获取里程碑列表
[**get_enterprise_id_milestones_milestone_id**](MilestonesApi.md#get_enterprise_id_milestones_milestone_id) | **GET** /{enterprise_id}/milestones/{milestone_id} | 获取里程碑信息
[**get_enterprise_id_milestones_milestone_id_participants**](MilestonesApi.md#get_enterprise_id_milestones_milestone_id_participants) | **GET** /{enterprise_id}/milestones/{milestone_id}/participants | 里程碑参与人列表
[**post_enterprise_id_milestones**](MilestonesApi.md#post_enterprise_id_milestones) | **POST** /{enterprise_id}/milestones | 新建里程碑
[**put_enterprise_id_milestones_milestone_id**](MilestonesApi.md#put_enterprise_id_milestones_milestone_id) | **PUT** /{enterprise_id}/milestones/{milestone_id} | 编辑里程碑

# **delete_enterprise_id_milestones_milestone_id**
> delete_enterprise_id_milestones_milestone_id(enterprise_id, milestone_id, access_token=access_token)

删除里程碑

删除里程碑

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MilestonesApi()
enterprise_id = 56 # int | 企业ID
milestone_id = 56 # int | 里程碑ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 删除里程碑
    api_instance.delete_enterprise_id_milestones_milestone_id(enterprise_id, milestone_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling MilestonesApi->delete_enterprise_id_milestones_milestone_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业ID | 
 **milestone_id** | **int**| 里程碑ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_milestones**
> list[MilestoneList] get_enterprise_id_milestones(enterprise_id, access_token=access_token, program_id=program_id, project_id=project_id, state=state, assignee_ids=assignee_ids, author_ids=author_ids, search=search, sort=sort, direction=direction, scope=scope, page=page, per_page=per_page)

获取里程碑列表

获取里程碑列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MilestonesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
program_id = 56 # int | 项目 id (optional)
project_id = 56 # int | 仓库 id (optional)
state = 'state_example' # str | 状态，开启的:active，待开始:pending，进行中:processing，已关闭:closed，已逾期:overdued， 未过期：not_overdue (optional)
assignee_ids = 'assignee_ids_example' # str | 负责人id以,分隔的字符串 (optional)
author_ids = 'author_ids_example' # str | 创建者id以,分隔的字符串 (optional)
search = 'search_example' # str | 搜索字符串 (optional)
sort = 'sort_example' # str | 排序字段(title、created_at、start_date、due_date) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
scope = 'scope_example' # str | 筛选当前用户参与的里程碑 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取里程碑列表
    api_response = api_instance.get_enterprise_id_milestones(enterprise_id, access_token=access_token, program_id=program_id, project_id=project_id, state=state, assignee_ids=assignee_ids, author_ids=author_ids, search=search, sort=sort, direction=direction, scope=scope, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MilestonesApi->get_enterprise_id_milestones: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **program_id** | **int**| 项目 id | [optional] 
 **project_id** | **int**| 仓库 id | [optional] 
 **state** | **str**| 状态，开启的:active，待开始:pending，进行中:processing，已关闭:closed，已逾期:overdued， 未过期：not_overdue | [optional] 
 **assignee_ids** | **str**| 负责人id以,分隔的字符串 | [optional] 
 **author_ids** | **str**| 创建者id以,分隔的字符串 | [optional] 
 **search** | **str**| 搜索字符串 | [optional] 
 **sort** | **str**| 排序字段(title、created_at、start_date、due_date) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **scope** | **str**| 筛选当前用户参与的里程碑 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[MilestoneList]**](MilestoneList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_milestones_milestone_id**
> MilestoneDetail get_enterprise_id_milestones_milestone_id(enterprise_id, milestone_id, access_token=access_token)

获取里程碑信息

获取里程碑信息

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MilestonesApi()
enterprise_id = 56 # int | 企业ID
milestone_id = 56 # int | 里程碑ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取里程碑信息
    api_response = api_instance.get_enterprise_id_milestones_milestone_id(enterprise_id, milestone_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MilestonesApi->get_enterprise_id_milestones_milestone_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业ID | 
 **milestone_id** | **int**| 里程碑ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**MilestoneDetail**](MilestoneDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_milestones_milestone_id_participants**
> UserWithRemark get_enterprise_id_milestones_milestone_id_participants(enterprise_id, milestone_id, access_token=access_token)

里程碑参与人列表

里程碑参与人列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MilestonesApi()
enterprise_id = 56 # int | 企业ID
milestone_id = 56 # int | 里程碑ID
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 里程碑参与人列表
    api_response = api_instance.get_enterprise_id_milestones_milestone_id_participants(enterprise_id, milestone_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MilestonesApi->get_enterprise_id_milestones_milestone_id_participants: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业ID | 
 **milestone_id** | **int**| 里程碑ID | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**UserWithRemark**](UserWithRemark.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_milestones**
> MilestoneDetail post_enterprise_id_milestones(body, enterprise_id)

新建里程碑

新建里程碑

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MilestonesApi()
body = gitee_api.EnterpriseIdMilestonesBody() # EnterpriseIdMilestonesBody | 
enterprise_id = 56 # int | 企业ID

try:
    # 新建里程碑
    api_response = api_instance.post_enterprise_id_milestones(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MilestonesApi->post_enterprise_id_milestones: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnterpriseIdMilestonesBody**](EnterpriseIdMilestonesBody.md)|  | 
 **enterprise_id** | **int**| 企业ID | 

### Return type

[**MilestoneDetail**](MilestoneDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_milestones_milestone_id**
> MilestoneDetail put_enterprise_id_milestones_milestone_id(enterprise_id, milestone_id, body=body)

编辑里程碑

编辑里程碑

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.MilestonesApi()
enterprise_id = 56 # int | 企业ID
milestone_id = 56 # int | 里程碑ID
body = gitee_api.MilestonesMilestoneIdBody() # MilestonesMilestoneIdBody |  (optional)

try:
    # 编辑里程碑
    api_response = api_instance.put_enterprise_id_milestones_milestone_id(enterprise_id, milestone_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MilestonesApi->put_enterprise_id_milestones_milestone_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业ID | 
 **milestone_id** | **int**| 里程碑ID | 
 **body** | [**MilestonesMilestoneIdBody**](MilestonesMilestoneIdBody.md)|  | [optional] 

### Return type

[**MilestoneDetail**](MilestoneDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


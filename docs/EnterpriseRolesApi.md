# gitee_api.EnterpriseRolesApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_enterprise_roles_enterprise_role_id**](EnterpriseRolesApi.md#delete_enterprise_id_enterprise_roles_enterprise_role_id) | **DELETE** /{enterprise_id}/enterprise_roles/{enterprise_role_id} | 删除角色
[**get_enterprise_id_enterprise_roles**](EnterpriseRolesApi.md#get_enterprise_id_enterprise_roles) | **GET** /{enterprise_id}/enterprise_roles | 获取企业角色列表
[**get_enterprise_id_enterprise_roles_enterprise_role_id**](EnterpriseRolesApi.md#get_enterprise_id_enterprise_roles_enterprise_role_id) | **GET** /{enterprise_id}/enterprise_roles/{enterprise_role_id} | 获取企业角色详情
[**post_enterprise_id_enterprise_roles**](EnterpriseRolesApi.md#post_enterprise_id_enterprise_roles) | **POST** /{enterprise_id}/enterprise_roles | 新增企业角色
[**put_enterprise_id_enterprise_roles_enterprise_role_id**](EnterpriseRolesApi.md#put_enterprise_id_enterprise_roles_enterprise_role_id) | **PUT** /{enterprise_id}/enterprise_roles/{enterprise_role_id} | 更新企业角色

# **delete_enterprise_id_enterprise_roles_enterprise_role_id**
> delete_enterprise_id_enterprise_roles_enterprise_role_id(enterprise_id, enterprise_role_id, access_token=access_token)

删除角色

删除角色

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.EnterpriseRolesApi()
enterprise_id = 56 # int | 
enterprise_role_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 删除角色
    api_instance.delete_enterprise_id_enterprise_roles_enterprise_role_id(enterprise_id, enterprise_role_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling EnterpriseRolesApi->delete_enterprise_id_enterprise_roles_enterprise_role_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**|  | 
 **enterprise_role_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_enterprise_roles**
> list[EnterpriseRole] get_enterprise_id_enterprise_roles(enterprise_id, access_token=access_token, sort=sort, direction=direction, scope=scope, page=page, per_page=per_page)

获取企业角色列表

获取企业角色列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.EnterpriseRolesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
sort = 'sort_example' # str | 排序字段(created_at: 创建时间 updated_at: 更新时间) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
scope = 'scope_example' # str | 筛选项 (system: 系统默认类型; custom: 自定义类型; admin: 管理员类型; can_operate: 非企业拥有者角色, can_invite: 能添加成员角色) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取企业角色列表
    api_response = api_instance.get_enterprise_id_enterprise_roles(enterprise_id, access_token=access_token, sort=sort, direction=direction, scope=scope, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnterpriseRolesApi->get_enterprise_id_enterprise_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **sort** | **str**| 排序字段(created_at: 创建时间 updated_at: 更新时间) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **scope** | **str**| 筛选项 (system: 系统默认类型; custom: 自定义类型; admin: 管理员类型; can_operate: 非企业拥有者角色, can_invite: 能添加成员角色) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[EnterpriseRole]**](EnterpriseRole.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_enterprise_roles_enterprise_role_id**
> list[EnterpriseRoleDetail] get_enterprise_id_enterprise_roles_enterprise_role_id(enterprise_id, enterprise_role_id, access_token=access_token)

获取企业角色详情

获取企业角色详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.EnterpriseRolesApi()
enterprise_id = 56 # int | 
enterprise_role_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取企业角色详情
    api_response = api_instance.get_enterprise_id_enterprise_roles_enterprise_role_id(enterprise_id, enterprise_role_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnterpriseRolesApi->get_enterprise_id_enterprise_roles_enterprise_role_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**|  | 
 **enterprise_role_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**list[EnterpriseRoleDetail]**](EnterpriseRoleDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_enterprise_roles**
> list[EnterpriseRoleDetail] post_enterprise_id_enterprise_roles(body, enterprise_id)

新增企业角色

新增企业角色

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.EnterpriseRolesApi()
body = gitee_api.EnterpriseIdEnterpriseRolesBody() # EnterpriseIdEnterpriseRolesBody | 
enterprise_id = 56 # int | 

try:
    # 新增企业角色
    api_response = api_instance.post_enterprise_id_enterprise_roles(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnterpriseRolesApi->post_enterprise_id_enterprise_roles: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnterpriseIdEnterpriseRolesBody**](EnterpriseIdEnterpriseRolesBody.md)|  | 
 **enterprise_id** | **int**|  | 

### Return type

[**list[EnterpriseRoleDetail]**](EnterpriseRoleDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_enterprise_roles_enterprise_role_id**
> list[EnterpriseRoleDetail] put_enterprise_id_enterprise_roles_enterprise_role_id(enterprise_id, enterprise_role_id, body=body)

更新企业角色

更新企业角色

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.EnterpriseRolesApi()
enterprise_id = 56 # int | 
enterprise_role_id = 56 # int | 
body = gitee_api.EnterpriseRolesEnterpriseRoleIdBody() # EnterpriseRolesEnterpriseRoleIdBody |  (optional)

try:
    # 更新企业角色
    api_response = api_instance.put_enterprise_id_enterprise_roles_enterprise_role_id(enterprise_id, enterprise_role_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EnterpriseRolesApi->put_enterprise_id_enterprise_roles_enterprise_role_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**|  | 
 **enterprise_role_id** | **int**|  | 
 **body** | [**EnterpriseRolesEnterpriseRoleIdBody**](EnterpriseRolesEnterpriseRoleIdBody.md)|  | [optional] 

### Return type

[**list[EnterpriseRoleDetail]**](EnterpriseRoleDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


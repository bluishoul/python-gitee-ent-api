# MemberApplication

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 成员申请的 id | [optional] 
**applicant_name** | **str** | 成员申请时的名称 | [optional] 
**applicant** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**inviter** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**remark** | **str** | 申请时的备注 | [optional] 
**need_check** | **str** | 是否需要审核 | [optional] 
**state** | **str** | 审核的状态。申请中：pending; 审核通过: approved; 已过期: overdue; 已拒绝: refused; 接收邀请链接：invite_pass | [optional] 
**operator** | **str** | 操作者 | [optional] 
**role_id** | **int** | 角色 ID | [optional] 
**access** | **int** | 权限属性 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# PrOperateLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**icon** | **str** | 图标 | [optional] 
**content** | **str** | 内容 | [optional] 
**user** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# gitee_api.WorkflowApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_enterprise_id_workflow_list**](WorkflowApi.md#get_enterprise_id_workflow_list) | **GET** /{enterprise_id}/workflow/list | 获取任务类型的工作流列表

# **get_enterprise_id_workflow_list**
> TreeNote get_enterprise_id_workflow_list(enterprise_id, access_token=access_token, issue_type_ids=issue_type_ids, page=page, per_page=per_page)

获取任务类型的工作流列表

获取任务类型的工作流列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.WorkflowApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
issue_type_ids = 'issue_type_ids_example' # str | 任务状态的 id，如存在多个使用英文逗号(,)分割 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取任务类型的工作流列表
    api_response = api_instance.get_enterprise_id_workflow_list(enterprise_id, access_token=access_token, issue_type_ids=issue_type_ids, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WorkflowApi->get_enterprise_id_workflow_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **issue_type_ids** | **str**| 任务状态的 id，如存在多个使用英文逗号(,)分割 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**TreeNote**](TreeNote.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


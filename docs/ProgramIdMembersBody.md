# ProgramIdMembersBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**user_ids** | **str** | 成员ids，英文逗号(,)隔开: 1,2 | [optional] 
**group_ids** | **str** | 团队ids，英文逗号(,)隔开: 1,2 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


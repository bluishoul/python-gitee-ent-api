# DocAttachFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**download_url** | **str** | 下载链接 | [optional] 
**preview_path** | **str** | 预览地址 | [optional] 
**file_type** | **str** | 文件类型 | [optional] 
**size** | **str** | 文件大小 | [optional] 
**ext** | **str** | 文件后缀 | [optional] 
**belong_project** | **bool** | 是否仓库内的附件 | [optional] 
**branch** | **str** | 关联分支 | [optional] 
**description** | **str** | 描述 | [optional] 
**download_count** | **int** | 下载次数 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


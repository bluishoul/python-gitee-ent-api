# DocNodesCoverAttachFileBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**file** | **str** | 上传的文件 | 
**parent_id** | **int** | 上传的层级 | 
**file_id** | **int** | 覆盖的文件节点 | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


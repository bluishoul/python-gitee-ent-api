# Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 动态id | [optional] 
**target_type** | **str** |  | [optional] 
**ident** | **str** | 动态标识 | [optional] 
**is_proper** | **bool** | 是否合规 | [optional] 
**created_at** | **str** | 动态产生的时间 | [optional] 
**action** | **str** | 动作 | [optional] 
**action_human_name** | **str** | 动作名称 | [optional] 
**author** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**in_project** | **bool** | 是否在仓库 | [optional] 
**in_enterprise** | **bool** | 是否在企业 | [optional] 
**project** | [**Project**](Project.md) |  | [optional] 
**enterprise** | **str** | 企业 | [optional] 
**payload** | **object** | 不同类型动态的内容 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


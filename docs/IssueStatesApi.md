# gitee_api.IssueStatesApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_issue_states_issue_state_id**](IssueStatesApi.md#delete_enterprise_id_issue_states_issue_state_id) | **DELETE** /{enterprise_id}/issue_states/{issue_state_id} | 删除任务状态
[**get_enterprise_id_issue_states**](IssueStatesApi.md#get_enterprise_id_issue_states) | **GET** /{enterprise_id}/issue_states | 获取任务状态列表
[**get_enterprise_id_issue_states_issue_state_id**](IssueStatesApi.md#get_enterprise_id_issue_states_issue_state_id) | **GET** /{enterprise_id}/issue_states/{issue_state_id} | 任务状态详情
[**post_enterprise_id_issue_states**](IssueStatesApi.md#post_enterprise_id_issue_states) | **POST** /{enterprise_id}/issue_states | 新增任务状态
[**put_enterprise_id_issue_states_issue_state_id**](IssueStatesApi.md#put_enterprise_id_issue_states_issue_state_id) | **PUT** /{enterprise_id}/issue_states/{issue_state_id} | 更新任务状态

# **delete_enterprise_id_issue_states_issue_state_id**
> delete_enterprise_id_issue_states_issue_state_id(enterprise_id, issue_state_id, access_token=access_token)

删除任务状态

删除任务状态

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueStatesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_state_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 删除任务状态
    api_instance.delete_enterprise_id_issue_states_issue_state_id(enterprise_id, issue_state_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssueStatesApi->delete_enterprise_id_issue_states_issue_state_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_state_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issue_states**
> list[IssueState] get_enterprise_id_issue_states(enterprise_id, access_token=access_token, sort=sort, direction=direction, search=search, issue_type_category=issue_type_category, page=page, per_page=per_page)

获取任务状态列表

获取任务状态列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueStatesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
sort = 'sort_example' # str | 排序字段(created_at、updated_at) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
search = 'search_example' # str | 搜索关键字 (optional)
issue_type_category = 'issue_type_category_example' # str | 任务类型属性 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取任务状态列表
    api_response = api_instance.get_enterprise_id_issue_states(enterprise_id, access_token=access_token, sort=sort, direction=direction, search=search, issue_type_category=issue_type_category, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueStatesApi->get_enterprise_id_issue_states: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **sort** | **str**| 排序字段(created_at、updated_at) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **search** | **str**| 搜索关键字 | [optional] 
 **issue_type_category** | **str**| 任务类型属性 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[IssueState]**](IssueState.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issue_states_issue_state_id**
> IssueStateDetail get_enterprise_id_issue_states_issue_state_id(enterprise_id, issue_state_id, access_token=access_token)

任务状态详情

任务状态详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueStatesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_state_id = 56 # int | 任务状态 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 任务状态详情
    api_response = api_instance.get_enterprise_id_issue_states_issue_state_id(enterprise_id, issue_state_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueStatesApi->get_enterprise_id_issue_states_issue_state_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_state_id** | **int**| 任务状态 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**IssueStateDetail**](IssueStateDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_issue_states**
> IssueState post_enterprise_id_issue_states(body, enterprise_id)

新增任务状态

新增任务状态

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueStatesApi()
body = gitee_api.EnterpriseIdIssueStatesBody() # EnterpriseIdIssueStatesBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 新增任务状态
    api_response = api_instance.post_enterprise_id_issue_states(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueStatesApi->post_enterprise_id_issue_states: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnterpriseIdIssueStatesBody**](EnterpriseIdIssueStatesBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

[**IssueState**](IssueState.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_issue_states_issue_state_id**
> IssueState put_enterprise_id_issue_states_issue_state_id(body, enterprise_id, issue_state_id)

更新任务状态

更新任务状态

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueStatesApi()
body = gitee_api.IssueStatesIssueStateIdBody() # IssueStatesIssueStateIdBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_state_id = 56 # int | 

try:
    # 更新任务状态
    api_response = api_instance.put_enterprise_id_issue_states_issue_state_id(body, enterprise_id, issue_state_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueStatesApi->put_enterprise_id_issue_states_issue_state_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**IssueStatesIssueStateIdBody**](IssueStatesIssueStateIdBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_state_id** | **int**|  | 

### Return type

[**IssueState**](IssueState.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# IssueMain

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 任务 ID | [optional] 
**root_id** | **int** | 根结点 ID | [optional] 
**ident** | **str** | 任务全局唯一标识符 | [optional] 
**title** | **str** | 任务标题 | [optional] 
**state** | **str** | 任务状态标识符: open, progressing, closed, rejected | [optional] 
**comments_count** | **int** | 评论数量 | [optional] 
**priority** | **int** | 优先级标识符 | [optional] 
**priority_human** | **str** | 优先级中文名称 | [optional] 
**duration** | **int** | 预计工时。（单位：分钟） | [optional] 
**is_overdue** | **bool** | 是否过期 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


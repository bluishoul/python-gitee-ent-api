# UserStatistic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 唯一标示 | [optional] 
**user_id** | **int** | 对应用户的 ID | [optional] 
**enterprise_id** | **int** | 企业 ID | [optional] 
**_date** | **str** | 日期 | [optional] 
**close_issue_count** | **int** | 关闭的任务总数 | [optional] 
**commit_count** | **int** | 提交的总数 | [optional] 
**create_issue_count** | **int** | 创建的任务数 | [optional] 
**create_pr_count** | **int** | 创建的 PR 数 | [optional] 
**merge_pr_count** | **int** | 合并的 PR 数 | [optional] 
**add_code_line** | **int** | 新增代码行 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# gitee_api.ProjectCommitsApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_enterprise_id_projects_project_id_commit_commit_id**](ProjectCommitsApi.md#get_enterprise_id_projects_project_id_commit_commit_id) | **GET** /{enterprise_id}/projects/{project_id}/commit/{commit_id} | commit详情
[**get_enterprise_id_projects_project_id_commit_commit_id_branches**](ProjectCommitsApi.md#get_enterprise_id_projects_project_id_commit_commit_id_branches) | **GET** /{enterprise_id}/projects/{project_id}/commit/{commit_id}/branches | 获取commit的分支和tag
[**get_enterprise_id_projects_project_id_commit_commit_id_diff_for_path**](ProjectCommitsApi.md#get_enterprise_id_projects_project_id_commit_commit_id_diff_for_path) | **GET** /{enterprise_id}/projects/{project_id}/commit/{commit_id}/diff_for_path | 获取 commit 详情中差异较大的文件内容
[**get_enterprise_id_projects_project_id_commit_commit_id_notes**](ProjectCommitsApi.md#get_enterprise_id_projects_project_id_commit_commit_id_notes) | **GET** /{enterprise_id}/projects/{project_id}/commit/{commit_id}/notes | Commit 评论列表
[**get_enterprise_id_projects_project_id_commits_ref**](ProjectCommitsApi.md#get_enterprise_id_projects_project_id_commits_ref) | **GET** /{enterprise_id}/projects/{project_id}/commits/{ref} | commits列表
[**get_enterprise_id_projects_project_id_compare_from___to**](ProjectCommitsApi.md#get_enterprise_id_projects_project_id_compare_from___to) | **GET** /{enterprise_id}/projects/{project_id}/compare/{from}...{to} | 对比commit
[**post_enterprise_id_projects_project_id_commit_commit_id_notes**](ProjectCommitsApi.md#post_enterprise_id_projects_project_id_commit_commit_id_notes) | **POST** /{enterprise_id}/projects/{project_id}/commit/{commit_id}/notes | 评论 Commit

# **get_enterprise_id_projects_project_id_commit_commit_id**
> CommitDetail get_enterprise_id_projects_project_id_commit_commit_id(enterprise_id, project_id, commit_id, access_token=access_token, qt=qt)

commit详情

commit详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectCommitsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
commit_id = 'commit_id_example' # str | commit sha
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # commit详情
    api_response = api_instance.get_enterprise_id_projects_project_id_commit_commit_id(enterprise_id, project_id, commit_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectCommitsApi->get_enterprise_id_projects_project_id_commit_commit_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **commit_id** | **str**| commit sha | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**CommitDetail**](CommitDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_commit_commit_id_branches**
> CommitBranch get_enterprise_id_projects_project_id_commit_commit_id_branches(enterprise_id, project_id, commit_id, access_token=access_token, qt=qt)

获取commit的分支和tag

获取commit的分支和tag

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectCommitsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
commit_id = 'commit_id_example' # str | commit sha
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 获取commit的分支和tag
    api_response = api_instance.get_enterprise_id_projects_project_id_commit_commit_id_branches(enterprise_id, project_id, commit_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectCommitsApi->get_enterprise_id_projects_project_id_commit_commit_id_branches: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **commit_id** | **str**| commit sha | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**CommitBranch**](CommitBranch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_commit_commit_id_diff_for_path**
> get_enterprise_id_projects_project_id_commit_commit_id_diff_for_path(enterprise_id, project_id, commit_id, file_identifier, new_path, old_path, access_token=access_token, qt=qt)

获取 commit 详情中差异较大的文件内容

获取 commit 详情中差异较大的文件内容

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectCommitsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
commit_id = 'commit_id_example' # str | commit sha
file_identifier = 'file_identifier_example' # str | git 文件标识符
new_path = 'new_path_example' # str | 旧路径
old_path = 'old_path_example' # str | 新路径
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 获取 commit 详情中差异较大的文件内容
    api_instance.get_enterprise_id_projects_project_id_commit_commit_id_diff_for_path(enterprise_id, project_id, commit_id, file_identifier, new_path, old_path, access_token=access_token, qt=qt)
except ApiException as e:
    print("Exception when calling ProjectCommitsApi->get_enterprise_id_projects_project_id_commit_commit_id_diff_for_path: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **commit_id** | **str**| commit sha | 
 **file_identifier** | **str**| git 文件标识符 | 
 **new_path** | **str**| 旧路径 | 
 **old_path** | **str**| 新路径 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_commit_commit_id_notes**
> CommitNote get_enterprise_id_projects_project_id_commit_commit_id_notes(enterprise_id, project_id, commit_id, access_token=access_token, qt=qt)

Commit 评论列表

Commit 评论列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectCommitsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
commit_id = 'commit_id_example' # str | commit sha
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # Commit 评论列表
    api_response = api_instance.get_enterprise_id_projects_project_id_commit_commit_id_notes(enterprise_id, project_id, commit_id, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectCommitsApi->get_enterprise_id_projects_project_id_commit_commit_id_notes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **commit_id** | **str**| commit sha | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**CommitNote**](CommitNote.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_commits_ref**
> list[CommitList] get_enterprise_id_projects_project_id_commits_ref(enterprise_id, project_id, ref, access_token=access_token, qt=qt, author=author, start_date=start_date, end_date=end_date, page=page, per_page=per_page)

commits列表

commits列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectCommitsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
ref = 'ref_example' # str | 提交起始的SHA值或者分支名及文件路径. 默认: 仓库的默认分支
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)
author = 'author_example' # str | 提交作者的邮箱或个人空间地址(username/login) (optional)
start_date = 'start_date_example' # str | 提交的起始时间，时间格式为 ISO 8601 (optional)
end_date = 'end_date_example' # str | 提交的最后时间，时间格式为 ISO 8601 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # commits列表
    api_response = api_instance.get_enterprise_id_projects_project_id_commits_ref(enterprise_id, project_id, ref, access_token=access_token, qt=qt, author=author, start_date=start_date, end_date=end_date, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectCommitsApi->get_enterprise_id_projects_project_id_commits_ref: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **ref** | **str**| 提交起始的SHA值或者分支名及文件路径. 默认: 仓库的默认分支 | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
 **author** | **str**| 提交作者的邮箱或个人空间地址(username/login) | [optional] 
 **start_date** | **str**| 提交的起始时间，时间格式为 ISO 8601 | [optional] 
 **end_date** | **str**| 提交的最后时间，时间格式为 ISO 8601 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[CommitList]**](CommitList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_projects_project_id_compare_from___to**
> CommitCompare get_enterprise_id_projects_project_id_compare_from___to(enterprise_id, project_id, _from, to, access_token=access_token, qt=qt)

对比commit

对比commit

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectCommitsApi()
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
_from = '_from_example' # str | 起始 commit
to = 'to_example' # str | 结束 commit
access_token = 'access_token_example' # str | 用户授权码 (optional)
qt = 'qt_example' # str | path类型（查询参数为path）, 空则表示查询参数为id (optional)

try:
    # 对比commit
    api_response = api_instance.get_enterprise_id_projects_project_id_compare_from___to(enterprise_id, project_id, _from, to, access_token=access_token, qt=qt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectCommitsApi->get_enterprise_id_projects_project_id_compare_from___to: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **_from** | **str**| 起始 commit | 
 **to** | **str**| 结束 commit | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **qt** | **str**| path类型（查询参数为path）, 空则表示查询参数为id | [optional] 

### Return type

[**CommitCompare**](CommitCompare.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_projects_project_id_commit_commit_id_notes**
> CommitNote post_enterprise_id_projects_project_id_commit_commit_id_notes(body, enterprise_id, project_id, commit_id)

评论 Commit

评论 Commit

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.ProjectCommitsApi()
body = gitee_api.CommitIdNotesBody() # CommitIdNotesBody | 
enterprise_id = 56 # int | 企业id
project_id = 'project_id_example' # str | 仓库 id 或 path
commit_id = 'commit_id_example' # str | commit sha

try:
    # 评论 Commit
    api_response = api_instance.post_enterprise_id_projects_project_id_commit_commit_id_notes(body, enterprise_id, project_id, commit_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectCommitsApi->post_enterprise_id_projects_project_id_commit_commit_id_notes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CommitIdNotesBody**](CommitIdNotesBody.md)|  | 
 **enterprise_id** | **int**| 企业id | 
 **project_id** | **str**| 仓库 id 或 path | 
 **commit_id** | **str**| commit sha | 

### Return type

[**CommitNote**](CommitNote.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# ProjectDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 仓库ID | [optional] 
**name** | **str** | 仓库名称 | [optional] 
**path** | **str** | 仓库路径 | [optional] 
**public** | **int** | 仓库的公开选项。0: 私有; 1: 公开; 2: 内部公开; | [optional] 
**enterprise_id** | **int** | 企业 id | [optional] 
**security_hole_enabled** | **bool** | 是否允许用户创建涉及敏感信息的任务 | [optional] 
**namespace** | [**Namespace**](Namespace.md) |  | [optional] 
**creator** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**is_fork** | **bool** | 是否是fork仓库 | [optional] 
**parent_project** | [**Project**](Project.md) |  | [optional] 
**status** | **int** | 状态值 | [optional] 
**status_name** | **int** | 状态中文名称 | [optional] 
**outsourced** | **bool** | 是否外包 | [optional] 
**repo_size** | **int** | 仓库大小 | [optional] 
**can_admin_project** | **bool** | 能否操作当前仓库 | [optional] 
**members_count** | **int** | 成员数 | [optional] 
**last_push_at** | **bool** | 最近push | [optional] 
**watches_count** | **int** | watches数 | [optional] 
**stars_count** | **int** | stars数 | [optional] 
**forked_count** | **int** | 被fork数 | [optional] 
**enable_backup** | **bool** | 是否开启备份 | [optional] 
**has_backups** | **bool** | 是否有备份 | [optional] 
**vip** | **bool** | 是否vip | [optional] 
**recomm** | **bool** | 是否推荐 | [optional] 
**template** | [**Project**](Project.md) |  | [optional] 
**template_enabled** | **bool** | 是否为模板仓库 | [optional] 
**description** | **str** | 仓库描述 | [optional] 
**get_default_branch** | **str** | 默认分支 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


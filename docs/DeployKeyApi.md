# gitee_api.DeployKeyApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_deploy_keys_deploy_key_id_projects**](DeployKeyApi.md#delete_enterprise_id_deploy_keys_deploy_key_id_projects) | **DELETE** /{enterprise_id}/deploy_keys/{deploy_key_id}/projects | 部署公钥移除仓库
[**get_enterprise_id_deploy_keys**](DeployKeyApi.md#get_enterprise_id_deploy_keys) | **GET** /{enterprise_id}/deploy_keys | 查看企业部署公钥
[**get_enterprise_id_deploy_keys_deploy_key_id_projects**](DeployKeyApi.md#get_enterprise_id_deploy_keys_deploy_key_id_projects) | **GET** /{enterprise_id}/deploy_keys/{deploy_key_id}/projects | 查看公钥部署的仓库
[**post_enterprise_id_deploy_keys**](DeployKeyApi.md#post_enterprise_id_deploy_keys) | **POST** /{enterprise_id}/deploy_keys | 添加部署公钥
[**post_enterprise_id_deploy_keys_deploy_key_id_projects**](DeployKeyApi.md#post_enterprise_id_deploy_keys_deploy_key_id_projects) | **POST** /{enterprise_id}/deploy_keys/{deploy_key_id}/projects | 部署公钥添加仓库
[**put_enterprise_id_deploy_keys_deploy_key_id**](DeployKeyApi.md#put_enterprise_id_deploy_keys_deploy_key_id) | **PUT** /{enterprise_id}/deploy_keys/{deploy_key_id} | 修改部署公钥

# **delete_enterprise_id_deploy_keys_deploy_key_id_projects**
> delete_enterprise_id_deploy_keys_deploy_key_id_projects(deploy_key_id, project_ids, enterprise_id, access_token=access_token)

部署公钥移除仓库

部署公钥移除仓库

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DeployKeyApi()
deploy_key_id = 56 # int | 部署公钥id
project_ids = 'project_ids_example' # str | 添加仓库id，英文逗号分隔
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 部署公钥移除仓库
    api_instance.delete_enterprise_id_deploy_keys_deploy_key_id_projects(deploy_key_id, project_ids, enterprise_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling DeployKeyApi->delete_enterprise_id_deploy_keys_deploy_key_id_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deploy_key_id** | **int**| 部署公钥id | 
 **project_ids** | **str**| 添加仓库id，英文逗号分隔 | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_deploy_keys**
> EnterpriseDeployKey get_enterprise_id_deploy_keys(enterprise_id, access_token=access_token, project_id=project_id, search=search, page=page, per_page=per_page)

查看企业部署公钥

查看企业部署公钥

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DeployKeyApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
project_id = 56 # int | 仓库id (optional)
search = 'search_example' # str | 公钥SHA256指纹或标题 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 查看企业部署公钥
    api_response = api_instance.get_enterprise_id_deploy_keys(enterprise_id, access_token=access_token, project_id=project_id, search=search, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeployKeyApi->get_enterprise_id_deploy_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **project_id** | **int**| 仓库id | [optional] 
 **search** | **str**| 公钥SHA256指纹或标题 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**EnterpriseDeployKey**](EnterpriseDeployKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_deploy_keys_deploy_key_id_projects**
> Project get_enterprise_id_deploy_keys_deploy_key_id_projects(deploy_key_id, enterprise_id, access_token=access_token, page=page, per_page=per_page)

查看公钥部署的仓库

查看公钥部署的仓库

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DeployKeyApi()
deploy_key_id = 56 # int | 部署公钥id
enterprise_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 查看公钥部署的仓库
    api_response = api_instance.get_enterprise_id_deploy_keys_deploy_key_id_projects(deploy_key_id, enterprise_id, access_token=access_token, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeployKeyApi->get_enterprise_id_deploy_keys_deploy_key_id_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deploy_key_id** | **int**| 部署公钥id | 
 **enterprise_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_deploy_keys**
> EnterpriseDeployKey post_enterprise_id_deploy_keys(body, enterprise_id)

添加部署公钥

添加部署公钥

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DeployKeyApi()
body = gitee_api.EnterpriseIdDeployKeysBody() # EnterpriseIdDeployKeysBody | 
enterprise_id = 56 # int | 

try:
    # 添加部署公钥
    api_response = api_instance.post_enterprise_id_deploy_keys(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeployKeyApi->post_enterprise_id_deploy_keys: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnterpriseIdDeployKeysBody**](EnterpriseIdDeployKeysBody.md)|  | 
 **enterprise_id** | **int**|  | 

### Return type

[**EnterpriseDeployKey**](EnterpriseDeployKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_deploy_keys_deploy_key_id_projects**
> Project post_enterprise_id_deploy_keys_deploy_key_id_projects(body, deploy_key_id, enterprise_id)

部署公钥添加仓库

部署公钥添加仓库

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DeployKeyApi()
body = gitee_api.DeployKeyIdProjectsBody() # DeployKeyIdProjectsBody | 
deploy_key_id = 56 # int | 部署公钥id
enterprise_id = 56 # int | 

try:
    # 部署公钥添加仓库
    api_response = api_instance.post_enterprise_id_deploy_keys_deploy_key_id_projects(body, deploy_key_id, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeployKeyApi->post_enterprise_id_deploy_keys_deploy_key_id_projects: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DeployKeyIdProjectsBody**](DeployKeyIdProjectsBody.md)|  | 
 **deploy_key_id** | **int**| 部署公钥id | 
 **enterprise_id** | **int**|  | 

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_deploy_keys_deploy_key_id**
> EnterpriseDeployKey put_enterprise_id_deploy_keys_deploy_key_id(body, deploy_key_id, enterprise_id)

修改部署公钥

修改部署公钥

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DeployKeyApi()
body = gitee_api.DeployKeysDeployKeyIdBody1() # DeployKeysDeployKeyIdBody1 | 
deploy_key_id = 56 # int | 部署公钥id
enterprise_id = 56 # int | 

try:
    # 修改部署公钥
    api_response = api_instance.put_enterprise_id_deploy_keys_deploy_key_id(body, deploy_key_id, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DeployKeyApi->put_enterprise_id_deploy_keys_deploy_key_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DeployKeysDeployKeyIdBody1**](DeployKeysDeployKeyIdBody1.md)|  | 
 **deploy_key_id** | **int**| 部署公钥id | 
 **enterprise_id** | **int**|  | 

### Return type

[**EnterpriseDeployKey**](EnterpriseDeployKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


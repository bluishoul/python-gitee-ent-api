# ProjectRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role** | **str** | 角色 | [optional] 
**access** | **str** | 角色名称 | [optional] 
**role_level** | **str** | 角色等级 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# ProjectsProjectIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**qt** | **str** | path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
**name** | **str** | 仓库名称 | [optional] 
**path** | **str** | 仓库路径 | [optional] 
**description** | **str** | 仓库介绍 | [optional] 
**homepage** | **str** | 主页 | [optional] 
**lang_id** | **int** | 语言 | [optional] 
**default_branch** | **str** | 默认分支 | [optional] 
**outsourced** | **int** | 类型，0：内部，1：外包 | [optional] 
**creator_id** | **int** | 仓库负责人 | [optional] 
**can_comment** | **int** | 允许用户对仓库进行评论 | [optional] 
**issue_comment** | **int** | 允许用户对\&quot;关闭\&quot;状态的Issues进行评论 | [optional] 
**issues_enabled** | **int** | 轻量级的issue跟踪系统 | [optional] 
**security_hole_enabled** | **int** | 允许用户创建涉及敏感信息的Issue，提交后不公开此Issue（可见范围：仓库成员、企业成员） | [optional] 
**fork_enabled** | **int** | 是否允许仓库被Fork | [optional] 
**online_edit_enabled** | **int** | 是否允许仓库文件在线编辑 | [optional] 
**pull_requests_enabled** | **int** | 接受pull request，协作开发 | [optional] 
**wiki_enabled** | **int** | 可以编写文档 | [optional] 
**lightweight_pr_enabled** | **int** | 接受轻量级 Pull Request | [optional] 
**pr_master_only** | **int** | 开启的Pull Request，仅管理员、审查者、测试者可见 | [optional] 
**forbid_force_push** | **int** | 禁止强制推送 | [optional] 
**import_url** | **str** | 仓库远程地址 | [optional] 
**forbid_force_sync** | **int** | 禁止仓库同步 | [optional] 
**svn_enabled** | **int** | 使用SVN管理您的仓库 | [optional] 
**can_readonly** | **int** | 开启文件/文件夹只读功能 | [optional] 
**program_add_ids** | **str** | 待添加的关联项目列表，英文逗号分隔 | [optional] 
**program_remove_ids** | **str** | 待移除的关联项目列表，英文逗号分隔 | [optional] 
**gitee_go_enabled** | **int** | GiteeGo 启停状态, 1表示启用，0表示停用 | [optional] 
**template_enabled** | **bool** | 仓库模板启停状态 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


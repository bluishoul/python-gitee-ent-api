# DocDirectory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | [optional] 
**parent_id** | **int** | 父层级的 id | [optional] 
**name** | **str** | 名称 | [optional] 
**public** | **str** | 权限值 | [optional] 
**public_name** | **str** | 权限名称 | [optional] 
**program** | [**Program**](Program.md) |  | [optional] 
**file_type** | **str** | 关联类型。(目录：DocDirectory，文档：WikiInfo，附件：AttachFile) | [optional] 
**file_id** | **str** | 关联类型的 id | [optional] 
**children** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# ProgramsProgramIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**name** | **str** | 项目名称 | 
**description** | **str** | 项目简介 | [optional] 
**assignee_id** | **str** | 负责人ID | [optional] 
**outsourced** | **bool** | 项目类型:内部(false)/外包(true)项目 | [optional] 
**status** | **int** | 项目状态:（0:开始 1:暂停 2:关闭） | [optional] 
**color** | **str** | 颜色 | [optional] 
**ident** | **str** | 项目编号 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


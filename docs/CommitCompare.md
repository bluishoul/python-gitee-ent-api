# CommitCompare

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**too_more_commits** | **int** | commits 数量超出限制 | [optional] 
**commits_size** | **int** | commits 数量 | [optional] 
**commits_limit_size** | **int** | commits 数量上限 | [optional] 
**diffs_size** | **str** | diff差异文件数量 | [optional] 
**commits** | [**CommitBase**](CommitBase.md) |  | [optional] 
**diffs** | [**Diff**](Diff.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


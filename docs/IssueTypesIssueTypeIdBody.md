# IssueTypesIssueTypeIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**title** | **str** | 任务类型名称 | 
**state_ids** | **str** | 任务状态ID(注意预期顺序排列), 逗号隔开 | 
**progressing_ids** | **str** | 状态属性：进行中。任务状态ID, 逗号隔开 | 
**closed_ids** | **str** | 状态属性：已完成。任务状态ID, 逗号隔开 | 
**description** | **str** | 任务类型描述 | [optional] 
**rejected_ids** | **str** | 状态属性：已拒绝。任务状态ID, 逗号隔开 | [optional] 
**template** | **str** | 任务类型模版 | [optional] 
**category** | **str** | 任务类型属性 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


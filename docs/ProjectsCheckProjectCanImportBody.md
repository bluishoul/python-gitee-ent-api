# ProjectsCheckProjectCanImportBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**import_url** | **str** | 待导入仓库克隆路径 | 
**user_sync_code** | **str** | 仓库导入-账号（私有仓库需要） | [optional] 
**password_sync_code** | **str** | 仓库导入-密码（私有仓库需要） | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


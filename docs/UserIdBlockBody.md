# UserIdBlockBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**is_block** | **bool** | true: 锁定，false: 解除锁定 | 
**block_msg** | **str** | 锁定原因 | [optional] 
**sms_captcha** | **str** | 短信验证码 | [optional] 
**password** | **str** | 用户密码 | [optional] 
**validate_type** | **str** | 验证方式 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# EnterpriseIdIssuesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**title** | **str** | 任务标题 | 
**description** | **str** | 任务内容 | [optional] 
**assignee_id** | **int** | 负责人的 user id | [optional] 
**collaborator_ids** | **str** | 协作者的 user id，如有多个请用英文逗号分割。eg: 1,2,3 | [optional] 
**issue_type_id** | **int** | 任务类型的 id | [optional] 
**program_id** | **int** | 关联项目的 id | [optional] 
**project_id** | **int** | 关联仓库的 id | [optional] 
**milestone_id** | **int** | 关联里程碑的 id | [optional] 
**label_ids** | **str** | 关联标签的 id，如有多个请用英文逗号分割。eg: 1,2,3 | [optional] 
**priority** | **int** | 优先级(0: 不指定 1: 不重要 2: 次要 3: 主要 4: 严重) | [optional] 
**parent_id** | **int** | 父级任务的 id | [optional] 
**branch** | **str** | 关联分支的名称 | [optional] 
**duration** | **int** | 预计工时。（单位：分钟） | [optional] 
**plan_started_at** | **datetime** | 计划开始日期。格式：yyyy-mm-ddTHH:MM:SS | [optional] 
**deadline** | **datetime** | 计划完成日期。格式：yyyy-mm-ddTHH:MM:SS | [optional] 
**attachment_ids** | **str** | 附件id，如有多个请用英文逗号分割。eg: 1,2,3 | [optional] 
**security_hole** | **int** | 是否是私有issue, 0:否，1:是 | [optional] 
**extra_fields_issue_field_id** | **list[int]** | 任务字段 id | [optional] 
**extra_fields_value** | **list[str]** | 自定义字段的值（options类型的字段传对应选项的id，使用 , 隔开，如\&quot;1,2,3\&quot;） | [optional] 
**kanban_id** | **int** | 看板ID | [optional] 
**kanban_column_id** | **int** | 看板的栏ID | [optional] 
**scrum_sprint_id** | **int** | 迭代ID | [optional] 
**scrum_version_id** | **int** | 版本ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


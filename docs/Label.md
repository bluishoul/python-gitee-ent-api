# Label

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 任务标签 ID | [optional] 
**name** | **str** | 任务标签的名称 | [optional] 
**color** | **str** | 标签颜色 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


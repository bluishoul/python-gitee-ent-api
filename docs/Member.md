# Member

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 成员 id | [optional] 
**username** | **str** | 成员个性地址 | [optional] 
**name** | **str** | 成员名称 | [optional] 
**remark** | **str** | 成员在企业的备注姓名 | [optional] 
**pinyin** | **str** | 成员备注或名称拼音 | [optional] 
**occupation** | **str** | 职位 | [optional] 
**is_block** | **bool** | 是否被锁定 | [optional] 
**block_message** | **str** | 成员被锁定的原因 | [optional] 
**phone** | **str** | 手机号码 | [optional] 
**email** | **str** | 邮箱 | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**enterprise_role** | [**EnterpriseRoleBase**](EnterpriseRoleBase.md) |  | [optional] 
**is_feedback** | **bool** | 成员是否填写过 | [optional] 
**is_guided** | **bool** | 是否完成引导 | [optional] 
**user_guide_closed** | **bool** | 是否关闭了新手引导 | [optional] 
**user_guide_steps_finished** | **list** | 新手引导的步骤指示 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# Branch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | 分支名称 | [optional] 
**branch_type** | **object** | 分支类型 | [optional] 
**is_default** | **bool** | 是否默认分支 | [optional] 
**protection_rule** | **object** | 保护分支的操作规则 | [optional] 
**commit** | **object** | commit信息 | [optional] 
**operating** | **object** | 操作相关权限 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# IssueDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 任务 ID | [optional] 
**root_id** | **int** | 根结点 ID | [optional] 
**parent_id** | **int** | 父任务 ID | [optional] 
**ident** | **str** | 任务全局唯一标识符 | [optional] 
**title** | **str** | 任务标题 | [optional] 
**state** | **str** | 任务状态标识符: open, progressing, closed, rejected | [optional] 
**comments_count** | **int** | 评论数量 | [optional] 
**priority** | **int** | 优先级标识符 | [optional] 
**branch** | **str** | 关联的分支名 | [optional] 
**priority_human** | **str** | 优先级中文名称 | [optional] 
**duration** | **int** | 预计工时。（单位：分钟） | [optional] 
**collaborators** | **list** | 任务协作者 | [optional] 
**author** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**milestone** | [**Milestone**](Milestone.md) |  | [optional] 
**issue_state** | [**IssueState**](IssueState.md) |  | [optional] 
**issue_type** | [**IssueType**](IssueType.md) |  | [optional] 
**labels** | [**Label**](Label.md) |  | [optional] 
**security_hole** | **bool** | 是否是私有Issue | [optional] 
**is_star** | **bool** | 当前用户是否收藏过此任务 | [optional] 
**description** | **str** | 任务内容(markdown 格式) | [optional] 
**description_html** | **str** | 任务内容(html 格式) | [optional] 
**issue_url** | **str** | PC的任务详情链接 | [optional] 
**program** | [**Program**](Program.md) |  | [optional] 
**project** | [**Project**](Project.md) |  | [optional] 
**parent** | [**Issue**](Issue.md) |  | [optional] 
**children** | [**Issue**](Issue.md) |  | [optional] 
**children_count** | **int** | 子任务数量 | [optional] 
**operate_logs_count** | **int** | 操作日志的数量 | [optional] 
**reactions** | **object** | 表态 | [optional] 
**attach_file_ids** | **int** | issue附件id列表 | [optional] 
**security_hole_operater** | **bool** | 私有issue操作者 | [optional] 
**scrum_sprint** | [**ScrumSprint**](ScrumSprint.md) |  | [optional] 
**scrum_version** | [**ScrumVersion**](ScrumVersion.md) |  | [optional] 
**trigger_execute_info** | **bool** | 是否有异步执行的触发器 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


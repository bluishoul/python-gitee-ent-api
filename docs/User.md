# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 用户 id | [optional] 
**username** | **str** | 用户个性地址 | [optional] 
**name** | **str** | 用户名称 | [optional] 
**avatar_url** | **str** | 用户头像 | [optional] 
**state** | **str** | 账号是否可用(active: 可用 blocked: 已被系统屏蔽) | [optional] 
**user_color** | **int** | 用户代码风格 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


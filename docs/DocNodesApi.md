# gitee_api.DocNodesApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_doc_nodes_batch_delete**](DocNodesApi.md#delete_enterprise_id_doc_nodes_batch_delete) | **DELETE** /{enterprise_id}/doc_nodes/batch_delete | 批量彻底删除文件节点
[**delete_enterprise_id_doc_nodes_doc_node_id**](DocNodesApi.md#delete_enterprise_id_doc_nodes_doc_node_id) | **DELETE** /{enterprise_id}/doc_nodes/{doc_node_id} | 彻底删除文件节点
[**delete_enterprise_id_doc_nodes_doc_node_id_file_versions**](DocNodesApi.md#delete_enterprise_id_doc_nodes_doc_node_id_file_versions) | **DELETE** /{enterprise_id}/doc_nodes/{doc_node_id}/file_versions | 删除历史版本
[**get_enterprise_id_doc_nodes_check_attach_file_name**](DocNodesApi.md#get_enterprise_id_doc_nodes_check_attach_file_name) | **GET** /{enterprise_id}/doc_nodes/check_attach_file_name | 检测附件是否重名
[**get_enterprise_id_doc_nodes_collection**](DocNodesApi.md#get_enterprise_id_doc_nodes_collection) | **GET** /{enterprise_id}/doc_nodes/collection | 获取授权用户收藏的文件节点（仅顶层）
[**get_enterprise_id_doc_nodes_directories**](DocNodesApi.md#get_enterprise_id_doc_nodes_directories) | **GET** /{enterprise_id}/doc_nodes/directories | 获取文件夹
[**get_enterprise_id_doc_nodes_doc_node_id**](DocNodesApi.md#get_enterprise_id_doc_nodes_doc_node_id) | **GET** /{enterprise_id}/doc_nodes/{doc_node_id} | 查看文件节点详情
[**get_enterprise_id_doc_nodes_doc_node_id_auth**](DocNodesApi.md#get_enterprise_id_doc_nodes_doc_node_id_auth) | **GET** /{enterprise_id}/doc_nodes/{doc_node_id}/auth | 查看文件节点的权限
[**get_enterprise_id_doc_nodes_doc_node_id_file_versions**](DocNodesApi.md#get_enterprise_id_doc_nodes_doc_node_id_file_versions) | **GET** /{enterprise_id}/doc_nodes/{doc_node_id}/file_versions | 查看历史版本
[**get_enterprise_id_doc_nodes_doc_node_id_operate_auths**](DocNodesApi.md#get_enterprise_id_doc_nodes_doc_node_id_operate_auths) | **GET** /{enterprise_id}/doc_nodes/{doc_node_id}/operate_auths | 获取文件节点的权限
[**get_enterprise_id_doc_nodes_level**](DocNodesApi.md#get_enterprise_id_doc_nodes_level) | **GET** /{enterprise_id}/doc_nodes/level | 获取文件节点列表（层级）
[**get_enterprise_id_doc_nodes_programs**](DocNodesApi.md#get_enterprise_id_doc_nodes_programs) | **GET** /{enterprise_id}/doc_nodes/programs | 获取与文档有关的项目
[**get_enterprise_id_doc_nodes_recent**](DocNodesApi.md#get_enterprise_id_doc_nodes_recent) | **GET** /{enterprise_id}/doc_nodes/recent | 获取最近编辑的文件
[**get_enterprise_id_doc_nodes_recent_doc**](DocNodesApi.md#get_enterprise_id_doc_nodes_recent_doc) | **GET** /{enterprise_id}/doc_nodes/recent_doc | 获取最近编辑的文档
[**get_enterprise_id_doc_nodes_recycle**](DocNodesApi.md#get_enterprise_id_doc_nodes_recycle) | **GET** /{enterprise_id}/doc_nodes/recycle | 获取回收站的内容列表
[**get_enterprise_id_doc_nodes_tile**](DocNodesApi.md#get_enterprise_id_doc_nodes_tile) | **GET** /{enterprise_id}/doc_nodes/tile | 获取文件节点列表（平铺）
[**post_enterprise_id_doc_nodes_attach_file**](DocNodesApi.md#post_enterprise_id_doc_nodes_attach_file) | **POST** /{enterprise_id}/doc_nodes/attach_file | 上传附件
[**post_enterprise_id_doc_nodes_batch_recycle**](DocNodesApi.md#post_enterprise_id_doc_nodes_batch_recycle) | **POST** /{enterprise_id}/doc_nodes/batch_recycle | 批量移除到回收站
[**post_enterprise_id_doc_nodes_cover_attach_file**](DocNodesApi.md#post_enterprise_id_doc_nodes_cover_attach_file) | **POST** /{enterprise_id}/doc_nodes/cover_attach_file | 上传附件（覆盖）
[**post_enterprise_id_doc_nodes_create**](DocNodesApi.md#post_enterprise_id_doc_nodes_create) | **POST** /{enterprise_id}/doc_nodes/create | 新建文档
[**post_enterprise_id_doc_nodes_create_wiki**](DocNodesApi.md#post_enterprise_id_doc_nodes_create_wiki) | **POST** /{enterprise_id}/doc_nodes/create_wiki | 新建 Wiki
[**post_enterprise_id_doc_nodes_directory**](DocNodesApi.md#post_enterprise_id_doc_nodes_directory) | **POST** /{enterprise_id}/doc_nodes/directory | 创建文件夹
[**post_enterprise_id_doc_nodes_doc_node_id_backup**](DocNodesApi.md#post_enterprise_id_doc_nodes_doc_node_id_backup) | **POST** /{enterprise_id}/doc_nodes/{doc_node_id}/backup | 备份文档
[**post_enterprise_id_doc_nodes_doc_node_id_recycle**](DocNodesApi.md#post_enterprise_id_doc_nodes_doc_node_id_recycle) | **POST** /{enterprise_id}/doc_nodes/{doc_node_id}/recycle | 移动到回收站
[**put_enterprise_id_doc_nodes_batch_auth**](DocNodesApi.md#put_enterprise_id_doc_nodes_batch_auth) | **PUT** /{enterprise_id}/doc_nodes/batch_auth | 批量更新文件节点的权限
[**put_enterprise_id_doc_nodes_batch_collection**](DocNodesApi.md#put_enterprise_id_doc_nodes_batch_collection) | **PUT** /{enterprise_id}/doc_nodes/batch_collection | 批量收藏/取消收藏文件节点
[**put_enterprise_id_doc_nodes_batch_move**](DocNodesApi.md#put_enterprise_id_doc_nodes_batch_move) | **PUT** /{enterprise_id}/doc_nodes/batch_move | 批量移动文件节点
[**put_enterprise_id_doc_nodes_doc_node_id**](DocNodesApi.md#put_enterprise_id_doc_nodes_doc_node_id) | **PUT** /{enterprise_id}/doc_nodes/{doc_node_id} | 更新文件节点
[**put_enterprise_id_doc_nodes_doc_node_id_auth**](DocNodesApi.md#put_enterprise_id_doc_nodes_doc_node_id_auth) | **PUT** /{enterprise_id}/doc_nodes/{doc_node_id}/auth | 更新文件节点的权限
[**put_enterprise_id_doc_nodes_doc_node_id_collection**](DocNodesApi.md#put_enterprise_id_doc_nodes_doc_node_id_collection) | **PUT** /{enterprise_id}/doc_nodes/{doc_node_id}/collection | 收藏/取消收藏文件节点
[**put_enterprise_id_doc_nodes_doc_node_id_is_top**](DocNodesApi.md#put_enterprise_id_doc_nodes_doc_node_id_is_top) | **PUT** /{enterprise_id}/doc_nodes/{doc_node_id}/is_top | 置顶节点
[**put_enterprise_id_doc_nodes_doc_node_id_move**](DocNodesApi.md#put_enterprise_id_doc_nodes_doc_node_id_move) | **PUT** /{enterprise_id}/doc_nodes/{doc_node_id}/move | 移动文件节点

# **delete_enterprise_id_doc_nodes_batch_delete**
> delete_enterprise_id_doc_nodes_batch_delete(enterprise_id, doc_node_ids, access_token=access_token)

批量彻底删除文件节点

批量彻底删除文件节点

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_ids = 'doc_node_ids_example' # str | 文件 id，使用,，隔开
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 批量彻底删除文件节点
    api_instance.delete_enterprise_id_doc_nodes_batch_delete(enterprise_id, doc_node_ids, access_token=access_token)
except ApiException as e:
    print("Exception when calling DocNodesApi->delete_enterprise_id_doc_nodes_batch_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_ids** | **str**| 文件 id，使用,，隔开 | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_doc_nodes_doc_node_id**
> delete_enterprise_id_doc_nodes_doc_node_id(enterprise_id, doc_node_id, access_token=access_token)

彻底删除文件节点

彻底删除文件节点

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 彻底删除文件节点
    api_instance.delete_enterprise_id_doc_nodes_doc_node_id(enterprise_id, doc_node_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling DocNodesApi->delete_enterprise_id_doc_nodes_doc_node_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_enterprise_id_doc_nodes_doc_node_id_file_versions**
> delete_enterprise_id_doc_nodes_doc_node_id_file_versions(enterprise_id, doc_node_id, attach_file_ids, access_token=access_token)

删除历史版本

删除历史版本

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_id = 56 # int | 节点 id
attach_file_ids = 'attach_file_ids_example' # str | 要删除的文件id（多个用逗号分隔）
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 删除历史版本
    api_instance.delete_enterprise_id_doc_nodes_doc_node_id_file_versions(enterprise_id, doc_node_id, attach_file_ids, access_token=access_token)
except ApiException as e:
    print("Exception when calling DocNodesApi->delete_enterprise_id_doc_nodes_doc_node_id_file_versions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_id** | **int**| 节点 id | 
 **attach_file_ids** | **str**| 要删除的文件id（多个用逗号分隔） | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_doc_nodes_check_attach_file_name**
> get_enterprise_id_doc_nodes_check_attach_file_name(enterprise_id, name, parent_id, access_token=access_token)

检测附件是否重名

检测附件是否重名

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
name = 'name_example' # str | 附件名称
parent_id = 56 # int | 层级 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 检测附件是否重名
    api_instance.get_enterprise_id_doc_nodes_check_attach_file_name(enterprise_id, name, parent_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling DocNodesApi->get_enterprise_id_doc_nodes_check_attach_file_name: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **name** | **str**| 附件名称 | 
 **parent_id** | **int**| 层级 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_doc_nodes_collection**
> list[DocNode] get_enterprise_id_doc_nodes_collection(enterprise_id, access_token=access_token, program_id=program_id, sort=sort, direction=direction, scrum_sprint_id=scrum_sprint_id, wiki_info_only=wiki_info_only)

获取授权用户收藏的文件节点（仅顶层）

获取授权用户收藏的文件节点（仅顶层）

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
program_id = 56 # int | 项目 id（默认：0） (optional)
sort = 'sort_example' # str | 排序字段(name size updated_at created_at creator_id public) (optional)
direction = 'desc' # str | 可选。升序/降序 (optional) (default to desc)
scrum_sprint_id = 56 # int | 迭代 ID (optional)
wiki_info_only = true # bool | 是否仅展示文档 (optional)

try:
    # 获取授权用户收藏的文件节点（仅顶层）
    api_response = api_instance.get_enterprise_id_doc_nodes_collection(enterprise_id, access_token=access_token, program_id=program_id, sort=sort, direction=direction, scrum_sprint_id=scrum_sprint_id, wiki_info_only=wiki_info_only)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->get_enterprise_id_doc_nodes_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **program_id** | **int**| 项目 id（默认：0） | [optional] 
 **sort** | **str**| 排序字段(name size updated_at created_at creator_id public) | [optional] 
 **direction** | **str**| 可选。升序/降序 | [optional] [default to desc]
 **scrum_sprint_id** | **int**| 迭代 ID | [optional] 
 **wiki_info_only** | **bool**| 是否仅展示文档 | [optional] 

### Return type

[**list[DocNode]**](DocNode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_doc_nodes_directories**
> list[DocDirectory] get_enterprise_id_doc_nodes_directories(enterprise_id, access_token=access_token, program_id=program_id, scrum_sprint_id=scrum_sprint_id, sprint_doc_id=sprint_doc_id)

获取文件夹

获取文件夹

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
program_id = 0 # int | 项目 id (optional) (default to 0)
scrum_sprint_id = 56 # int | 迭代 ID (optional)
sprint_doc_id = 56 # int | 迭代默认生成的文件夹 ID (optional)

try:
    # 获取文件夹
    api_response = api_instance.get_enterprise_id_doc_nodes_directories(enterprise_id, access_token=access_token, program_id=program_id, scrum_sprint_id=scrum_sprint_id, sprint_doc_id=sprint_doc_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->get_enterprise_id_doc_nodes_directories: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **program_id** | **int**| 项目 id | [optional] [default to 0]
 **scrum_sprint_id** | **int**| 迭代 ID | [optional] 
 **sprint_doc_id** | **int**| 迭代默认生成的文件夹 ID | [optional] 

### Return type

[**list[DocDirectory]**](DocDirectory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_doc_nodes_doc_node_id**
> DocNodeDetail get_enterprise_id_doc_nodes_doc_node_id(enterprise_id, doc_node_id, access_token=access_token)

查看文件节点详情

查看文件节点详情

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 查看文件节点详情
    api_response = api_instance.get_enterprise_id_doc_nodes_doc_node_id(enterprise_id, doc_node_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->get_enterprise_id_doc_nodes_doc_node_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**DocNodeDetail**](DocNodeDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_doc_nodes_doc_node_id_auth**
> DocNode get_enterprise_id_doc_nodes_doc_node_id_auth(enterprise_id, doc_node_id, access_token=access_token)

查看文件节点的权限

查看文件节点的权限

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_id = 56 # int | doc_node 节点 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 查看文件节点的权限
    api_response = api_instance.get_enterprise_id_doc_nodes_doc_node_id_auth(enterprise_id, doc_node_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->get_enterprise_id_doc_nodes_doc_node_id_auth: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_id** | **int**| doc_node 节点 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**DocNode**](DocNode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_doc_nodes_doc_node_id_file_versions**
> list[FileVersion] get_enterprise_id_doc_nodes_doc_node_id_file_versions(enterprise_id, doc_node_id, access_token=access_token, page=page, per_page=per_page)

查看历史版本

查看历史版本

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_id = 56 # int | 节点 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 查看历史版本
    api_response = api_instance.get_enterprise_id_doc_nodes_doc_node_id_file_versions(enterprise_id, doc_node_id, access_token=access_token, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->get_enterprise_id_doc_nodes_doc_node_id_file_versions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_id** | **int**| 节点 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[FileVersion]**](FileVersion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_doc_nodes_doc_node_id_operate_auths**
> get_enterprise_id_doc_nodes_doc_node_id_operate_auths(enterprise_id, doc_node_id, access_token=access_token)

获取文件节点的权限

获取文件节点的权限

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_id = 56 # int | 
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取文件节点的权限
    api_instance.get_enterprise_id_doc_nodes_doc_node_id_operate_auths(enterprise_id, doc_node_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling DocNodesApi->get_enterprise_id_doc_nodes_doc_node_id_operate_auths: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_id** | **int**|  | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_doc_nodes_level**
> list[DocNodeLevel] get_enterprise_id_doc_nodes_level(enterprise_id, parent_id, access_token=access_token, program_id=program_id, scope=scope, file_type=file_type)

获取文件节点列表（层级）

获取文件节点列表（层级）

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
parent_id = 0 # int | 父级 id（默认：0） (default to 0)
access_token = 'access_token_example' # str | 用户授权码 (optional)
program_id = 0 # int | 项目 id (optional) (default to 0)
scope = 'scope_example' # str | 可筛选类型：directory, recycle (optional)
file_type = 'file_type_example' # str | 文件类型（WikiInfo,DocDirectory,AttachFile），支持多选，用逗号分割 (optional)

try:
    # 获取文件节点列表（层级）
    api_response = api_instance.get_enterprise_id_doc_nodes_level(enterprise_id, parent_id, access_token=access_token, program_id=program_id, scope=scope, file_type=file_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->get_enterprise_id_doc_nodes_level: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **parent_id** | **int**| 父级 id（默认：0） | [default to 0]
 **access_token** | **str**| 用户授权码 | [optional] 
 **program_id** | **int**| 项目 id | [optional] [default to 0]
 **scope** | **str**| 可筛选类型：directory, recycle | [optional] 
 **file_type** | **str**| 文件类型（WikiInfo,DocDirectory,AttachFile），支持多选，用逗号分割 | [optional] 

### Return type

[**list[DocNodeLevel]**](DocNodeLevel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_doc_nodes_programs**
> list[DocNode] get_enterprise_id_doc_nodes_programs(enterprise_id, access_token=access_token)

获取与文档有关的项目

获取与文档有关的项目

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取与文档有关的项目
    api_response = api_instance.get_enterprise_id_doc_nodes_programs(enterprise_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->get_enterprise_id_doc_nodes_programs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**list[DocNode]**](DocNode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_doc_nodes_recent**
> list[DocRecent] get_enterprise_id_doc_nodes_recent(enterprise_id, access_token=access_token, sort=sort, direction=direction, scrum_sprint_id=scrum_sprint_id, page=page, per_page=per_page)

获取最近编辑的文件

获取最近编辑的文件

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
sort = 'sort_example' # str | 排序字段(name size updated_at created_at creator_id public) (optional)
direction = 'desc' # str | 可选。升序/降序 (optional) (default to desc)
scrum_sprint_id = 56 # int | 迭代 ID (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取最近编辑的文件
    api_response = api_instance.get_enterprise_id_doc_nodes_recent(enterprise_id, access_token=access_token, sort=sort, direction=direction, scrum_sprint_id=scrum_sprint_id, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->get_enterprise_id_doc_nodes_recent: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **sort** | **str**| 排序字段(name size updated_at created_at creator_id public) | [optional] 
 **direction** | **str**| 可选。升序/降序 | [optional] [default to desc]
 **scrum_sprint_id** | **int**| 迭代 ID | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[DocRecent]**](DocRecent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_doc_nodes_recent_doc**
> list[ScrumDocNode] get_enterprise_id_doc_nodes_recent_doc(enterprise_id, access_token=access_token, program_id=program_id, page=page, per_page=per_page)

获取最近编辑的文档

获取最近编辑的文档

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
program_id = 56 # int | 项目 ID (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取最近编辑的文档
    api_response = api_instance.get_enterprise_id_doc_nodes_recent_doc(enterprise_id, access_token=access_token, program_id=program_id, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->get_enterprise_id_doc_nodes_recent_doc: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **program_id** | **int**| 项目 ID | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[ScrumDocNode]**](ScrumDocNode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_doc_nodes_recycle**
> get_enterprise_id_doc_nodes_recycle(enterprise_id, access_token=access_token, parent_id=parent_id, scrum_sprint_id=scrum_sprint_id)

获取回收站的内容列表

获取回收站的内容列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
parent_id = 56 # int | 父级 id （默认：0） (optional)
scrum_sprint_id = 56 # int | 迭代 ID (optional)

try:
    # 获取回收站的内容列表
    api_instance.get_enterprise_id_doc_nodes_recycle(enterprise_id, access_token=access_token, parent_id=parent_id, scrum_sprint_id=scrum_sprint_id)
except ApiException as e:
    print("Exception when calling DocNodesApi->get_enterprise_id_doc_nodes_recycle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **parent_id** | **int**| 父级 id （默认：0） | [optional] 
 **scrum_sprint_id** | **int**| 迭代 ID | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_doc_nodes_tile**
> list[DocNode] get_enterprise_id_doc_nodes_tile(enterprise_id, access_token=access_token, parent_id=parent_id, creator_id=creator_id, program_id=program_id, scope=scope, sort=sort, direction=direction, scrum_sprint_id=scrum_sprint_id, page=page, per_page=per_page)

获取文件节点列表（平铺）

获取文件节点列表（平铺）

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
parent_id = 0 # int | 父级 id（默认：0） (optional) (default to 0)
creator_id = 56 # int | 创建者 id (optional)
program_id = 56 # int | 项目 id(默认：0) (optional)
scope = 'scope_example' # str | 筛选项，Wiki、AttachFile（附件）、create_by_me (optional)
sort = 'sort_example' # str | 排序字段(name size updated_at created_at creator_id public) (optional)
direction = 'desc' # str | 可选。升序/降序 (optional) (default to desc)
scrum_sprint_id = 56 # int | 迭代 ID (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取文件节点列表（平铺）
    api_response = api_instance.get_enterprise_id_doc_nodes_tile(enterprise_id, access_token=access_token, parent_id=parent_id, creator_id=creator_id, program_id=program_id, scope=scope, sort=sort, direction=direction, scrum_sprint_id=scrum_sprint_id, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->get_enterprise_id_doc_nodes_tile: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **parent_id** | **int**| 父级 id（默认：0） | [optional] [default to 0]
 **creator_id** | **int**| 创建者 id | [optional] 
 **program_id** | **int**| 项目 id(默认：0) | [optional] 
 **scope** | **str**| 筛选项，Wiki、AttachFile（附件）、create_by_me | [optional] 
 **sort** | **str**| 排序字段(name size updated_at created_at creator_id public) | [optional] 
 **direction** | **str**| 可选。升序/降序 | [optional] [default to desc]
 **scrum_sprint_id** | **int**| 迭代 ID | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[DocNode]**](DocNode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_doc_nodes_attach_file**
> list[DocNode] post_enterprise_id_doc_nodes_attach_file(body, enterprise_id)

上传附件

上传附件

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
body = gitee_api.DocNodesAttachFileBody() # DocNodesAttachFileBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 上传附件
    api_response = api_instance.post_enterprise_id_doc_nodes_attach_file(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->post_enterprise_id_doc_nodes_attach_file: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocNodesAttachFileBody**](DocNodesAttachFileBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

[**list[DocNode]**](DocNode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_doc_nodes_batch_recycle**
> post_enterprise_id_doc_nodes_batch_recycle(body, enterprise_id)

批量移除到回收站

批量移除到回收站

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
body = gitee_api.DocNodesBatchRecycleBody() # DocNodesBatchRecycleBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 批量移除到回收站
    api_instance.post_enterprise_id_doc_nodes_batch_recycle(body, enterprise_id)
except ApiException as e:
    print("Exception when calling DocNodesApi->post_enterprise_id_doc_nodes_batch_recycle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocNodesBatchRecycleBody**](DocNodesBatchRecycleBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_doc_nodes_cover_attach_file**
> post_enterprise_id_doc_nodes_cover_attach_file(body, enterprise_id)

上传附件（覆盖）

上传附件（覆盖）

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
body = gitee_api.DocNodesCoverAttachFileBody() # DocNodesCoverAttachFileBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 上传附件（覆盖）
    api_instance.post_enterprise_id_doc_nodes_cover_attach_file(body, enterprise_id)
except ApiException as e:
    print("Exception when calling DocNodesApi->post_enterprise_id_doc_nodes_cover_attach_file: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocNodesCoverAttachFileBody**](DocNodesCoverAttachFileBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_doc_nodes_create**
> list[DocNode] post_enterprise_id_doc_nodes_create(body, enterprise_id)

新建文档

新建文档

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
body = gitee_api.DocNodesCreateBody() # DocNodesCreateBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 新建文档
    api_response = api_instance.post_enterprise_id_doc_nodes_create(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->post_enterprise_id_doc_nodes_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocNodesCreateBody**](DocNodesCreateBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

[**list[DocNode]**](DocNode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_doc_nodes_create_wiki**
> list[DocNode] post_enterprise_id_doc_nodes_create_wiki(body, enterprise_id)

新建 Wiki

新建 Wiki

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
body = gitee_api.DocNodesCreateWikiBody() # DocNodesCreateWikiBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 新建 Wiki
    api_response = api_instance.post_enterprise_id_doc_nodes_create_wiki(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->post_enterprise_id_doc_nodes_create_wiki: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocNodesCreateWikiBody**](DocNodesCreateWikiBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

[**list[DocNode]**](DocNode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_doc_nodes_directory**
> post_enterprise_id_doc_nodes_directory(body, enterprise_id)

创建文件夹

创建文件夹

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
body = gitee_api.DocNodesDirectoryBody() # DocNodesDirectoryBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 创建文件夹
    api_instance.post_enterprise_id_doc_nodes_directory(body, enterprise_id)
except ApiException as e:
    print("Exception when calling DocNodesApi->post_enterprise_id_doc_nodes_directory: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocNodesDirectoryBody**](DocNodesDirectoryBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_doc_nodes_doc_node_id_backup**
> DocNode post_enterprise_id_doc_nodes_doc_node_id_backup(body, enterprise_id, doc_node_id)

备份文档

备份文档

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
body = gitee_api.DocNodeIdBackupBody() # DocNodeIdBackupBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_id = 56 # int | 

try:
    # 备份文档
    api_response = api_instance.post_enterprise_id_doc_nodes_doc_node_id_backup(body, enterprise_id, doc_node_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->post_enterprise_id_doc_nodes_doc_node_id_backup: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocNodeIdBackupBody**](DocNodeIdBackupBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_id** | **int**|  | 

### Return type

[**DocNode**](DocNode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_doc_nodes_doc_node_id_recycle**
> post_enterprise_id_doc_nodes_doc_node_id_recycle(enterprise_id, doc_node_id, body=body)

移动到回收站

移动到回收站

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_id = 56 # int | 
body = gitee_api.DocNodeIdRecycleBody() # DocNodeIdRecycleBody |  (optional)

try:
    # 移动到回收站
    api_instance.post_enterprise_id_doc_nodes_doc_node_id_recycle(enterprise_id, doc_node_id, body=body)
except ApiException as e:
    print("Exception when calling DocNodesApi->post_enterprise_id_doc_nodes_doc_node_id_recycle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_id** | **int**|  | 
 **body** | [**DocNodeIdRecycleBody**](DocNodeIdRecycleBody.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_doc_nodes_batch_auth**
> put_enterprise_id_doc_nodes_batch_auth(body, enterprise_id)

批量更新文件节点的权限

批量更新文件节点的权限

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
body = gitee_api.DocNodesBatchAuthBody() # DocNodesBatchAuthBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 批量更新文件节点的权限
    api_instance.put_enterprise_id_doc_nodes_batch_auth(body, enterprise_id)
except ApiException as e:
    print("Exception when calling DocNodesApi->put_enterprise_id_doc_nodes_batch_auth: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocNodesBatchAuthBody**](DocNodesBatchAuthBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_doc_nodes_batch_collection**
> put_enterprise_id_doc_nodes_batch_collection(body, enterprise_id)

批量收藏/取消收藏文件节点

批量收藏/取消收藏文件节点

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
body = gitee_api.DocNodesBatchCollectionBody() # DocNodesBatchCollectionBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 批量收藏/取消收藏文件节点
    api_instance.put_enterprise_id_doc_nodes_batch_collection(body, enterprise_id)
except ApiException as e:
    print("Exception when calling DocNodesApi->put_enterprise_id_doc_nodes_batch_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocNodesBatchCollectionBody**](DocNodesBatchCollectionBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_doc_nodes_batch_move**
> put_enterprise_id_doc_nodes_batch_move(body, enterprise_id)

批量移动文件节点

批量移动文件节点

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
body = gitee_api.DocNodesBatchMoveBody() # DocNodesBatchMoveBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 批量移动文件节点
    api_instance.put_enterprise_id_doc_nodes_batch_move(body, enterprise_id)
except ApiException as e:
    print("Exception when calling DocNodesApi->put_enterprise_id_doc_nodes_batch_move: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocNodesBatchMoveBody**](DocNodesBatchMoveBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_doc_nodes_doc_node_id**
> DocNode put_enterprise_id_doc_nodes_doc_node_id(enterprise_id, doc_node_id, body=body)

更新文件节点

更新文件节点

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_id = 56 # int | doc_node 节点 id
body = gitee_api.DocNodesDocNodeIdBody() # DocNodesDocNodeIdBody |  (optional)

try:
    # 更新文件节点
    api_response = api_instance.put_enterprise_id_doc_nodes_doc_node_id(enterprise_id, doc_node_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->put_enterprise_id_doc_nodes_doc_node_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_id** | **int**| doc_node 节点 id | 
 **body** | [**DocNodesDocNodeIdBody**](DocNodesDocNodeIdBody.md)|  | [optional] 

### Return type

[**DocNode**](DocNode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_doc_nodes_doc_node_id_auth**
> DocNode put_enterprise_id_doc_nodes_doc_node_id_auth(enterprise_id, doc_node_id, body=body)

更新文件节点的权限

更新文件节点的权限

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_id = 56 # int | doc_node 节点 id
body = gitee_api.DocNodeIdAuthBody() # DocNodeIdAuthBody |  (optional)

try:
    # 更新文件节点的权限
    api_response = api_instance.put_enterprise_id_doc_nodes_doc_node_id_auth(enterprise_id, doc_node_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->put_enterprise_id_doc_nodes_doc_node_id_auth: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_id** | **int**| doc_node 节点 id | 
 **body** | [**DocNodeIdAuthBody**](DocNodeIdAuthBody.md)|  | [optional] 

### Return type

[**DocNode**](DocNode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_doc_nodes_doc_node_id_collection**
> put_enterprise_id_doc_nodes_doc_node_id_collection(body, enterprise_id, doc_node_id)

收藏/取消收藏文件节点

收藏/取消收藏文件节点

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
body = gitee_api.DocNodeIdCollectionBody() # DocNodeIdCollectionBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_id = 56 # int | doc_node 节点 id

try:
    # 收藏/取消收藏文件节点
    api_instance.put_enterprise_id_doc_nodes_doc_node_id_collection(body, enterprise_id, doc_node_id)
except ApiException as e:
    print("Exception when calling DocNodesApi->put_enterprise_id_doc_nodes_doc_node_id_collection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocNodeIdCollectionBody**](DocNodeIdCollectionBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_id** | **int**| doc_node 节点 id | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_doc_nodes_doc_node_id_is_top**
> DocNode put_enterprise_id_doc_nodes_doc_node_id_is_top(body, enterprise_id, doc_node_id)

置顶节点

置顶节点

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
body = gitee_api.DocNodeIdIsTopBody() # DocNodeIdIsTopBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_id = 56 # int | doc_node 节点 id

try:
    # 置顶节点
    api_response = api_instance.put_enterprise_id_doc_nodes_doc_node_id_is_top(body, enterprise_id, doc_node_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->put_enterprise_id_doc_nodes_doc_node_id_is_top: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocNodeIdIsTopBody**](DocNodeIdIsTopBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_id** | **int**| doc_node 节点 id | 

### Return type

[**DocNode**](DocNode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_doc_nodes_doc_node_id_move**
> DocNode put_enterprise_id_doc_nodes_doc_node_id_move(body, enterprise_id, doc_node_id)

移动文件节点

移动文件节点

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.DocNodesApi()
body = gitee_api.DocNodeIdMoveBody() # DocNodeIdMoveBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
doc_node_id = 56 # int | doc_node 节点 id

try:
    # 移动文件节点
    api_response = api_instance.put_enterprise_id_doc_nodes_doc_node_id_move(body, enterprise_id, doc_node_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DocNodesApi->put_enterprise_id_doc_nodes_doc_node_id_move: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DocNodeIdMoveBody**](DocNodeIdMoveBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **doc_node_id** | **int**| doc_node 节点 id | 

### Return type

[**DocNode**](DocNode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


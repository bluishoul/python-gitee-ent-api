# BranchesBatchDestroyBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**names** | **list[str]** | 分支名数组 | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


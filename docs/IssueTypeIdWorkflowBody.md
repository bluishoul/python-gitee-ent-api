# IssueTypeIdWorkflowBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**relations_current_state_id** | **list[int]** | 当前状态 id | [optional] 
**relations_next_state_id** | **list[int]** | 下一个任务状态的 id | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


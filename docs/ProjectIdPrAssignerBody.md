# ProjectIdPrAssignerBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**qt** | **str** | path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
**assigner_ids** | **str** | 审查人员id，英文逗号分隔 | [optional] 
**pr_assign_num** | **int** | 可合并的审查人员门槛数 | [optional] 
**tester_ids** | **str** | 测试人员id，英文逗号分隔 | [optional] 
**pr_test_num** | **int** | 可合并的测试人员门槛数 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


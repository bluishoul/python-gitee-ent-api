# PullRequestIdNotesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**qt** | **str** | path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
**body** | **str** | 评论内容 | 
**line_code** | **str** | 代码行标记值 | [optional] 
**diff_position_id** | **int** | 代码评论组id | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# Namespace

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 空间 ID | [optional] 
**type** | **str** | 空间类型(Enterprise: 企业; Group: 组织; 其它情况：个人) | [optional] 
**name** | **str** | 空间名称 | [optional] 
**path** | **str** | 空间路径 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


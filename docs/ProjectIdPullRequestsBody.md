# ProjectIdPullRequestsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**qt** | **str** | path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
**source_repo** | **str** | 源仓库 id | 
**source_branch** | **str** | 源分支名称 | 
**target_branch** | **str** | 目标分支名称 | 
**title** | **str** | 标题 | 
**body** | **str** | 内容 | 
**assignee_id** | **str** | 审查者。可多选，英文逗号分隔 | 
**pr_assign_num** | **str** | 审查人员数量 | 
**tester_id** | **str** | 测试人员。可多选，英文逗号分隔 | 
**pr_test_num** | **str** | 测试人员数量 | 
**milestone_id** | **str** | 关联的里程碑 | 
**priority** | **str** | 优先级 0~4 | 
**prune_branch** | **int** | 是否需要在合并 PR 后删除提交分支. 0: 否 1: 是 | 
**close_related_issue** | **str** | 是否需要在合并 PR 后关闭关联的任务. 0: 否 1: 是 | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


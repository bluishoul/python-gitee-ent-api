# gitee_api.IssueTypesApi

All URIs are relative to *//https://api.gitee.comenterprises*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_enterprise_id_issue_types_issue_type_id**](IssueTypesApi.md#delete_enterprise_id_issue_types_issue_type_id) | **DELETE** /{enterprise_id}/issue_types/{issue_type_id} | 删除任务类型
[**get_enterprise_id_issue_types**](IssueTypesApi.md#get_enterprise_id_issue_types) | **GET** /{enterprise_id}/issue_types | 获取任务类型列表
[**get_enterprise_id_issue_types_issue_type_id**](IssueTypesApi.md#get_enterprise_id_issue_types_issue_type_id) | **GET** /{enterprise_id}/issue_types/{issue_type_id} | 任务类型详情(状态管理)
[**get_enterprise_id_issue_types_issue_type_id_issue_state_id_issues**](IssueTypesApi.md#get_enterprise_id_issue_types_issue_type_id_issue_state_id_issues) | **GET** /{enterprise_id}/issue_types/{issue_type_id}/{issue_state_id}/issues | 指定任务类型,任务状态下任务列表
[**get_enterprise_id_issue_types_issue_type_id_issue_state_refs**](IssueTypesApi.md#get_enterprise_id_issue_types_issue_type_id_issue_state_refs) | **GET** /{enterprise_id}/issue_types/{issue_type_id}/issue_state_refs | 获取单个任务类型下的任务状态引用
[**get_enterprise_id_issue_types_issue_type_id_issue_states**](IssueTypesApi.md#get_enterprise_id_issue_types_issue_type_id_issue_states) | **GET** /{enterprise_id}/issue_types/{issue_type_id}/issue_states | 获取单个任务类型下的任务状态列表
[**get_enterprise_id_issue_types_issue_type_id_workflow**](IssueTypesApi.md#get_enterprise_id_issue_types_issue_type_id_workflow) | **GET** /{enterprise_id}/issue_types/{issue_type_id}/workflow | 获取任务状态的流转关系
[**post_enterprise_id_issue_types**](IssueTypesApi.md#post_enterprise_id_issue_types) | **POST** /{enterprise_id}/issue_types | 新增任务类型
[**put_enterprise_id_issue_types_change_serial**](IssueTypesApi.md#put_enterprise_id_issue_types_change_serial) | **PUT** /{enterprise_id}/issue_types/change_serial | 更新任务类型排序
[**put_enterprise_id_issue_types_issue_type_id**](IssueTypesApi.md#put_enterprise_id_issue_types_issue_type_id) | **PUT** /{enterprise_id}/issue_types/{issue_type_id} | 更新任务类型
[**put_enterprise_id_issue_types_issue_type_id_workflow**](IssueTypesApi.md#put_enterprise_id_issue_types_issue_type_id_workflow) | **PUT** /{enterprise_id}/issue_types/{issue_type_id}/workflow | 变更任务状态流转关系

# **delete_enterprise_id_issue_types_issue_type_id**
> delete_enterprise_id_issue_types_issue_type_id(enterprise_id, issue_type_id, access_token=access_token)

删除任务类型

删除任务类型

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueTypesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_type_id = 56 # int | 任务类型 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 删除任务类型
    api_instance.delete_enterprise_id_issue_types_issue_type_id(enterprise_id, issue_type_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssueTypesApi->delete_enterprise_id_issue_types_issue_type_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_type_id** | **int**| 任务类型 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issue_types**
> list[IssueType] get_enterprise_id_issue_types(enterprise_id, access_token=access_token, sort=sort, direction=direction, program_id=program_id, scope=scope, page=page, per_page=per_page)

获取任务类型列表

获取任务类型列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueTypesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
access_token = 'access_token_example' # str | 用户授权码 (optional)
sort = 'sort_example' # str | 排序字段(created_at、updated_at、serial) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
program_id = 56 # int | 项目ID (optional)
scope = 'scope_example' # str | 查询范围：all, customize (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取任务类型列表
    api_response = api_instance.get_enterprise_id_issue_types(enterprise_id, access_token=access_token, sort=sort, direction=direction, program_id=program_id, scope=scope, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTypesApi->get_enterprise_id_issue_types: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **sort** | **str**| 排序字段(created_at、updated_at、serial) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **program_id** | **int**| 项目ID | [optional] 
 **scope** | **str**| 查询范围：all, customize | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[IssueType]**](IssueType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issue_types_issue_type_id**
> IssueTypeWithStateRef get_enterprise_id_issue_types_issue_type_id(enterprise_id, issue_type_id, access_token=access_token)

任务类型详情(状态管理)

任务类型详情(状态管理)

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueTypesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_type_id = 56 # int | 任务类型 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 任务类型详情(状态管理)
    api_response = api_instance.get_enterprise_id_issue_types_issue_type_id(enterprise_id, issue_type_id, access_token=access_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTypesApi->get_enterprise_id_issue_types_issue_type_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_type_id** | **int**| 任务类型 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

[**IssueTypeWithStateRef**](IssueTypeWithStateRef.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issue_types_issue_type_id_issue_state_id_issues**
> Issue get_enterprise_id_issue_types_issue_type_id_issue_state_id_issues(enterprise_id, issue_type_id, issue_state_id, access_token=access_token, page=page, per_page=per_page)

指定任务类型,任务状态下任务列表

指定任务类型,任务状态下任务列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueTypesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_type_id = 56 # int | 任务类型 id
issue_state_id = 56 # int | 任务状态 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 指定任务类型,任务状态下任务列表
    api_response = api_instance.get_enterprise_id_issue_types_issue_type_id_issue_state_id_issues(enterprise_id, issue_type_id, issue_state_id, access_token=access_token, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTypesApi->get_enterprise_id_issue_types_issue_type_id_issue_state_id_issues: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_type_id** | **int**| 任务类型 id | 
 **issue_state_id** | **int**| 任务状态 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issue_types_issue_type_id_issue_state_refs**
> list[IssueState] get_enterprise_id_issue_types_issue_type_id_issue_state_refs(enterprise_id, issue_type_id, access_token=access_token, page=page, per_page=per_page)

获取单个任务类型下的任务状态引用

获取单个任务类型下的任务状态引用

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueTypesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_type_id = 56 # int | 任务类型的 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取单个任务类型下的任务状态引用
    api_response = api_instance.get_enterprise_id_issue_types_issue_type_id_issue_state_refs(enterprise_id, issue_type_id, access_token=access_token, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTypesApi->get_enterprise_id_issue_types_issue_type_id_issue_state_refs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_type_id** | **int**| 任务类型的 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[IssueState]**](IssueState.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issue_types_issue_type_id_issue_states**
> list[IssueState] get_enterprise_id_issue_types_issue_type_id_issue_states(enterprise_id, issue_type_id, access_token=access_token, sort=sort, direction=direction, page=page, per_page=per_page)

获取单个任务类型下的任务状态列表

获取单个任务类型下的任务状态列表

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueTypesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_type_id = 56 # int | 任务类型的 id
access_token = 'access_token_example' # str | 用户授权码 (optional)
sort = 'sort_example' # str | 排序字段(created_at、updated_at) (optional)
direction = 'direction_example' # str | 排序方向(asc: 升序 desc: 倒序) (optional)
page = 1 # int | 当前的页码 (optional) (default to 1)
per_page = 56 # int | 每页的数量，最大为 100 (optional)

try:
    # 获取单个任务类型下的任务状态列表
    api_response = api_instance.get_enterprise_id_issue_types_issue_type_id_issue_states(enterprise_id, issue_type_id, access_token=access_token, sort=sort, direction=direction, page=page, per_page=per_page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTypesApi->get_enterprise_id_issue_types_issue_type_id_issue_states: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_type_id** | **int**| 任务类型的 id | 
 **access_token** | **str**| 用户授权码 | [optional] 
 **sort** | **str**| 排序字段(created_at、updated_at) | [optional] 
 **direction** | **str**| 排序方向(asc: 升序 desc: 倒序) | [optional] 
 **page** | **int**| 当前的页码 | [optional] [default to 1]
 **per_page** | **int**| 每页的数量，最大为 100 | [optional] 

### Return type

[**list[IssueState]**](IssueState.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_enterprise_id_issue_types_issue_type_id_workflow**
> get_enterprise_id_issue_types_issue_type_id_workflow(enterprise_id, issue_type_id, access_token=access_token)

获取任务状态的流转关系

获取任务状态的流转关系

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueTypesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_type_id = 56 # int | 任务类型 id
access_token = 'access_token_example' # str | 用户授权码 (optional)

try:
    # 获取任务状态的流转关系
    api_instance.get_enterprise_id_issue_types_issue_type_id_workflow(enterprise_id, issue_type_id, access_token=access_token)
except ApiException as e:
    print("Exception when calling IssueTypesApi->get_enterprise_id_issue_types_issue_type_id_workflow: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_type_id** | **int**| 任务类型 id | 
 **access_token** | **str**| 用户授权码 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_enterprise_id_issue_types**
> IssueType post_enterprise_id_issue_types(body, enterprise_id)

新增任务类型

新增任务类型

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueTypesApi()
body = gitee_api.EnterpriseIdIssueTypesBody() # EnterpriseIdIssueTypesBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 新增任务类型
    api_response = api_instance.post_enterprise_id_issue_types(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTypesApi->post_enterprise_id_issue_types: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EnterpriseIdIssueTypesBody**](EnterpriseIdIssueTypesBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

[**IssueType**](IssueType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_issue_types_change_serial**
> IssueType put_enterprise_id_issue_types_change_serial(body, enterprise_id)

更新任务类型排序

更新任务类型排序

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueTypesApi()
body = gitee_api.IssueTypesChangeSerialBody() # IssueTypesChangeSerialBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)

try:
    # 更新任务类型排序
    api_response = api_instance.put_enterprise_id_issue_types_change_serial(body, enterprise_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTypesApi->put_enterprise_id_issue_types_change_serial: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**IssueTypesChangeSerialBody**](IssueTypesChangeSerialBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 

### Return type

[**IssueType**](IssueType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_issue_types_issue_type_id**
> IssueType put_enterprise_id_issue_types_issue_type_id(body, enterprise_id, issue_type_id)

更新任务类型

更新任务类型

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueTypesApi()
body = gitee_api.IssueTypesIssueTypeIdBody() # IssueTypesIssueTypeIdBody | 
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_type_id = 56 # int | 任务类型 id

try:
    # 更新任务类型
    api_response = api_instance.put_enterprise_id_issue_types_issue_type_id(body, enterprise_id, issue_type_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTypesApi->put_enterprise_id_issue_types_issue_type_id: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**IssueTypesIssueTypeIdBody**](IssueTypesIssueTypeIdBody.md)|  | 
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_type_id** | **int**| 任务类型 id | 

### Return type

[**IssueType**](IssueType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_enterprise_id_issue_types_issue_type_id_workflow**
> put_enterprise_id_issue_types_issue_type_id_workflow(enterprise_id, issue_type_id, body=body)

变更任务状态流转关系

变更任务状态流转关系

### Example
```python
from __future__ import print_function
import time
import gitee_api
from gitee_api.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = gitee_api.IssueTypesApi()
enterprise_id = 56 # int | 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id)
issue_type_id = 56 # int | 任务类型 id
body = gitee_api.IssueTypeIdWorkflowBody() # IssueTypeIdWorkflowBody |  (optional)

try:
    # 变更任务状态流转关系
    api_instance.put_enterprise_id_issue_types_issue_type_id_workflow(enterprise_id, issue_type_id, body=body)
except ApiException as e:
    print("Exception when calling IssueTypesApi->put_enterprise_id_issue_types_issue_type_id_workflow: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise_id** | **int**| 企业id (https://gitee.com/api/v8/swagger#/getList 的返回值的 id) | 
 **issue_type_id** | **int**| 任务类型 id | 
 **body** | [**IssueTypeIdWorkflowBody**](IssueTypeIdWorkflowBody.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


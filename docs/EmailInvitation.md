# EmailInvitation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **str** | 邮箱 | [optional] 
**status** | **str** | 邀请状态，true: 成功，false: 失败 | [optional] 
**message** | **str** | 失败原因 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# GroupWithAuth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 团队 id | [optional] 
**name** | **str** | 团队名称 | [optional] 
**path** | **str** | 团队路径 | [optional] 
**avatar_url** | **str** | 团队头像 | [optional] 
**description** | **str** | 团队描述 | [optional] 
**group_type** | **int** | deprecated.团队的类型值。0: 内部 1:公开 2:外包 | [optional] 
**group_type_human_name** | **str** | 团队的类型名称 | [optional] 
**public** | **int** | 团队的类型值。0: 内部 1:公开 2:外包 | [optional] 
**can_quit** | **bool** | 能否编辑退出 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


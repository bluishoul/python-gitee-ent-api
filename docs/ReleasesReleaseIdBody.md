# ReleasesReleaseIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**qt** | **str** | path类型（查询参数为path）, 空则表示查询参数为id | [optional] 
**release_tag_version** | **str** | 发行版版本 | 
**release_title** | **str** | 发行版标题 | 
**release_ref** | **str** | 发行版所属分支 | [optional] [default to 'master']
**release_description** | **str** | 发行版描述 | [optional] 
**release_release_type** | **str** | 发行版类型, 0：发行版、1：预发行版 | [optional] 
**attach_ids** | **str** | 附件id列表，英文逗号分隔 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# MembersEmailInvitationBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**role_id** | **int** | 企业角色的 ID | [optional] 
**need_check** | **int** | 是否需要管理员审核。1：需要，0：不需要 | [optional] 
**emails** | **str** | 邮件列表，逗号(,)隔开。每次邀请最多只能发送20封邀请邮件 | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


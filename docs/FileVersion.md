# FileVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | 版本 id | [optional] 
**message** | **str** | 提交信息 | [optional] 
**author** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


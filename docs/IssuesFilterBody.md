# IssuesFilterBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**filter_id** | **int** | 筛选器id | [optional] 
**filter_conditions_property** | **list[str]** | 筛选类型 | 
**filter_conditions_comparator** | **list[str]** | 比较符 | [optional] 
**filter_conditions_value** | **list[str]** | 值 | [optional] 
**filter_conditions_issue_field_type** | **list[str]** | 自定义字段值类型 | [optional] 
**filter_conditions_issue_field_id** | **list[int]** | 自定义字段id | [optional] 
**priority_filters_issue_type** | **str** | 任务类型 | [optional] 
**priority_filters_issue_state** | **str** | 任务状态 | [optional] 
**priority_filters_issue_assignee** | **str** | 任务负责人 | [optional] 
**priority_filters_issue_label** | **str** | 任务标签 | [optional] 
**search** | **str** | 搜索关键字 | [optional] 
**sort** | **str** | 排序字段(created_at、updated_at、deadline、priority) | [optional] 
**direction** | **str** | 排序方向(asc: 升序 desc: 倒序) | [optional] 
**page** | **int** | 当前的页码 | [optional] [default to 1]
**per_page** | **int** | 每页的数量，最大为 100 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


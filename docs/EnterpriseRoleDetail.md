# EnterpriseRoleDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 角色 id | [optional] 
**name** | **str** | 角色名称 | [optional] 
**description** | **str** | 角色备注信息 | [optional] 
**is_system_default** | **bool** | 是否系统默认角色 | [optional] 
**ident** | **str** | 角色类型标识符 | [optional] 
**is_default** | **bool** | 是否企业默认角色 | [optional] 
**week_report** | [**WeekReport**](WeekReport.md) |  | [optional] 
**issue** | [**Issue**](Issue.md) |  | [optional] 
**program** | [**Program**](Program.md) |  | [optional] 
**project** | [**Project**](Project.md) |  | [optional] 
**doc** | [**Doc**](Doc.md) |  | [optional] 
**member** | [**Member**](Member.md) |  | [optional] 
**statistic** | [**Statistic**](Statistic.md) |  | [optional] 
**is_admin** | **bool** | 是否是企业管理员角色 | [optional] 
**admin_rules** | **str** |  | [optional] 
**access** | **object** | 能否访问企业 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


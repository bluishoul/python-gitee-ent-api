# BulkResponseWithKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | **list** | 消息数组模版[&#x27;key&#x27;, &#x27;message&#x27;] | [optional] 
**errors** | **list** | 错误消息数组 | [optional] 
**successes** | **list** | 成功消息数组 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


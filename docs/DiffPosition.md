# DiffPosition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**diff_position_id** | **int** | 评论引用的代码块id | [optional] 
**statistic** | **str** | diff 统计 | [optional] 
**head** | **str** | diff head | [optional] 
**content** | **str** | diff 内容 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


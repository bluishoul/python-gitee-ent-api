# EnterpriseAuth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**read_member** | **bool** | 是否可查看成员列表 | [optional] 
**add_member** | **bool** | 是否可添加企业成员 | [optional] 
**read_group** | **bool** | 是否可查看授权用户参与的团队列表 | [optional] 
**add_group** | **bool** | 是否可新增团队 | [optional] 
**read_issue** | **bool** | 是否可查看授权用户参与的任务列表 | [optional] 
**create_issue** | **bool** | 是否可创建任务 | [optional] 
**read_program** | **bool** | 是否可查看授权用户参与的项目列表 | [optional] 
**create_program** | **bool** | 是否可创建项目 | [optional] 
**read_project** | **bool** | 是否可查看授权用户参与的仓库列表 | [optional] 
**read_week_report** | **bool** | 是否可读取周报 | [optional] 
**read_issue_type** | **bool** | 是否可读取任务类型 | [optional] 
**setting_issue_type** | **bool** | 是否可管理任务类型 | [optional] 
**read_label** | **bool** | 是否可读取标签 | [optional] 
**setting_label** | **bool** | 是否可管理标签 | [optional] 
**admin_enterprise** | **bool** | 是否可管理企业 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


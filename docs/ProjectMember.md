# ProjectMember

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 用户 id | [optional] 
**name** | **str** | 用户名称 | [optional] 
**nickname** | **str** | 用户昵称 | [optional] 
**username** | **str** | 用户个性域名 | [optional] 
**target** | **str** | 跳转类型 | [optional] 
**avatar_url** | **str** | 用户头像链接 | [optional] 
**email** | **str** | 用户邮箱 | [optional] 
**is_enterprise_member** | **bool** | 是否是企业成员 | [optional] 
**access_level** | **str** | 用户企业角色名称 | [optional] 
**access_level_ident** | **str** | 用户企业角色ident | [optional] 
**is_blocked** | **bool** | 是否被锁定 | [optional] 
**is_myself** | **bool** | 是否是自己 | [optional] 
**project_access** | **str** | 用户在仓库中的级别 | [optional] 
**project_access_name** | **str** | 用户在仓库中的级别名称 | [optional] 
**project_creator** | **bool** | 是否是仓库创建者 | [optional] 
**project_owner** | **bool** | 是否是仓库拥有者 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# EnterpriseIdMilestonesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**title** | **str** | 里程碑标题 | 
**due_date** | **str** | 里程碑结束日期如：2020-08-13 | 
**start_date** | **str** | 里程碑开始日期如：2020-08-13 | [optional] 
**description** | **str** | 里程碑描述 | [optional] 
**program_id** | **int** | 关联项目ID | [optional] 
**project_ids** | **str** | 关联仓库ID, 以,分隔的字符串 | [optional] 
**assignee_id** | **int** | 里程碑负责人ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


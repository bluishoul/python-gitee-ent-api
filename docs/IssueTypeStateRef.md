# IssueTypeStateRef

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 任务状态 ID | [optional] 
**title** | **str** | 任务状态的名称 | [optional] 
**state** | **str** | 任务状态的属性 | [optional] 
**color** | **str** | 任务状态的颜色 | [optional] 
**icon** | **str** | 任务状态的 Icon | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# IssueMemberSelect

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 用户 id | [optional] 
**username** | **str** | 用户个性地址 | [optional] 
**name** | **str** | 用户名称 | [optional] 
**remark** | **str** | 用户在企业的备注名 | [optional] 
**pinyin** | **str** | 成员备注或名称拼音 | [optional] 
**avatar_url** | **str** | 用户头像 | [optional] 
**is_enterprise_member** | **bool** | 是否企业成员 | [optional] 
**outsourced** | **bool** | 是否外包成员 | [optional] 
**is_program_member** | **bool** | 是否项目成员 | [optional] 
**is_project_member** | **bool** | 是否仓库成员 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


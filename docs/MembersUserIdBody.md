# MembersUserIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**remark** | **str** | 企业备注 | [optional] 
**occupation** | **str** | 职位信息 | [optional] 
**phone** | **str** | 手机号码 | [optional] 
**role_id** | **str** | 成员角色 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


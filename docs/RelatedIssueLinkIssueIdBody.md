# RelatedIssueLinkIssueIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**ref_type** | **str** | 关联关系(normal, finish_to_finish, finish_to_start, start_to_start, start_to_finish) | [optional] 
**direction** | **str** | 关联关系的方向(none, pre, latter) | [optional] 
**result_type** | **str** | 返回结果类型：包括issue, dependence | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


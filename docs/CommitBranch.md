# CommitBranch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**branches** | **list** | 分支 | [optional] 
**tags** | **list** | tag | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# ProgramList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 项目 id | [optional] 
**ident** | **str** | 项目编号 | [optional] 
**name** | **str** | 项目名称 | [optional] 
**description** | **str** | 项目描述 | [optional] 
**status** | **int** | 项目状态（0:开始 1:暂停 2:关闭） | [optional] 
**outsourced** | **bool** | 是否外包项目 | [optional] 
**type** | **str** | 项目类型（内部、外包） | [optional] 
**color** | **str** | 颜色 | [optional] 
**category** | **str** | 项目类型 | [optional] 
**users_count** | **int** | 成员数 | [optional] 
**projects_count** | **int** | 仓库数 | [optional] 
**issues_count** | **int** | 任务总数 | [optional] 
**closed_issues_count** | **int** | 已关闭的任务总数 | [optional] 
**star_status** | **bool** | 是否被当前用户收藏 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


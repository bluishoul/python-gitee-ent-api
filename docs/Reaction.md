# Reaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | [optional] 
**target_type** | **str** | 表态的类型。(PullRequest、Note、Issue) | [optional] 
**target_id** | **int** | 对应表态类型的 id | [optional] 
**user_id** | **int** | 用户的 id | [optional] 
**user** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**text** | **str** | 表态内容 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


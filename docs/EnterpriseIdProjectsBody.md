# EnterpriseIdProjectsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**project_name** | **str** | 仓库名称 | 
**project_namespace_path** | **str** | 归属路径 | 
**project_path** | **str** | 仓库路径 | 
**project_template_id** | **int** | 模板仓库 id | [optional] 
**project_description** | **str** | 仓库介绍 | [optional] 
**project_public** | **int** | 是否开源 | [optional] 
**project_outsourced** | **int** | 类型，0：内部，1：外包 | [optional] 
**project_program_ids** | **str** | 关联项目列表，英文逗号分隔 | [optional] 
**project_member_ids** | **str** | 关联成员列表，英文逗号分隔 | [optional] 
**project_group_ids** | **str** | 授权团队列表，英文逗号分隔 | [optional] 
**project_import_url** | **str** | 导入已有仓库路径 | [optional] 
**normal_refs** | **bool** | 是否包含所有分支 | [optional] 
**import_program_users** | **int** | 是否导入项目成员: 0:否，1:是 | [optional] 
**readme** | **int** | 是否初始化readme: 0:否，1:是 | [optional] 
**issue_template** | **int** | 是否初始化issue模版: 0:否，1:是 | [optional] 
**pull_request_template** | **int** | 是否初始化PR模版: 0:否，1:是 | [optional] 
**user_sync_code** | **str** | 仓库导入-账号 | [optional] 
**password_sync_code** | **str** | 仓库导入-密码 | [optional] 
**language** | **int** | 语言id | [optional] 
**ignore** | **str** | .gitignore | [optional] [default to 'no']
**license** | **str** | 开源许可证 | [optional] [default to 'no']
**model** | **str** | 分支模型id | [optional] 
**custom_branches_prod** | **str** | Production environment branch | [optional] 
**custom_branches_dev** | **str** | Development branch | [optional] 
**custom_branches_feat** | **str** | Function branch | [optional] 
**custom_branches_rel** | **str** | Release branch | [optional] 
**custom_branches_bugfix** | **str** | Patch branch | [optional] 
**custom_branches_tag** | **str** | Version tag branch | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


# TreeNote

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 评论的 id | [optional] 
**type** | **str** | 评论类型 | [optional] 
**author** | [**UserWithRemark**](UserWithRemark.md) |  | [optional] 
**content** | **str** | 评论内容(markdown 格式) | [optional] 
**content_html** | **str** | 评论内容(html 格式) | [optional] 
**ancestry** | **str** | 祖先层级 | [optional] 
**head** | **str** | 是否最外层评论 | [optional] 
**parent** | **str** | 是否父级评论 | [optional] 
**reactions** | [**Reaction**](Reaction.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

